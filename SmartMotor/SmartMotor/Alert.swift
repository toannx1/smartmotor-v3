//
//  Alert.swift
//  SmartMotor
//
//  Created by vietnv2 on 12/16/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

public class Alert: NSObject {
    class func alertAutoDismiss(delegate: UIViewController,message:String) {
        Loading().hideLoading()
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
        //
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        
        let messageText = NSMutableAttributedString(
            string: message,
            attributes: [
                NSAttributedStringKey.paragraphStyle: paragraphStyle,
                NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16.0)
            ]
        )
        alert.setValue(messageText, forKey: "attributedMessage")
        //
        delegate.present(alert, animated: true, completion: nil)
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when){
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
}


