//
//  DefineVariable.swift
//  Smart Motor
//
//  Created by vietnv2 on 11/5/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import Foundation
import UIKit

let cornerRadius5:CGFloat! = 5
let borderWidth1:CGFloat! = 1
let cornerRadius10:CGFloat! = 10
let borderWidth05:CGFloat! = 0.5
let titleViewImage:UIImage! = UIImage(named: "Header bg@3x.png")
let titleViewHeight:CGFloat! = 80
let getMethod:String! = "GET"
let postMethod:String! = "POST"
let contentTypeString:String = "Content-Type"
let applicationJsonString:String = "application/json"
let googleMapsURL:String = "https://maps.googleapis.com/maps/api/geocode/json?latlng="

//GOOGLE MAPS
//bundle ID: com.viettel.motosmartcare2
//let googleMapsAPI:String = "AIzaSyD-1x3fKYWPnouGgmVOWliidWsLsjgYlDA"
//let googlePlaceAPI:String = "AIzaSyAN0sNBz9Et6iQXEstR6-PdjbtKcEEEiAI"

//bundle ID: com.viettel.motosmartcare3
//let googleMapsAPI:String = "AIzaSyCuCdpPAJwu95pI_0Vt_M6scxNjyQt-4gU"
//let googlePlaceAPI:String = "AIzaSyAN0sNBz9Et6iQXEstR6-PdjbtKcEEEiAI"

//bundle ID: com.viettel.mtracking.v3
//let googleMapsAPI:String = "AIzaSyBbu_xIjPRvrct4wQV5D88V4Br29XQ3ubk"
//let googlePlaceAPI:String = "AIzaSyAN0sNBz9Et6iQXEstR6-PdjbtKcEEEiAI"

//bundle ID: com.viettel.smartmotor3
let googleMapsAPI:String = "AIzaSyBxgPVQbb-h2Y7f0k-L9DO68j5ilsGHHOc"
let googlePlaceAPI:String = "AIzaSyAN0sNBz9Et6iQXEstR6-PdjbtKcEEEiAI"


//VIETTEL MAPS
//bundle ID: com.viettel.motosmartcare2 && com.viettel.motosmartcare3
//let APP_VTMAP_KEY = "45b22516ef28a92764698a111f0245b9"

//bundle ID: com.viettel.motosmartcare2 app cu
//let APP_VTMAP_KEY = "c68b1593773100ac23933ef4ae79a2f1"

//bundle ID: com.viettel.mtracking.v3
let APP_VTMAP_KEY = "c72b7491e2526a69b21abdf8eb421798"

//
var smartmotorUrl:String!
var loginUrl:String!

let smartmotorUrl1:String = "https://smartmotor.viettel.vn"
let loginUrl1:String = "/mtapi/rest/auth/newlogin/"

let smartmotorUrl2:String = "http://203.190.173.70:9115"
let loginUrl2:String = "/mtapi/rest/auth/newlogin/"

let smartmotorUrl3:String = "http://10.60.108.90:9115"
let loginUrl3:String = "/mtapi/rest/auth/newlogin/"

//
let logoutUrl:String = "/mtapi/rest/auth/logout/"
let captchaUrl:String = "/mtapi/jcaptcha.jpg"
let listTransportUrl:String = "/mtapi/rest/mobile/monitortransport/listTransport/"
let listTransportRegNoUrl:String = "/0/2?registerNo="
let listTransportPageIndexUrl:String = "&state=0123456&groupCode=&pageSize=10&pageIndex="
let reviewtransportUrl:String = "/mtapi/rest/mobile/reviewtransport/"
let chargingUrl:String = "/mtapi/rest/mobile/charging/"
let payMoneyUrl:String = "/mtapi/rest/mobile/charging/processCharging/"
let getInfoUrl:String = "/mtapi/rest/mobile/charging/getInfo/"
let getAccountInfoUrl:String = "/mtapi/rest/mobile/accountinfo/getAccountInfo/"
let ipAdressURL:String = "http://ip.jsontest.com"  // https://www.ipify.org
let checkBoxImage:UIImage = UIImage(named: "Checked Checkbox-35.png")!
let uncheckBoxImage:UIImage = UIImage(named: "Unchecked Checkbox-35.png")!
var logoutFlag:Bool! = false
var keyboardHeight:CGFloat! = 215

var deviceIPAddress:String! = ""
var tokenSave:String!
var userIdSave:String!
var groupRootSave:Int!
var loginFlag:Bool! = true
var heightScreen:CGFloat!
var widthScreen:CGFloat!

enum vehicleInfoString:String {
    case accIllegalState = "accIllegalState"
    case devicePin = "devicePin"
    case deviceType = "deviceType"
    case deviceTypeIdOrginal = "deviceTypeIdOrginal"
    case factory = "factory"
    case gpsDate = "gpsDate"
    case gpsSpeed = "gpsSpeed"
    case gpsState = "gpsState"
    case groupsCode = "groupsCode"
    case id = "id"
    case illegalMoveState = "illegalMoveState"
    case lat = "lat"
    case lng = "lng"
    case lowBatteryState = "lowBatteryState"
    case motoSpeed = "motoSpeed"
    case offPowerState = "offPowerState"
    case regNo = "regNo"
    case sim = "sim"
    case sosState = "sosState"
    case state = "state"
    case timeState = "timeState"
    case type = "type"
    case vibrationState = "vibrationState"
    case isForbidden = "isForbidden"
}


