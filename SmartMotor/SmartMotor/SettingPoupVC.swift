//
//  SettingPoupVC.swift
//  SmartMotor
//
//  Created by Toan on 2/4/21.
//  Copyright © 2021 vietnv2. All rights reserved.
//

import UIKit
protocol PassDataDelegate {
    func  passData(_ olddPass : String , _ newPass : String)
}
class SettingPoupVC: UIViewController,UITextFieldDelegate{

    @IBOutlet weak var bottomConstant: NSLayoutConstraint!
    @IBOutlet weak var reNewPasstf: UITextField!
    @IBOutlet weak var newPassTf: UITextField!
    @IBOutlet weak var oldPassTf: UITextField!
    var delegate : PassDataDelegate?
    var didSendData: ((_ oldPass :String , _ newPass : String) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: NSNotification.Name.UIKeyboardWillShow,
            object: nil
        )
        // Do any additional setup after loading the view.
    }


    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onConfirm(_ sender: Any) {
        guard let newPass = newPassTf.text , let oldPass = oldPassTf.text,let reNewPass = reNewPasstf.text else {
            return
        }
        if(reNewPass == newPass){
            if (newPass.count > 0 || oldPass.count > 0 ){
                delegate?.passData(oldPass, newPass)
                self.dismiss(animated: true, completion: nil)
            }
        }else{
            self.dismiss(animated: true, completion: nil)
            Alert.alertAutoDismiss(delegate: self, message: "Mật khẩu không chính xác !")
        }
        
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame: NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        
        if (self.bottomConstant.constant  <=  keyboardHeight){
            self.bottomConstant.constant  = keyboardHeight + 20
            
        }
    }
    func hide(){
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
