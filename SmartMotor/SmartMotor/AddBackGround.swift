//
//  AddBackGround.swift
//  SmartMotor
//
//  Created by vietnv2 on 11/25/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

extension UIView {
    func addBackground(width: CGFloat, height: CGFloat, image: UIImage) {
        let imageViewBackground = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        imageViewBackground.image = image
        imageViewBackground.contentMode = UIViewContentMode.scaleAspectFill
        self.addSubview(imageViewBackground)
        self.sendSubview(toBack: imageViewBackground)
    }
}

