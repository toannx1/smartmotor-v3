
//
//  DetailCell.swift
//  SmartMotor
//
//  Created by Toan on 2/1/21.
//  Copyright © 2021 vietnv2. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {
    var  station : Station!
    var arrStation : [Station]!
    var numberOfRow = 0
    let arrColor = [0x771CF9,0x358700,0x007C9C,0xCC6600,0x777777,0x777777]
      var displayVehicleStatusString:[String]! = ["Không có thông tin",
                                           "Chạy",
                                           "Dừng",
                                           "Đỗ",
                                           "Mất GPS",
                                           "Ngủ đông",
                                           "Mất GPRS",
                                           "Không xác định"]
    let motorStateImage:[String] = ["ic_motor_no_infor",
                                           "ic_motor_run",
                                           "ic_motor_stop",
                                           "ic_motor_park",
                                           "ic_motor_lost_gps",
                                           "ic_motor_hibernate",
                                           "ic_motor_lost_gprs",
                                           "ic_motor_no_infor"]
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var timeStart: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var timeStatus: UILabel!
    @IBOutlet weak var bgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    func setupDate(station : Station){
//         = self.formatDateTimeToSting(time: station.gpsDate)
         
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        guard let dateStr = formatter.date(from: station.gpsDate) else {
            return
        }
        formatter.dateFormat = "MM/dd/yyyy"
        lblDate.text = formatter.string(from: dateStr)
        formatter.dateFormat = "HH:mm:ss"
        timeStart.text = formatter.string(from: dateStr)
        if (arrColor.count > station.state){
            self.bgView.backgroundColor =  UIColor.init(rgb: self.arrColor[station.state])
            self.lblStatus.text = self.displayVehicleStatusString[station.state]
        }
//        timeStart.text = self.formatDateTToSting(time: station.gpsDate)
        
        self.timeStatus.text = "\(String(self.getTimeStamp(timeStamp: station.timeState))) giây"
        print(station.state)
        if (station.state != 1 ){
            self.timeStatus.text =   secondsToHoursMinutesSeconds(seconds: station.timeState)
        }else{
            self.timeStatus.text =   "\(station.gpsSpeed) km/h"
        }
        
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> String {
        let hour = seconds / 3600
        let munites = (seconds % 3600)/60
        let seconds = (seconds % 3600)%60
        if (hour > 0 ){
            return "\(hour) giờ \(munites) phút \(seconds) giây"
        }else if(munites > 0 && hour == 0){
            return "\(munites) phút \(seconds) giây"
        }else{
            return "\(seconds) giây"
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func  getTimeStamp(timeStamp : Int) -> Int{
      let second = timeStamp/1000
      return second
    }
    
}
extension Double {
    var clean: String {
        return self / 1 == 0 ? String(format: "%.0f", self) : String(self)
    }
}

