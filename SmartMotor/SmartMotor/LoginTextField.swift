//
//  LoginTextField.swift
//  SmartMotor
//
//  Created by vietnv2 on 12/5/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

extension LoginVC: UITextFieldDelegate {
    @IBAction func btnClearTxtUserTapped(_ sender: Any) {
        txtUser.text = ""
    }
    
    @IBAction func btnSecurePasswordTapped(_ sender: Any) {
        txtPassword.isSecureTextEntry = !txtPassword.isSecureTextEntry
    }
    
    func initTextField() {
        let paddingForUser = UIImageView(frame: CGRect(x: 0, y: 0, width: 35, height: 30))
        paddingForUser.image = UIImage(named: "")
        txtUser.leftView = paddingForUser
        txtUser.leftViewMode = UITextFieldViewMode.always
        txtUser.returnKeyType = UIReturnKeyType.next
        txtUser.delegate = self
        _ = textFieldShouldReturn(txtUser)
        //
        let paddingForPassword = UIImageView(frame: CGRect(x: 0, y: 0, width: 35, height: 30))
        paddingForPassword.image = UIImage(named: "")
        txtPassword.leftView = paddingForPassword
        txtPassword.leftViewMode = UITextFieldViewMode.always
        txtPassword.returnKeyType = UIReturnKeyType.done
        txtPassword.delegate = self
        _ = textFieldShouldReturn(txtPassword)
        //
        txtCaptcha.leftViewMode = UITextFieldViewMode.always
        txtCaptcha.returnKeyType = UIReturnKeyType.done
        txtCaptcha.delegate = self
        _ = textFieldShouldReturn(txtCaptcha)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool { // called when 'return' key pressed. return false to ignore.
        if textFieldDidBeginEditingFlag == false {
            return true
        }
        if textField == self.txtUser {
            self.txtPassword.becomeFirstResponder()
        }else if textField == self.txtPassword {
            if loginWithCaptchaFlag == false {
                textField.resignFirstResponder()
                loginViewContraint.constant = 0
                login()
            }else {
                self.txtCaptcha.becomeFirstResponder()
            }
        }else {
            if loginWithCaptchaFlag == true {
                textField.resignFirstResponder()
                loginViewContraint.constant = 0
                login()
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textFieldDidBeginEditingFlag = true
        if textField == self.txtUser {
            btnClearTxtUser.isHidden = false
        }else if textField == self.txtPassword {
            //btnSecurePassword.isHidden = false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textFieldDidBeginEditingFlag = false
        if textField == self.txtUser {
            btnClearTxtUser.isHidden = true
        }else if textField == self.txtPassword {
            //btnSecurePassword.isHidden = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtUser || textField == txtPassword {
            let text = textField.text
            let newLength = (text?.characters.count)! + string.characters.count - range.length
            return newLength <= 50
        }
        return true
    }
    
}


