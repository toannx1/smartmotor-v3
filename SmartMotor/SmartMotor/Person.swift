//
//  Person.swift
//  Smart Motor
//
//  Created by vietnv2 on 11/8/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import Foundation
import CoreData

class Person: NSManagedObject {
    // Insert code here to add functionality to your managed object subclass
    @NSManaged var token: String?
    @NSManaged var userId:String?
    @NSManaged var groupRoot:String?
}


