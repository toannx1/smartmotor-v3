//
//  ChargeMoney.swift
//  SmartMotor
//
//  Created by vietnv2 on 11/25/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

struct Number {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = "." // or possibly "." / ","
        formatter.numberStyle = .decimal
        return formatter
    }()
}
extension BinaryInteger {
    var stringWithSepator: String {
        return Number.withSeparator.string(from: NSNumber(value: hashValue)) ?? ""
    }
}

extension ListVehicleVC:URLSessionDownloadDelegate {
    enum info2PayMoneyString:String {
        case data = "data"
        case deviceCode = "deviceCode"
        case deviceSim = "deviceSim"
        case monthlyFee = "monthlyFee"
        case registerNo = "registerNo"
        case requestedPhone = "requestedPhone"
    }
    
    enum resultViewChargeCodeString:String {
        case chargeMoneySucess = "Đã nạp thành công "
        case forPhoneNumber = " VNĐ cho số điện thoại "
        case codeChargeNotAvailable = "Mã thẻ không hợp lệ hoặc đã được sử dụng!"
        case userOrVehiclePhoneNumberNotAvailable = "Số điện thoại thiết bị hoặc số điện thoại người dùng không hợp lệ!"
        case errorWhileConnectSystemPlsReLogin = "Có lỗi xảy ra khi kết nối tới hệ thống. Vui lòng đăng nhập lại!"
        case captchaWrongAndErrorSystem = "Mã xác nhận không đúng! Lỗi hệ thống!"
        case captchaWrong = "Mã xác nhận không đúng!"
        case accountNotAvailable = "Tài khoản không có quyền giám sát xe hiện tại!"
        case chargeMoneyFail = "Nạp tiền không thành công! Lỗi hệ thống!"
    }
    
    func resultViewChargeCode(_ code: Int) {
        switch code {
        case 1:
            Alert.alertAutoDismiss(delegate: self, message: resultViewChargeCodeString.chargeMoneySucess.rawValue + moneyPay + resultViewChargeCodeString.forPhoneNumber.rawValue + deviceSim)
        case -8:
            Alert.alertAutoDismiss(delegate: self, message: resultViewChargeCodeString.codeChargeNotAvailable.rawValue)
        case -9:
            Alert.alertAutoDismiss(delegate: self, message: resultViewChargeCodeString.userOrVehiclePhoneNumberNotAvailable.rawValue)
        case -1:
            Alert.alertAutoDismiss(delegate: self, message: resultViewChargeCodeString.errorWhileConnectSystemPlsReLogin.rawValue)
        case -2:
            Alert.alertAutoDismiss(delegate: self, message: resultViewChargeCodeString.captchaWrongAndErrorSystem.rawValue)
        case -3:
            Alert.alertAutoDismiss(delegate: self, message: resultViewChargeCodeString.captchaWrong.rawValue)
        case -6:
            Alert.alertAutoDismiss(delegate: self, message: resultViewChargeCodeString.accountNotAvailable.rawValue)
        default:
            Alert.alertAutoDismiss(delegate: self, message: resultViewChargeCodeString.chargeMoneyFail.rawValue)
            break
        }
    }
    
    @IBAction func btnConfirmChargeCardCodeTapped(_ sender: Any) {
        closeContainerChargeAndPayMoneyView()
        chargeMoney()
    }
    
    @IBAction func btnCancelChargeCardCodeTapped(_ sender: Any) {
        closeContainerChargeAndPayMoneyView()
    }
    
    @IBAction func btnConfirmPayMoneyTapped(_ sender: Any) {
        closeContainerChargeAndPayMoneyView()
        payMoney()
    }
    
    @IBAction func btnCancelPayMoneyTapped(_ sender: Any) {
        confirmPayMoneyView.isHidden = true
        backgroundChargeMoneyView.isHidden = true
        backgroundView.isHidden = true
    }
    
    //
    @IBAction func btnconfirmChargeCardCodeTapped(_ sender: Any) {
        hideKeyBoad()
        if txtChargeCode.text == "" {
            Alert.alertAutoDismiss(delegate: self, message: "Xin vui lòng nhập mã thẻ cào!")
            return
        }else if txtCaptchaCardCode.text == "" {
            Alert.alertAutoDismiss(delegate: self, message: "Xin vui lòng nhập mã captcha!")
            return
        }
        lblConfirmChargeCardCode.text = "Xác nhận nạp tiền cho số điện thoại " + vehicle2show.sim + " ?"
        confirmChargeCardCodeView.isHidden = false
        backgroundChargeMoneyView.isHidden = false
        chargeCardCodeView.isHidden = true
        backgroundView.isHidden = false
    }
    
    @IBAction func btncancelChargeCardCodeTapped(_ sender: Any) {
        closeContainerChargeAndPayMoneyView()
    }
    
    @IBAction func btnconfirmPayMoneyTapped(_ sender: Any) {
        hideKeyBoad()
        if txtCaptchaPayMoney.text == "" {
            Alert.alertAutoDismiss(delegate: self, message: "Xin vui lòng nhập mã captcha!")
            return
        }
        lblConfirmPayMoney.text = "Xác nhận nạp tiền cho số điện thoại " + vehicle2show.sim + " ?"
        confirmPayMoneyView.isHidden = false
        backgroundChargeMoneyView.isHidden = false
        payMoneyView.isHidden = true
        backgroundView.isHidden = false
    }
    
    @IBAction func btncancelPayMoneyTapped(_ sender: Any) {
        closeContainerChargeAndPayMoneyView()
    }
    //
    @IBAction func chargeMoneyTapped(_ sender: Any) {
        backgroundChargeMoneyView.isHidden = false
        backgroundView.isHidden = false
        listChargeMoneyView.isHidden = false
        //getIPAddress()
    }
    
    func urlEncode(string: String) -> String {
        var rString:String! = ""
        for char in string.characters {
            if char == " " {
                rString.append("+")
            }else if (char == "." || char == "-" || char == "_" || char == "~" ||
                (char >= "a" && char <= "z") ||
                (char >= "A" && char <= "Z") ||
                (char >= "0" && char <= "9")) {
                rString.append(String(char))
            } else {
                //rString.append(String(format: "%%2X", char.hashValue))
                rString.append(String(format: "0", char.hashValue))
            }
        }
        return rString
    }
    
    func chargeMoney() {
        Loading().showLoading()
        if tokenSave == nil || userIdSave == nil {
            gotoLoginView()
        }
        let cardCode = urlEncode(string: txtChargeCode.text!)
        let captcha = urlEncode(string: txtCaptchaCardCode.text!)
        var urlString = smartmotorUrl + chargingUrl + tokenSave + "/" + String(userIdSave) + "/" + String(vehicle2show.id) + "/" + vehicle2show.sim + "/" + cardCode + "/" + captcha + "/ios"
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        let test = String(urlString)
        let url = URL(string: test)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        request.addValue(cookieName + "=" + cookieValue, forHTTPHeaderField: "Cookie")
        //
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        //
        let task = defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
            Loading().hideLoading()
            if error != nil {
                Alert.alertAutoDismiss(delegate: self, message: self.connectionFail)
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    Alert.alertAutoDismiss(delegate: self, message: self.connectionFail)
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let resultCode = jsonResult["resultCode"] as! Int
                    self.resultViewChargeCode(resultCode)
                }catch {
                    Alert.alertAutoDismiss(delegate: self, message: "Lỗi kết nối. Vui lòng thử lại sau!")
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    func getIPAddress() {
        Loading().showLoading()
        if deviceIPAddress != "" {
            Loading().hideLoading()
            return
        }
        let url = URL(string: ipAdressURL)  
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 5
        defaultConfigObject.timeoutIntervalForResource = 5
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        let task = defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
            Loading().hideLoading()
            if error != nil {
                Alert.alertAutoDismiss(delegate: self, message: self.connectionFail)
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    Alert.alertAutoDismiss(delegate: self, message: self.connectionFail)
                    Alert.alertAutoDismiss(delegate: self, message: self.connectionFail)
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    deviceIPAddress = jsonResult["ip"] as! String
                }catch {
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    func getInfo2PayMoney() {
        //urlSessionDelegate()
        Loading().showLoading()
        if tokenSave == nil || userIdSave == nil {
            gotoLoginView()
        }
        let urlString = "http://smartmotor.viettel.vn" + getInfoUrl + tokenSave
        let urlRequest =  urlString + "/" + userIdSave + "/" + String(vehicle2show.id) + "/" + deviceIPAddress
        let url = URL(string: urlRequest)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        let task = defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
            Loading().hideLoading()
            if error != nil {
                Alert.alertAutoDismiss(delegate: self, message: self.connectionFail)
                self.closeContainerChargeAndPayMoneyView()
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    Alert.alertAutoDismiss(delegate: self, message: self.connectionFail)
                    self.closeContainerChargeAndPayMoneyView()
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let resultCode = jsonResult["resultCode"] as! Int
                    if resultCode != 1 {
                        self.backgroundChargeMoneyView.isHidden = true
                        self.backgroundView.isHidden = true
                        Alert.alertAutoDismiss(delegate: self, message: self.connectionFail)
                        return
                    }
                    if let jsonData = jsonResult[info2PayMoneyString.data.rawValue] as? NSDictionary {
                        self.deviceCode = jsonData [info2PayMoneyString.deviceCode.rawValue] as! String
                        self.deviceSim = jsonData [info2PayMoneyString.deviceSim.rawValue] as! String
                        self.monthlyFee = jsonData [info2PayMoneyString.monthlyFee.rawValue] as! Int!
                        self.registerNo = jsonData [info2PayMoneyString.registerNo.rawValue] as! String
                        self.requestedPhone = jsonData [info2PayMoneyString.requestedPhone.rawValue] as! String
                        self.lblPayMoneyInfo.text = self.payMoneyInfo[0] + self.requestedPhone + self.payMoneyInfo[1] + self.deviceSim + self.payMoneyInfo[2] + self.deviceCode + self.payMoneyInfo[3] + self.registerNo + self.payMoneyInfo[4]
                        //
                        self.monthPay = "1"
                        self.moneyPay = String(self.monthlyFee)
                        self.btnChooseNumberOfMonth2Pay.titleLabel?.text = "1 Tháng"
                        //self.txtMoneyPay.text = self.moneyPay + " VNĐ"
                        self.txtMoneyPay.text = self.monthlyFee.stringWithSepator + " VNĐ"
                        //
                        self.payMoneyView.isHidden = false
                        self.listChargeMoneyView.isHidden = true
                        self.urlSessionDelegate()
                    }
                }catch {
                    Alert.alertAutoDismiss(delegate: self, message: "Lỗi kết nối. Vui lòng thử lại sau!")
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    func payMoney() {
        Loading().showLoading()
        if tokenSave == nil || userIdSave == nil {
            gotoLoginView()
        }
        let captcha = urlEncode(string: txtCaptchaPayMoney.text!)
        var urlString = "http://smartmotor.viettel.vn" + payMoneyUrl + tokenSave + "/" + userIdSave
        var urlRequest = urlString + "/" + String(vehicle2show.id) + "/"  + moneyPay + "/" + monthPay + "/" + captcha + "/" + deviceIPAddress
        urlRequest = urlRequest.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        let url = URL(string: urlRequest)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        request.addValue(cookieName + "=" + cookieValue, forHTTPHeaderField: "Cookie")
        //
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        //
        let task = defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
            Loading().hideLoading()
            if error != nil {
                Alert.alertAutoDismiss(delegate: self, message: self.connectionFail)
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    Alert.alertAutoDismiss(delegate: self, message: self.connectionFail)
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    var resultCode = jsonResult["resultCode"] as! Int
                    self.payMoneyResultDisplay(result: resultCode)
                }catch {
                    Alert.alertAutoDismiss(delegate: self, message: "Lỗi kết nối. Vui lòng thử lại sau!")
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    func payMoneyResultDisplay(result: Int) {
        var display: String!
        switch result {
        case 1: display = "Thanh toán thành công. Quý khách vui lòng đợi trong 5 phút để hệ thống cập nhật lại dữ liệu!"
                initTimerGoToLogin()
        case -1: display = "Thanh toán không thành công! (Mã -1)."
        case -2: display = "Thanh toán không thành công! (Mã -2)."
        case -3: display = "Thuê bao chủ không đủ điều kiện thanh toán! (Mã -3)."
        case -4: display = "Thanh toán không thành công! (Mã -4)."
        case -5: display = "Thanh toán không thành công! (Mã -5)."
        case -6: display = "Thanh toán không thành công! (Mã -6)."
        case -7: display = "Thanh toán không thành công! (Mã -7)."
        case -8: display = "Mã xác nhận không đúng, Quý khách vui lòng nhập lại mã Captcha! (Mã -8)."
        case -9: display = "Thanh toán không thành công! (Mã -9)."
        case -10: display = "Thanh toán không thành công! (Mã -10)."
        case -11: display = "Thanh toán không thành công! (Mã -11)."
        case -12: display = "Thanh toán không thành công! (Mã -12)."
        case -13: display = "Sim trên thiết bị của Quý khách đã bị Chặn/Hủy, không thể thanh toán cho sim này! (Mã -13)"
        case -14: display = "Thanh toán không thành công! (Mã -14)."
        default: break
        }
        Alert.alertAutoDismiss(delegate: self, message: display)
    }
    
    func initTimerGoToLogin() {
        if timerGoToLogin != nil {
            timerGoToLogin.invalidate()
            timerGoToLogin = nil
            return
        }
        timerGoToLogin = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(stopTimerGoToLogin), userInfo: nil, repeats: true)
    }
    
    @objc func stopTimerGoToLogin() {
        if timerGoToLogin != nil {
            timerGoToLogin.invalidate()
            timerGoToLogin = nil
            logout()
        }
    }
    
    func initURLSession() {
        let sessionConfig = URLSessionConfiguration.default
        session = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
    }

    func deleteCookies() {
        let cookieStore = HTTPCookieStorage.shared
        for cookie in cookieStore.cookies ?? [] {
            cookieStore.deleteCookie(cookie)
        }
    }
    
    func urlSessionDelegate() {
        deleteCookies()
        let imageUrl: String = smartmotorUrl + captchaUrl  // https://smartmotor.viettel.vn/mtapi/jcaptcha.jpg
        let task: URLSessionDownloadTask = session.downloadTask(with: NSURL(string: imageUrl as String)! as URL)
        task.resume()
        Loading().showLoading()
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let downloadedImage = UIImage(data: NSData(contentsOf: location) as! Data)
        DispatchQueue.main.async {
            if self.chargeCardCodeView.isHidden == false {
                self.imgCaptchaChargeCode.image = downloadedImage
            }else if self.payMoneyView.isHidden == false {
                self.imgCaptchaPayMoney.image = downloadedImage
            }
        }
        for cookie in (session.configuration.httpCookieStorage?.cookies)! {
            if cookie.name == "JSESSIONID" {
                cookieName = cookie.name
                cookieValue = cookie.value
                DispatchQueue.main.async {
                    Loading().hideLoading()
                }
                return
            }
        }
        Loading().hideLoading()
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
    }
}


