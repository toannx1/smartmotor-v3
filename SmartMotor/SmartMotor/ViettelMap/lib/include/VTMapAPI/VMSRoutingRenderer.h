//
//  VMSRoutingRenderer.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/28/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMSRoutingService.h"
#import "VMSMarkerLayer.h"

@class VMSMapView;

@interface VMSRoutingRenderer : NSObject
/**
 * Delegate cho các sự kiện của routing service
 */
@property (nonatomic, weak) id<VMSRoutingServiceDelegate> routingDelegate;

/**
 * Delegate cho sự kiện tab vào marker
 */
@property (nonatomic, weak) id<VMSMapObjectSingleTabDelegate> markerTabDelegate;

/**
 * Delegate cho sự kiện bắt đầu và kết thúc kéo thả marker
 */
@property (nonatomic, weak) id<VMSMapObjectDragDelegate> markerDragDelegate;

/**
 * Điểm bắt đầu của đường đi
 * Chỉ có thể set giá trị thông qua hàm routing và việc kéo thả marker xuất phát trên map
 */
@property (nonatomic, readonly) VMSLatLng *startPoint;

/**
 * Điểm kết thúc của đường đi
 * Chỉ có thể set giá trị thông qua hàm routing và việc kéo thả marker xuất phát trên map
 */
@property (nonatomic, readonly) VMSLatLng *endPoint;

/**
 * Kết quả của việc tìm đường trả về từ server
 * Khi set giá trị mới vào, renderer không tự động thay đổi hiển thị mà phải chờ đến khi gọi lại hàm render
 */
@property (nonatomic, strong) VMSRoutingServiceResult *routingResult;

/**
 * Màu sắc được sử dụng để vẽ lên tuyến đường giữa 2 điểm
 * Khi set giá trị mới vào, renderer không tự động thay đổi hiển thị mà phải chờ đến khi gọi lại hàm render
 */
@property (nonatomic, strong) UIColor *lineColor;

/**
 * Độ rộng của tuyến đường giữa 2 điểm, đơn vị tính theo pixel
 * Khi set giá trị mới vào, renderer không tự động thay đổi hiển thị mà phải chờ đến khi gọi lại hàm render
 */
@property (nonatomic, assign) CGFloat lineWeight;

/**
 * Options cho việc routing
 * Routing options chỉ được áp dụng sau khi gọi hàm routing hoặc rerouting.
 */
@property (nonatomic, copy) VMSRoutingOptions *routingOptions;

/**
 * Cho phép thêm điểm trung gian mà tuyến đường phải đi qua bằng việc kéo thả marker
 * Từ 1 điểm bất kỳ trên tuyến đường kéo đi sẽ tạo ra điểm trung gian tại nơi nhấc ngón tay lên
 * Khi set giá trị mới vào, renderer không tự động thay đổi hiển thị mà phải chờ đến khi gọi lại hàm render
 */
@property (nonatomic, assign, getter = isEnabledWayPoint) BOOL enabledWaypoint;

/**
 * Hiển thị các điểm chuyển tiếp của tuyến đường trên map
 * Khi set giá trị mới vào, renderer không tự động thay đổi hiển thị mà phải chờ đến khi gọi lại hàm render
 */
@property (nonatomic, assign, getter = isEnabledWaypoint) BOOL enabledTurnPoint;

/**
 * Xác lập thuộc tính cho phép kéo thả 2 marker xuất phát và kết thúc của tuyến đường
 * Kéo thả 2 marker này, khi thả tay sẽ thực hiện routing lại theo toạ độ mới của 2 marker
 * Khi set giá trị mới vào, renderer không tự động thay đổi hiển thị mà phải chờ đến khi gọi lại hàm render
 */
@property (nonatomic, assign) BOOL markerDraggable;

/**
 * Hàm khởi tạo routingrenderer theo mapView
 * Tất cả các sự kiện, thao tác vẽ của renderer sẽ thực hiện trên mapView này.
 */
- (id)initWithMap:(VMSMapView *)mapView;


/**
 * Hàm render lại tuyến đường trên map.
 * Thường thì hệ thống sẽ tự động gọi render hộ người dùng.
 * Thường sử dụng trong các trường hợp như người dùng clear toàn bộ các mapObject trước đó làm mất phần hiển thị
 */
- (void)render;

/**
 * Hàm bắt đầu tìm kiếm đường đi theo 2 điểm cho trước
 * Nếu chưa set RoutingOptions trước khi gọi hàm này, sẽ sử dụng routingOptions mặc định
 * Khi kết thúc routing sẽ tự động vẽ lên màn hình, di chuyển và thay đổi mức zoom phù hợp để hiển thị
 */
- (BOOL)routingFrom:(VMSLatLng *)startPoint to:(VMSLatLng *)endPoint;

/**
 * Tương tự hàm routing nhưng sử dụng 2 điểm đầu cuối có sẵn trong lớp renderer đã được set trước đó.
 * Sử dụng trong trường hợp người sử dụng thay đổi RoutingOptions xong muốn tìm kiếm lại theo đúng các điểm trước đó
 */
- (BOOL)rerouting;

/**
 * Hiển thị hướng dẫn chỉ đường tại 1 index bất kỳ trong danh sách index trả về trong kết quả routing
 */
- (void)showDirectionAtIndex:(int)index;

/**
 * Hiển thị hướng dẫn chỉ đường tại điểm tiếp theo so với điểm hiện tại trong tuyến đường.
 * Quay về điểm xuất phát nếu đang ở điểm kết thúc của tuyến đường
 */
- (void)nextTurnPoint;

/**
 * Hiển thị hướng dẫn chỉ đường tại điểm ngay trước điểm hiện tại trong tuyến đường
 * Chuyển về điểm kết thúc của tuyến đường nếu điểm hiện tại đang là điểm xuất phát
 */
- (void)previousTurnPoint;

/**
 * Clear toàn bộ các đối tượng được đối tượng routingRenderer vẽ lên màn hình
 */
- (void)clear;

/**
 * Clear toàn bộ map view ?
 */
- (void)clearMap;

/**
 * Ẩn hướng dẫn chỉ đường (mà lớp đang quản lý)
 */
- (void)hideDirection;

@end
