//
//  VMSMarkerOptions.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/15/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VMSLatLng;

@interface VMSMarkerOptions : NSObject

/**
 * Index của đối tượng marker trong layer. Giá trị default là 0
 * Ảnh hương đến thứ tự vẽ của đối tượng
 * Đối tượng có index lớn hơn sẽ được vẽ lên trên các đối tượng có index nhỏ hơn.
 */
@property (nonatomic, assign) int zIndex;

/**
 * Điểm đặt marker.
 * Trong trường hợp không set anchor, điểm giữa dưới cùng của marker icon sẽ hiển thị chính xác tại điểm này.
 * Trong trường hợp set anchor thì đúng vị trí được set của icon sẽ hiển thị tại điểm này
 */
@property (nonatomic, copy) VMSLatLng *position;

/**
 * Title của infowindow cho marker.
 * Trong trường hợp title được set, khi click vào marker, không có xử lý gì từ phía người dùng
 * Hệ thống sẽ hiển thị infowindow với title tương ứng
 * Trường hợp không set title thì default sẽ không hiển thị infowindow
 */
@property (nonatomic, copy) NSString *title;

/**
 * Desciption của infowindow cho marker (snippet).
 * Để hiển thị nhiều dòng thì các dòng phải sử dụng ký tự xuống dòng "\n"
 * Trong trường hợp không có title thì dù set desciption cũng không hiển thị infowindow
 */
@property (nonatomic, copy) NSString *description;

/**
 * Icon đại diện dùng để hiển thị infoWindow
 * Trong trường hợp không set hoặc set nil thì sẽ sử dụng icon mặc định của hệ thống
 */
@property (nonatomic, copy) UIImage *icon;

/**
 * Anchor của marker
 * Vị trí tương ứng của icon sẽ được hiển thị đúng tại position của marker
 * Trường hợp không set sẽ sử dụng anchor mặc định là điểm chính giữa dưới cùng (Bottom - Center) của icon làm anchor
 */
@property (nonatomic, assign) CGPoint anchor;

/**
 * Anchor của infowindow mặc định khi click vào marker
 * Vị trí tương ứng của icon sẽ được sử dụng để hiển thị infowindow
 * Trường hợp không set sẽ lấy điểm chính giữa trên cùng (Top - Center) của icon làm anchor hiển thị
 */
@property (nonatomic, assign) CGPoint infoWindowAnchor;

/**
 * Góc xoay của marker. Đơn vị là độ
 * Giá trị dương : xoay theo chiều kim đồng hồ
 * Giá trị âm : xoay ngược chiều kim đồng hồ
 * Giá trị 0, ảnh không xoay.
 * Các anchor cũng sẽ xoay theo để đảm bảo hiển thị tại đúng vị trí tương ứng như khi không xoay
 */
@property (nonatomic, assign) int rotate;

/**
 * Đối tượng cho phép người dùng lưu trữ các thông tin bất kỳ mà người dùng muốn vào marker
 * Thông qua các sự kiện có trả về marker hay lấy marker từ layer
 * Người dùng có thể lấy ra các dữ liệu này để tiện cho các thao tác
 * Giá trị được marker retain.
 */
@property (nonatomic, strong) id userData;

/**
 * Tương tự như userData nhưng là weak reference, marker không retain đối tượng
 */
@property (nonatomic, weak) id userDataRef;

/**
 * Thuộc tính xác định ẩn / hiện của đối tượng marker
 */
@property (nonatomic, assign, getter=isVisible) BOOL visible;

/**
 * Thuộc tính cho phép kéo / thả marker
 */
@property (nonatomic, assign, getter=isDraggable) BOOL draggable;

/**
 * Hàm khởi tạo marker tại một vị trí xác định
 * Các giá trị khác như icon, anchor sử dụng giá trị mặc định
 */
- (id)initWithPosition:(VMSLatLng *)point;

@end