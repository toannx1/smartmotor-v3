//
//  VMSInfoWindowLayer.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/18/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSMapLayer.h"

@class VMSInfoWindow;
@class VMSInfoWindowOptions;

/**
 * Lớp quản lý các đối tượng info window của map view
 */
@interface VMSInfoWindowLayer : VMSMapLayer

/**
 * Delegate cho sự kiện tab vào hyperlink
 */
@property (nonatomic, weak) id<VMSHyperlinkDelegate> hyperlinkDelegate;

/**
 * Hàm khởi tạo layer
 */
- (id)init;

/**
 * Hàm add infoWindow thông qua infoWindowOptions
 */
- (VMSInfoWindow *)addInfoWindow:(VMSInfoWindowOptions *)opts;

@end
