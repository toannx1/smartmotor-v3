//
//  VMSScaleControl.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/14/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSBaseControl.h"

@interface VMSScaleControl : VMSBaseControl

/**
 * Note : Control chi bao gom cac ham mac dinh cua VMSBaseControl.
 */

@end
