//
//  VMSPolyline.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/15/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSMapObject.h"
@class VMSPolylineOptions;

@interface VMSPolyline : VMSMapObject

/**
 * Danh sách các điểm latlng mà polyline đi qua
 */
@property (nonatomic, copy) NSMutableArray *path;

/**
 * Màu sắc của polyline
 */
@property (nonatomic, copy) UIColor *strokeColor;

/**
 * Độ rộng của polyline tính theo pixel
 */
@property (nonatomic, assign) float strokeWidth;

/**
 * Thuộc tính xác định ẩn / hiện của đối tượng
 */
@property (nonatomic, assign, getter=isVisible) BOOL visible;

/**
 * Thuộc tính round tại các điểm trong đường polyline
 * Tuy nhiên hiện tại chưa hỗ trợ được thuộc tính này
 * API sẽ hỗ trợ thuộc tính trong các phiên bản tiếp theo
 */
@property (nonatomic, assign, getter=isRoundCorner) BOOL roundCorner;

/**
 * Khởi tạo polyline sử dụng polyline options
 */
- (id)initWithOptions:(VMSPolylineOptions *)opts;

/**
 * Hàm tiện ích tạo polyline sử dụng path cho trước
 * Các thuộc tính khác sử dụng giá trị mặc định
 */
+ (instancetype)polylineWithPath:(NSArray *)path;

/**
 * Thêm điểm vào danh sách các điểm hiện tại
 */
- (void)addPoint:(VMSLatLng *)pt;

/**
 * Thêm một danh sách các điểm vào danh sách điểm hiện tại
 */
- (void)addPoints:(NSArray *)pts;

@end