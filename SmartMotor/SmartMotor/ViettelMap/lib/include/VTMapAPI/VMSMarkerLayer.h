//
//  VMSMarkerLayer.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/18/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSMapLayer.h"
#import "VMSMarker.h"
#import "VMSMarkerOptions.h"

@interface VMSMarkerLayer : VMSMapLayer

/**
 * Ham khoi tao cua doi tuong Marker layer
 */
- (id)init;

/**
 * Ham tien ich add marker vao layer su dung opts
 */
- (VMSMarker *)addMarker:(VMSMarkerOptions *)opts;

@end
