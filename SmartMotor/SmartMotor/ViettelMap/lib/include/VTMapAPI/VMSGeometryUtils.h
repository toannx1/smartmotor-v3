//
//  VMSGeometryUtils.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/12/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@class VMSLatLng;
@class VMSLatLngBounds;
@class VMSProjPointInfo;

@interface VMSGeometryUtils : NSObject

/**
 * Ham tien ich convert degree to radian
 */
+ (double)toRadians:(double)degrees;

/**
 * Ham tien ich convert radian to degree
 */
+ (double)toDegrees:(double)radians;

/**
 * Ham tien ich tinh khoang cach giua 2 diem theo he toa do
 */
+ (double)lengthFromPoint:(VMSLatLng *)pt1 toPoint:(VMSLatLng *)pt2;

/**
 * Ham tien ich tinh chieu dai cua path
 * @param isClosed
 */
+ (double)lengthOfPath:(NSArray *)paths isClosed:(BOOL)isClosed;

/**
 * Ham tien ich tinh chieu dai cua path
 */
+ (double)lengthOfPath:(NSArray *)paths;

/**
 * Ham tien ich tao diem tu 1 diem theo khoang cach va goc
 */
+ (VMSLatLng *)computeOffsetFromPoint:(VMSLatLng *)pt distance:(double)distance heading:(double)heading;

/**
 * Ham tien ich tinh boundary cua duong tron
 */
+ (VMSLatLngBounds *)computeCircleBoundaryWithCenter:(VMSLatLng *)center radius:(double)radius;

/**
 * Ham tien ich tinh dien tich cua path
 */
+ (double)computePathArea:(NSArray *)paths;

/**
 * Ham tien ich kiem tra diem co thuoc polygon hay khong
 */
+ (BOOL)point:(VMSLatLng *)pt inPolygon:(NSArray *)pts;

/**
 * Ham tien ich chieu diem len 1 duong thang
 */
+ (VMSProjPointInfo *)projectPoint:(VMSLatLng *)pt onSegment:(VMSLatLng *)pt1 :(VMSLatLng *)pt2;

/**
 * Ham tien ich tinh khoang cach khi chieu 1 diem len 1 polyline
 */
+ (VMSProjPointInfo *)distancePoint:(VMSLatLng *)pt toPoly:(NSArray *)pts;

/**
 * Ham tien ich rut gon path tai 1 ty le resolutions truyen vao
 * (Giam so diem cua duong ma van giu duoc tuong doi hinh dang cua duong ban dau)
 */
+ (NSMutableArray *)reducePath:(NSArray *)path atResolution:(double)resolution;

/**
 * Ham tien ich rut gon path tai 1 muc zoom truyen vao
 * (Giam so diem cua duong ma van giu duoc tuong doi hinh dang cua duong ban dau)
 */
+ (NSMutableArray *)reducePath:(NSArray *)path atZoomLevel:(int)zoomLevel;

/**
 * Ham tien ich lay diem giua duong thang tao ra boi 2 point
 */
+ (VMSLatLng*)midPointBetweenLatLng:(VMSLatLng*)latLng1 andLatLng:(VMSLatLng*)latLng2;

@end
