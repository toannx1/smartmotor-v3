//
//  VMSVectorLayer.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/18/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSMapLayer.h"

@class VMSCircle;
@class VMSCircleOptions;
@class VMSPolygon;
@class VMSPolygonOptions;
@class VMSPolyline;
@class VMSPolylineOptions;

@interface VMSVectorLayer : VMSMapLayer

/**
 * Hàm khởi tạo của các đối tượng vector layer
 */
- (id)init;

/**
 * Hàm add Circle vào layer sử dụng Circle options
 */
- (VMSCircle *)addCircle:(VMSCircleOptions *)opts;

/**
 * Hàm add Polygon vào layer sử dụng Polygon options
 */
- (VMSPolygon *)addPolygon:(VMSPolygonOptions *)opts;

/**
 * Hàm add Polyline vào layer sử dụng Polyline options
 */
- (VMSPolyline *)addPolyline:(VMSPolylineOptions *)opts;

@end
