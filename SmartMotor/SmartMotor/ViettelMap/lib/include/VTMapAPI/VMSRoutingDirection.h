//
//  VMSRoutingDirection.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/14/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <UIKit/UIImage.h>


typedef enum {
    VMSRoutingHeadOn = 10,
    VMSRoutingGoAhead = 1,
    VMSRoutingLeftLane = 8,
    VMSRoutingRightLane = 2,
    VMSRoutingTurnLeft = 7,
    VMSRoutingTurnRight = 3,
    VMSRoutingTwistLeft = 6,
    VMSRoutingTwistRight = 4,
    VMSRoutingTurnBack = 5,
    VMSRoutingDestination = 17,
} VMSRoutingTurn;

#define VMS_HEAD_ON_MSG NSLocalizedStringFromTable(@"Bắt đầu từ", @"VTMapAPILocalizable", @"Head on msg")
#define VMS_GO_AHEAD_MSG NSLocalizedStringFromTable(@"Đi thẳng vào", @"VTMapAPILocalizable", @"Go ahead msg")
#define VMS_LEFT_LANE_MSG NSLocalizedStringFromTable(@"Vòng sang trái vào", @"VTMapAPILocalizable", @"Left lane msg")
#define VMS_RIGHT_LANE_MSG NSLocalizedStringFromTable(@"Vòng sang phải vào", @"VTMapAPILocalizable", @"Right lane msg")
#define VMS_TURN_LEFT_MSG NSLocalizedStringFromTable(@"Rẽ trái vào", @"VTMapAPILocalizable", @"Turn left msg")
#define VMS_TURN_RIGHT_MSG NSLocalizedStringFromTable(@"Rẽ phải vào", @"VTMapAPILocalizable", @"Turn right msg")
#define VMS_TWIST_LEFT_MSG NSLocalizedStringFromTable(@"Chếch sang trái vào", @"VTMapAPILocalizable", @"Twist left msg")
#define VMS_TWIST_RIGHT_MSG NSLocalizedStringFromTable(@"Chếch sang phải vào", @"VTMapAPILocalizable", @"Twist right msg")
#define VMS_TURN_BACK_MSG NSLocalizedStringFromTable(@"Quay đầu xe vào", @"VTMapAPILocalizable", @"Turn back msg")
#define VMS_TO_DESTINATION_MSG NSLocalizedStringFromTable(@"Về đích tại", @"VTMapAPILocalizable", @"Destination msg")
#define VMS_ROUND_ABOUT_MSG NSLocalizedStringFromTable(@"Qua bùng binh", @"VTMapAPILocalizable", @"Round about msg")
#define VMS_UNKNOWN_ROAD_MSG NSLocalizedStringFromTable(@"Đường chưa xác định", @"VTMapAPILocalizable", @"Unknown road name msg")

@interface VMSRoutingDirection : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) double distance;
@property (nonatomic, assign) int turn;
@property (nonatomic, assign) BOOL roundAbout;
@property (nonatomic, assign) int index;
@property (nonatomic, readonly) NSString *instruction;
@property (nonatomic, readonly) UIImage *directionImage;

/**
 * Parse ket qua json tu server tra ve thanh VMSRoutingDirection object
 */
+ (VMSRoutingDirection *)parseRoutingDirection:(NSDictionary *)dict;

/**
 * Ham khoi tao
 */
- (id)init;

/**
 * Ham update lai gia tri kieu turn.
 * Nguoi dung khong nen tu y su dung
 */
- (void)updateTurn:(int)algo;

@end

