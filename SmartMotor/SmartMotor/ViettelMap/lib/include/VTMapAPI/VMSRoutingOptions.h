//
//  VMSRoutingOptions.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/12/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Dinh nghia cac kieu phuong tien cho routing
 */
typedef enum {
    VMSModeWalking = 0,
    VMSModeMotor = 1,
    VMSModeCar = 2,
    VMSTruck = 3,
} VMSTravelMode;

/**
 * Dinh nghia cac thuat toan routing
 */
typedef enum {
    VMSAlgoDijkstra = 0,
    VMSAlgoAstar = 1,
    VMSAlgoShootingStar = 2,
} VMSAlgoType;


//==================Class thuộc tính phụ trợ cho dịch vụ tìm kiếm đường đi==================//
@interface VMSRoutingOptions : NSObject

/**
 * Kiểu phương tiện sử dụng để tìm đường
 */
@property (nonatomic, assign) VMSTravelMode travelMode;

/**
 * Thuật toán tìm đường. Biến này không cần thiết xác lập 
 */
@property (nonatomic, assign) VMSAlgoType algoType;

/**
 * Kết quả trả về có bao gồm hướng dẫn chỉ đường hay không
 * Giá trị mặc định hiện tại là có
 */
@property (nonatomic, assign) BOOL hasInstruction;

/**
 * Kết quả trả về có bao gồm tuyến phụ trợ ngoài đường chính hay không
 * Hiện iOS chưa hỗ trợ phần này nên đang mặc định là không
 */
@property (nonatomic, assign) BOOL hasAlternative;

/**
 * Danh sách các điểm trung gian mà tuyến đường sẽ phải đi qua thêm ngoài 2 điểm bắt đầu, kết thúc
 */
@property (nonatomic, retain) NSMutableArray *wapypoint;

@end
