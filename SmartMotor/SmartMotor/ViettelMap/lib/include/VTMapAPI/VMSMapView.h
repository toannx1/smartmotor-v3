//
//  VMSMapView.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/13/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMSMapLayer.h"
#import "VMSBaseControl.h"
#import <CoreLocation/CoreLocation.h>

@class VMSLatLng;
@class VMSLatLngBounds;
@class VMSMapOptions;
@class VMSBaseControl;
@class VMSProjection;
@class VMSUISetting;

@class VMSMapLayer;

@class VMSMapObject;
@class VMSMarker;
@class VMSMarkerOptions;
@class VMSCircle;
@class VMSCircleOptions;
@class VMSPolygon;
@class VMSPolygonOptions;
@class VMSPolyline;
@class VMSPolylineOptions;
@class VMSInfoWindow;
@class VMSInfoWindowOptions;


typedef enum {
    VMSMapTypeTransport = 0,
    VMSMapTypeAdmin = 1,
    VMSMapTypeTerrain = 2,
    VMSMapTypeSattelite = 3,
} VMSMapType;

typedef enum {
    TOUCH_MODE_REST = -1,
    TOUCH_MODE_DOWN = 0,
    TOUCH_MODE_SCROLL = 1,
    TOUCH_MDE_FLING = 2,
} VMSTouchMode;

typedef enum {
    ACTION_MODE_NONE = 0,
    ACTION_MODE_DRAG = 1,
    ACTION_MODE_ZOOM = 2,
} VMSActionMode;


@protocol VMSMapDelegate <NSObject>
@optional
- (BOOL)singleTapAtPoint:(CGPoint)pt latLng:(VMSLatLng *)latLng;
- (BOOL)longPressAtPoint:(CGPoint)pt latLng:(VMSLatLng *)latLng;
- (BOOL)doubleTapAtPoint:(CGPoint)pt latLng:(VMSLatLng *)latLng;
- (void)centerChanged;
- (void)boundChanged;
- (void)mapIdle;
- (void)locationUpdate:(CLLocation *)location;
- (void)locationError:(NSError *)error;

@end

@interface VMSMapView : UIView

/**
 * Thuoc tinh cua application
 */
@property (nonatomic, readonly) NSString *appId;
@property (nonatomic, copy) NSString *appKey;

/**
 * Thuoc tinh cua map
 */
@property (nonatomic, readonly) int mapHeight;
@property (nonatomic, readonly) int mapWidth;
@property (nonatomic, copy) VMSLatLng *mapCenter;
@property (nonatomic, readonly) float mapScale;
@property (nonatomic, assign) int zoomLevel;
@property (nonatomic, assign) VMSMapType mapType;
@property (nonatomic, readonly) int zoomMin;
@property (nonatomic, readonly) int zoomMax;
@property (nonatomic, readonly) VMSProjection *projection;
@property (nonatomic, readonly) VMSUISetting *uiSetting;
@property (nonatomic, readonly) VMSActionMode mapMode;

/**
 * Delegate cho cac su kien cua map
 */
@property (nonatomic, weak) id<VMSMapDelegate> mapDelegate;

/*------------------------INIT--------------------------*/

/**
 * Khởi tạo frame của map theo frame truyền vào cho trước
 * Nếu sử dụng hàm này thì cần set key rồi gọi initMap để hoàn thiện map
 */
- (id)initWithFrame:(CGRect)frame;

/**
 * Khởi tạo map theo thông tin key, frame.
 * Gọi sau khi init với frame và set key cho map.
 */
- (void)initMap;


/**
 * Khoi tao map theo key và frame cho trước.
 * Sử dụng hàm này sẽ trả về mapView hoàn thiện, không cần gọi hàm setKey hay initMap nữa.
 */
- (id)initWithKey:(NSString *)key frame:(CGRect)frame;

/**
 * Khoi tao map theo key và frame cho trước.
 * Đưa ra options cho các control
 * Sử dụng hàm này sẽ trả về mapView hoàn thiện, không cần gọi hàm setKey hay initMap nữa.
 */
- (id)initWithKey:(NSString *)key frame:(CGRect)frame options:(VMSUISetting *)opts;


/*-----------------------ACTIONS------------------------*/

/**
 * Request yêu cầu map vẽ lại toàn bộ màn hình map
 */
- (void)refresh;

/**
 * Set lại mức zoom của map
 * @param zoomLevel mức zoom map sẽ chuyển tới
 * @param refresh có vẽ lại map sau khi thay đổi mức zoom không. 
 * Trường hợp NO map sẽ không thay đổi giao diện cho tới khi được gọi hàm refresh
 */
- (void)setZoomLevel:(int)zoomLevel refresh:(BOOL)refresh;

/**
 * Set lại điểm tâm mới của map
 * @param center điểm tâm mới mà map sẽ chuyển tới
 * @param refresh có vẽ lại map sau khi thay đổi tâm không.
 * Trường hợp NO map sẽ không thay đổi giao diện cho tới khi được gọi hàm refresh
 */
- (void)setCenter:(VMSLatLng *)center refresh:(BOOL)refresh;

/**
 * Map tự động dịch chuyển đến mức zoom và tâm mới để chứa toàn bộ vùng bound truyền vào
 * @param bound vùng không gian cần chứa đủ trong không gian map
 * @param minZoom mức zoom tối thiểu cho phép. Nếu mức zoom cần để chứa cả bound nhỏ hơn minZoom thì sẽ chỉ zoom tới minZoom
 * @param maxZoom mức zoom tối đa cho phép. Nếu mức zoom cần để chứa cả bound lớn hơn maxZoom thì sẽ chỉ zoom tới maxZoom
 * @param bestSuit zoom ở mức zoom vừa khít nhất để chứa cả vùng không gian truyền vào hay ra ngoài 1 mức zoom cho thoáng.
 */
- (BOOL)fitBounds:(VMSLatLngBounds *)boundary minZoom:(int)minZoomLevel maxZoom:(int)maxZoomLevel bestSuit:(BOOL)theBestSuit;

/**
 * Map tự động dịch chuyển đến mức zoom và tâm mới để chứa toàn bộ vùng bound truyền vào.
 * Không có giới hạn mức zoom. Sử dụng đúng mức zoom phù hợp
 * @param bound vùng không gian cần chứa đủ trong không gian map
 * @param bestSuit zoom ở mức zoom vừa khít nhất để chứa cả vùng không gian truyền vào hay ra ngoài 1 mức zoom cho thoáng.
 */
- (BOOL)fitBounds:(VMSLatLngBounds *)boundary bestSuit:(BOOL)theBestSuit;

/**
 * Map tự động dịch chuyển đến mức zoom và tâm mới để chứa toàn bộ vùng bound truyền vào
 * Không có giới hạn mức zoom. Sử dụng đúng mức zoom phù hợp 
 * @param bound vùng không gian cần chứa đủ trong không gian map
 */
- (BOOL)fitBounds:(VMSLatLngBounds *)boundary;

/**
 * Di chuyển bản đồ tới 1 điểm chỉ định
 * Trong trường hợp điểm đó gần với tâm, sẽ tương đương setCenter.
 * Trong trường hợp điểm đó cách xa tâm, sẽ có hiệu ứng di chuyển bản đồ.
 */
- (void)moveTo:(VMSLatLng* const)ptNewCenter;



/*==========================LAYER==========================*/

/**
 * Tìm kiếm layer có id tương ứng với giá trị truyền vào trong danh sách các layer mà map quản lý.
 */
- (VMSMapLayer *)layerForId:(NSString *)layerId;

/**
 * Lấy danh sách của tất cả các layer map đang quản lý có cùng zIndex với giá trị truyền vào
 */
- (NSMutableArray *const)layersForZIndex:(int)zIndex;

/**
 * Thêm layer vào danh sách quản lý của map
 */
- (BOOL)addLayer:(VMSMapLayer *)layer;

/**
 * Thêm một danh sách các layer vào danh sách quản lý của map
 */
- (void)addLayers:(NSArray *)layers;

/**
 * Loại bỏ layer khỏi danh sách quản lý của map
 */
- (BOOL)removeLayer:(VMSMapLayer *)layer;

/**
 * Loại bỏ một danh sách các layer khỏi danh sách quản lý của map
 */
- (void)removeLayers:(NSArray *)lstLayer;

/**
 * Loại bỏ tất cả các layer có cùng zIndex với giá trị truyền vào khỏi danh sách quản lý của map
 */
- (NSMutableArray *)removeLayersByZIndex:(int)zIndex;

/**
 * Loại bỏ toàn bộ các layer mà map đang quản lý
 * Đối tượng trong các layer không thay đổi
 */
- (void)removeAllLayer;

/**
 * Loại bỏ tất cả các đối tượng trong các layer mà map đang quản lý.
 * Không loại bỏ layer
 */
- (void)clear;

/**
 * Loại bỏ tất cả các đối tượng trong layer có cùng id với giá trị truyền vào mà map đang quản lý.
 * Không loại bỏ layer khỏi danh sách quản lý
 */
- (void)clearLayer:(NSString *)layerId;

/**
 * Chỉ loại bỏ các đối tượng add trực tiếp vào map, đang được map quản lý trên các layer default của map
 * Với các đối tượng người dùng add vào các layer người dùng tự tạo thì không thay đổi
 */
- (void)clearDefault;

/**
 * Loại bỏ toàn bộ các layer mà map đang quản lý
 * Các đối tượng trong layer cũng bị remove khỏi layer
 */
- (void)reset;

/**
 * Tổng số layer mà map đang quản lý
 */
- (int)totalLayers;


/*========================OBJECT==========================*/

/**
 * Thêm marker vào layer default mà map đang quản lý
 */
- (void)addMarker:(VMSMarker *)marker;

/**
 * Thêm marker vào layer default mà map đang quản lý sử dụng marker options
 */
- (VMSMarker *)addMarkerByOptions:(VMSMarkerOptions *)opts;

/**
 * Thêm circle vào layer default mà map đang quản lý
 */
- (void)addCircle:(VMSCircle *)circle;

/**
 * Thêm circle vào layer default mà map đang quản lý sử dụng circle options
 */
- (VMSCircle *)addCircleByOptions:(VMSCircleOptions *)opts;

/**
 * Thêm polygon vào layer default mà map đang quản lý
 */
- (void)addPolygon:(VMSPolygon *)polygon;

/**
 * Thêm polygon vào layer default mà map đang quản lý sử dụng polygon options
 */
- (VMSPolygon *)addPolygonByOptions:(VMSPolygonOptions *)opts;

/**
 * Thêm polyline vào layer default mà map đang quản lý
 */
- (void)addPolyline:(VMSPolyline *)polyline;

/**
 * Thêm polyline vào layer default mà map đang quản lý sử dụng polyline options
 */
- (VMSPolyline *)addPolylineByOptions:(VMSPolylineOptions*)opts;

/**
 * Loại bỏ đối tượng marker khỏi layer default của map
 */
- (BOOL)removeMarker:(VMSMarker *)marker;

/**
 * Loại bỏ đối tượng vector (Circle / Polygon / Polyline) khỏi layer default của map
 */
- (BOOL)removeVector:(VMSMapObject *)vector;

/*----------------------INFO WINDOW-----------------------*/

/**
 * Hiển thị info window theo options truyền vào.
 * Tự động di chuyển tâm màn hình về position của info window
 */
- (VMSInfoWindow *)showInfoWindow:(VMSInfoWindowOptions *)opts;

/**
 * Hiển thị info window theo options truyền vào.
 * @param animateToCenter 
        YES sẽ tự động di chuyển tâm màn hình về position của info window
        NO sẽ giữ nguyên vị trí hiện tại của màn hình không thay đổi
 */
- (VMSInfoWindow *)showInfoWindowWithOpts:(VMSInfoWindowOptions *)opts animateToCenter:(BOOL)shouldAnimate;

/**
 * Hiển thị info window truyền vào.
 * @param animateToCenter
        YES sẽ tự động di chuyển tâm màn hình về position của info window
        NO sẽ giữ nguyên vị trí hiện tại của màn hình không thay đổi
 */
- (void)showInfoWindow:(VMSInfoWindow *)infoWindow animateToCenter:(BOOL)shouldAnimate;

/**
 * Ẩn tất cả các infowindow của default layer mà map đang quản lý
 */
- (void)hideInfoWindow;

/**
 * Ẩn tất cả các infowindow có id giống với giá trị truyền vào
 */
- (void)hideInfoWindow:(NSString *)infoWindowId;

/**
 * Ẩn tất cả các infowindow của layer có layer id giống với giá trị truyền vào
 */
- (void)hideInfoWindowFromLayer:(NSString *)layerId;



/*==========================ZOOM==========================*/

/**
 * Tăng mức zoom của map lên một đơn vị so với mức zoom hiện tại
 * Sử dụng tâm bản đồ làm điểm zoom
 */
- (BOOL)zoomIn;

/**
 * Giảm mức zoom của map lên một đơn vị so với mức zoom hiện tại
 * Sử dụng tâm bản đồ làm điểm zoom
 */
- (BOOL)zoomOut;

/**
 * Tăng mức zoom của map lên một đơn vị so với mức zoom hiện tại
 * Sử dụng điểm truyền vào làm tâm zoom của bản đồ
 */
- (BOOL)zoomIn:(CGPoint)point;

/**
 * Giảm mức zoom của map lên một đơn vị so với mức zoom hiện tại
 * Sử dụng điểm truyền vào làm tâm zoom của bản đồ
 */
- (BOOL)zoomOut:(CGPoint)point;

/**
 * Hàm tiện ích kiểm tra bản đồ có thể zoom in được nữa không
 * Không trong trường hợp đã đạt mức maxZoom
 */
- (BOOL)canZoomIn;

/**
 * Hàm tiện ích kiểm tra bản đồ có thể zoom out được nữa không
 * Không trong trường hợp đã đạt mức minZoom
 */
- (BOOL)canZoomOut;



/*=========================CONTROL=========================*/

/**
 *
 */
- (VMSBaseControl *)addControl:(VMSBaseControl *)control;

/**
 *
 */
- (VMSBaseControl *)addControlByType:(VMSControlType)controlType;

/**
 *
 */
- (VMSBaseControl *)getControl:(VMSControlType)controlType;

/**
 *
 */
- (BOOL)removeControl:(VMSBaseControl *)control;

/**
 *
 */
- (BOOL)removeControlByType:(VMSControlType)controlType;

/**
 *
 */
- (void)removeAllControl;

/*------------------DEFAULT LAYER DELEGATE------------------*/

/**
 * Các hàm set sự kiện cho lớp marker layer default của mapView
 */
- (void)setMarkerTabDelegate:(id<VMSMapObjectSingleTabDelegate>)delegate;
- (void)setMarkerLongPressDelegate:(id<VMSMapObjectLongPressDelegate>)delegate;
- (void)setMarkerDoubleTabDelegate:(id<VMSMapObjectDoubleTabDelegate>)delegate;
- (void)setMarkerDragDelegate:(id<VMSMapObjectDragDelegate>)delegate;

/**
 * Các hàm set sự kiện cho lớp vector default của mapView
 */
- (void)setVectorTabDelegate:(id<VMSMapObjectSingleTabDelegate>)delegate;
- (void)setVectorLongPressDelegate:(id<VMSMapObjectLongPressDelegate>)delegate;
- (void)setVectorDoubleTabDelegate:(id<VMSMapObjectDoubleTabDelegate>)delegate;
- (void)setVectorDragDelegate:(id<VMSMapObjectDragDelegate>)delegate;

/**
 * Các hàm set sự kiện cho lớp info window default của mapView
 */
- (void)setInfoWindowTabDelegate:(id<VMSMapObjectSingleTabDelegate>)delegate;
- (void)setInfoWindowLongPressDelegate:(id<VMSMapObjectLongPressDelegate>)delegate;
- (void)setInfoWindowDoubleTabDelegate:(id<VMSMapObjectDoubleTabDelegate>)delegate;
- (void)setInfoWindowHyperlinkDelegate:(id<VMSHyperlinkDelegate>)delegate;


/*----------------------------------------------------------*/
/**
 * Ham reset lai context cua opengl ve dung voi context hien tai ma mapview dang giu
 * Can su dung khi su dung multi mapView
 */
- (void) reloadContext;
/**
 * Hàm giải phóng ảnh cache trong map
 * Sử dụng khi chương trình nhận cảnh báo về memory
 * Hoặc khi người dùng tự muốn xoá bớt ảnh cache để giảm bộ nhớ
 */
- (void)didReceiveMemoryWarning;

@end