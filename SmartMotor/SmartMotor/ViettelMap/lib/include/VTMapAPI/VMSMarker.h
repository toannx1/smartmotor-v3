//
//  VMSMarker.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/15/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSMapObject.h"

@class VMSMarkerOptions;
@class VMSInfoWindow;

@interface VMSMarker : VMSMapObject

/**
 * Điểm đặt marker.
 * Trong trường hợp không set anchor, điểm giữa dưới cùng của marker icon sẽ hiển thị chính xác tại điểm này.
 * Trong trường hợp set anchor thì đúng vị trí được set của icon sẽ hiển thị tại điểm này
 */
@property (nonatomic, copy) VMSLatLng *position;

/**
 * Title của infowindow cho marker.
 * Trong trường hợp title được set, khi click vào marker, không có xử lý gì từ phía người dùng
 * Hệ thống sẽ hiển thị infowindow với title tương ứng
 * Trường hợp không set title thì default sẽ không hiển thị infowindow
 */
@property (nonatomic, copy) NSString *title;

/**
 * Desciption của infowindow cho marker (snippet).
 * Để hiển thị nhiều dòng thì các dòng phải sử dụng ký tự xuống dòng "\n"
 * Trong trường hợp không có title thì dù set desciption cũng không hiển thị infowindow
 */
@property (nonatomic, copy) NSString *description;

/**
 * Anchor của marker
 * Vị trí tương ứng của icon sẽ được hiển thị đúng tại position của marker
 * Trường hợp không set sẽ sử dụng anchor mặc định là điểm chính giữa dưới cùng (Bottom - Center) của icon làm anchor
 */
@property (nonatomic, assign) CGPoint anchor;

/**
 * Anchor của infowindow mặc định khi click vào marker
 * Vị trí tương ứng của icon sẽ được sử dụng để hiển thị infowindow
 * Trường hợp không set sẽ lấy điểm chính giữa trên cùng (Top - Center) của icon làm anchor hiển thị
 */
@property (nonatomic, assign) CGPoint infoWindowAnchor;

/**
 * Đối tượng cho phép người dùng lưu trữ các thông tin bất kỳ mà người dùng muốn vào marker
 * Thông qua các sự kiện có trả về marker hay lấy marker từ layer
 * Người dùng có thể lấy ra các dữ liệu này để tiện cho các thao tác
 * Giá trị được marker retain.
 */
@property (nonatomic, strong) id userData;

/**
 * Tương tự như userData nhưng là weak reference, marker không retain đối tượng
 */
@property (nonatomic, weak) id userDataRef;

/**
 * Icon đại diện dùng để hiển thị infoWindow
 * Trong trường hợp không set hoặc set nil thì sẽ sử dụng icon mặc định của hệ thống
 */
@property (nonatomic, copy) UIImage *icon;

/**
 * Góc xoay của marker. Đơn vị là độ
 * Giá trị dương : xoay theo chiều kim đồng hồ
 * Giá trị âm : xoay ngược chiều kim đồng hồ
 * Giá trị 0, ảnh không xoay.
 * Các anchor cũng sẽ xoay theo để đảm bảo hiển thị tại đúng vị trí tương ứng như khi không xoay
 */
@property (nonatomic, assign) int rotate;

/**
 * Thuộc tính xác định ẩn / hiện của đối tượng marker
 */
@property (nonatomic, assign, getter=isVisible) BOOL visible;

/**
 * Thuộc tính cho phép kéo / thả marker
 */
@property (nonatomic, assign, getter=isDraggable) BOOL draggable;

/**
 * Hàm khởi tạo marker sử dụng marker options
 */
- (id)initWithOptions:(VMSMarkerOptions *)option;

/**
 * Hàm tiện ích tạo marker từ vị trí của marker
 * Các thuộc tính còn lại sử dụng giá trị default
 */
+ (instancetype)markerWithPosition:(VMSLatLng *)position;

/**
 * Hiển thị infowindow default của marker trên mapView mà layer chứa marker được add vào
 * Trả về infowindow được hiển thị ra từ mapView
 * Trường hợp marker chưa được add vào layer hoặc layer chứa marker chưa được add vào mapview thì sẽ không xử lý
 */
- (VMSInfoWindow *)showInfoWindow;


/**
 * Hiển thị infowindow default của marker trên mapView mà layer chứa marker được add vào
 * Trả về infowindow được hiển thị ra từ mapView
 * Trường hợp marker chưa được add vào layer hoặc layer chứa marker chưa được add vào mapview thì sẽ không xử lý
 */
- (void)closeInfoWindow;

/**
 * Hàm trả về vùng không gian mà marker chiếm trên màn hình
 * Marker phải được vẽ lên màn hình mới trả về giá trị chính xác
 */
- (CGRect)getRect;

@end