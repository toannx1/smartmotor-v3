//
//  VMSProjection.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/13/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@class VMSMapView;
@class VMSLatLng;
@class VMSLatLngBounds;

@interface VMSProjection : NSObject

/**
 * Map ma projection dang chieu len
 */
@property (nonatomic, weak) VMSMapView *mapView;

/**
 * Ham khoi tao su dung mapView de tinh toan. Projection thay doi theo su thay doi cua map
 * @param map ban do ma projection se su dung de tinh toan
 */
- (id)initWithMap:(VMSMapView *)mapView;

/**
 * Ham khoi tao su dung cac bien xac dinh de thiet lap projection. Projection co dinh khong thay doi
 */
- (id) initWithCenter:(VMSLatLng*) center viewRect:(CGRect) viewRect zoomLevel:(int) zoomLevel mapScale:(float) mapScale;

/**
 * Chuyen toa do x, y cua man hinh sang toa do longitude, latitude cua ban do
 * @param x toa do theo chieu ngang
 * @param y toa do theo chieu doc
 */
- (VMSLatLng *)fromViewPixelX:(int)x y:(int)y;

/**
 * Chuyen toa do x, y cua man hinh sang toa do longitude, latitude cua ban do
 * @param point toa do diem tren man hinh
 */
- (VMSLatLng *)fromViewPixelPoint:(CGPoint)point;

/**
 * Chuyen toa do longitude, latitude cua ban do sang toa do x, y cua man hinh
 * @param latLng toa do latLng tren map
 */
- (CGPoint)toViewPixel:(VMSLatLng *)latLng;

/**
 * Ham tien ich doi khoang cach giua 2 diem tu meters sang pixels
 * Su dung tam lam diem tinh toan gia tri
 */
- (double)metersToEquatorPixels:(double)meters;

/**
 * Ham tien ich lay ra vung boundary cua man hinh hien thi map
 */
- (VMSLatLngBounds *)mapBoundary;

/**
 * Ham tien ich kiem tra xem 1 diem co nam trong vung boundary cua man hinh hien thi map hay khong
 */
- (BOOL)containsLatLng:(VMSLatLng*)latLng;

@end
