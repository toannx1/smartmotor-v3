//
//  VMSAdminItem.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/14/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSAdminType.h"
#import "VMSLatLngBounds.h"

@interface VMSAdminItem : NSObject

/**
 * Id cua vung hanh chinh
 */
@property (nonatomic, copy) NSString *objId;

/**
 * Code cua vung hanh chinh
 */
@property (nonatomic, copy) NSString *code;

/**
 * Ten vung hanh chinh
 */
@property (nonatomic, copy) NSString *name;

/**
 * Danh sach diem bao quanh cua vung hanh chinh
 */
@property (nonatomic, copy) NSMutableArray *paths;

/**
 * Boundary bao quanh cua vung hanh chinh
 */
@property (nonatomic, copy) VMSLatLngBounds *boundary;

/**
 * Muc level cua vung hanh chinh
 */
@property (nonatomic, assign) VMSAdminLevelType level;

/**
 * Mau su dung de ve vung hanh chinh tren nen hanh chinh
 */
@property (nonatomic, assign) int colorIndex;




/**
 * Parse json tra ve tu server thanh object VMSAdminItem
 */
+ (VMSAdminItem *)parseAdminItem:(NSDictionary *)dict;

/**
 * Ham khoi tao
 */
- (id)init;

/**
 * Tong so path bao cua vung hanh chinh
 */
- (int)totalPaths;

/**
 * Them path vao danh sach cac path bao quanh vung hanh chinh
 */
- (void)addPath:(NSMutableArray *)path;

/**
 * Lay theo index
 */
- (NSMutableArray *)pathAtIndex:(int)index;

@end
