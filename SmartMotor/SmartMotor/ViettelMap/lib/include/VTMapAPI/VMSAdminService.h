//
//  VMSAdminService.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/14/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSServiceAsyncTask.h"
#import "VMSAdminType.h"

@class VMSLatLng;
@class VMSLatLngBounds;
@class VMSAdminServiceResult;

/**
 * Các kiểu tìm kiếm hành chính
 * Sử dụng để phân biệt loại request trước đó trong hàm delegate
 */
typedef enum {

    getFeaturesAroundPoint,

    getFeaturesIntersectBounds,

    getFeaturesContainPoint,

    getFeaturesByCode,

} VMSAdminServiceType;


//==================Protocol delegate cho dịch vụ tìm kiếm hành chính==================//
@protocol VMSAdminServiceDelegate <NSObject>

@optional
/**
 * Hàm gọi khi bắt đầu thực hiện gọi AdminService
 * Thường sử dụng để tạo waiting dialog
 */
- (void)adminServiceWillStart:(VMSAdminServiceType)type;

@required
/**
 * Hàm trả về kết quả tìm đường sau khi AdminService hoàn thành request server
 */
- (void)adminServiceDidComplete:(VMSAdminServiceResult *)result ofType:(VMSAdminServiceType)type;

@end


//===========================Lớp dịch vụ tìm kiếm hành chính===========================//
@interface VMSAdminService : NSObject <VMSAsyncDelegate>

/**
 * Delegate su dung de tra ket qua sau khi hoan thanh request den server
 */
@property (nonatomic, weak) id<VMSAdminServiceDelegate> delegate;

/**
 * Level tim kiem doi tuong hanh chinh
 */
@property (nonatomic, assign) VMSAdminLevelType levelType;

/**
 * Cac loai thuoc tinh se tra ve cua doi tuong hanh chinh
 * Su dung VMSAdminReturnType : Truong hop nhieu thuoc tinh thi cac su dung | de ket hop cac type voi nhau
 */
@property (nonatomic, assign) int returnType;

/**
 * Ham khoi tao
 */
- (id)initWithDelegate:(id<VMSAdminServiceDelegate>)listener;

/**
 * Tim kiem vung hanh chinh chua diem truyen vao
 */
- (void)getFeaturesContainPoint:(VMSLatLng *)pt;

/**
 * Tim kiem cac vung hanh chinh giao voi 1 vung tron co tam, ban kinh truyen vao
 */
- (void)getFeaturesIntersecCircleCenter:(VMSLatLng*)center radius:(double)radius;

/**
 * Tim kiem cac vung hanh chinh giao voi 1 vung boundary truyen vao
 */
- (void)getFeaturesIntersectBounds:(VMSLatLngBounds *)bound;

/**
 * Tim kiem vung hanh chinh theo ma vung
 */
- (void)getFeaturesByCode:(NSString *)code;

@end
