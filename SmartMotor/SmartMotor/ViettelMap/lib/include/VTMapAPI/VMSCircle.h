//
//  VMSCircle.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/15/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSMapObject.h"
#import "VMSCircleOptions.h"

@interface VMSCircle : VMSMapObject

/**
 * Tâm của đường tròn.
 */
@property (nonatomic, copy) VMSLatLng *center;

/**
 * Màu của đường viền circle
 */
@property (nonatomic, copy) UIColor *strokeColor;

/**
 * Màu của vùng fill bên trong đường bao của đường tròn.
 */
@property (nonatomic, copy) UIColor *fillColor;

/**
 * Bán kính của đường tròn.
 */
@property (nonatomic, assign) double radius;

/**
 * Độ rộng của đường viền đường tròn
 */
@property (nonatomic, assign) float strokeWidth;

/**
 * Xác định đối tượng có hiển thị trên mapView hay không
 */
@property (nonatomic, assign, getter=isVisible) BOOL visible;

/**
 * Ham khoi tao su dung opt
 */
- (id)initWithOptions:(VMSCircleOptions *)opt;

/**
 * Ham tien ich khoi tao doi tuong su dung tam va ban kinh
 */
+ (instancetype)circleWithCenter:(VMSLatLng *)center radius:(double)radius;

@end