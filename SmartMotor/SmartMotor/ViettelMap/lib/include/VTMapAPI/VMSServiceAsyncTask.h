//
//  VMSServiceAsyncTask.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/14/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

@protocol VMSAsyncDelegate <NSObject>

@required

/**
 * Request thanh cong, khong co loi xay ra
 * @param connection connection su dung de request service
 * @param data du lieu tra ve tu server
 */
- (void) didFinish:(NSURLConnection *)connection withData:(NSMutableData *) data;

/**
 * Request thanh cong, khong co loi xay ra
 * @param connection connection su dung de request service
 * @param error loai loi xay ra khi request server
 */
- (void) didFail:(NSURLConnection *)connection withError:(NSError *)error;

@end



@interface VMSServiceAsyncTask : NSObject

/**
 * Chuoi URL su dung de request server
 */
@property (nonatomic, copy) NSString *requestUrl;

/**
 * Delegate su dung cho viec tra ve su kien khi hoan thanh request
 */
@property (nonatomic, retain) id<VMSAsyncDelegate> delegate;

/**
 * Khoi tao object theo URL va delegate
 */
- (id)initWithURL:(NSString *)url delegate:(id<VMSAsyncDelegate>)delegate;

/**
 * Bat dau request server. Goi nhieu lan thi lan goi sau se cancel lan goi truoc
 */
- (void)start;

/**
 * Force ngung request ( Goi truoc nhieu lan khong gay loi)
 */
- (void)cancel;

@end