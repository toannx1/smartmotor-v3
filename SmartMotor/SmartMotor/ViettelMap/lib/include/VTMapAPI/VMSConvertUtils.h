//
//  VMSConvertUtils.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/12/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

@class VMSLatLng;
@class VMSMapConfig;
@class VMSLatLngBounds;

@interface VMSConvertUtils : NSObject

/**
 * Ham chuyen doi toa do diem theo he quy chieu 4326 sang toa do he quy chieu 900913
 */
+ (VMSLatLng *)degrees2metersForLat:(double)latitude lng:(double)longitude;

/**
 * Ham chuyen doi toa do diem theo he quy chieu 900913 sang toa do he quy chieu 4326
 */
+ (VMSLatLng *)meters2degreesForLat:(double)latitude lng:(double)longitude;

/**
 * Ham chuyen doi vung khong gian theo he quy chieu 900913 sang he quy chieu 4326
 */
+ (VMSLatLngBounds *)degrees2metersForBoundary:(VMSLatLngBounds *)boundary;

/**
* Ham chuyen doi vung khong gian theo he quy chieu 4326 sang he quy chieu 900913
*/
+ (VMSLatLngBounds *)meters2degreesForBoundary:(VMSLatLngBounds *)boundary;

/**
 * Ham chuyen doi vi do theo he quy chieu 4326 sang toa do he quy chieu 900913
 */
+ (double)convertLatitude:(double)latitude;

/**
 * Ham chuyen doi kinh do theo he quy chieu 4326 sang toa do he quy chieu 900913
 */
+ (double)convertLongitude:(double)longitude;

/**
 * Ham chuyen doi vi do theo he quy chieu 900913 sang toa do he quy chieu 4326
 */
+ (double)revertLatitude:(double)latitude;

/**
 * Ham chuyen doi kinh do theo he quy chieu 900913 sang toa do he quy chieu 4326
 */
+ (double)revertLongitude:(double)longitude;

@end