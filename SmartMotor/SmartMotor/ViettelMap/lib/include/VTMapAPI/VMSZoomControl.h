//
//  VMSZoomControl.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/14/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSBaseControl.h"

@interface VMSZoomControl : VMSBaseControl

/**
 * Cho phep nguoi dung enable / disable nut zoomIn trong zoom control
 */
- (void)setZoomInEnabled:(BOOL) enabled;

/**
 * Cho phep nguoi dung enable / disable nut zoomOut trong zoom control
 */
- (void)setZoomOutEnabled:(BOOL) enabled;

@end
