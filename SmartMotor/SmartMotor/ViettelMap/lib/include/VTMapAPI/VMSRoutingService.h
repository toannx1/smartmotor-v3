//
//  VMSRoutingService.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/15/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSLatLng.h"
#import "VMSRoutingOptions.h"
#import "VMSServiceAsyncTask.h"
#import "VMSRoutingServiceResult.h"

//==================Protocol delegate cho dịch vụ tìm kiếm đường đi==================//
@protocol VMSRoutingServiceDelegate <NSObject>

@optional
/**
 * Hàm gọi khi bắt đầu thực hiện routing
 * Thường sử dụng để tạo waiting dialog
 */
- (void)routingServiceWillStart;

@required
/**
 * Hàm trả về kết quả tìm đường sau khi RoutingService hoàn thành request server
 */
- (void)routingServiceDidComplete:(VMSRoutingServiceResult *)result;

@end



//===========================Lớp dịch vụ tìm kiếm đường đi==========================//
@interface VMSRoutingService : NSObject <VMSAsyncDelegate> {
    id<VMSRoutingServiceDelegate> mListener;
}

/**
 * Delegate cho dịch vụ
 */
@property (nonatomic, weak) id<VMSRoutingServiceDelegate> delegate;

/**
 * Hàm khởi tạo
 */
- (id)initWithDelegate:(id<VMSRoutingServiceDelegate>)delegate;

/**
 * Tìm kiếm đường đi
 * @param startPoint điểm đầu
 * @param endPoint điểm cuối
 * @param opts Options hỗ trợ cho việc tìm kiếm đường đi. Trường hợp truyền nil tương đương với việc sử dụng opts mặc định
 */
- (void)routingFrom:(VMSLatLng *)startPoint to:(VMSLatLng *)endPoint options:(VMSRoutingOptions *)opts;

/**
 * Ngừng tìm kiếm. Có thể gọi nhiều lần không gây lỗi.
 */
- (void) cancelService;
@end