//
//  VMSGeoServiceResult.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/14/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSServiceStatus.h"

@class VMSGeoItem;
@class VMSLatLng;


@interface VMSGeoServiceResult : NSObject

/**
 * Trang thai cua service xu ly request
 */
@property (nonatomic, assign) VMSServiceStatus status;

/**
 * Tong so ket qua thuc te tim duoc (Co the lon hon tong so ket qua tra ve)
 */
@property (nonatomic, assign) int total;

/**
 * Danh sach cac doi tuong tra ve
 */
@property (nonatomic, retain) NSMutableArray *items;


/**
 * Parse json tra ve tu server thanh VMSGeoServiceResult
 * Truong hop tim kiem dia chi theo diem
 */
+ (VMSGeoServiceResult *)parsePoint:(VMSLatLng *)point address:(NSDictionary *)dict;

/**
 * Parse json tra ve tu server thanh VMSGeoServiceResult
 * Truong hop tim kiem dia chi theo danh sach diem
 */
+ (VMSGeoServiceResult *)parsePointsAddress:(NSDictionary *)dict;

/**
 * Parse json tra ve tu server thanh VMSGeoServiceResult
 * Truong hop tim kiem dia chi theo thuoc tinh
 */
+ (VMSGeoServiceResult *)parseLocations:(NSDictionary *)dict;


/**
 * Ham khoi tao
 */
- (id) init;

/**
 * Them item vao danh sach ket qua tra ve
 */
- (void)addItem:(VMSGeoItem *)item;

/**
 * Lay item theo index tu danh sach ket qua tra ve
 */
- (VMSGeoItem *)getItem:(int)index;

/**
 * Ham tien ich set status theo gia tri int
 */
- (void)setStatusInt:(int)status;

@end