//
//  VMSPairValue.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/15/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

@interface VMSPairValue : NSObject

/**
 * Thuộc tính tên của đối tượng
 */
@property (nonatomic, copy) NSString *name;

/**
 * Thuộc tính giá trị của đối tượng
 */
@property (nonatomic, copy) NSString *value;

/**
 * Hàm khởi tạo pairValue
 */
- (id)initWithName:(NSString *)name value:(NSString *)value;

@end