//
//  VMSCircleOptions.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/15/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VMSLatLng;

@interface VMSCircleOptions : NSObject

/**
 * Index của đối tượng circle trong layer. Giá trị default là 0
 * Ảnh hương đến thứ tự vẽ của đối tượng
 * Đối tượng có index lớn hơn sẽ được vẽ lên trên các đối tượng có index nhỏ hơn.
 */
@property (nonatomic, assign) int zIndex;

/**
 * Tâm của đường tròn. Một trong 2 thuộc tính bắt buộc phải set
 */
@property (nonatomic, copy) VMSLatLng *center;

/**
 * Màu của đường viền circle
 */
@property (nonatomic, strong) UIColor *strokeColor;

/**
 * Màu của vùng fill bên trong đường bao của đường tròn.
 */
@property (nonatomic, strong) UIColor *fillColor;

/**
 * Bán kính của đường tròn. Một trong 2 thuộc tính bắt buộc phải set
 */
@property (nonatomic, assign) double radius;

/**
 * Xác định đối tượng có hiển thị trên mapView hay không
 */
@property (nonatomic, assign) BOOL visible;

/**
 * Độ rộng của đường viền đường tròn
 */
@property (nonatomic, assign) float strokeWidth;

/**
 * Hàm khởi tạo, các giá trị được set giá trị mặc định
 */
-(id) initWithCenter:(VMSLatLng *)center radius:(long)radius;

@end