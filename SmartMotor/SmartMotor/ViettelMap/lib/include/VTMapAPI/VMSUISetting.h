//
//  VMSUISetting.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/13/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMSUISetting : NSObject

/**
 * Quyet dinh thao tac di chuyen map bang tay co duoc hay khong. Mac dinh la co
 * Neu set disable thi se khong the keo, di chuyen map bang tay duoc.
 * Tuy nhien khong anh huong den viec di chuyen map bang code
 */
@property (nonatomic, assign) BOOL scrollGestures;

/**
 * Quyet dinh thao tac zoom map bang tay ( zoom 2 ngon, nhay dup ) co duoc hay khong. Mac dinh la co
 * Neu set disable thi se khong the zoom map bang tay duoc.
 * Tuy nhien khong anh huong den viec zoom map bang code hay su dung zoom control
 */
@property (nonatomic, assign) BOOL zoomGestures;

/**
 * An hien zoom control
 */
@property (nonatomic, assign) BOOL zoomControl;

/**
 * An hien scale control
 */
@property (nonatomic, assign) BOOL scaleControl;


/**
 * An hien map type control
 */
@property (nonatomic, assign) BOOL mapTypeControl;

/**
 * An hien gps control
 */
@property (nonatomic, assign) BOOL gpsControl;

/**
 * Thiet lap enable / disable cho tat ca cac thao tac bang tay doi voi map.
 */
- (void)setAllGesturesEnabled:(BOOL)enabled;

@end
