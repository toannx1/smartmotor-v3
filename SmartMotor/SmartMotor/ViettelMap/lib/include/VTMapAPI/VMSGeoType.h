//
//  VMSGeoType.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/12/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

/**
 * Dinh nghia cac loai geo type khi tim kiem
 */
typedef enum {
    /** Lấy tất cả các loại địa điểm */
    VMSGeoTypeAll = 0,

    /** Trạm xăng */
    VMSGeoTypeGasStation = 1,
    
    /** Các dịch vụ xe ô tô */
    VMSGeoTypeCarService = 2,

    /** Cây ATM */
    VMSGeoTypeAtm = 3,

    /** Ngân hàng */
    VMSGeoTypeBank = 4,

    /** Trung tâm giáo dục */
    VMSGeoTypeEducation = 5,

    /** Chăm sóc sức khoẻ */
    VMSGeoTypeHealthcare = 6,

    /** Các khu giải trí */
    VMSGeoTypeEntertainment = 7,

    /** Các cơ quan nhà nước */
    VMSGeoTypeGoverment = 8,

    /** Dịch vụ công cộng */
    VMSGeoTypeService = 9,

    /** Thư giãn / giải trí */
    VMSGeoTypeRecreation = 10,

    /** Chung cư */
    VMSGeoTypeBuilding = 11,

    /** Địa điểm mua sắm */
    VMSGeoTypeShopping = 12,

    /** Địa điểm du lịch / ngắm cảnh */
    VMSGeoTypeSight = 13,

    /** Giao thông */
    VMSGeoTypeTransportation = 14,
    
    /** Uỷ ban nhân dân */
    VMSGeoTypeCommitee = 15,

    /** Các tuyến đường */
    VMSGeoTypeRoad = 16,

    /** Các loại khác (Chưa được phân loại) */
    VMSGeoTypeOther = 17,

    /** Giá trị lỗi, không phải là đối tượng mà bản đồ Viettel quản lý */
    VMSGeoTypeUnknown = -1,
    
} VMSGeoType;