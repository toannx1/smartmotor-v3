//
//  VMSMapObject.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/15/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@class VMSMapView;
@class VMSProjection;
@class VMSLatLng;
@class VMSLatLngBounds;
@class VMSMapLayer;

@interface VMSMapObject : NSObject


/**
 * Id cua object. Moi object co 1 id khac nhau
 * Moi kieu object se co ky tu dau phan biet
 */
@property (nonatomic, readonly) NSString *objectId;

/**
 * Layer dang quan ly doi tuong object
 * Truong hop mapLayer nil co nghia la object chua duoc quan ly
 */
@property (nonatomic, strong) VMSMapLayer *mapLayer;

/**
 * zIndex cua doi tuong
 * Quan ly thu tu ve, nhan su kien cua doi tuong
 * Moi lop object se co chung 1 gia tri khoi tao 1 zIndex. Nguoi su dung tu set lai de thay doi zIndex
 * Object nao co zIndex cao hon se duoc ve nam ben tren cac object co zIndex thap hon
 */
@property (nonatomic, assign) int zIndex;

/**
 * Vung boundary cua doi tuong.
 * Cac object nhu polygon, polyline deu co boundary bao khit path cua object
 * Cac doi tuong nhu marker, infoWindow thi boundary la 1 diem, coi nhu ko co boundary
 */
@property (nonatomic, readonly) VMSLatLngBounds *boundary;


/**
 * Ham khoi tao cua doi tuong mapObject
 * Su dung cac gia tri mac dinh.
 */
- (id)init;

/**
 * Ham tien ich kiem tra 1 diem (theo gia tri point va latlng) co nam trong trong vung khong gian cua doi tuong khong
 * Cac doi tuong extend tu mapObject phai override lai ham nay de nhan cac su kien nhu singleTab
 */
- (BOOL)containPoint:(CGPoint)point latLng:(VMSLatLng *)latlng;

/**
 * Render doi tuong tren map. 
 * Cac doi tuong extend tu mapObject phai override lai ham nay de hien thi tren man hinh
 */
- (void)render:(VMSMapView *)mapView projection:(VMSProjection *)proj bound:(VMSLatLngBounds *)mapBound;

/**
 * Loại bỏ doi tuong khoi layer
 * Return YES trong truong hop remove duoc doi tuong khoi layer
 * Return NO trong truong hop doi tuong chua duoc add vao map / layer hoac khong remove duoc
 */
- (BOOL)remove;

@end
