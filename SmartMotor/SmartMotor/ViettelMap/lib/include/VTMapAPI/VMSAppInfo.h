//
//  VMSAppInfo.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/12/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VMSAppInfo : NSObject

/**
 * Ham tien ich get appId
 */
+ (NSString *)appId;

/**
 * Ham set / get key cho he thong
 */
+ (void)setAppKey:(NSString *)appKey;
+ (NSString *)appKey;

/** Ham set / get screen width */
+ (void)setScreenWidth:(int)screenWidth;
+ (int)screenWidth;

/** Ham set / get screen Height */
+ (void)setScreenHeight:(int)screenHeight;
+ (int)screenHeight;

/** 
 * Ham tien ich de thay doi dia chi ket noi server viettelmap cua he thong 
 */
+ (BOOL)setServerAddressProtocol:(NSString *)protocol ipAddress:(NSString *)ipAddress port:(int)port;

/**
 * Ham tien ich thay doi thiet lap so luong thread de load anh cua he thong
 */
+ (BOOL)setLoadTileThreadMax:(int)max;

/**
 * Version cua ViettelMap API
 */
+ (NSString *)APIVersion;

@end
