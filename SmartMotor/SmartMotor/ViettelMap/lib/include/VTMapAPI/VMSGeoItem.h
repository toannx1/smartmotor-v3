//
//  VMSGeoItem.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/14/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VMSGeoType.h"

@class VMSLatLng;

@interface VMSGeoItem : NSObject

@property (nonatomic, assign) long itemId;
@property (nonatomic, assign) VMSGeoType geoType;
@property (nonatomic, copy) VMSLatLng *location;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *website;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *fax;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, copy) NSString *tourismInfo;
@property (nonatomic, assign) BOOL hasImage;

/**
 * Parse ket qua json tra ve tu server thanh VMSGeoItem
 */
+ (VMSGeoItem *)parseGeoItem:(NSDictionary *)dict;

/**
 * Ham khoi tao
 */
- (id)init;

/**
 * Ham tien ich set kieu cua geo item bang gia tri int
 */
- (void)setGeoTypeInt:(int)type;

/**
 * Link get anh cua image
 */
- (NSString *)getImageLink:(BOOL)isAvatar;

@end
