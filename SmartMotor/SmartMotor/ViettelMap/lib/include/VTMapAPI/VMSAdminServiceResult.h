//
//  VMSAdminServiceResult.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/14/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSServiceStatus.h"
#import "VMSAdminItem.h"

@interface VMSAdminServiceResult : NSObject

/**
 * Trang thai cua service xu ly request
 */
@property (nonatomic, assign) VMSServiceStatus status;

/**
 * Tong so ket qua thuc te tim duoc (Co the lon hon tong so ket qua tra ve)
 */
@property (nonatomic, assign) int total;

/**
 * Danh sach cac doi tuong tra ve
 */
@property (nonatomic, retain) NSMutableArray *items;



/**
 * Parse Json tra ve tu sever thanh VMSAdminServiceResult
 */
+ (VMSAdminServiceResult *)parseAdminServiceResult:(NSDictionary *) dict;

/**
 * Ham khoi tao voi cac gia tri mac dinh
 */
- (id)init;

/**
 * Them admin item vao danh sach ket qua
 */
- (void)addItem:(VMSAdminItem *)item;

/**
 * Lay admin item theo index tu danh sach ket qua
 */
- (VMSAdminItem *)getItem:(int)index;

@end