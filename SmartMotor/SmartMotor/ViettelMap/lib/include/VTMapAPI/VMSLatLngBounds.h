//
//  VMSLatLngBounds.h
//  VTMapAPI
//

@class VMSLatLng;

@interface VMSLatLngBounds : NSObject

/** Toa do phia dong bac cua boundary */
@property (nonatomic, copy) VMSLatLng *northEast;

/** Toa do phia tay nam cua boundary */
@property (nonatomic, copy) VMSLatLng *southWest;

/** Tam cua vung khong gian */
@property (nonatomic, readonly) VMSLatLng *center;

/**
 * Ham khoi tao doi tuong boundary theo 1 boundary cho truoc
 */
- (id)init:(VMSLatLngBounds *)bound;

/**
 * Ham khoi tao doi tuong boundary theo 2 diem NorthEast va SouthWest
 */
- (id)initWithNE:(VMSLatLng *)northEast SW:(VMSLatLng *) southWest;

/**
 * Ham khoi tao doi tuong boundary theo danh sach cac diem LatLng
 */
- (id)initWithPath:(NSArray *)path;

/**
 * Ham khoi tao doi tuong boundary tu 1 diem latlng cho truoc va khoang cach lat, lng cua boundary
 */
- (id)initWithLat:(double)lat lng:(double)lng latDistance:(double)latDistance lngDistance:(double)lngDistance;

/**
 * Ham tra ve chieu rong cua boundary tinh theo don vi truc toa do
 */
- (double)width;

/**
 * Ham tra ve chieu cao cua boundary tinh theo don vi truc toa do
 */
- (double)height;

/**
 * Ham kiem tra xem boundary da duoc set gia tri hay chua
 */
- (BOOL)isEmpty;

/**
 * Ham kiem tra 1 diem co thuoc boundary hay khong
 * YES: Diem nam trong boundary
 * NO : Diem nam ngoai boundary
 */
- (BOOL)contains:(VMSLatLng *)pt;

/**
 * Ham kiem tra boundary hien tai co giao voi boundary truyen vao hay khong
 * YES: 2 boundary giao nhau
 * NO : 2 boundary khong giao nhau
 */
- (BOOL)intersect:(VMSLatLngBounds *)bound;

/**
 * Ham kiem tra boundary co khong gian bang nhau hay khong
 */
- (BOOL)isEqualToBound:(VMSLatLngBounds *)bound;

/**
 * Mo rong vung boundary hien tai o muc toi thieu de co the chua diem truyen vao
 */
- (VMSLatLngBounds *)extendLatLng:(VMSLatLng *)latLng;

/**
 * Mo rong vung boundary hien tai o muc toi thieu de co the chua mot danh sach cac diem truyen vao
 */
- (VMSLatLngBounds *)extendPath:(NSArray *)path;

/**
 * Mo rong vung boundary hien tai o muc toi thieu de chua ca boundary truyen vao
 */
- (VMSLatLngBounds *)unions:(VMSLatLngBounds *)bound;

/**
 * Ham clone tao ra doi tuong moi co gia tri giong voi ban than doi tuong
 */
- (VMSLatLngBounds *)clone;

/**
 * Ham tra ve string gia tri cua doi tuong
 */
- (NSString *)toUrlValue;

/**
 * Ham tra ve string gia tri cua doi tuong
 */
- (NSString *)toUrlValueEx;

@end