//
//  VMSRoutingType.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/12/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

/**
 * Dinh nghia cac kieu phuong tien cho routing
 */
typedef enum {
    
    /** Phương tiện : Đi bộ ( Hiện dịch vụ BĐS chưa hỗ trợ ) */
    VMSWalking = 0,

    /** Phương tiện : Xe máy */
    VMSMotor = 1,
    
    /** Phương tiện : Ô tô  */
    VMSCar = 2,
    
    /** Phương tiện : Xe tải ( Hiện dịch vụ BĐS chưa hỗ trợ ) */
    VMSTruck = 3,
    
} VMSTravelMode;


/**
 * Dinh nghia cac thuat toan routing
 */
typedef enum {
    
    VMSDijkstra = 0,
    
    VMSAstar = 1,
    
    VMSShootingStar = 2,
    
} VMSAlgoType;