//
//  VMSPolygon.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/15/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSMapObject.h"
#import "VMSPolygonOptions.h"

@interface VMSPolygon : VMSMapObject

/**
 * Danh sách điểm bao quanh của polygon
 * Thuộc tính bắt buộc phải set của polygon
 */
@property (nonatomic, copy) NSMutableArray *path;

/**
 * Màu đường viền bao quanh của polygon
 */
@property (nonatomic, copy) UIColor *strokeColor;

/**
 * Màu vùng không gian bao bên trong của polygon
 */
@property (nonatomic, copy) UIColor *fillColor;

/**
 * Độ rộng đường viền của polygon
 */
@property (nonatomic, assign) float strokeWidth;

/**
 * Thuộc tính xác định ẩn / hiện của đối tượng
 */
@property (nonatomic, assign, getter=isVisible) BOOL visible;

/**
 * Thuộc tính round tại các điểm trong đường của polygon
 * Tuy nhiên hiện tại chưa hỗ trợ được thuộc tính này
 * API sẽ hỗ trợ thuộc tính trong các phiên bản tiếp theo
 */
@property (nonatomic, assign, getter=isRoundCorner) BOOL roundCorner;


/**
 * Thuộc tính hỗ trợ cho phép giảm số lượng điểm của path theo từng mức zoom
 * Mức zoom càng nhỏ (Ra phía ngoài nhìn toàn cảnh thế giới) thì số lượng điểm giảm đi càng nhiều
 * Giúp tăng performance của hệ thống
 * Giá trị mặc định là YES
 */
@property (nonatomic, assign, getter=isEnableReducePath) BOOL enableReducePath;

/**
 * Khởi tạo polygon theo PolygonOptions
 */
- (id)initWithOptions:(VMSPolygonOptions *)options;

/**
 * Hàm tiện ích khởi tạo polygon theo path cho trước
 * Các thuộc tính còn lại sử dụng giá trị mặc định
 */
+ (instancetype)polygonWithPath:(NSArray *)path;

/**
 * Thêm điểm vào danh sách các điểm hiện tại
 */
- (void)addPoint:(VMSLatLng *)point;

/**
 * Thêm một danh sách các điểm vào danh sách điểm hiện tại
 */
- (void)addPoints:(NSArray *)points;

@end