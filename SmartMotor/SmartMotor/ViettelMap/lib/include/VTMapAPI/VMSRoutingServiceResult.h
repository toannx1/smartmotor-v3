//
//  VMSRoutingServiceResult.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/15/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSServiceStatus.h"
#import "VMSRoutingDirection.h"
#import "VMSLatLng.h"


//===========================Lớp thông tin kết quả dịch vụ tìm đường==========================//
@interface VMSRoutingServiceResult : NSObject

/**
 * Trạng thái xử lý request của dịch vụ phía server
 */
@property (nonatomic, assign) VMSServiceStatus status;

/**
 * Độ dài quãng đường (Đường chính)
 */
@property (nonatomic, assign) double length;

/**
 * Thuật toán tìm đường trên server sử dụng (Người dùng không cần quan tâm)
 */
@property (nonatomic, assign) int algo;

/**
 * Danh sách chỉ dẫn dẫn đường
 */
@property (nonatomic, retain) NSMutableArray *directions;

/**
 * Danh sách các điểm mà tuyến đường đi qua (Đường chính)
 */
@property (nonatomic, retain) NSMutableArray *paths;

/**
 * Danh sách các điểm trung gian cần đi qua ngoài 2 điểm đầu cuối
 */
@property (nonatomic, retain) NSMutableArray *waypoints;

/**
 * Kiểm tra kết quả trả về có hỗ trợ tuyến đường phụ không
 */
@property (readonly, getter=isAlternativeSupported) BOOL alternativeSupported;

/**
 * Kiểm tra kết quả trả về có hỗ trợ điểm trung gian hay không
 */
@property (readonly, getter=isWaypointsSupported) BOOL waypointsSupported;

/**
 * Thắng HM: Thời gian đường đi ngắn nhất
 */
@property (nonatomic, assign) double time;

/**
 * Thắng HM: kết quả đường đi phụ
 */
@property (nonatomic, retain) VMSRoutingServiceResult *resultAlternatived;

/**
 * Thắng HM: Đường trung gian (đại diện đường đi qua)
 */
@property (nonatomic, retain) NSString *midWay;


/**
 * Parse json tu sever truyen ve thanh VMSRoutingServiceResult
 */
+ (VMSRoutingServiceResult *)parseRoutingServiceResult:(NSDictionary *)dict;




/**
 * Hàm khởi tạo
 */
- (id)init;

/**
 * Thêm chỉ dẫn vào danh sách dẫn đường.
 */
- (void)addDirection:(VMSRoutingDirection *)direction;

/**
 * Thêm điểm vào đường đi.
 */
- (void)addPathPoint:(VMSLatLng *)point;

/**
 * Thêm điểm trung gian vào đường đi.
 */
- (void)addWayPoint:(VMSLatLng *)point;

@end
