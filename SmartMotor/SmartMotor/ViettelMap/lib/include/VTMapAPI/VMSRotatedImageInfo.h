//
//  VMSRotatedImageInfo.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/22/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 * Lớp ẩn sử dụng nội bộ không public ra ngoài
 */
@interface VMSRotatedImageInfo : NSObject

@property (nonatomic, assign) CGPoint anchor;
@property (nonatomic, assign) CGPoint infoAnchor;
@property (nonatomic, assign) size_t height;
@property (nonatomic, assign) size_t width;
@property (nonatomic, readonly) BOOL didSetInfoAnchor;

- (id)initWithAnchor:(CGPoint)anchor infoAnchor:(CGPoint)infoAnchor width:(size_t)width height:(size_t)height;

@end
