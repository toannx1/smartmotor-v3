//
//  VMSPolygonOptions.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/15/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <UIKit/UIkit.h>

@class VMSLatLng;

@interface VMSPolygonOptions : NSObject

/**
 * Index của đối tượng polygon trong layer. Giá trị default là 0
 * Ảnh hương đến thứ tự vẽ của đối tượng
 * Đối tượng có index lớn hơn sẽ được vẽ lên trên các đối tượng có index nhỏ hơn.
 */
@property (nonatomic, assign) int zIndex;

/**
 * Danh sách điểm bao quanh của polygon
 * Thuộc tính bắt buộc phải set của polygon
 */
@property (nonatomic, copy) NSMutableArray *path;

/**
 * Màu đường viền bao quanh của polygon
 */
@property (nonatomic, strong) UIColor *strokeColor;

/**
 * Màu vùng không gian bao bên trong của polygon
 */
@property (nonatomic, strong) UIColor *fillColor;

/**
 * Độ rộng đường viền của polygon
 */
@property (nonatomic, assign) float strokeWidth;

/**
 * Thuộc tính xác định ẩn / hiện của đối tượng
 */
@property (nonatomic, assign, getter=isVisible) BOOL visible;

/**
 * Thuộc tính round tại các điểm trong đường của polygon
 * Tuy nhiên hiện tại chưa hỗ trợ được thuộc tính này
 * API sẽ hỗ trợ thuộc tính trong các phiên bản tiếp theo
 */
@property (nonatomic, assign, getter=isRoundCorner) BOOL roundCorner;

/**
 * Thuộc tính hỗ trợ cho phép giảm số lượng điểm của path theo từng mức zoom
 * Mức zoom càng nhỏ (Ra phía ngoài nhìn toàn cảnh thế giới) thì số lượng điểm giảm đi càng nhiều
 * Giúp tăng performance của hệ thống
 * Giá trị mặc định là YES
 */
@property (nonatomic, assign, getter=isEnableReducePath) BOOL enableReducePath;

/**
 * Hàm khởi tạo polygon theo danh sách các điểm latlng cho trước
 * Hiện polygon chưa hỗ trợ multi polygon
 * API sẽ cố gắng hỗ trợ multi polygon trong các phiên bản tiếp theo
 */
- (id)initWithPath:(NSArray *)path;

/**
 * Thêm điểm vào danh sách các điểm hiện tại
 */
- (void)addPoint:(VMSLatLng *)pt;

/**
 * Thêm một danh sách các điểm vào danh sách điểm hiện tại
 */
- (void)addPoints:(NSArray *)pts;

@end