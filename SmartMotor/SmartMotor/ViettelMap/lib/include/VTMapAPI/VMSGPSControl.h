//
//  VMSGPSControl.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/18/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSBaseControl.h"
#import <CoreLocation/CoreLocation.h>
#import "VMSLatLng.h"
#import "VMSMapLayer.h"
#import "VMSMapObject.h"
#import "VMSCircle.h"
#import "VMSCircleOptions.h"
#import "VMSMarker.h"
#import "VMSMarkerOptions.h"
#import "VMSVectorLayer.h"
#import "VMSMarkerLayer.h"

typedef enum {
    VMSMapStatusGPSNomal,
    VMSMapStatusGPSWaiting,// Doi de xac dinh co lay duoc GPS khong, neu khong lay duoc se delegate ve app
    VMSMapStatusGPSGetting, // 
} VMSMapStatusGPS;

/**
 * Delegate lắng nghe các sự kiện của GPSControl
 */
@protocol VMSGPSDelegate <NSObject>
@required
- (void)locationUpdate:(CLLocation *)location;
- (void)locationError:(NSError *)error;
@end


/**
 * Control quản lý GPS
 */
@interface VMSGPSControl : VMSBaseControl

/**
 * Delegate cho các sự kiện update / error của GPS
 */
@property (nonatomic, weak) id<VMSGPSDelegate> delegate;

/**
 * Hàm khởi tạo GPS control theo map có sử dụng delegate
 */
- (id)initWithMap:(VMSMapView *)map delegate:(id<VMSGPSDelegate>) delegate;

/**
 * Hàm tiện ích request map di chuyển đến điểm GPS hiện tại của người sử dụng
 * Trong trường hợp chương trình không xác định được GPS của người sử dụng thì không thể di chuyển
 */
- (IBAction)goToMyLocation:(id)sender;

/**
 * Hàm request GPSControl update lại icon hiển thị điểm GPS hiện tại
 */
- (void)updateGPSLocationObject;

/**
 * Hàm request GPSControl update lại vị trí GPS hiện tại
 */
- (void)updateMyLocation;

/**
 * Ngừng update lại vị trí GPS hiện tại
 */
- (void)stopUpdateLocation;

/**
 * Lấy toạ độ GPS hiện tại của người sử dụng
 */
- (VMSLatLng *)getMyLocation;

/**
 * Bật GPS control
 */
- (void)enableGPS;

/**
 * Tắt GPS control
 */
- (void)disableGPS;

/**
 *  Set thong so quy dinh sai so khi su dung GPS
 *      kCLLocationAccuracyBest;
 *      kCLLocationAccuracyNearestTenMeters;
 *      kCLLocationAccuracyHundredMeters;
 *      kCLLocationAccuracyKilometer;
 *      kCLLocationAccuracyThreeKilometers;
 *  Tuy vao yeu cau bai toan de lua chon sai so.
 *  No co the gay ton pin thiet bi, xu ly cham khi su dung khong dung muc dich
 *  Hien tai API dang de gia tri default kCLLocationAccuracyBest
 */
- (void)setDesiredAccturacy: (CLLocationAccuracy) desiredAccuracy;

/**
 *  Day la thong so quy dinh khoang cach toi thieu (tinh theo met) update vi tri
 *  No co the gay ton pin thiet bi, xu ly cham khi su dung khong dung muc dich
 *  Thong thuong se su dung kCLDistanceFilterNone, tuc la luon update vi tri khi chuyen dong
 *  Hien tai API dang de gia tri default la 5 (m)
 */
- (void)setDistanceFilter: (CLLocationDistance) distanceFilter;

/**
 * ThangHM: Khi add control ma khong muon hien thi len ban do thi set hidden = YES
 * hien tai o VMSUISetting co set roi nhung khi add control GPS vao thi van se hien thi
 */
- (void)setHiddenControl:(BOOL)isHidden;

/**
 * ThangHM: co hien thi vong tron quanh vi tri hien tai khong?
 * default la khong hien thi
 */
- (void)setVisibleCircle: (BOOL)isVisible;

/**
 * ThangHM: lan dau tien vao co di chuyen den vi tri hien tai khong?
 * default dang move den vi tri hien tai
 */
- (void)setMoveToMyLocation: (BOOL)isMove;

@end
