//
//  VMSMapLayer.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/15/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@class VMSLatLng;
@class VMSLatLngBounds;
@class VMSMapView;
@class VMSMapObject;
@class VMSProjection;
@class VMSInfoWindow;
@class VMSPairValue;

FOUNDATION_EXPORT int const ZINDEX_LAYER_VECTOR;
FOUNDATION_EXPORT int const ZINDEX_LAYER_MARKER;
FOUNDATION_EXPORT int const ZINDEX_LAYER_INFOWINDOW;
FOUNDATION_EXPORT int const ZINDEX_LAYER_CUSTOM;

/**
 * Delegate lắng nghe sự kiện single tab lên đối tượng map object
 */
@protocol VMSMapObjectSingleTabDelegate <NSObject>
- (BOOL)singleTabMapObject:(VMSMapObject *)mapObject point:(CGPoint)pt latLng:(VMSLatLng *)latLng;
- (BOOL)singleTabMapObject1:(VMSMapObject *)mapObject point:(CGPoint)pt latLng:(VMSLatLng *)latLng viewController:(UIViewController *)viewController mapView:(VMSMapView *)mapView;
@end

/**
 * Delegate lắng nghe sự kiện long press lên đối tượng map object
 */
@protocol VMSMapObjectLongPressDelegate <NSObject>
- (BOOL) longPressMapObject:(VMSMapObject *)mapObject point:(CGPoint)pt latLng:(VMSLatLng *)latLng;
@end

/**
 * Delegate lắng nghe sự kiện double tab lên đối tượng map object
 */
@protocol VMSMapObjectDoubleTabDelegate <NSObject>
- (BOOL) doubleTabMapObject:(VMSMapObject *)mapObject point:(CGPoint)pt latLng:(VMSLatLng *)latLng;
@end

/**
 * Delegate lắng nghe sự kiện drag đối tượng map object
 */
@protocol VMSMapObjectDragDelegate <NSObject>
- (BOOL) startDragMapObject:(VMSMapObject *)mapObject point:(CGPoint)pt latLng:(VMSLatLng *)latLng;
- (BOOL) endDragMapObject:(VMSMapObject *)mapObject point:(CGPoint)pt latLng:(VMSLatLng *)latLng;
@end

/**
 * Delegate lắng nghe sự kiện tab vào hyperlink trên info window
 */
@protocol VMSHyperlinkDelegate <NSObject>
- (BOOL)didTab:(VMSInfoWindow *)infoWindow onLink:(VMSPairValue *)hyperlink;
@end


//===========================Lớp quản lý các đối tượng Map Object===========================//
@interface VMSMapLayer : NSObject

/**
 * Id riêng của lớp layer.
 * Mỗi lớp layer có một ký hiệu id riêng
 */
@property (nonatomic, readonly) NSString *layerId;

/**
 * Index của lớp đối tượng. Ảnh hưởng đến thứ tự vẽ của bản đồ
 * Trong trường hợp không set, sử dụng giá trị mặc định của từng lớp. 
 * Thứ tự vẽ sẽ theo thứ tự add layer vào map
 */
@property (nonatomic, assign) int zIndex;

/**
 * Map quản lý lớp đối tượng.
 */
@property (nonatomic, weak) VMSMapView* mapView;

/**
 * Các delegate cho lớp đối tượng
 */
@property (nonatomic, weak) id<VMSMapObjectSingleTabDelegate> singleTabDelegate;
@property (nonatomic, weak) id<VMSMapObjectDoubleTabDelegate> doubleTabDelegate;
@property (nonatomic, weak) id<VMSMapObjectLongPressDelegate> longPressDelegate;
@property (nonatomic, weak) id<VMSMapObjectDragDelegate> dragDelegate;

/**
 * Mức zoom nhỏ nhất các object trong layer còn hiển thị
 * Các mức zoom nhỏ hơn mức zoom này thì object không được hiển thị nữa.
 */
@property (nonatomic, assign) int zoomMin;

/**
 * Mức zoom lớn nhất các object trong layer còn hiển thị
 * Các mức zoom lớn hơn mức zoom này thì object không được hiển thị nữa.
 */
@property (nonatomic, assign) int zoomMax;

/**
 * Boundary của object. 
 * Tồn tại chủ yếu với các đối tượng mapobject dạng không gian.
 * Các đối tượng dạng điểm thường không có boundary
 */
@property (nonatomic, readonly) VMSLatLngBounds *boudnary;

/**
 * Tổng số mapObject mà layer đang quản lý
 */
@property (nonatomic, readonly) int totalMapObject;

/**
 * Hàm khởi tạo lớp đối tượng.
 */
- (id)init;

/**
 * Hàm thêm đối tượng vào lớp đối tượng để quản lý
 */
- (void)add:(VMSMapObject *)obj;

/**
 * Hàm xoá đối tượng khỏi lớp
 */
- (BOOL)removeObject:(VMSMapObject *)obj;

/**
 * Xoá các đối tượng có cùng index với giá trị index truyền vào
 */
- (BOOL)removeZIndex:(int)index;

/**
 * Kiểm tra object có thuộc layer hay không
 */
- (BOOL)containsMapObject:(VMSMapObject *)object;

/**
 * Hàm lấy tất cả các đối tượng có cùng index với giá trị index truyền vào
 */
- (NSArray *)mapObjectsForIndex:(int)zIndex;

/**
 * Hàm lấy đối tượng có cùng giá trị objectId truyền vào
 */
- (VMSMapObject *)mapObjectForId:(NSString *)objectId;

/**
 * Remove toàn bộ các đối tượng đang có trong lớp
 */
- (void) clear;

@end
