//
//  VMSImageUtils.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/12/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VMSImageUtils : NSObject

/**
 * Ham lay anh default cho marker
 */
+ (UIImage *)defaultMarker;

/**
 * Ham lay anh theo ten
 */
+ (UIImage *)imageWithName:(NSString *)imageName;

/**
 * Ham lay anh background cua map
 */
+ (const UIImage *)noMapBackgroundImage;

/**
 * Ham tien ich ve anh marker default len map
 * Luu tru tap trung buffer ve default marker tai 1 cho
 */
+ (void)drawDefaultMarkerInRect:(CGRect)drawRect depth:(double)depth;

/**
 * Ham free / release image default.
 * De goi trong dealloc cua map
 */
+ (void)freeDefaultImage;

@end
