//
//  VMSPolylineOptions.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/15/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <UIKit/UIkit.h>

@class VMSLatLng;

@interface VMSPolylineOptions : NSObject

/**
 * Index của đối tượng polyline trong layer. Giá trị default là 0
 * Ảnh hương đến thứ tự vẽ của đối tượng
 * Đối tượng có index lớn hơn sẽ được vẽ lên trên các đối tượng có index nhỏ hơn.
 */
@property (nonatomic, assign) int zIndex;

/**
 * Danh sách các điểm latlng mà polyline đi qua
 */
@property (nonatomic, copy) NSArray* path;

/**
 * Màu sắc của polyline
 */
@property (nonatomic, strong) UIColor *strokeColor;

/**
 * Độ rộng của polyline tính theo pixel
 */
@property (nonatomic, assign) float strokeWidth;

/**
 * Thuộc tính xác định ẩn / hiện của đối tượng
 */
@property (nonatomic, assign, getter=isVisible) BOOL visible;

/**
 * Thuộc tính round tại các điểm trong đường polyline
 * Tuy nhiên hiện tại chưa hỗ trợ được thuộc tính này
 * API sẽ hỗ trợ thuộc tính trong các phiên bản tiếp theo
 */
@property (nonatomic, assign, getter=isRoundCorner) BOOL roundCorner;

/**
 * Hàm khởi tạo đối tượng theo danh sách các điểm VMSLatLng
 */
- (id)initWithPath:(NSArray *)path;

/**
 * Thêm điểm vào danh sách các điểm hiện tại
 */
- (void)addPoint:(VMSLatLng *)point;

/**
 * Thêm một danh sách các điểm vào danh sách điểm hiện tại
 */
- (void)addPoints:(NSArray *)points;

@end

