//
//  VMSMapTypeControl.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/13/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSBaseControl.h"
#import "VMSMapView.h"

@interface VMSMapTypeControl : VMSBaseControl

/**
 * Set map type cho control va map
 */
- (void)setMapType:(VMSMapType)mapType;

@end
