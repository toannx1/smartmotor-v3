//
//  VMSAdminType.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/12/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

/**
 * Dinh nghia cac muc level cua doi tuong hanh chinh
 */
typedef enum {
    /** Giá trị sai */
    VMSAdminLevelError    = -1,
    
    /** Mức level Tỉnh / Thành phố */
    VMSAdminLevelProvince =  1,
    
    /** Mức level Quận / Huyện */
    VMSAdminLevelDistrict =  2,
    
    /** Mức level Xã / Phường */
    VMSAdminLevelCommune  =  3,
    
} VMSAdminLevelType;

/**
 * Dinh nghia cac thuoc tinh tra ve cua admin item
 */
typedef enum  {
    /** Code của vùng hành chính */
    VMSAdminReturnCode       = 0x01,
    
    /** Tên vùng hành chính */
    VMSAdminReturnName       = 0x02,
    
    /** Danh sách điểm bao quanh của vùng hành chính */
    VMSAdminReturnPath       = 0x04,
    
    /** Màu sử dụng để vẽ vùng hành chính trên bản đồ vùng hành chính */
    VMSAdminReturnIndexColor = 0x08,
    
    /** Id của vùng hành chính */
    VMSAdminReturnObjId      = 0x10,
    
    /** VMSLatLngBound của toàn vùng hành chính */
    VMSAdminReturnBound      = 0x20,
    
    /** Lấy tất cả thông tin của vùng hành chính */
    VMSAdminReturnAll        = 0xFF,
    
} VMSAdminReturnType;

