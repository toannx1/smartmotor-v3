//
//  VMSServiceStatus.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/14/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

/**
 * Dinh nghia cac kieu status do service VTMapService tra ve
 */
typedef enum {
    /** Khong phai la status cua VTMapService, tra ve khi xu dung ham convert voi gia tri khong dung */
    VMSNotServiceStatus = -1,
    
    /** Xu ly thanh cong, khong co loi xay ra */
    VMSStatusOK = 0,
    
    /** Invalid request, thieu hoac truyen sai kieu parameter */
    VMSStatusInvalid = 1,
    
    /** Thieu quyen thuc hien request, kiem tra app name va app key */
    VMSStatusDenied = 2,
    
    /** Loi khong ro nguyen nhan, request co the thanh cong trong lan thu lai sau do */
    VMSStatusUnknowError = 3,
    
    /** Khong co ket qua tra ve, tim kiem duoc 0 record */
    VMSStatusZeroResult = 4,
    
    /** Co loi xay ra phia server khi xu ly request */
    VMSStatusError = 5,
    
    /** Loi danh rieng cho routing, truyen qua nhieu diem waypoint cho xu ly routing */
    VMSStatusExceedMaxPoints = 6,

} VMSServiceStatus;

/**
 * Tien ich cho viec convert, get message tu status
 */
@interface VMSServiceStatusUtils : NSObject

/**
 * Convert integer value to VMSServiceStatus
 */
+ (VMSServiceStatus)toServiceStatus:(int)status;

/**
 * Lấy status message theo gia tri cua status
 */
+ (NSString *)getStatusMessage:(VMSServiceStatus)status;

/**
 * Get status message theo gia tri cua status (Ho tro gia tri kieu int)
 */
+ (NSString *)getStatusMessageByIntValue:(int)status;

@end
