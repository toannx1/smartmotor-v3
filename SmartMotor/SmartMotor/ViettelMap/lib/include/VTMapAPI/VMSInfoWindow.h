//
//  VMSInfoWindow.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/15/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMSInfoWindowOptions.h"
#import "VMSMapObject.h"

@interface VMSInfoWindow : VMSMapObject

/**
 * Độ rộng tối đa của infoWindow cho phép
 * Trong trường hợp không set thì sẽ lấy giá trị mặc định làm giá trị tối đa
 */
@property (nonatomic, assign) int maxWidth;

/**
 * Toạ độ của infoWindow. Trong trường hợp không set anchor, infoWindow sẽ hiển thị tại đúng điểm này
 */
@property (nonatomic, copy) VMSLatLng *position;

/**
 * Anchor của infowindow so với position
 * Trong trường hợp không set, sẽ sử dụng anchor mặc định, infowindow sẽ hiển thị tại đúng position
 */
@property (nonatomic, assign) CGPoint anchor;

/**
 * Ảnh đại diện hiển thị ở bên góc trái của infowindow.
 * Trong trường hợp ảnh quá to sẽ được thu nhỏ lại cho vừa phải với khung cửa sổ
 */
@property (nonatomic, copy) UIImage *image;

/**
 * Title của infoWindow . Được hiển thị ở trên cùng của cửa sổ
 * Đây là một trong 2 giá trị bắt buộc của đối tượng
 */
@property (nonatomic, copy) NSString *title;

/**
 * Cỡ chữ của title khi hiển thị
 */
@property (nonatomic, assign) int titleTextSize;

/**
 * Màu sắc của title khi hiển thị.
 * Màu mặc định là màu đen
 */
@property (nonatomic, copy) UIColor *titleTextColor;

/**
 * Phần nội dung detail bên dưới title của infoWindow
 * Bao gồm 1 danh sách với mỗi phần tử có thể bao gồm 2 phần tên và giá trị
 * Nếu chỉ có 1 trong 2 phần tử thì hiển thị thành 1 dòng bình thường
 * Trường hợp có cả 2 thì hiển thị theo format "Name: Value" cách nhau bởi dấu 2 chấm
 */
@property (nonatomic, copy) NSArray* snippets;

/**
 * Cỡ chữ của phần snippet
 */
@property (nonatomic, assign) int snippetTextSize;

/**
 * Màu sắc cho phần tên trong snippet.
 * Màu sắc của tên và value có thể khác nhau để dễ phân biệt
 * Màu sắc mặc định là màu đen
 */
@property (nonatomic, copy) UIColor *nameTextColor;

/**
 * Màu sắc cho phần giá trị trong snippet.
 * Màu sắc của tên và giá trị có thể khác nhau để dễ phân biệt
 * Màu sắc mặc định là màu đen
 */
@property (nonatomic, copy) UIColor *valueTextColor;

/**
 * Danh sách các hyperlinks trong cửa sổ thông tin
 * Hyperlink là danh sách các VMSPairValue.
 * Khi hiển thị sẽ chỉ hiện thị phần value giá trị
 */
@property (nonatomic, copy) NSArray *hyperlinks;

/**
 * Hàm khởi tạo sử dụng InfoWindow Options
 */
- (id)initWithOptions:(VMSInfoWindowOptions *) opts;

/**
 * Hàm khởi tạo sử dụng custom view.
 * Hiển thị infowindow theo view người dùng set vào
 */
- (id)initWithCustomView:(UIView *) customView center:(VMSLatLng *)center;

/**
 * Trả về vùng không gian mà infowindow chiếm
 */
- (CGRect)getRect;

/**
 * Thêm snippet vào danh sách có sẵn sử dụng tên và value
 */
- (void)addSnippetWithName:(NSString *)name value:(NSString *)value;

/**
 * Thêm snippet vào danh sách có sẵn sử dụng đối tượng VMSPairValue
 */
- (void)addSnippet:(VMSPairValue *)snippet;

/**
 * Thêm 1 danh sách các VMSPairValue vào danh sách snippet có sẵn
 */
- (void)addSnippets:(NSArray *)snippets;

/**
 * Thêm hyperlink theo tên(ẩn) và value(hiển thị) vào danh sách hyperlink có sẵn
 */
- (void)addHyperlinkWithName:(NSString *)name value:(NSString *)value;

/**
 * Thêm hyperlink theo VMSPairValue vào danh sách hyperlink có sẵn
 */
- (void)addHyperlink:(VMSPairValue *)hyperlink;

/**
 * Thêm 1 danh sách các VMSPairValue vào danh sách hyperlink có sẵn
 */
- (void)addHyperlinks:(NSArray *)hyperlinks;

@end
