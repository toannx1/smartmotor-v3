//
//  VMSGeoService.h
//  VTMapAPI
//
//  Created by Nguyen Van Tho on 11/14/13.
//  Copyright (c) 2013 Viettel. All rights reserved.
//

#import "VMSLatLng.h"
#import "VMSLatLngBounds.h"
#import "VMSServiceAsyncTask.h"
#import "VMSGeoType.h"

@class VMSGeoServiceResult;

/**
 * Geo type : Phân loại kiểu tìm kiếm của Geo service. Tên tương ứng với tên hàm của geoservice
 * Trả về thông qua delegate, cho phép người dùng phân biệt kiểu tìm kiếm trước đó.
 */
typedef enum {
    VMSGetPointAddress,
    VMSGetPointsAddress,
    VMSGetLocationWithinCircle,
    VMSGetLocationWithinBound,
    VMSGetLocationWithinPolygon,
    VMSGetLocationFromText,
    VMSGetLocationFromTextAndLocation,
} VMSGeoServiceType;



//==================Protocol delegate cho dịch vụ tìm kiếm địa điểm==================//
@protocol VMSGeoServiceDelegate <NSObject>

@optional
/**
 * Hàm gọi khi bắt đầu thực hiện gọi geoService
 * Thường sử dụng để tạo waiting dialog
 */
- (void)geoServiceWillStart:(VMSGeoServiceType)type;

@required
/**
 * Hàm trả về kết quả tìm đường sau khi GeoService hoàn thành request server
 */
- (void)geoServiceDidComplete:(VMSGeoServiceResult *)result ofType:(VMSGeoServiceType)type;

@end



//===========================Lớp dịch vụ tìm kiếm địa điểm===========================//
@interface VMSGeoService : NSObject <VMSAsyncDelegate>

/**
 * Delegate của service, sử dụng để trả kết quả về khi kết thúc request server
 */
@property (nonatomic, weak) id<VMSGeoServiceDelegate> delegate;

/**
 * Offset, xác định lấy từ record thứ bao nhiêu trong danh sách kết quả tìm được trên server
 * Thường được sử dụng trong xử lý phân trang
 */
@property (nonatomic, assign) int offset;

/**
 * Limit, giới hạn số lượng record trả về trong một lần request server.
 */
@property (nonatomic, assign) int limit;


/**
 * Ham khoi tao
 */
- (id)initWithDelegate:(id<VMSGeoServiceDelegate>)delegate;

/**
 * Tìm kiếm địa chỉ theo điểm
 */
- (void)getPointAddress:(VMSLatLng *)point;

/**
 * Tìm kiếm list địa chỉ theo 1 list điểm
 */
- (void)getPointsAddress:(NSArray *)points;

/**
 * Tìm kiếm địa chỉ nằm trong vùng tròn và kiểu đối tượng
 * @param center tâm vùng tròn tìm kiếm
 * @param text nội dung tìm kiếm
 * @param radius bán kính vùng tròn tìm kiếm
 * @param type danh sách kiểu đối tượng tìm kiếm
 * @param totalType số lượng type trong danh sách Type trên
 */
- (void)getLocationWithinCircleCenter:(VMSLatLng *)pt radius:(int)radius text:(NSString *)text ofType:(VMSGeoType[])arrType totalType:(int)total;

/**
 * Tìm kiếm điểm nằm trong vùng boundary và kiểu đối tượng
 * @param boundary không gian vùng tìm kiếm
 * @param text nội dung tìm kiếm
 * @param type danh sách kiểu đối tượng tìm kiếm
 * @param totalType số lượng type trong danh sách Type trên
 */
- (void)getLocationWithinBound:(VMSLatLngBounds *)boundary text:(NSString *)text ofType:(VMSGeoType[])arrType totalType:(int)total;

/**
 * ThangHM: Tìm kiếm điểm nằm trong vùng poligon và kiểu đối tượng
 * @param arrayPoint Các điểm của polygon
 * @param text nội dung tìm kiếm
 * @param type danh sách kiểu đối tượng tìm kiếm
 * @param totalType số lượng type trong danh sách Type trên
 */
- (void)getLocationWithinPoligonPoints:(NSArray *)arrayPoint text:(NSString *)text ofType:(VMSGeoType[])arrType totalType:(int)total ;

/**
 * Tìm kiếm điểm theo tên
 */
- (void)getLocationFromText:(NSString *)text;

/**
 * ThangHM add: Tìm kiếm điểm theo tên, ket qua tra ve uu tien gan voi latlng truyen vao
 */
- (void)getLocationFromText:(NSString *)text location:(VMSLatLng*)latlng;

/**
 * Ngừng request. Không ảnh hưởng khi gọi nhiều lần
 */
- (void)cancelService;
@end