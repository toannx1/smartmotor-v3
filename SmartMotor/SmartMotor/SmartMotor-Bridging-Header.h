//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#import "VMSMapView.h"
#import "VMSMapLayer.h"
#import "VMSInfoWindow.h"
#import "VMSInfoWindowOptions.h"
#import "VMSLatLng.h"
#import "VMSMarker.h"
#import "VMSMarkerLayer.h"
#import "VMSMarkerOptions.h"
#import "VMSPolyline.h"
#import "VMSPolylineOptions.h"
#import "VMSPolylineOptions.h"
#import "VMSGeoService.h"
#import "VMSGeoServiceResult.h"
#import "VMSGeoItem.h"
#import "VMSRoutingRenderer.h"
#import "VMSRoutingService.h"
#import "VMSRoutingOptions.h"
#import "VMSRoutingDirection.h"
#import "VMSUISetting.h"
#import "VMSLatLngBounds.h"
#import "VMSMapObject.h"



/*
 var vtMap: VMSMapView!
 let APP_MAP_KEY = "45b22516ef28a92764698a111f0245b9"
 
 override func viewDidLoad() {
 super.viewDidLoad()
 
 let mapCGRect = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
 vtMap = VMSMapView.init(key: APP_MAP_KEY, frame: mapCGRect)
 vtMap.initMap()
 view.addSubview(vtMap)
 
 }


 
*/


