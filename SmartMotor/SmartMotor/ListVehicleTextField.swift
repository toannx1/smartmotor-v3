//
//  ListVehicleTextField.swift
//  SmartMotor
//
//  Created by vietnv2 on 12/5/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

extension ListVehicleVC: UITextFieldDelegate {
    func initTextField() {
//        imgIconSearchOnTitle.image = UIImage(named: "icon search.png")
        let paddingForUser = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        paddingForUser.image = UIImage(named: "")
        txtSearchVehicle.leftView = paddingForUser
        txtSearchVehicle.leftViewMode = UITextFieldViewMode.always
        txtSearchVehicle.returnKeyType = UIReturnKeyType.done
        txtSearchVehicle.delegate = self
        //
        txtChargeCode.keyboardType = UIKeyboardType.numberPad
        txtCaptchaCardCode.keyboardType = UIKeyboardType.default
        txtChargeCode.delegate = self
        txtCaptchaCardCode.delegate = self
        txtCaptchaPayMoney.delegate = self
        _ = textFieldShouldReturn(txtSearchVehicle)
        _ = textFieldShouldReturn(txtChargeCode)
        _ = textFieldShouldReturn(txtCaptchaCardCode)
        _ = textFieldShouldReturn(txtCaptchaPayMoney)
        chargeCardCodeViewContraint.constant = (heightScreen - chargeCardCodeView.frame.size.height)/2
        payMoneyViewContraint.constant = (heightScreen - payMoneyView.frame.size.height)/2
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool { // called when 'return' key pressed.
        //txtSearchVehicle.becomeFirstResponder()
        if searchVehicleFlag == true {
            if txtSearchVehicle.text != "" && tapReturnToSearchFlag == false {
                tapReturnToSearchFlag = true
            }
            registerNo2Query = txtSearchVehicle.text
            txtSearchVehicleTextField = txtSearchVehicle.text
            pageIndex = 1
            endOfListFlag = false
            numberOfVehcle = 0
            vehicleInformations.removeAll()
            registerNo2Query = registerNo2Query.replacingOccurrences(of: "\\s+", with: "", options: .regularExpression, range: nil)
            loadListVehicle()
        }
        if textField == txtChargeCode && txtChargeCode.text == "" {
            Alert.alertAutoDismiss(delegate: self, message: "Xin vui lòng nhập mã thẻ cào!")
            return true
        }else if textField == txtCaptchaCardCode && txtCaptchaCardCode.text == "" {
            Alert.alertAutoDismiss(delegate: self, message: "Xin vui lòng nhập mã captcha!")
            return true
        }
        if textField == txtCaptchaPayMoney && txtCaptchaPayMoney.text == "" {
            Alert.alertAutoDismiss(delegate: self, message: "Xin vui lòng nhập mã captcha!")
            return true
        }
        hideKeyBoad()
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        var distance:CGFloat!
        switch textField {
        case txtSearchVehicle:
            searchVehicleFlag = true
        case txtChargeCode,txtCaptchaCardCode:
            distance = (heightScreen - chargeCardCodeView.frame.size.height - keyboardHeight)/2
            if distance >= 20 {
                chargeCardCodeViewContraint.constant = distance
            }else {
                chargeCardCodeViewContraint.constant = 20
            }
            chargeCardCodeViewSizeContraint.constant = heightScreen - keyboardHeight - 20
        case txtCaptchaPayMoney:
            distance = (heightScreen - payMoneyView.frame.size.height - keyboardHeight)/2
            if distance >= 20 {
                payMoneyViewContraint.constant = distance
            }else {
                payMoneyViewContraint.constant = 20
            }
            payMoneyViewSizeContraint.constant = heightScreen - keyboardHeight - 20
        default:
            break
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case txtSearchVehicle:
            break
        case txtChargeCode:
            chargeCardCodeViewContraint.constant = (heightScreen - chargeCardCodeViewHeight)/2
            chargeCardCodeViewSizeContraint.constant = chargeCardCodeViewHeight
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        case txtCaptchaCardCode:
            chargeCardCodeViewContraint.constant = (heightScreen - chargeCardCodeViewHeight)/2
            chargeCardCodeViewSizeContraint.constant = chargeCardCodeViewHeight
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        case txtCaptchaPayMoney:
            payMoneyViewContraint.constant = (heightScreen - payMoneyViewHeight)/2
            payMoneyViewSizeContraint.constant = payMoneyViewHeight
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        default:
            break
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if tapReturnToSearchFlag == true {
            hideKeyBoad()
            tapReturnToSearchFlag = false
            searchVehicleFlag = false
            registerNo2Query = ""
            txtSearchVehicle.text = ""
            txtSearchVehicleTextField = ""
            vehicleInformations.removeAll()
            numberOfVehcle = 0
            pageIndex = 1
            endOfListFlag = false
            listVehicleTableView.reloadData()
            loadListVehicle()
        }
        return true
    }
    
}



