//
//  LoginVC.swift
//  Smart Motor
//
//  Created by vietnv2 on 17/10/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit
import CoreData
import CryptoSwift
import GoogleMaps
import MapboxGeocoder
class LoginVC: UIViewController, URLSessionDownloadDelegate {
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var containerButtonLoginView: UIView!
    @IBOutlet weak var containerCaptchaView: UIView!
    @IBOutlet weak var backgroundEmptyAlertView: UIView!
    @IBOutlet weak var emptyAlertView: UIView!
    @IBOutlet weak var lblEmptyAlert: UILabel!
    @IBOutlet weak var hotlineAlertView: UIView!
    @IBOutlet weak var imgViewLogo: UIImageView!
    @IBOutlet weak var txtUser: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtCaptcha: UITextField!
    @IBOutlet weak var loginViewContraint: NSLayoutConstraint!
    @IBOutlet weak var containerButtonLoginViewContraint: NSLayoutConstraint!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnSecurePassword: UIButton!
    @IBOutlet weak var btnDontRememberPassword: UIButton!
    @IBOutlet weak var btnAccountInfo: UIButton!
    @IBOutlet weak var btnCallToCustomCareCenter: UIButton!
    @IBOutlet weak var btnClearTxtUser: UIButton!
    @IBOutlet weak var imgCaptcha: UIImageView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var btnRegister: UIButton!
    
    var checkBoxSavePasswordFlag:Bool! = false
    var checkBoxAutoLoginFlag:Bool! = false
    var loginSuccessedFlag:Bool!
    var countLoginFail:Int! = 0
    var stringToCountTxtLoginFail:String!
    var loginWithCaptchaFlag:Bool! = false
    var cookieName:String!
    var cookieValue:String!
    let userOrPasswordInvalid:String! = "Tên đăng nhập hoặc Mật khẩu không hợp lệ!"
    let checkInternetConnection:String! = "Kiểm tra lại kết nối internet!"
    let cannotConnect2Server:String! = "Lỗi kết nối. Vui lòng thử lại sau!"
    let loginUnsuccess:String! = "Đăng nhập không thành công!"
    let loginSuccess:String! = "SUCCESS"
    var session = URLSession()

    let encrypKeyForLogin:String! = "PHANNANGDUYBKAHN"
    enum loginInformationString:String {
        case groupRoot = "groupRoot"
        case groupType = "groupType"
        case lat = "lat"
        case lng = "lng"
        case loginAttemp = "loginAttemp"
        case message = "message"
        case resultCode = "resultCode"
        case roleId = "roleId"
        case token = "token"
        case userId = "userId"
    }
    
    enum sqliteItemString:String {
        case person = "Person"
        case token = "token"
        case userId = "userId"
        case groupRoot = "groupRoot"
    }
    
    enum loginInvalidString:String {
        case captchaEmpty = "Xin vui lòng nhập Captcha!"
        case userEmpty = "Vui lòng nhập Tên đăng nhập!"
        case passwordEmpty = "Vui lòng nhập Mật khẩu!"
    }
    
    var chooseServer:Int!
    var textFieldDidBeginEditingFlag:Bool! = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chooseServer = 1
        switch chooseServer {
        case 1:
            smartmotorUrl = smartmotorUrl1
            loginUrl = loginUrl1
            break
        case 2:
            smartmotorUrl = smartmotorUrl2
            loginUrl = loginUrl2
            break
        case 3:
            smartmotorUrl = smartmotorUrl3
            loginUrl = loginUrl3
            break
        default:
            break
        }
        initLoginVC()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getPersonInfoFromSqlite()
    }
    
    func initLoginVC() {
        getUUID()
        checkLogout2ClearInfo()
        initView()
        getKeyBoardHeight()
        initTextField()
        
        initURLSession()
        initTapInBackgroundView()
    }
    
    func initView() {
        loginView.addBackground(width: view.frame.size.width,height: view.frame.size.height,image: #imageLiteral(resourceName: "bg_login.png"))
        imgViewLogo.contentMode = UIViewContentMode.scaleAspectFit
        imgCaptcha.isUserInteractionEnabled = true //image has been tapped
        heightScreen = view.frame.size.height
        widthScreen = view.frame.size.width
        hideCaptchaView()
        backgroundEmptyAlertView.isHidden = true
        emptyAlertView.isHidden = true
        hotlineAlertView.isHidden = true
        btnClearTxtUser.isHidden = true
    }
    
    func hideCaptchaView() {
        containerCaptchaView.isHidden = true
        containerButtonLoginViewContraint.constant = 15
    }
    
    func showCaptchaView() {
        containerCaptchaView.isHidden = false
        containerButtonLoginViewContraint.constant = 76
    }
    
    func getUUID() {
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            //print(uuid)
        }
    }
    
    func checkLogout2ClearInfo() {
        if logoutFlag == true {
            logoutFlag = false
            clearPersonDataInSqlite()
        }
    }
    
    func checkLoginTextField() -> Bool {
        if txtUser.text == "" {
            lblEmptyAlert.text = loginInvalidString.userEmpty.rawValue
            emptyAlertView.isHidden = false
            backgroundEmptyAlertView.isHidden = false
            backgroundView.isHidden = false
            txtUser.becomeFirstResponder()
            return false
        }
        if txtPassword.text == "" {
            lblEmptyAlert.text = loginInvalidString.passwordEmpty.rawValue
            emptyAlertView.isHidden = false
            backgroundEmptyAlertView.isHidden = false
            backgroundView.isHidden = false
            txtPassword.becomeFirstResponder()
            return false
        }
        if loginWithCaptchaFlag == true && txtCaptcha.text == "" {
            lblEmptyAlert.text = loginInvalidString.captchaEmpty.rawValue
            emptyAlertView.isHidden = false
            backgroundEmptyAlertView.isHidden = false
            backgroundView.isHidden = false
            txtCaptcha.becomeFirstResponder()
            return false
        }
        return true
    }
    
    // Mark:
    @IBAction func btnLoginTapped(_ sender: AnyObject) {
        login()
    }
    
    @IBAction func btnEmptyAlertOKTapped(_ sender: Any) {
        emptyAlertView.isHidden = true
        backgroundEmptyAlertView.isHidden = true
        backgroundView.isHidden = true
    }
    
    @IBAction func btnDontRememberPasswordTapped(_ sender: Any) {
        UIApplication.shared.openURL(URL(string: "http://smartmotor.viettel.vn/fogetpassword.html")!)
    }
    
    @IBAction func btnAccountInfoTapped(_ sender: Any) {
        UIApplication.shared.openURL(URL(string: "http://vietteltelecom.vn/ung-dung-so/giam-sat-va-chong-trom-xe-may-smart-motor_dwpc4")!)
    }
    
    @IBAction func btnCallToCustomCareCenterTapped(_ sender: Any) {
        backgroundEmptyAlertView.isHidden = false
        backgroundView.isHidden = false
        hotlineAlertView.isHidden = false
    }
    
    @IBAction func btnHotlineAlertOKTapped(_ sender: Any) {
        hotlineAlertView.isHidden = true
        backgroundEmptyAlertView.isHidden = true
        backgroundView.isHidden = true
        call2APhone()
    }
    
    @IBAction func onRegister(_ sender: Any) {
        let vc = RegisterVc.loadFromNib()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
    }
    @IBAction func btnHotlineAlertCancelTapped(_ sender: Any) {
        backgroundEmptyAlertView.isHidden = true
        backgroundView.isHidden = true
        hotlineAlertView.isHidden = true
    }
    
    // Mark: Call to vehicle
    func call2APhone() {
        let phoneUrl = "tel://\(18008098)"
        let url:URL = URL(string: phoneUrl)!
        UIApplication.shared.openURL(url)
    }

    func login() {
        loginViewContraint.constant = 0
        loginSuccessedFlag = false
        fncLogin()
    }
        
    //
    func checkLoginFailToDisplayCaptcha() {
        if txtUser.text != stringToCountTxtLoginFail {
            self.stringToCountTxtLoginFail = self.txtUser.text
            self.countLoginFail = 1
            return
        }
        self.countLoginFail = self.countLoginFail + 1
        if self.countLoginFail >= 3 {
            self.showCaptchaView()
            loginWithCaptchaFlag = true
            urlSessionDelegate()
        }
    }
    
    func checkResultCode(resultCode: Int) {
        switch resultCode {
        case -10:
            Alert.alertAutoDismiss(delegate: self, message: "Mã captcha không chính xác!")
        case -11:
            Alert.alertAutoDismiss(delegate: self, message: "Mã captcha không chính xác. Lỗi hệ thống!")
        case -1:
            Alert.alertAutoDismiss(delegate: self, message: "Tên đăng nhập hoặc mật khẩu không chính xác!")
        default:
            break
        }
    }
    
    func urlEncode(string: String) -> String {
        var rString:String! = ""
        for char in string.characters {
            if char == " " {
                rString.append("+")
            }else if (char == "." || char == "-" || char == "_" || char == "~" ||
                (char >= "a" && char <= "z") ||
                (char >= "A" && char <= "Z") ||
                (char >= "0" && char <= "9")) {
                rString.append(String(char))
            } else {
                rString.append(String(format: "%%2X", char.hashValue))
            }
        }
        return rString
    }
    
    
    // Mark: Request data
    //sysadmin  Gpud@123456
    func fncLogin() {
        if checkLoginTextField() == false {
            return
        }
        txtUser.text = txtUser.text?.replacingOccurrences(of: "\\s+", with: "", options: .regularExpression, range: nil)
        Loading().showLoading()
        let request:NSMutableURLRequest!
//        // https GET
//        let encryptedBase64Password = try! txtPassword.text!.encrypt(cipher: AES(key: encrypKeyForLogin, iv: ""))
//
//        let url = URL(string: smartmotorUrl + loginUrl + txtUser.text! + "/" + encryptedBase64Password)
//        //
//        request = NSMutableURLRequest(url: url!)
//        request.httpMethod = getMethod
//        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)

        // https POST
        var capcha: String = ""
        let encryptedBase64Password = try! txtPassword.text!.encrypt(cipher: AES(key: encrypKeyForLogin, iv: ""))
        let url = URL(string: smartmotorUrl + loginUrl)
        request = NSMutableURLRequest(url: url!)
        request.httpMethod = postMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        if loginWithCaptchaFlag == true {
            capcha = urlEncode(string: txtCaptcha.text!)
            request.addValue(cookieName + "=" + cookieValue, forHTTPHeaderField: "Cookie")
        }
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let dataString:[String: String] = ["username" : txtUser.text!,"password" : encryptedBase64Password, "captcha" : capcha] as Dictionary
        request.httpBody = try! JSONSerialization.data(withJSONObject: dataString, options: [])
        //

        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        
        let task = defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
            Loading().hideLoading()
            if error != nil {
                Alert.alertAutoDismiss(delegate: self, message: self.cannotConnect2Server)
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    Alert.alertAutoDismiss(delegate: self, message: self.loginUnsuccess)
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let resultCode = jsonResult["resultCode"] as! Int
                    if resultCode != 1 {
                        self.checkResultCode(resultCode: resultCode)
                        self.checkLoginFailToDisplayCaptcha()
                        return
                    }
                    self.loginSuccessedFlag = true
                    loginInfomations = self.parseResult2GetPersonInfo(jsonResult)
                    numberOfVehcle = 0
                    self.savePersonInfo2Sqlite(token: tokenSave,userid: userIdSave,groupRoot: groupRootSave)
                    self.loadListVehicle()
                    //self.gotoListVehicleView()
                }catch {
                    Alert.alertAutoDismiss(delegate: self, message: self.cannotConnect2Server)
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    // Mark: Request data
    func loadListVehicle() {
        Loading().showLoading()
        pageIndex = 1
        registerNo2Query = ""
        let urlString = smartmotorUrl + listTransportUrl + tokenSave + "/" + userIdSave + listTransportRegNoUrl + registerNo2Query + listTransportPageIndexUrl + String(pageIndex)
        let url = URL(string: urlString)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        //
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        //
        let task = defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
            Loading().hideLoading()
            if error != nil {
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)) as! NSDictionary
                    let resultCode = jsonResult["resultCode"] as! Int
                    if resultCode != 1 {
                        Alert.alertAutoDismiss(delegate: self, message: self.loginUnsuccess)
                        return
                    }
                    let totalOfVehicle = jsonResult["total"] as! Int
                    if totalOfVehicle == 1 {
                        vehicleInformations.removeAll()
                        let jsonData:NSArray! = jsonResult["data"] as! NSArray
                        for element in jsonData {
                            let result = element as AnyObject
                            vehicleInformations.append(self.parseJson2GetListVehicle(result: result))
                            numberOfVehcle = vehicleInformations.count
                        }
                        vehicle2show = vehicleInformations[0]
                        
                        
                        //vehicle2show.isForbidden = true
                        
                        
                        if vehicle2show.isForbidden == false {
                            vehicle2showIndex = 0
                            self.gotoMapView()
                            return
                        }
                    }
                    self.gotoListVehicleView()
                }catch {
                    Alert.alertAutoDismiss(delegate: self, message: self.cannotConnect2Server)
                    fatalError("Failure\(error)")
                }
            }else {
               
            }
        }
        task.resume()
    }
    
    // Mark: Parse data
    func parseResult2GetPersonInfo(_ result: NSDictionary) -> loginInformation {
        var resultReturn: loginInformation!
        let groupRoot = result.value(forKey: loginInformationString.groupRoot.rawValue) as! Int
        groupRootSave = groupRoot
        let groupType = result.value(forKey: loginInformationString.groupType.rawValue) as! Int
        let lat = result.value(forKey: loginInformationString.lat.rawValue) as! Double
        let lng = result.value(forKey: loginInformationString.lng.rawValue) as! Double
        let loginAttemp = result.value(forKey: loginInformationString.loginAttemp.rawValue) as! Int
        let message = result.value(forKey: loginInformationString.message.rawValue) as! String
        let resultCode = result.value(forKey: loginInformationString.resultCode.rawValue) as! Int
        let roleId = result.value(forKey: loginInformationString.roleId.rawValue) as! Int
        let token = result.value(forKey: loginInformationString.token.rawValue) as! String
        tokenSave = token
        let userId = result.value(forKey: loginInformationString.userId.rawValue) as! Int
        userIdSave = String(userId)
        resultReturn = loginInformation(groupRoot: groupRoot,groupType:groupType,lat:lat ,lng:lng,loginAttemp:loginAttemp,message:message,resultCode:resultCode,roleId:roleId ,token:token ,userId: userId)
        return resultReturn
    }
    
    func parseJson2GetListVehicle(result: AnyObject) -> vehicleInformation {
        var resultReturn: vehicleInformation!
        var gpsDate:String! = ""
        var isForbidden:Bool! = false
        let accIllegalState = result.value(forKeyPath: vehicleInfoString.accIllegalState.rawValue) as! Int
        let devicePin = result.value(forKeyPath: vehicleInfoString.devicePin.rawValue) as! String
        let deviceType = result.value(forKeyPath: vehicleInfoString.deviceType.rawValue) as! Int
        let deviceTypeIdOrginal = result.value(forKeyPath: vehicleInfoString.deviceTypeIdOrginal.rawValue) as! Int
        let factory = result.value(forKeyPath: vehicleInfoString.factory.rawValue) as! String
        if let _gpsDate = (result.value(forKeyPath: vehicleInfoString.gpsDate.rawValue) as? String) {
            gpsDate = _gpsDate
        }
        let gpsSpeed = result.value(forKeyPath: vehicleInfoString.gpsSpeed.rawValue) as! Int
        let gpsState = result.value(forKeyPath: vehicleInfoString.gpsState.rawValue) as! Int
        let groupsCode = result.value(forKeyPath: vehicleInfoString.groupsCode.rawValue) as! String
        let id = result.value(forKeyPath: vehicleInfoString.id.rawValue) as! Int
        let illegalMoveState = result.value(forKeyPath: vehicleInfoString.illegalMoveState.rawValue) as! Int
        let lat = result.value(forKeyPath: vehicleInfoString.lat.rawValue) as! Double
        let lng = result.value(forKeyPath: vehicleInfoString.lng.rawValue) as! Double
        let lowBatteryState = result.value(forKeyPath: vehicleInfoString.lowBatteryState.rawValue) as! Int
        let motoSpeed = result.value(forKeyPath: vehicleInfoString.motoSpeed.rawValue) as! Int
        let offPowerState = result.value(forKeyPath: vehicleInfoString.offPowerState.rawValue) as! Int
        let regNo = result.value(forKeyPath: vehicleInfoString.regNo.rawValue) as! String
        let sim = result.value(forKeyPath: vehicleInfoString.sim.rawValue) as! String
        let sosState = result.value(forKeyPath: vehicleInfoString.sosState.rawValue) as! Int
        let state = result.value(forKeyPath: vehicleInfoString.state.rawValue) as! Int
        let timeState = result.value(forKeyPath: vehicleInfoString.timeState.rawValue) as! Int
        let type = result.value(forKeyPath: vehicleInfoString.type.rawValue) as! String
        let vibrationState = result.value(forKeyPath: vehicleInfoString.vibrationState.rawValue) as! Int
        if let _isForbidden = result.value(forKeyPath: vehicleInfoString.isForbidden.rawValue) as? Bool {
            isForbidden = _isForbidden
        }
        resultReturn = vehicleInformation(accIllegalState:accIllegalState,devicePin:devicePin,deviceType:deviceType,deviceTypeIdOrginal:deviceTypeIdOrginal,factory:factory,gpsDate:gpsDate,gpsSpeed:gpsSpeed,gpsState:gpsState,groupsCode:groupsCode,id:id,illegalMoveState:illegalMoveState,lat:lat,lng:lng,lowBatteryState:lowBatteryState,motoSpeed:motoSpeed,offPowerState:offPowerState,regNo:regNo,sim:sim,sosState:sosState,state:state,timeState:timeState,type:type,vibrationState:vibrationState,isForbidden:isForbidden)
        return resultReturn
    }

    // Mark: touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //init image has been tapped
        let touch:UITouch = touches.first!
        if touch.view == imgCaptcha {
            urlSessionDelegate()
            return
        }
        loginViewContraint.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func encrypt(string:String) -> String {
        do {
            let aes = try AES(key: encrypKeyForLogin, iv: "", blockMode: .CBC, padding: .pkcs7)
            let ciphertext = try aes.encrypt(string.utf8.map({$0}))
            let encryptValue = Data(bytes: ciphertext).base64EncodedString()
            return encryptValue
        }catch { }
        return ""
    }
    
    func decrypt(string:String) -> String {
        do {
            let aes = try AES(key: encrypKeyForLogin, iv: "", blockMode: .CBC, padding: .pkcs7)
            let cipherdata = Data(base64Encoded: string)
            let ciphertext = try aes.decrypt(cipherdata!.bytes)
            let decryptValue = String(bytes:ciphertext, encoding:String.Encoding.utf8)
            return decryptValue!
        }catch { }
        return ""
    }

    
    // Mark: managedObjectContext
    func savePersonInfo2Sqlite(token: String, userid:String,groupRoot:Int) {
        let moc = DataController().managedObjectContext
        let entity = NSEntityDescription.insertNewObject(forEntityName: sqliteItemString.person.rawValue, into: moc) as! Person
        let _token = encrypt(string: token)
        entity.setValue(_token, forKey: sqliteItemString.token.rawValue)
        entity.setValue(userid, forKey: sqliteItemString.userId.rawValue)
        entity.setValue(String(groupRoot), forKey: sqliteItemString.groupRoot.rawValue)
        tokenSave = token
        userIdSave = userid
        groupRootSave = groupRoot
        do {
            try moc.save()
        } catch {
            fatalError("Failure\(error)")
        }
    }
    
    func getPersonInfoFromSqlite() {
        let moc = DataController().managedObjectContext
        let personFetch = NSFetchRequest<NSFetchRequestResult> (entityName: sqliteItemString.person.rawValue)
        do {
            let result = try moc.fetch(personFetch) as! [Person]
            if result.count > 0 {
                let _token = decrypt(string: result.last!.token!)
                tokenSave = _token
                userIdSave = result.last!.userId!
                groupRootSave = Int(result.last!.groupRoot!)
            }
        } catch {
            fatalError("Failed\(error)")
        }
        checkData2Use()
    }
    
    func clearPersonDataInSqlite() {
        tokenSave = nil
        userIdSave = nil
        groupRootSave = nil
        let moc = DataController().managedObjectContext
        let personFetch = NSFetchRequest<NSFetchRequestResult> (entityName: sqliteItemString.person.rawValue)
        do {
            let result = try moc.fetch(personFetch) as! [Person]
            if result.count > 0 {
                for item in result {
                    moc.delete(item)
                }
            }
            try moc.save()
        } catch {
            fatalError("Failed\(error)")
        }
    }
    
    func checkData2Use() {
        if tokenSave != nil && userIdSave != nil && groupRootSave != nil {
            loadListVehicle()
            //gotoListVehicleView()
        }else {
            clearPersonDataInSqlite()
        }
    }
    
    // Mark:
    func gotoListVehicleView() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "listVehicle") as! ListVehicleVC
        vehicleInformations.removeAll()
        vehicle2show = nil
        loginFlag = true
        DispatchQueue.main.async {
            nextViewController.modalPresentationStyle = .fullScreen
            self.present(nextViewController, animated:true, completion:nil)
        }
    }
        
    func gotoMapView() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "map") as! MapVC
        registerNo2Query = vehicle2show.regNo
        loginFlag = true
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    // Mark: Keyboard
    func getKeyBoardHeight() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            let heightLoginView = loginView.frame.size.height
            let heightTxtPassword = txtPassword.frame.size.height
            let heightBtnLogin = btnLogin.frame.size.height
            let heightTxtCaptcha = txtCaptcha.frame.size.height
            var move:CGFloat = 0
            if loginWithCaptchaFlag == false {
                move = -keyboardHeight + (heightLoginView/2) - (heightTxtPassword/2) - heightBtnLogin - 15
            }else {
                move = -keyboardHeight + (heightLoginView/2) - (heightTxtPassword/2) - heightBtnLogin - heightTxtCaptcha - 30
            }
            var top = -loginView.bounds.size.height/2 + txtPassword.frame.size.height/2 + imgViewLogo.frame.size.height + btnLogin.frame.size.height
            top = top + 50
            if move < top {
                move = top
            }
            if move > 0 {
                return
            }
            loginViewContraint.constant = move
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    // Mark: Tap on background to close warning
    func initTapInBackgroundView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(LoginVC.handleBackgroundViewTap(_:)))
        backgroundView.addGestureRecognizer(tap)
    }
    
    @objc func handleBackgroundViewTap(_ sender: UITapGestureRecognizer) {
        closeViewUpperBackground()
    }
    
    func closeViewUpperBackground() {
        backgroundEmptyAlertView.isHidden = true
        backgroundView.isHidden = true
        emptyAlertView.isHidden = true
        hotlineAlertView.isHidden = true
    }
    
    // Mark:
    func initURLSession() {
        let sessionConfig = URLSessionConfiguration.default
        session = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
    }
    
    func deleteCookies() {
        let cookieStore = HTTPCookieStorage.shared
        for cookie in cookieStore.cookies ?? [] {
            cookieStore.deleteCookie(cookie)
        }
    }
    
    func urlSessionDelegate() {
        deleteCookies()
        let imageUrl: String = smartmotorUrl + captchaUrl
        let task: URLSessionDownloadTask = session.downloadTask(with: NSURL(string: imageUrl as String)! as URL)
        task.resume()
        Loading().showLoading()
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let downloadedImage = UIImage(data: NSData(contentsOf: location) as! Data)
        DispatchQueue.main.async {
            self.imgCaptcha.image = downloadedImage
            self.loginViewContraint.constant = 0
        }
        for cookie in (session.configuration.httpCookieStorage?.cookies)! {
            if cookie.name == "JSESSIONID" {
                cookieName = cookie.name
                cookieValue = cookie.value
                Loading().hideLoading()
                return
            }
        }
        Loading().hideLoading()
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
    }
    
}

extension LoginVC: URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
    
}


