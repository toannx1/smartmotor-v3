//
//  SettingVC.swift
//  Smart Motor
//
//  Created by vietnv2 on 26/10/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit
import MessageUI

class SettingVC: UIViewController {
//
    @IBOutlet weak var settingTableView: UITableView!
    @IBOutlet weak var alarmTableView: UITableView!
    @IBOutlet weak var backGroundSettingView: UIView!
    @IBOutlet weak var listAlarmSettingView: UIView!
    @IBOutlet weak var accAlarmSettingView: UIView!
    @IBOutlet weak var vibrateAlarmSettingView: UIView!
    @IBOutlet weak var fenceAlarmSettingView: UIView!
    @IBOutlet weak var speedAlarmSettingView: UIView!
    @IBOutlet weak var changePasswordView: UIView!
    @IBOutlet weak var vehicleRegistryView: UIView!
    @IBOutlet weak var deviceRebootView: UIView!
    @IBOutlet weak var deviceFormatView: UIView!
    @IBOutlet weak var titleSettingView: UIView!
    @IBOutlet weak var askPasswordView: UIView!
    @IBOutlet weak var backGroundAskPasswordView: UIView!
    
    @IBOutlet weak var accAlarmContraint: NSLayoutConstraint!
    @IBOutlet weak var vibrateAlarmContraint: NSLayoutConstraint!
    @IBOutlet weak var fenceAlarmContraint: NSLayoutConstraint!
    @IBOutlet weak var speedAlarmContraint: NSLayoutConstraint!
    @IBOutlet weak var changePasswordContraint: NSLayoutConstraint!
    @IBOutlet weak var vehicleRegistryViewContraint: NSLayoutConstraint!
    @IBOutlet weak var askPasswordContraint: NSLayoutConstraint!

    @IBOutlet weak var btnCheckBoxAccAlarm: UIButton!
    @IBOutlet weak var btnCheckBoxAccSMS: UIButton!
    @IBOutlet weak var btnCheckBoxAccCall: UIButton!
    @IBOutlet weak var btnCheckBoxAccSiren: UIButton!
    @IBOutlet weak var btnCheckBoxVibrateAlarm: UIButton!
    @IBOutlet weak var btnCheckBoxVibrateSMS: UIButton!
    @IBOutlet weak var btnCheckBoxVibrateCall: UIButton!
    @IBOutlet weak var btnCheckBoxVibrateSiren: UIButton!
    @IBOutlet weak var btnCheckBoxFenceAlarm: UIButton!
    @IBOutlet weak var btnCheckBoxFenceSMS: UIButton!
    @IBOutlet weak var btnCheckBoxFenceCall: UIButton!
    @IBOutlet weak var btnCheckBoxFenceSiren: UIButton!
    @IBOutlet weak var txtFence: UITextField!
    @IBOutlet weak var btnCheckBoxSpeedAlarm: UIButton!
    @IBOutlet weak var btnCheckBoxSpeedSMS: UIButton!
    @IBOutlet weak var btnCheckBoxSpeedCall: UIButton!
    @IBOutlet weak var btnCheckBoxSpeedSiren: UIButton!
    @IBOutlet weak var txtSpeed: UITextField!
    @IBOutlet weak var txtOldPassword_ChangePassword: UITextField!
    @IBOutlet weak var txtNewPassword_ChangePassword: UITextField!
    @IBOutlet weak var txtConfirmNewPassword_ChangePassword: UITextField!
    @IBOutlet weak var txtOldPassword_Registry: UITextField!
    @IBOutlet weak var txtNewPassword_Registry: UITextField!
    @IBOutlet weak var txtConfirmNewPassword_Registry: UITextField!
    
    @IBOutlet weak var lblRegNoTitle: UILabel!
    @IBOutlet weak var lblFormatNotice: UILabel!
    @IBOutlet weak var lblRebootNotice: UILabel!
    @IBOutlet weak var lblVehicleRegistrySim: UILabel!
    
    
    @IBOutlet weak var btnChangePasswordConfirm2SetParameter: UIButton!
    @IBOutlet weak var btnChangePasswordCancelSetParameter: UIButton!
    
    @IBOutlet weak var btnVehicleRegistryConfirm2SetParameter: UIButton!
    @IBOutlet weak var btnVehicleRegistryCancelSetParameter: UIButton!
    
    @IBOutlet weak var btnDeviceRebootConfirm2SetParameter: UIButton!
    @IBOutlet weak var btnDeviceRebootCancelSetParameter: UIButton!
    
    @IBOutlet weak var btnDeviceFormatConfirm2SetParameter: UIButton!
    @IBOutlet weak var btnDeviceFormatCancelSetParameter: UIButton!
    
    @IBOutlet weak var btnAccAlarmConfirm2SetParameter: UIButton!
    @IBOutlet weak var btnAccAlarmConfirm2GetParameter: UIButton!
    
    @IBOutlet weak var btnVibrateAlarmConfirm2SetParameter: UIButton!
    @IBOutlet weak var btnVibrateAlarmConfirm2GetParameter: UIButton!
    
    @IBOutlet weak var btnFenceAlarmConfirm2SetParameter: UIButton!
    @IBOutlet weak var btnFenceAlarmConfirm2GetParameter: UIButton!
    
    @IBOutlet weak var btnSpeedAlarmConfirm2SetParameter: UIButton!
    @IBOutlet weak var btnSpeedAlarmConfirm2GetParameter: UIButton!
    
    @IBOutlet weak var btnSecurityVehiclePassword: UIButton!
    
    @IBOutlet weak var btnSecurityOldPasswordInChangePassword: UIButton!
    @IBOutlet weak var btnSecurityNewPasswordInChangePassword: UIButton!
    @IBOutlet weak var btnSecurityConfirmNewPasswordInChangePassword: UIButton!
    
    @IBOutlet weak var btnSecurityOldPasswordVehicleRegistry: UIButton!
    @IBOutlet weak var btnSecurityNewPasswordVehicleRegistry: UIButton!
    @IBOutlet weak var btnSecurityConfirmNewPasswordVehicleRegistry: UIButton!

    @IBOutlet weak var txtVehiclePassword: UITextField!
    
    
    var checkBoxAccAlarmFlag:Bool! = false
    var checkBoxAccSMSFlag:Bool! = false
    
    var checkBoxAccCallFlag:Bool! = false
    var checkBoxAccSirenFlag:Bool = false
    var checkBoxVibrateAlarmFlag:Bool! = false
    var checkBoxVibrateSMSFlag:Bool! = false
    
    var checkBoxVibrateCallFlag:Bool! = false
    var checkBoxVibrateSirenFlag:Bool = false
    var checkBoxFenceAlarmFlag:Bool! = false
    var checkBoxFenceSMSFlag:Bool! = false
    
    var checkBoxFenceCallFlag:Bool! = false
    var checkBoxFenceSirenFlag:Bool = false
    var checkBoxSpeedAlarmFlag:Bool! = false
    var checkBoxSpeedSMSFlag:Bool! = false
    var checkBoxSpeedCallFlag:Bool! = false
    var checkBoxSpeedSirenFlag:Bool = false
    
    var flagStartEndEditForTextField:Int! = 0
    
    let settingArray:[String] = ["Kích hoạt thiết bị",
                                 "Thiết lập/ Kiểm tra chế độ cảnh báo",
                                 "Khôi phục cài đặt gốc",
                                 "Đổi mật khẩu ứng dụng",
                                 "Đổi mật khẩu thiết bị",
                                 "Khởi động lại thiết bị"]
    let settingImageArray:[UIImage] = [#imageLiteral(resourceName: "icon activate.png"),#imageLiteral(resourceName: "icon_setting01.png"),#imageLiteral(resourceName: "icon_setting05.png"),UIImage.init(named:"ChangePassword")!, #imageLiteral(resourceName: "icon_setting02.png"),#imageLiteral(resourceName: "icon_setting04.png")]
    let noItemChose:String = "Không có mục nào được chọn!"
    let alarmSettingArray:[String] = ["Mở khoá trái phép",
                                      "Rung lắc",
                                      "Quá phạm vi cho phép",
                                      "Quá tốc độ"]
    let alarmImageArray:[UIImage] = [#imageLiteral(resourceName: "icon_alarmsetting01.png"),#imageLiteral(resourceName: "icon_alarmsetting02.png"),#imageLiteral(resourceName: "icon_alarmsetting03.png"),#imageLiteral(resourceName: "icon_alarmsetting04.png")]
    let forwardImage:UIImage! = UIImage(named: "Forward-35.png")
    var vehiclePassword:String! = ""
    let isYes:String! = "=1"
    let isNo:String! = "=0"
    let getString:String = "GET"
    let setString:String = "SET"
    let rebootString:String = "REBOOT"
    let formatString:String = "FORMAT"
    
    enum alarmString:String {
        case accAlarm = ",ACCALARM"
        case accSMS = ",ACCSMS"
        case accCall = ",ACCCALL"
        case accSiren = ",ACCSIREN"
        //
        case vib = ",VIB"
        case vibSMS = ",VIBSMS"
        case vibCall = ",VIBCALL"
        case vibSiren = ",VIBSIREN"
        //
        case radAlarm = ",RADALARM"
        case radSMS = ",RADSMS"
        case radCall = ",RADCALL"
        case radSiren = ",RADSIREN"
        case rad = ",RAD"
        //
        case speedAlarm = ",SPEEDALARM"
        case speedSMS = ",SPEEDSMS"
        case speedCall = ",SPEEDCALL"
        case speedSiren = ",SPEEDSIREN"
        case speed = ",SPEED"
    }
    
    var alarmSet:Int!
    var textFieldCurrent:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetting()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false

    }
    
    func initSettingView() {
        backGroundSettingView.isHidden = true
        listAlarmSettingView.isHidden = true
        accAlarmSettingView.isHidden = true
        vibrateAlarmSettingView.isHidden = true
        fenceAlarmSettingView.isHidden = true
        speedAlarmSettingView.isHidden = true
        changePasswordView.isHidden = true
        vehicleRegistryView.isHidden = true
        deviceRebootView.isHidden = true
        deviceFormatView.isHidden = true
        askPasswordView.isHidden = true
        backGroundAskPasswordView.isHidden = true
        btnSecurityOldPasswordInChangePassword.isHidden = true
        btnSecurityNewPasswordInChangePassword.isHidden = true
        btnSecurityConfirmNewPasswordInChangePassword.isHidden = true
        btnSecurityOldPasswordVehicleRegistry.isHidden = true
        btnSecurityNewPasswordVehicleRegistry.isHidden = true
        btnSecurityConfirmNewPasswordVehicleRegistry.isHidden = true
        btnSecurityVehiclePassword.isHidden = true
        lblFormatNotice.text = "Bạn muốn khôi phục cài đặt gốc bằng SMS ?\n(Thực hiện chức năng này bạn sẽ bị mất quyền chủ xe. Vui lòng nhắn tin kích hoạt lại thiết bị)"
        lblRebootNotice.text = "Bạn có muốn REBOOT xe " + vehicle2show.regNo + " bằng SMS?"
        lblVehicleRegistrySim.text = vehicle2show.sim
    }
    
    func initSetting() {
        initSettingView()
        //
        lblRegNoTitle.text = "Cấu hình xe " + vehicle2show.regNo
        //
        titleSettingView.addBackground(width: UIScreen.main.bounds.size.width,height: titleViewHeight, image: titleViewImage)
        //
        initTextField()
        contraintDefault()
        initTapInBackgroundView()
        initTapInBackgroundAskPasswordView()
    }
    
    @IBAction func btnBack2MapTapped(_ sender: AnyObject) {
        gotoMapView()
    }
    
    @IBAction func btnCancelAlarmSettingTapped(_ sender: Any) {
        gotoMapView()
    }
    
    func gotoMapView() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "map") as! MapVC
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    // Mark: Vehicle' password
    @IBAction func saveVehiclePasswordTapped(_ sender: AnyObject) {
        if txtVehiclePassword.text?.characters.count != 6 {
            Alert.alertAutoDismiss(delegate: self, message: "Mật khẩu phải có độ dài 6 số!")
            return
        }
        vehiclePassword = txtVehiclePassword.text
        askPasswordView.isHidden = true
        backGroundAskPasswordView.isHidden = true
        self.view.endEditing(true)
        
        switch alarmSet {
            case 0:accSetParameter()
            case 1:vibSetParameter()
            case 2:fenceSetParameter()
            case 3:speedSetParameter()
            default:break
        }
        btnSecurityVehiclePassword.isHidden = true
    }
    
    @IBAction func cacelSaveVehiclePasswordTapped(_ sender: AnyObject) {
        askPasswordView.isHidden = true
        backGroundAskPasswordView.isHidden = true
        btnSecurityVehiclePassword.isHidden = true
        keyBoardHide()
    }
    
    @IBAction func btnSecurityVehiclePasswordTapped(_ sender: Any) {
        txtVehiclePassword.isSecureTextEntry = !txtVehiclePassword.isSecureTextEntry
    }
    
    // Mark: - Acc Alarm
    @IBAction func btnCheckBoxAccAlarmTapped(_ sender: AnyObject) {
        if !checkBoxAccAlarmFlag {
            checkBoxAccAlarmFlag = true
            btnCheckBoxAccAlarm.setImage(checkBoxImage, for: UIControlState.normal)
        }else {
            checkBoxAccAlarmFlag = false
            btnCheckBoxAccAlarm.setImage(uncheckBoxImage, for: UIControlState.normal)
        }
    }
    
    @IBAction func btnCheckBoxAccSMSTapped(_ sender: AnyObject) {
        if !checkBoxAccSMSFlag {
            checkBoxAccSMSFlag = true
            btnCheckBoxAccSMS.setImage(checkBoxImage, for: UIControlState.normal)
        }else {
            checkBoxAccSMSFlag = false
            btnCheckBoxAccSMS.setImage(uncheckBoxImage, for: UIControlState.normal)
        }
    }
    
    @IBAction func btnCheckBoxAccCallTapped(_ sender: AnyObject) {
        if !checkBoxAccCallFlag {
            checkBoxAccCallFlag = true
            btnCheckBoxAccCall.setImage(checkBoxImage, for: UIControlState.normal)
        }else {
            checkBoxAccCallFlag = false
            btnCheckBoxAccCall.setImage(uncheckBoxImage, for: UIControlState.normal)
        }
    }
    
    @IBAction func btnCheckBoxAccSirenTapped(_ sender: AnyObject) {
        if !checkBoxAccSirenFlag {
            checkBoxAccSirenFlag = true
            btnCheckBoxAccSiren.setImage(checkBoxImage, for: UIControlState.normal)
        }else {
            checkBoxAccSirenFlag = false
            btnCheckBoxAccSiren.setImage(uncheckBoxImage, for: UIControlState.normal)
        }
    }
    
    func accSetParameter() {
        var smsContent:String! = setString
        smsContent = smsContent + "," + vehiclePassword
        if !checkBoxAccAlarmFlag {
            smsContent = smsContent + alarmString.accAlarm.rawValue + isNo
        }else {
            smsContent = smsContent + alarmString.accAlarm.rawValue + isYes
        }
        if !checkBoxAccSMSFlag {
            smsContent = smsContent + alarmString.accSMS.rawValue + isNo
        }else {
            smsContent = smsContent + alarmString.accSMS.rawValue + isYes
        }
        if !checkBoxAccCallFlag {
            smsContent = smsContent + alarmString.accCall.rawValue + isNo
        }else {
            smsContent = smsContent + alarmString.accCall.rawValue + isYes
        }
        if !checkBoxAccSirenFlag {
            smsContent = smsContent + alarmString.accSiren.rawValue + isNo
        }else {
            smsContent = smsContent + alarmString.accSiren.rawValue + isYes
        }
        accAlarmSettingView.isHidden = true
        backGroundSettingView.isHidden = true
        clearCheckBoxAccFlag()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsContent)
    }
    
    @IBAction func btnAccAlarmConfirm2SetParameterTapped(_ sender: AnyObject) {
        alarmSet = 0
        openAskPasswordView()
    }
    
    @IBAction func btnAccAlarmConfirm2GetParameterTapped(_ sender: Any) {
        let smsContent = "GET,PSW,ACCALARM,ACCSMS,ACCCALL,ACCSIREN"
        accAlarmSettingView.isHidden = true
        backGroundSettingView.isHidden = true
        //clearCheckBoxAccFlag()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsContent)
    }
    
    func clearCheckBoxAccFlag() {
        checkBoxAccAlarmFlag = false
        checkBoxAccSMSFlag = false
        checkBoxAccCallFlag = false
        checkBoxAccSirenFlag = false
        btnCheckBoxAccAlarm.setImage(uncheckBoxImage, for: UIControlState.normal)
        btnCheckBoxAccSMS.setImage(uncheckBoxImage, for: UIControlState.normal)
        btnCheckBoxAccCall.setImage(uncheckBoxImage, for: UIControlState.normal)
        btnCheckBoxAccSiren.setImage(uncheckBoxImage, for: UIControlState.normal)
    }
    
    // Mark: - Vibrate Alarm
    @IBAction func btnCheckBoxVibrateAlarmTapped(_ sender: AnyObject) {
        if !checkBoxVibrateAlarmFlag {
            checkBoxVibrateAlarmFlag = true
            btnCheckBoxVibrateAlarm.setImage(checkBoxImage, for: UIControlState.normal)
        }else {
            checkBoxVibrateAlarmFlag = false
            btnCheckBoxVibrateAlarm.setImage(uncheckBoxImage, for: UIControlState.normal)
        }
    }
    
    @IBAction func btnCheckBoxVibrateSMSTapped(_ sender: AnyObject) {
        if !checkBoxVibrateSMSFlag {
            checkBoxVibrateSMSFlag = true
            btnCheckBoxVibrateSMS.setImage(checkBoxImage, for: UIControlState.normal)
        }else {
            checkBoxVibrateSMSFlag = false
            btnCheckBoxVibrateSMS.setImage(uncheckBoxImage, for: UIControlState.normal)
        }
    }
    
    @IBAction func btnCheckBoxVibrateCallTapped(_ sender: AnyObject) {
        if !checkBoxVibrateCallFlag {
            checkBoxVibrateCallFlag = true
            btnCheckBoxVibrateCall.setImage(checkBoxImage, for: UIControlState.normal)
        }else {
            checkBoxVibrateCallFlag = false
            btnCheckBoxVibrateCall.setImage(uncheckBoxImage, for: UIControlState.normal)
        }
    }
    
    @IBAction func btnCheckBoxVibrateSirenTapped(_ sender: AnyObject) {
        if !checkBoxVibrateSirenFlag {
            checkBoxVibrateSirenFlag = true
            btnCheckBoxVibrateSiren.setImage(checkBoxImage, for: UIControlState.normal)
        }else {
            checkBoxVibrateSirenFlag = false
            btnCheckBoxVibrateSiren.setImage(uncheckBoxImage, for: UIControlState.normal)
        }
    }
    
    func vibSetParameter() {
        var smsContent:String! = setString
        smsContent = smsContent + "," + vehiclePassword
        if !checkBoxVibrateAlarmFlag {
            smsContent = smsContent + alarmString.vib.rawValue + isNo
        }else {
            smsContent = smsContent + alarmString.vib.rawValue + isYes
        }
        if !checkBoxVibrateSMSFlag {
            smsContent = smsContent + alarmString.vibSMS.rawValue + isNo
        }else {
            smsContent = smsContent + alarmString.vibSMS.rawValue + isYes
        }
        if !checkBoxVibrateCallFlag {
            smsContent = smsContent + alarmString.vibCall.rawValue + isNo
        }else {
            smsContent = smsContent + alarmString.vibCall.rawValue + isYes
        }
        if !checkBoxVibrateSirenFlag {
            smsContent = smsContent + alarmString.vibSiren.rawValue + isNo
        }else {
            smsContent = smsContent + alarmString.vibSiren.rawValue + isYes
        }
        vibrateAlarmSettingView.isHidden = true
        backGroundSettingView.isHidden = true
        clearCheckBoxVibrateFlag()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsContent)
    }
    
    @IBAction func btnVibrateAlarmConfirm2SetParameterTapped(_ sender: AnyObject) {
        alarmSet = 1
        openAskPasswordView()
    }
    
    @IBAction func btnVibrateAlarmConfirm2GetParameterTapped(_ sender: Any) {
        let smsContent = "GET,PSW,VIB,VIBSMS,VIBCALL,VIBSIREN"
        vibrateAlarmSettingView.isHidden = true
        backGroundSettingView.isHidden = true
        //clearCheckBoxVibrateFlag()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsContent)
    }
    
    func clearCheckBoxVibrateFlag() {
        checkBoxVibrateAlarmFlag = false
        checkBoxVibrateSMSFlag = false
        checkBoxVibrateCallFlag = false
        checkBoxVibrateSirenFlag = false
        btnCheckBoxVibrateAlarm.setImage(uncheckBoxImage, for: UIControlState.normal)
        btnCheckBoxVibrateSMS.setImage(uncheckBoxImage, for: UIControlState.normal)
        btnCheckBoxVibrateCall.setImage(uncheckBoxImage, for: UIControlState.normal)
        btnCheckBoxVibrateSiren.setImage(uncheckBoxImage, for: UIControlState.normal)
    }
    
    // Mark: - Fence Alarm
    @IBAction func btnCheckBoxFenceAlarmTapped(_ sender: AnyObject) {
        if !checkBoxFenceAlarmFlag {
            checkBoxFenceAlarmFlag = true
            btnCheckBoxFenceAlarm.setImage(checkBoxImage, for: UIControlState.normal)
        }else {
            checkBoxFenceAlarmFlag = false
            btnCheckBoxFenceAlarm.setImage(uncheckBoxImage, for: UIControlState.normal)
        }
    }
    
    @IBAction func btnCheckBoxFenceSMSTapped(_ sender: AnyObject) {
        if !checkBoxFenceSMSFlag {
            checkBoxFenceSMSFlag = true
            btnCheckBoxFenceSMS.setImage(checkBoxImage, for: UIControlState.normal)
        }else {
            checkBoxFenceSMSFlag = false
            btnCheckBoxFenceSMS.setImage(uncheckBoxImage, for: UIControlState.normal)
        }
    }
    
    @IBAction func btnCheckBoxFenceCall(_ sender: AnyObject) {
        if !checkBoxFenceCallFlag {
            checkBoxFenceCallFlag = true
            btnCheckBoxFenceCall.setImage(checkBoxImage, for: UIControlState.normal)
        }else {
            checkBoxFenceCallFlag = false
            btnCheckBoxFenceCall.setImage(uncheckBoxImage, for: UIControlState.normal)
        }
    }
    
    @IBAction func btnCheckBoxFenceSiren(_ sender: AnyObject) {
        if !checkBoxFenceSirenFlag {
            checkBoxFenceSirenFlag = true
            btnCheckBoxFenceSiren.setImage(checkBoxImage, for: UIControlState.normal)
        }else {
            checkBoxFenceSirenFlag = false
            btnCheckBoxFenceSiren.setImage(uncheckBoxImage, for: UIControlState.normal)
        }
    }
    
    func fenceSetParameter() {
        var smsContent:String! = setString
        smsContent = smsContent + "," + vehiclePassword
        if !checkBoxFenceAlarmFlag {
            smsContent = smsContent + alarmString.radAlarm.rawValue + isNo
        }else {
            smsContent = smsContent + alarmString.radAlarm.rawValue + isYes
        }
        if !checkBoxFenceSMSFlag {
            smsContent = smsContent + alarmString.radSMS.rawValue + isNo
        }else {
            smsContent = smsContent + alarmString.radSMS.rawValue + isYes
        }
        if !checkBoxFenceCallFlag {
            smsContent = smsContent + alarmString.radCall.rawValue + isNo
        }else {
            smsContent = smsContent + alarmString.radCall.rawValue + isYes
        }
        if !checkBoxFenceSirenFlag {
            smsContent = smsContent + alarmString.radSiren.rawValue + isNo
        }else {
            smsContent = smsContent + alarmString.radSiren.rawValue + isYes
        }
        smsContent = smsContent + alarmString.rad.rawValue + "=" + txtFence.text!
        fenceAlarmSettingView.isHidden = true
        backGroundSettingView.isHidden = true
        clearCheckBoxFenceFlag()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsContent)
    }
   
    @IBAction func btnFenceAlarmConfirm2SetParameterTapped(_ sender: AnyObject) {
        let fenceString = Int(txtFence.text!)
        if fenceString! < 100 || fenceString! > 10000 {
            Alert.alertAutoDismiss(delegate: self, message: "Bán kính cho phép phải trong khoảng 100 - 10000m")
            return
        }
        alarmSet = 2
        openAskPasswordView()
    }
    
    @IBAction func btnFenceAlarmConfirm2GetParameterTapped(_ sender: Any) {
        let smsContent = "GET,PSW,RADALARM,RADSMS,RADCALL,RADSIREN,RAD"
        fenceAlarmSettingView.isHidden = true
        backGroundSettingView.isHidden = true
        //clearCheckBoxFenceFlag()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsContent)
    }
    
    func clearCheckBoxFenceFlag() {
        checkBoxFenceAlarmFlag = false
        checkBoxFenceSMSFlag = false
        checkBoxFenceCallFlag = false
        checkBoxFenceSirenFlag = false
        btnCheckBoxFenceAlarm.setImage(uncheckBoxImage, for: UIControlState.normal)
        btnCheckBoxFenceSMS.setImage(uncheckBoxImage, for: UIControlState.normal)
        btnCheckBoxFenceCall.setImage(uncheckBoxImage, for: UIControlState.normal)
        btnCheckBoxFenceSiren.setImage(uncheckBoxImage, for: UIControlState.normal)
    }
    
    // Mark: - Speed Alarm
    @IBAction func btnCheckBoxSpeedAlarmTapped(_ sender: AnyObject) {
        if !checkBoxSpeedAlarmFlag {
            checkBoxSpeedAlarmFlag = true
            btnCheckBoxSpeedAlarm.setImage(checkBoxImage, for: UIControlState.normal)
        }else {
            checkBoxSpeedAlarmFlag = false
            btnCheckBoxSpeedAlarm.setImage(uncheckBoxImage, for: UIControlState.normal)
        }
    }
    
    @IBAction func btnCheckBoxSpeedSMSTapped(_ sender: AnyObject) {
        if !checkBoxSpeedSMSFlag {
            checkBoxSpeedSMSFlag = true
            btnCheckBoxSpeedSMS.setImage(checkBoxImage, for: UIControlState.normal)
        }else {
            checkBoxSpeedSMSFlag = false
            btnCheckBoxSpeedSMS.setImage(uncheckBoxImage, for: UIControlState.normal)
        }
    }
    
    @IBAction func btnCheckBoxSpeedCallTapped(_ sender: AnyObject) {
        if !checkBoxSpeedCallFlag {
            checkBoxSpeedCallFlag = true
            btnCheckBoxSpeedCall.setImage(checkBoxImage, for: UIControlState.normal)
        }else {
            checkBoxSpeedCallFlag = false
            btnCheckBoxSpeedCall.setImage(uncheckBoxImage, for: UIControlState.normal)
        }
    }
    
    @IBAction func btnCheckBoxSpeedSirenTapped(_ sender: AnyObject) {
        if !checkBoxSpeedSirenFlag {
            checkBoxSpeedSirenFlag = true
            btnCheckBoxSpeedSiren.setImage(checkBoxImage, for: UIControlState.normal)
        }else {
            checkBoxSpeedSirenFlag = false
            btnCheckBoxSpeedSiren.setImage(uncheckBoxImage, for: UIControlState.normal)
        }
    }
    
    func speedSetParameter() {
        var smsContent:String! = setString
        smsContent = smsContent + "," + vehiclePassword
        if !checkBoxSpeedAlarmFlag {
            smsContent = smsContent + alarmString.speedAlarm.rawValue + isNo
        }else {
            smsContent = smsContent + alarmString.speedAlarm.rawValue + isYes
        }
        if !checkBoxSpeedSMSFlag {
            smsContent = smsContent + alarmString.speedSMS.rawValue + isNo
        }else {
            smsContent = smsContent + alarmString.speedSMS.rawValue + isYes
        }
        if !checkBoxSpeedCallFlag {
            smsContent = smsContent + alarmString.speedCall.rawValue + isNo
        }else {
            smsContent = smsContent + alarmString.speedCall.rawValue + isYes
        }
        if !checkBoxSpeedSirenFlag {
            smsContent = smsContent + alarmString.speedSiren.rawValue + isNo
        }else {
            smsContent = smsContent + alarmString.speedSiren.rawValue + isYes
        }
        smsContent = smsContent + alarmString.speed.rawValue + "=" + txtSpeed.text!
        speedAlarmSettingView.isHidden = true
        backGroundSettingView.isHidden = true
        clearCheckBoxSpeedFlag()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsContent)
    }
    
    @IBAction func btnSpeedAlarmConfirm2SetParameterTapped(_ sender: AnyObject) {
        let speedString = Int(txtSpeed.text!)
        if speedString! < 20 || speedString! > 150 {
            Alert.alertAutoDismiss(delegate: self, message: "Tốc độ cho phép phải trong khoảng 20 - 150m")
            return
        }
        alarmSet = 3
        openAskPasswordView()
    }
    
    @IBAction func btnSpeedAlarmConfirm2GetParameterTapped(_ sender: Any) {
        let smsContent = "GET,PSW,SPEEDALARM,SPEEDSMS,SPEEDCALL,SPEEDSIREN,SPEED"
        speedAlarmSettingView.isHidden = true
        backGroundSettingView.isHidden = true
        //clearCheckBoxSpeedFlag()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsContent)
    }
    
    func clearCheckBoxSpeedFlag() {
        checkBoxSpeedAlarmFlag = false
        checkBoxSpeedSMSFlag = false
        checkBoxSpeedCallFlag = false
        checkBoxSpeedSirenFlag = false
        btnCheckBoxSpeedAlarm.setImage(uncheckBoxImage, for: UIControlState.normal)
        btnCheckBoxSpeedSMS.setImage(uncheckBoxImage, for: UIControlState.normal)
        btnCheckBoxSpeedCall.setImage(uncheckBoxImage, for: UIControlState.normal)
        btnCheckBoxSpeedSiren.setImage(uncheckBoxImage, for: UIControlState.normal)
    }
    
    // Mark: - Change Device's password
    @IBAction func btnChangePasswordConfirm2SetParameterTapped(_ sender: AnyObject) {
        if txtOldPassword_ChangePassword.text?.characters.count != 6 {
            Alert.alertAutoDismiss(delegate: self, message: "Mật khẩu cũ phải có độ dài 6 số!")
            return
        }
        if txtNewPassword_ChangePassword.text?.characters.count != 6 {
            Alert.alertAutoDismiss(delegate: self, message: "Mật khẩu mới phải có độ dài 6 số!")
            return
        }
        if txtConfirmNewPassword_ChangePassword.text?.characters.count != 6 {
            Alert.alertAutoDismiss(delegate: self, message: "Xác nhận mật khẩu mới phải có độ dài 6 số!")
            return
        }
        if (txtNewPassword_ChangePassword.text == txtConfirmNewPassword_ChangePassword.text) == false {
            Alert.alertAutoDismiss(delegate: self, message: "Xác nhận mật khẩu mới không đúng!")
            return
        }
        changePasswordView.isHidden = true
        backGroundSettingView.isHidden = true
        let smsContent:String! = "CP," + txtOldPassword_ChangePassword.text! + "," + txtConfirmNewPassword_ChangePassword.text!
        sendMessage(phoneNumber: vehicle2show.sim, content: smsContent)
    }
    
    @IBAction func btnChangePasswordCancelSetParameterTapped(_ sender: AnyObject) {
        gotoMapView()
    }

    @IBAction func btnSecurityOldPasswordInChangePasswordTapped(_ sender: Any) {
        txtOldPassword_ChangePassword.isSecureTextEntry = !txtOldPassword_ChangePassword.isSecureTextEntry
    }
    
    @IBAction func btnSecurityNewPasswordInChangePasswordTapped(_ sender: Any) {
        txtNewPassword_ChangePassword.isSecureTextEntry = !txtNewPassword_ChangePassword.isSecureTextEntry
    }
    
    @IBAction func btnSecurityConfirmNewPasswordInChangePasswordTapped(_ sender: Any) {
        txtConfirmNewPassword_ChangePassword.isSecureTextEntry = !txtConfirmNewPassword_ChangePassword.isSecureTextEntry
    }
    
    // Mark: - Vehicle registry
    @IBAction func btnVehicleRegistryConfirm2SetParameterTapped(_ sender: AnyObject) {
        if txtOldPassword_Registry.text?.characters.count != 6 {
            Alert.alertAutoDismiss(delegate: self, message: "Mật khẩu cũ phải có độ dài 6 số!")
            return
        }
        if txtNewPassword_Registry.text?.characters.count != 6 {
            Alert.alertAutoDismiss(delegate: self, message: "Mật khẩu mới phải có độ dài 6 số!")
            return
        }
        if txtConfirmNewPassword_Registry.text?.characters.count != 6 {
            Alert.alertAutoDismiss(delegate: self, message: "Xác nhận mật khẩu mới phải có độ dài 6 số!")
            return
        }
        if (txtNewPassword_Registry.text == txtConfirmNewPassword_Registry.text) == false {
            Alert.alertAutoDismiss(delegate: self, message: "Xác nhận mật khẩu mới không đúng!")
            return
        }
        vehicleRegistryView.isHidden = true
        backGroundSettingView.isHidden = true
        let smsContent:String! = "START," + txtOldPassword_Registry.text! + "," + txtNewPassword_Registry.text! + "," + vehicle2show.sim
        sendMessage(phoneNumber: vehicle2show.sim, content: smsContent)
    }
    
    @IBAction func btnVehicleRegistryCancelSetParameterTapped(_ sender: AnyObject) {
        gotoMapView()
    }
    
    @IBAction func btnSecurityOldPasswordVehicleRegistryTapped(_ sender: Any) {
        txtOldPassword_Registry.isSecureTextEntry = !txtOldPassword_Registry.isSecureTextEntry
    }
    
    @IBAction func btnSecurityNewPasswordVehicleRegistryTapped(_ sender: Any) {
        txtNewPassword_Registry.isSecureTextEntry = !txtNewPassword_Registry.isSecureTextEntry
    }
    
    @IBAction func btnSecurityConfirmNewPasswordVehicleRegistryTapped(_ sender: Any) {
        txtConfirmNewPassword_Registry.isSecureTextEntry = !txtConfirmNewPassword_Registry.isSecureTextEntry
    }
    
    // Mark: - Device reboot
    @IBAction func btnDeviceRebootConfirm2SetParameterTapped(_ sender: AnyObject) {
        deviceRebootView.isHidden = true
        sendMessage(phoneNumber: vehicle2show.sim, content: rebootString)
        gotoMapView()
    }
    
    @IBAction func btnDeviceRebootCancelSetParameterTapped(_ sender: AnyObject) {
        gotoMapView()
    }
    
    // Mark: - Device format
    @IBAction func btnDeviceFormatConfirm2SetParameterTapped(_ sender: AnyObject) {
        deviceFormatView.isHidden = true
        sendMessage(phoneNumber: vehicle2show.sim, content: formatString)
    }
    
    @IBAction func btnDeviceFormatCancelSetParameterTapped(_ sender: AnyObject) {
        gotoMapView()
    }
        
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Mark: Tap on background to close warning
    func initTapInBackgroundView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(SettingVC.handleBackgroundViewTap(_:)))
        backGroundSettingView.addGestureRecognizer(tap)
    }
    
    @objc func handleBackgroundViewTap(_ sender: UITapGestureRecognizer) {
        gotoMapView()
    }
    
    func initTapInBackgroundAskPasswordView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(SettingVC.handleBackgroundAskPasswordViewTap(_:)))
        backGroundAskPasswordView.addGestureRecognizer(tap)
    }
    
    @objc func handleBackgroundAskPasswordViewTap(_ sender: UITapGestureRecognizer) {
        closeAskPasswordView()
    }
    
    func openAskPasswordView() {
        askPasswordView.isHidden = false
        backGroundAskPasswordView.isHidden = false
    }
    
    func closeAskPasswordView() {
        askPasswordView.isHidden = true
        backGroundAskPasswordView.isHidden = true
    }
    
}

extension SettingVC:MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch result.rawValue {
            case MessageComposeResult.cancelled.rawValue:break
            case MessageComposeResult.failed.rawValue:break
            case MessageComposeResult.sent.rawValue:break
            default:break
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func sendMessage(phoneNumber: String, content: String) {
        let messageVC = MFMessageComposeViewController()
        messageVC.messageComposeDelegate = self
        messageVC.body = content
        messageVC.recipients = [phoneNumber]
        messageVC.modalPresentationStyle = .fullScreen
        self.present(messageVC, animated: false, completion: nil)
        
    }
    
}





