//
//  DetailHistoryViewController.swift
//  SmartMotor
//
//  Created by Toan on 2/1/21.
//  Copyright © 2021 vietnv2. All rights reserved.
//

import UIKit
import Mapbox
class DetailHistoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    
    var arrAnotation : [Station]!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var tableDetail: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
        self.initTable()
//        if (arrAnotation.count > 0){
//        let distance =  CLLocation.distanceMotor(from: self.arrAnotation.first!.coordinate , to: self.arrAnotation.last!.coordinate)
//            self.lblDistance.text = "Tổng quãng đường : \(Double(distance).roundToDecimal(2).convert(from: .meters, to: .kilometers)) km"
//        }
        var totalDistance = 0.0
       
        for i in 1..<arrAnotation.count{
        
            totalDistance += Double(CLLocation.distanceMotor(from: self.arrAnotation[i-1].coordinate , to: self.arrAnotation[i].coordinate)).roundToDecimal(2).convert(from: .meters, to: .kilometers)

        }
           self.lblDistance.text = "Tổng quãng đường : \(Double(totalDistance).roundToDecimal(2)) km"
    }
    
    
    func initView(){
        self.navigationItem.title = "Chi Tiết Hành Tình"
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.barTintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        UINavigationBar.appearance().barTintColor = UIColor(red: 234.0/255.0, green: 46.0/255.0, blue: 73.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        let image = UIImage(named: "BacgroundView")
        self.navigationController?.navigationBar.setBackgroundImage(image, for: .default)
        let backButton =  UIButton()
        backButton.setImage(UIImage(named: "iconBack"), for: .normal)
        backButton.addTarget(self, action: #selector(self.backTap(sender:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        
    }
    @objc func backTap(sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    func initTable(){
//        self.tableDetail.delegate = self
//        self.tableDetail.dataSource = self
        self.tableDetail.register(UINib(nibName: "DetailCell", bundle: nil), forCellReuseIdentifier: "DetailCell")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAnotation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell" , for: indexPath) as! DetailCell
        cell.setupDate(station: self.arrAnotation[indexPath.row])
        if(self.arrAnotation.count  > 0){
            cell.arrStation = self.arrAnotation
            cell.numberOfRow = indexPath.row
        }
        if (indexPath.row % 2 == 0 ){
            cell.backgroundColor = UIColor.init(red: 234, green: 234, blue: 234)
        }else{
            cell.backgroundColor = .white
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

}
extension CLLocation {

    class func distanceMotor(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D) -> CLLocationDistance {
        let from = CLLocation(latitude: from.latitude, longitude: from.longitude)
        let to = CLLocation(latitude: to.latitude, longitude: to.longitude)
        return from.distance(from: to)
    }
}
extension UIColor {
   convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")

       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
   }

   convenience init(rgb: Int) {
       self.init(
           red: (rgb >> 16) & 0xFF,
           green: (rgb >> 8) & 0xFF,
           blue: rgb & 0xFF
       )
   }
}
extension Double {
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
    func convert(from originalUnit: UnitLength, to convertedUnit: UnitLength) -> Double {
      return Measurement(value: self, unit: originalUnit).converted(to: convertedUnit).value
    }
}
