//
//  SettingTextField.swift
//  SmartMotor
//
//  Created by vietnv2 on 12/5/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

extension SettingVC: UITextFieldDelegate {
    func initTextField() {
        txtFence.keyboardType = UIKeyboardType.numberPad
        txtSpeed.keyboardType = UIKeyboardType.numberPad
        txtOldPassword_ChangePassword.keyboardType = UIKeyboardType.numberPad
        txtNewPassword_ChangePassword.keyboardType = UIKeyboardType.numberPad
        txtConfirmNewPassword_ChangePassword.keyboardType = UIKeyboardType.numberPad
        txtOldPassword_Registry.keyboardType = UIKeyboardType.numberPad
        txtNewPassword_Registry.keyboardType = UIKeyboardType.numberPad
        txtConfirmNewPassword_Registry.keyboardType = UIKeyboardType.numberPad
        txtVehiclePassword.keyboardType = UIKeyboardType.numberPad
        
//        addNextButtonOnKeyboard()
//        addDoneButtonOnKeyboard()
        //
        txtOldPassword_ChangePassword.delegate = self
        txtNewPassword_ChangePassword.delegate = self
        txtConfirmNewPassword_ChangePassword.delegate = self
        txtOldPassword_Registry.delegate = self
        txtNewPassword_Registry.delegate = self
        txtConfirmNewPassword_Registry.delegate = self
        
        txtVehiclePassword.delegate = self
        txtSpeed.delegate = self
        txtFence.delegate = self
        _ = textFieldShouldReturn(txtOldPassword_ChangePassword)
        _ = textFieldShouldReturn(txtNewPassword_ChangePassword)
        _ = textFieldShouldReturn(txtConfirmNewPassword_ChangePassword)
        
        _ = textFieldShouldReturn(txtOldPassword_Registry)
        _ = textFieldShouldReturn(txtNewPassword_Registry)
        _ = textFieldShouldReturn(txtConfirmNewPassword_Registry)
        
        _ = textFieldShouldReturn(txtOldPassword_Registry)
        _ = textFieldShouldReturn(txtNewPassword_Registry)
        _ = textFieldShouldReturn(txtConfirmNewPassword_Registry)
        _ = textFieldShouldReturn(txtVehiclePassword)
        _ = textFieldShouldReturn(txtSpeed)
        _ = textFieldShouldReturn(txtFence)
        txtFence.text = "200"
        txtSpeed.text = "60"
    }
    
    func contraintDefault() {
        accAlarmContraint.constant = (heightScreen - accAlarmSettingView.frame.size.height)/2
        vibrateAlarmContraint.constant = (heightScreen - vibrateAlarmSettingView.frame.size.height)/2
        fenceAlarmContraint.constant = (heightScreen - fenceAlarmSettingView.frame.size.height)/2
        speedAlarmContraint.constant = (heightScreen - speedAlarmSettingView.frame.size.height)/2
        changePasswordContraint.constant = (heightScreen - changePasswordView.frame.size.height)/2
        vehicleRegistryViewContraint.constant = (heightScreen - vehicleRegistryView.frame.size.height)/2
        askPasswordContraint.constant = (heightScreen - askPasswordView.frame.size.height)/2
    }
    
//    func addNextButtonOnKeyboard() {
//        let nextToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
//        nextToolbar.barStyle       = UIBarStyle.default
//        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
//        let next: UIBarButtonItem  = UIBarButtonItem(title: "Next", style: UIBarButtonItemStyle.done, target: self, action: #selector(SettingVC.doneButtonAction))
//        var items = [UIBarButtonItem]()
//        items.append(flexSpace)
//        items.append(next)
//        nextToolbar.items = items
//        nextToolbar.sizeToFit()
//        txtOldPassword_ChangePassword.inputAccessoryView = nextToolbar
//        txtNewPassword_ChangePassword.inputAccessoryView = nextToolbar
//        txtOldPassword_Registry.inputAccessoryView = nextToolbar
//        txtNewPassword_Registry.inputAccessoryView = nextToolbar
//    }
//    
//    func addDoneButtonOnKeyboard() {
//        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
//        doneToolbar.barStyle       = UIBarStyle.default
//        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
//        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(SettingVC.doneButtonAction))
//        var items = [UIBarButtonItem]()
//        items.append(flexSpace)
//        items.append(done)
//        doneToolbar.items = items
//        doneToolbar.sizeToFit()
//        txtFence.inputAccessoryView = doneToolbar
//        txtSpeed.inputAccessoryView = doneToolbar
//        txtConfirmNewPassword_ChangePassword.inputAccessoryView = doneToolbar
//        txtConfirmNewPassword_Registry.inputAccessoryView = doneToolbar
//    }
//    
//    func doneButtonAction() {
//        switch textFieldCurrent {
//        case txtOldPassword_ChangePassword: txtNewPassword_ChangePassword.becomeFirstResponder()
//        case txtNewPassword_ChangePassword: txtConfirmNewPassword_ChangePassword.becomeFirstResponder()
//        case txtConfirmNewPassword_ChangePassword: txtConfirmNewPassword_ChangePassword.resignFirstResponder()
//        case txtOldPassword_Registry: txtNewPassword_Registry.becomeFirstResponder()
//        case txtNewPassword_Registry: txtConfirmNewPassword_Registry.becomeFirstResponder()
//        case txtConfirmNewPassword_Registry: txtConfirmNewPassword_Registry.resignFirstResponder()
//        default: break
//        }
//    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool { // called when 'return' key pressed. return false to ignore.
        textField.resignFirstResponder()
        keyBoardHide()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var distance:CGFloat!
        
        textFieldCurrent = textField
        switch textField {
        case txtFence:
            distance = (heightScreen - fenceAlarmSettingView.frame.size.height - keyboardHeight)/2
            if distance >= 20 {
                fenceAlarmContraint.constant = distance
            }else {
                fenceAlarmContraint.constant = 0
            }
            flagStartEndEditForTextField = flagStartEndEditForTextField + 1
            break
        case txtSpeed:
            distance = (heightScreen - speedAlarmSettingView.frame.size.height - keyboardHeight)/2
            if distance >= 20 {
                speedAlarmContraint.constant = distance
            }else {
                speedAlarmContraint.constant = 0
            }
            flagStartEndEditForTextField = flagStartEndEditForTextField + 1
            break
        case txtOldPassword_Registry,txtNewPassword_Registry,txtConfirmNewPassword_Registry:
            distance = (heightScreen - vehicleRegistryView.frame.size.height - keyboardHeight)/2
            if distance >= 20 {
                vehicleRegistryViewContraint.constant = distance
            }else {
                vehicleRegistryViewContraint.constant = 0
            }
            flagStartEndEditForTextField = flagStartEndEditForTextField + 1
            switch textField {
                case txtOldPassword_Registry:btnSecurityOldPasswordVehicleRegistry.isHidden = false
                case txtNewPassword_Registry:btnSecurityNewPasswordVehicleRegistry.isHidden = false
                case txtConfirmNewPassword_Registry:btnSecurityConfirmNewPasswordVehicleRegistry.isHidden = false
                default:break
            }
            break
        case txtOldPassword_ChangePassword,txtNewPassword_ChangePassword,txtConfirmNewPassword_ChangePassword:
            distance = (heightScreen - changePasswordView.frame.size.height - keyboardHeight)/2
            if distance >= 20 {
                changePasswordContraint.constant = distance
            }else {
                changePasswordContraint.constant = 0
            }
            flagStartEndEditForTextField = flagStartEndEditForTextField + 1
            switch textField {
                case txtOldPassword_ChangePassword:btnSecurityOldPasswordInChangePassword.isHidden = false
                case txtNewPassword_ChangePassword:btnSecurityNewPasswordInChangePassword.isHidden = false
                case txtConfirmNewPassword_ChangePassword:btnSecurityConfirmNewPasswordInChangePassword.isHidden = false
                default:break
            }
            break
        case txtVehiclePassword:
            btnSecurityVehiclePassword.isHidden = false
            distance = (heightScreen - askPasswordView.frame.size.height - keyboardHeight)/2
            if distance >= 20 {
                askPasswordContraint.constant = distance
            }else {
                askPasswordContraint.constant = 0
            }
            flagStartEndEditForTextField = flagStartEndEditForTextField + 1
            break
        default:
            break
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case txtFence:
            flagStartEndEditForTextField = flagStartEndEditForTextField - 1
            fenceAlarmContraint.constant = heightScreen/2 - fenceAlarmSettingView.frame.size.height/2
            break
        case txtSpeed:
            flagStartEndEditForTextField = flagStartEndEditForTextField - 1
            speedAlarmContraint.constant = heightScreen/2 - speedAlarmSettingView.frame.size.height/2
            break
        case txtOldPassword_Registry,txtNewPassword_Registry,txtConfirmNewPassword_Registry:
            flagStartEndEditForTextField = flagStartEndEditForTextField - 1
            if flagStartEndEditForTextField == 0 {
                vehicleRegistryViewContraint.constant = heightScreen/2 - vehicleRegistryView.frame.size.height/2
            }
            break
        case txtOldPassword_ChangePassword,txtNewPassword_ChangePassword,txtConfirmNewPassword_ChangePassword:
            flagStartEndEditForTextField = flagStartEndEditForTextField - 1
            if flagStartEndEditForTextField == 0 {
                changePasswordContraint.constant = heightScreen/2 - changePasswordView.frame.size.height/2
            }
            break
        case txtVehiclePassword:
            flagStartEndEditForTextField = flagStartEndEditForTextField - 1
            if flagStartEndEditForTextField == 0 {
                askPasswordContraint.constant = heightScreen/2 - askPasswordView.frame.size.height/2
            }
            break
        default:
            break
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtVehiclePassword || textField == txtOldPassword_ChangePassword || textField == txtNewPassword_ChangePassword || textField == txtConfirmNewPassword_ChangePassword || textField == txtOldPassword_Registry || textField == txtNewPassword_Registry || textField == txtConfirmNewPassword_Registry {
            let text = textField.text
            let newLength = (text?.characters.count)! + string.characters.count - range.length
            return newLength <= 6
        }
        if textField == txtFence {
            var startString = ""
            if (textField.text != nil)
            {
                startString += textField.text!
            }
            startString += string
            let limitNumber = Int(startString)
            if limitNumber! > 99999 {  // 100 - 10000
                return false
            }
            else {
                return true;
            }
        }
        if textField == txtSpeed {
            var startString = ""
            if (textField.text != nil)
            {
                startString += textField.text!
            }
            startString += string
            let limitNumber = Int(startString)
            if limitNumber! > 999 { // 20 - 150
                return false
            }
            else {
                return true;
            }
        }
        return true
    }

    // Mark: Keyboard
    func getKeyBoardHeight() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func keyBoardHide() {
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }

    
}


