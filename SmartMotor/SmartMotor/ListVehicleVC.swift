//
//  ListVehicleVC.swift
//  Smart Motor
//
//  Created by vietnv2 on 18/10/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

class ListVehicleVC: UIViewController {
    @IBOutlet weak var listChargeMoneyTypeTableView: UITableView!
    @IBOutlet weak var listMonth2PayTableView: UITableView!
    @IBOutlet weak var listVehicleTableView: UITableView!
    @IBOutlet weak var chooseNumberOfMonthToPayView: UIView!
    @IBOutlet weak var sosView: UIView!
    @IBOutlet weak var hotlineView: UIView!
    @IBOutlet weak var accountInfoView: UIView!
    @IBOutlet weak var listChargeMoneyView: UIView!
    @IBOutlet weak var warningHotlineView: UIView!
    @IBOutlet weak var chargeCardCodeView: UIScrollView!
    @IBOutlet weak var payMoneyView: UIScrollView!
    @IBOutlet weak var titleListView: UIView!
    @IBOutlet weak var warningLogoutView: UIView!
    
    @IBOutlet weak var warningLoadListVehicleView: UIView!
    
    
    
    
    @IBOutlet weak var searchVehicleContraint: NSLayoutConstraint! //50
    @IBOutlet weak var listViewContraint: NSLayoutConstraint! //340
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var backgroundChargeMoneyView: UIView!
    @IBOutlet weak var listVehicleView: UIView!
    @IBOutlet weak var isForbiddenAlertView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var imgIconSearchOnTitle: UIImageView!
    @IBOutlet weak var imgCaptchaChargeCode: UIImageView!
    @IBOutlet weak var imgCaptchaPayMoney: UIImageView!
    @IBOutlet weak var imgRefressCaptchaChargeCode: UIImageView!
    @IBOutlet weak var btnCloseListChargeMoneyType: UIButton!
    @IBOutlet weak var btnChooseNumberOfMonth2Pay: UIButton!
    @IBOutlet weak var btnChargeMoney: UIButton!
    @IBOutlet weak var btnInfomation: UIButton!
    @IBOutlet weak var btnDangXuat: UIButton!
    @IBOutlet weak var btnSOS: UIButton!
    @IBOutlet weak var txtCaptchaCardCode: UITextField!
    @IBOutlet weak var txtCaptchaPayMoney: UITextField!
    @IBOutlet weak var txtSearchVehicle: UITextField!
    @IBOutlet weak var txtChargeCode: UITextField!
    @IBOutlet weak var txtMoneyPay: UITextField!
    @IBOutlet weak var txtTestHyperLink: UITextField!
    @IBOutlet weak var lblVehiclePhoneNumber: UILabel!
    @IBOutlet weak var lblDisplayRegNo: UILabel!
    @IBOutlet weak var lblPayMoneyInfo: UILabel!
    @IBOutlet weak var lblSmsPromotion: UILabel!
    @IBOutlet weak var lblBasicBalance: UILabel!
    @IBOutlet weak var lblGprsFreePromotion: UILabel!
    @IBOutlet weak var lblMoneyPromotion: UILabel!
    @IBOutlet weak var lblSmsNgoaiMang: UILabel!
    @IBOutlet weak var lblTimeCallNgoaiMang: UILabel!
    @IBOutlet weak var lblIForbiddenAlert: UILabel!
    @IBOutlet weak var chargeCardCodeViewContraint: NSLayoutConstraint!
    @IBOutlet weak var chargeCardCodeViewSizeContraint: NSLayoutConstraint!
    @IBOutlet weak var payMoneyViewContraint: NSLayoutConstraint!
    @IBOutlet weak var payMoneyViewSizeContraint: NSLayoutConstraint!
    
    @IBOutlet weak var confirmChargeCardCodeView: UIView!
    @IBOutlet weak var confirmPayMoneyView: UIView!
    
    @IBOutlet weak var lblConfirmChargeCardCode: UILabel!
    @IBOutlet weak var lblConfirmPayMoney: UILabel!
    
    //
    let lblHotline = ActiveLabel()
    let payMoneyInfo:[String] = ["Khi thực hiện chức năng này, hệ thống sẽ trừ tài khoản chính của thuê bao ",
                                 ". Số tiền này sẽ được cộng vào tài khoản sim ",
                                 ", gắn trên thiết bị ",
                                 " trên xe ",
                                 ".\nSau khi thao tác, bạn vào chức năng Thông tin tài khoản thiết bị để kiểm tra."]
    let motorStateImage:[String] = ["icon_sm_no_info.png",
                                    "icon_sm_run.png",
                                    "icon_sm_stop.png",
                                    "icon_sm_park.png",
                                    "icon_sm_lost_gps.png",
                                    "icon_sm_hibernate.png",
                                    "icon_sm_lost_gprs.png",
                                    "icon_sm_no_info.png"]
    let listChargeMoneyString:[String] = ["Thanh toán qua thẻ cào","Thanh toán qua tài khoản điện thoại"]
    //let moneyPayArray:[String] = ["30", "60", "90", "180", "360"]
    //let numberOfMonth:[String] = ["1", "2", "3", "6", "12"]
    let numberOfMonth:[Int] = [1, 2, 3, 6, 12]
    let cannotLoadListVehicle:String! = "Không tải được danh sách xe!"
    let connectionFail:String = "Lỗi kết nối. Xin vui lòng thử lại sau!"
    let noVehicleChoose:String! = "CHƯA CHỌN PHƯƠNG TIỆN"
    //let sureLogoutQuestion:String! = "Bạn có chắc muốn thoát khỏi chương trình không ?"
    let warning:String! = "Cảnh báo"
    let yes:String! = "Có"
    let no:String! = "Không"
    let noDataString:String = "Không có dữ liệu!"
    var monthPay:String! = "1"
    var moneyPay:String! = "30000"
    var loadingDataInListVCFlag:Bool! = false
    var searchVehicleFlag:Bool = false
    var endOfListFlag:Bool = false
    var requestedPhone:String!
    var deviceCode:String!
    var deviceSim:String!
    var monthlyFee:Int! = 0
    var registerNo:String!
    var cookieName:String!
    var cookieValue:String!
    var session = URLSession()
    var totalOfVehicle:Int! = 0
    var tapReturnToSearchFlag:Bool! = false
    var chargeCardCodeViewHeight:CGFloat!
    var payMoneyViewHeight:CGFloat!
    var location = CGPoint(x: 0, y: 0)
    //var timerIsForbidden:Timer!
    //var timerIsForbiddenFlag:Bool! = false
    var timerGoToLogin:Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initListVehicleVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadListVehicle()
        getIPAddress()
    }
    
    func initListVehicleVC() {
        initVar()
        initTableView()
        initTextField()
        initURLSession()
        initHotline()
        initTapInBackgroundView()
        
    }
    
    func initVar() {
        closeViewUpperBackground()
        buttonView.isHidden = false
        warningLogoutView.isHidden = true
        warningHotlineView.isHidden = true
        confirmChargeCardCodeView.isHidden = true
        confirmPayMoneyView.isHidden = true
        warningLoadListVehicleView.isHidden = true
        //
        imgCaptchaChargeCode.isUserInteractionEnabled = true //image has been tapped
        imgCaptchaChargeCode.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ListVehicleVC.didTapImage) ))
        imgCaptchaPayMoney.isUserInteractionEnabled = true   //image has been tapped
        imgCaptchaPayMoney.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ListVehicleVC.didTapImage) ))
        imgRefressCaptchaChargeCode.isUserInteractionEnabled = true //image has been tapped
        imgRefressCaptchaChargeCode.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ListVehicleVC.didTapImage) ))
//        self.titleListView.addBackground(width: UIScreen.main.bounds.size.width,height: titleViewHeight, image: titleViewImage)
        //
        chargeCardCodeViewHeight = chargeCardCodeView.frame.size.height
        payMoneyViewHeight = payMoneyView.frame.size.height
        vehicleInformations.removeAll()
        registerNo2Query = txtSearchVehicleTextField
        txtSearchVehicle.text = txtSearchVehicleTextField
        numberOfVehcle = 0
        pageIndex = 1
        searchVehicleFlag = false
        if vehicle2showIndex == nil {
            lblDisplayRegNo.text = noVehicleChoose
            buttonView.isHidden = true
            return
        }
        if vehicle2show == nil {
            buttonView.isHidden = true
            return
        }
        if txtSearchVehicleTextField != "" {
            tapReturnToSearchFlag = true
        }
        lblDisplayRegNo.text = "XE ĐIỀU KHIỂN: " + vehicle2show.regNo
    }
    
//    func initTimerIsForbidden() {
//        if timerIsForbidden != nil {
//            timerIsForbidden.invalidate()
//            timerIsForbidden = nil
//            return
//        }
//        timerIsForbiddenFlag = true
//        timerIsForbidden = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(stopIsForbidden), userInfo: nil, repeats: true)
//    }
//    
//    func stopIsForbidden() {
//        timerIsForbiddenFlag = false
//        if timerIsForbidden != nil {
//            timerIsForbidden.invalidate()
//            timerIsForbidden = nil
//            return
//        }
//    }
    
    @objc func didTapImage(gesture: UIGestureRecognizer) {
        urlSessionDelegate()
    }
    
    // Mark:
    @IBAction func btnSOSTapped(_ sender: Any) {
        backgroundChargeMoneyView.isHidden = false
        backgroundView.isHidden = false
        sosView.isHidden = false
    }
    
    @IBAction func btnCancelSOSTapped(_ sender: Any) {
        backgroundChargeMoneyView.isHidden = true
        backgroundView.isHidden = true
        listChargeMoneyView.isHidden = true
        sosView.isHidden = true
    }
    
    @IBAction func btnCloseListChargeMoneyTypeTapped(_ sender: Any) {
        backgroundChargeMoneyView.isHidden = true
        backgroundView.isHidden = true
        listChargeMoneyView.isHidden = true
    }

    @IBAction func btnChooseNumberOfMonth2PayTapped(_ sender: Any) {
        chooseNumberOfMonthToPayView.isHidden = false
    }
    
    @IBAction func btnInfomationTapped(_ sender: Any) {
        accountInfo()
    }
    
    @IBAction func btnLogoutTapped(_ sender: AnyObject) {
        warningLogoutView.isHidden = false
        backgroundView.isHidden = false
        backgroundChargeMoneyView.isHidden = false
    }
    
    @IBAction func btnCancelAccountInfoTapped(_ sender: Any) {
        backgroundChargeMoneyView.isHidden = true
        backgroundView.isHidden = true
        listChargeMoneyView.isHidden = true
        accountInfoView.isHidden = true
    }
    
    // Mark: isForbidden
    @IBAction func btnIsForbiddenChargeMoneyTapped(_ sender: Any) {
        isForbiddenAlertView.isHidden = true
        backgroundChargeMoneyView.isHidden = false
        backgroundView.isHidden = false
        listChargeMoneyView.isHidden = false
    }
    
    @IBAction func btnIsForbiddenAlertCancelTapped(_ sender: Any) {
        isForbiddenAlertView.isHidden = true
        backgroundChargeMoneyView.isHidden = true
        backgroundView.isHidden = true
    }
    
    @IBAction func btnConfirmLogoutTapped(_ sender: Any) {
        warningLogoutView.isHidden = true
        backgroundView.isHidden = true
        backgroundChargeMoneyView.isHidden = true
        txtSearchVehicleTextField = ""
        txtSearchVehicle.text = ""
        logout()
    }
    
    @IBAction func btnCancelLogoutTapped(_ sender: Any) {
        warningLogoutView.isHidden = true
        backgroundView.isHidden = true
        backgroundChargeMoneyView.isHidden = true
    }
    
    // Mark: Alert confirm logout
//    func confirmLogout(message:String) {
//        let alert = UIAlertController(title: warning, message: message, preferredStyle: .alert)
//        let ok = UIAlertAction(title: yes, style: .default, handler: { (action) -> Void in
//            self.logout()
//        })
//        alert.addAction(ok)
//        let cancel = UIAlertAction(title: no, style: .default) { (action) -> Void in
//        }
//        alert.addAction(cancel)
//        //
//        let paragraphStyle = NSMutableParagraphStyle()
//        paragraphStyle.alignment = NSTextAlignment.center
//        
//        let messageText = NSMutableAttributedString(
//            string: message,
//            attributes: [
//                NSParagraphStyleAttributeName: paragraphStyle,
//                NSFontAttributeName: UIFont.systemFont(ofSize: 17.0)
//            ]
//        )
//        alert.setValue(messageText, forKey: "attributedMessage")
//        //
//        present(alert, animated: true, completion: nil)
//    }
    
    // Mark: Alert to retry load list vehicle
    func retryLoadListVehicle() {
        warningLoadListVehicleView.isHidden = false
        backgroundChargeMoneyView.isHidden = false
        backgroundView.isHidden = false
    }
    
    @IBAction func btnRetryLoadListVehicleTapped(_ sender: Any) {
        warningLoadListVehicleView.isHidden = true
        backgroundChargeMoneyView.isHidden = true
        backgroundView.isHidden = true
        loadListVehicle()
    }
    
    @IBAction func btnCancelRetryLoadListVehicleTapped(_ sender: Any) {
        warningLoadListVehicleView.isHidden = true
        backgroundChargeMoneyView.isHidden = true
        backgroundView.isHidden = true
    }
    
    func closeContainerChargeAndPayMoneyView() {
        backgroundChargeMoneyView.isHidden = true
        backgroundView.isHidden = true
        listChargeMoneyView.isHidden = true
        chargeCardCodeView.isHidden = true
        payMoneyView.isHidden = true
        confirmChargeCardCodeView.isHidden = true
        confirmPayMoneyView.isHidden = true
        hideKeyBoad()
    }
    
    // Mark: Hotline
    func initHotline() {
        let customType = ActiveType.custom(pattern: "\\s18008098\\b") //Looks for "are"
        lblHotline.enabledTypes.append(customType)
        lblHotline.urlMaximumLength = 31
        lblHotline.customize { label in
            lblHotline.text = "3. Liên hệ với Viettel thông qua hotline 18008098 (Miễn phí)."
            lblHotline.textColor = UIColor(red: 20.0/255, green: 71.0/255, blue: 110.0/255, alpha: 1)
            lblHotline.numberOfLines = 0
            lblHotline.lineSpacing = 4
            lblHotline.lineBreakMode = NSLineBreakMode.byWordWrapping
            lblHotline.customColor[customType] = UIColor.red
            lblHotline.customSelectedColor[customType] = UIColor.blue
            lblHotline.handleCustomTap(for: customType) { _ in self.confirmCallToHotline() }
        }
        lblHotline.frame = CGRect(x: 0, y: 0, width: view.frame.size.width*0.9 - 30, height: 45)
        lblHotline.textAlignment = NSTextAlignment.left
        hotlineView.addSubview(lblHotline)
    }
    
    func call2ToHotline() {
        let phoneUrl = "tel://\(18008098)"
        let url:URL = URL(string: phoneUrl)!
        UIApplication.shared.openURL(url)
    }
    
    @IBAction func btnConfirmCallHotlineTapped(_ sender: Any) {
        backgroundChargeMoneyView.isHidden = true
        backgroundView.isHidden = true
        sosView.isHidden = true
        warningHotlineView.isHidden = true
        self.call2ToHotline()
    }
    
    @IBAction func btnCancelCallHotlineTapped(_ sender: Any) {
        backgroundChargeMoneyView.isHidden = true
        backgroundView.isHidden = true
        sosView.isHidden = true
        warningHotlineView.isHidden = true
    }

    
    func confirmCallToHotline() {
        sosView.isHidden = true
        warningHotlineView.isHidden = false
    }
    
    // Mark: touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //init image has been tapped
        let touch:UITouch = touches.first!
        if touch.view == imgCaptchaChargeCode || touch.view == imgCaptchaPayMoney || touch.view == imgRefressCaptchaChargeCode {
            urlSessionDelegate()
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        hideKeyBoad()
    }
    
    // Mark: Request data
    func loadListVehicle() {
        Loading().showLoading()
        if tokenSave == nil || userIdSave == nil {
            gotoLoginView()
        }
        var urlString = smartmotorUrl + listTransportUrl + tokenSave + "/" + userIdSave + listTransportRegNoUrl + registerNo2Query + listTransportPageIndexUrl + String(pageIndex)
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        let url = URL(string: urlString)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        //
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        //
        let task = defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
            Loading().hideLoading()
            if error != nil {
                self.retryLoadListVehicle()
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    Alert.alertAutoDismiss(delegate: self, message: self.cannotLoadListVehicle)
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)) as! NSDictionary
                    let resultCode = jsonResult["resultCode"] as! Int
                    if resultCode != 1 {
                        self.gotoLoginView()
                        return
                    }
                    self.totalOfVehicle = jsonResult["total"] as! Int
                    if self.totalOfVehicle == 0 && self.searchVehicleFlag == true {
                        numberOfVehcle = 0
                        Alert.alertAutoDismiss(delegate: self, message: "Không có xe nào được tìm thấy!")
                        self.listVehicleTableView.reloadData()
                        return
                    }
                    
                    let jsonData:NSArray! = jsonResult["data"] as! NSArray
                    if jsonData.count == 0 {
                        Alert.alertAutoDismiss(delegate: self, message: self.noDataString)
                        return
                    }
                    if numberOfVehcle > self.totalOfVehicle {
                        vehicleInformations.removeAll()
                    }
                    if numberOfVehcle < pageIndex*10 {
                        for element in jsonData {
                            let result = element as AnyObject
                            vehicleInformations.append(self.parseJson2GetListVehicle(result: result))
                            numberOfVehcle = vehicleInformations.count
                        }
                    }
                    
                    if self.loadingDataInListVCFlag == true {
                        self.loadingDataInListVCFlag = false
                    }
                    if numberOfVehcle%10 > 0 {
                        self.endOfListFlag = true
                    }
                    self.listVehicleTableView.reloadData()
                    if numberOfVehcle == 1 {
                        vehicle2show = vehicleInformations[0]
                        if vehicle2show.isForbidden == true {
                            self.backgroundChargeMoneyView.isHidden = false
                            self.backgroundView.isHidden = false
                            self.isForbiddenAlertView.isHidden = false
                        }
                    }
                    if loginFlag == true {
                        loginFlag = false
                        if numberOfVehcle == 1 && self.searchVehicleFlag == false {
                            vehicle2show = vehicleInformations[0]
                            if vehicle2show.isForbidden == true {
                                return
                            }
                            vehicle2showIndex = 0
                            self.gotoMapView()
                        }
                    }
                }catch {
                    Alert.alertAutoDismiss(delegate: self, message: "Lỗi kết nối. Vui lòng thử lại sau!")
                    fatalError("Failure\(error)")
                }
            }else {
                Alert.alertAutoDismiss(delegate: self, message: self.cannotLoadListVehicle)
            }
        }
        task.resume()
    }
    
    func logout() {
        Loading().showLoading()
        if tokenSave == nil || userIdSave == nil {
            gotoLoginView()
        }
        // https GET
        let url = URL(string: smartmotorUrl + logoutUrl + userIdSave + "/" + tokenSave )
        //
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
       
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        
        let task = defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
            Loading().hideLoading()
            if error != nil {
                Alert.alertAutoDismiss(delegate: self, message: self.connectionFail)
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    Alert.alertAutoDismiss(delegate: self, message: self.connectionFail)
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let result = jsonResult["resultCode"] as! Int
                    if result != 1 {
                        Alert.alertAutoDismiss(delegate: self, message: "Đăng xuất không thành công!")
                        return
                    }
                    self.gotoLoginView()
                }catch {
                    Alert.alertAutoDismiss(delegate: self, message: "Lỗi kết nối. Vui lòng thử lại sau!")
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    func accountInfo() {
        Loading().showLoading()
        if tokenSave == nil || userIdSave == nil {
            gotoLoginView()
        }
        // https GET
        let url = URL(string: smartmotorUrl + getAccountInfoUrl + tokenSave + "/" + userIdSave + "/" + vehicle2show.sim)
        //
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        
        let task = defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
            Loading().hideLoading()
            if error != nil {
                Alert.alertAutoDismiss(delegate: self, message: self.connectionFail)
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    Alert.alertAutoDismiss(delegate: self, message: self.connectionFail)
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let resultCode = jsonResult["resultCode"] as! Int
                    if resultCode != 1 {
                        Alert.alertAutoDismiss(delegate: self, message: "Lấy tài khoản không thành công!")
                        return
                    }
                    self.lblSmsPromotion.text = jsonResult["smsPromotion"] as? String
                    self.lblBasicBalance.text = jsonResult["basicBalance"] as? String
                    self.lblGprsFreePromotion.text = jsonResult["gprsFreePromotion"] as? String
                    self.lblMoneyPromotion.text = jsonResult["moneyPromotion"] as? String
                    self.lblSmsNgoaiMang.text = jsonResult["smsNgoaiMang"] as? String
                    self.lblTimeCallNgoaiMang.text = jsonResult["timePromotion"] as? String
                    self.backgroundChargeMoneyView.isHidden = false
                    self.backgroundView.isHidden = false
                    self.accountInfoView.isHidden = false
                    //Loading().hideLoading()
                }catch {
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
        
    // Mark: Parse data
    func parseJson2GetListVehicle(result: AnyObject) -> vehicleInformation {
        var resultReturn: vehicleInformation!
        var gpsDate:String! = ""
        var isForbidden:Bool! = false
        let accIllegalState = result.value(forKeyPath: vehicleInfoString.accIllegalState.rawValue) as! Int
        let devicePin = result.value(forKeyPath: vehicleInfoString.devicePin.rawValue) as! String
        let deviceType = result.value(forKeyPath: vehicleInfoString.deviceType.rawValue) as! Int
        let deviceTypeIdOrginal = result.value(forKeyPath: vehicleInfoString.deviceTypeIdOrginal.rawValue) as! Int
        let factory = result.value(forKeyPath: vehicleInfoString.factory.rawValue) as! String
        if let _gpsDate = (result.value(forKeyPath: vehicleInfoString.gpsDate.rawValue) as? String) {
            gpsDate = _gpsDate
        }
        let gpsSpeed = result.value(forKeyPath: vehicleInfoString.gpsSpeed.rawValue) as! Int
        let gpsState = result.value(forKeyPath: vehicleInfoString.gpsState.rawValue) as! Int
        let groupsCode = result.value(forKeyPath: vehicleInfoString.groupsCode.rawValue) as! String
        let id = result.value(forKeyPath: vehicleInfoString.id.rawValue) as! Int
        let illegalMoveState = result.value(forKeyPath: vehicleInfoString.illegalMoveState.rawValue) as! Int
        let lat = result.value(forKeyPath: vehicleInfoString.lat.rawValue) as! Double
        let lng = result.value(forKeyPath: vehicleInfoString.lng.rawValue) as! Double
        let lowBatteryState = result.value(forKeyPath: vehicleInfoString.lowBatteryState.rawValue) as! Int
        let motoSpeed = result.value(forKeyPath: vehicleInfoString.motoSpeed.rawValue) as! Int
        let offPowerState = result.value(forKeyPath: vehicleInfoString.offPowerState.rawValue) as! Int
        let regNo = result.value(forKeyPath: vehicleInfoString.regNo.rawValue) as! String
        let sim = result.value(forKeyPath: vehicleInfoString.sim.rawValue) as! String
        let sosState = result.value(forKeyPath: vehicleInfoString.sosState.rawValue) as! Int
        let state = result.value(forKeyPath: vehicleInfoString.state.rawValue) as! Int
        let timeState = result.value(forKeyPath: vehicleInfoString.timeState.rawValue) as! Int
        let type = result.value(forKeyPath: vehicleInfoString.type.rawValue) as! String
        let vibrationState = result.value(forKeyPath: vehicleInfoString.vibrationState.rawValue) as! Int
        if let _isForbidden = result.value(forKeyPath: vehicleInfoString.isForbidden.rawValue) as? Bool {
            isForbidden = _isForbidden
        }
        resultReturn = vehicleInformation(accIllegalState:accIllegalState,devicePin:devicePin,deviceType:deviceType,deviceTypeIdOrginal:deviceTypeIdOrginal,factory:factory,gpsDate:gpsDate,gpsSpeed:gpsSpeed,gpsState:gpsState,groupsCode:groupsCode,id:id,illegalMoveState:illegalMoveState,lat:lat,lng:lng,lowBatteryState:lowBatteryState,motoSpeed:motoSpeed,offPowerState:offPowerState,regNo:regNo,sim:sim,sosState:sosState,state:state,timeState:timeState,type:type,vibrationState:vibrationState,isForbidden:isForbidden)
        return resultReturn
    }
    
    // Mark:
    func gotoLoginView() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "login") as! LoginVC
        logoutFlag = true
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }

    func gotoMapView() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "map") as! MapVC
        registerNo2Query = vehicle2show.regNo
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    // Mark: Keyboard
    func hideKeyBoad() {
        self.view.endEditing(true)
    }
        
    // Mark: Tap on background to close warning
    func initTapInBackgroundView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(ListVehicleVC.handleBackgroundViewTap(_:)))
        backgroundView.addGestureRecognizer(tap)
    }
    
    @objc func handleBackgroundViewTap(_ sender: UITapGestureRecognizer) {
        closeViewUpperBackground()
    }
    
    func closeViewUpperBackground() {
        backgroundView.isHidden = true
        listChargeMoneyView.isHidden = true
        chooseNumberOfMonthToPayView.isHidden = true
        backgroundChargeMoneyView.isHidden = true
        chargeCardCodeView.isHidden = true
        accountInfoView.isHidden = true
        payMoneyView.isHidden = true
        sosView.isHidden = true
        isForbiddenAlertView.isHidden = true
    }
}

extension ListVehicleVC: URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
    
}







