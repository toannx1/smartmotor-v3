//
//  ListVehicleTableView.swift
//  SmartMotor
//
//  Created by vietnv2 on 12/5/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit



extension ListVehicleVC: UITableViewDelegate, UITableViewDataSource {
    func initTableView() {
        listChargeMoneyTypeTableView.tableFooterView = UIView(frame: CGRect.zero)
        listChargeMoneyTypeTableView.isScrollEnabled = false
        listMonth2PayTableView.tableFooterView = UIView(frame: CGRect.zero)
        listMonth2PayTableView.isScrollEnabled = false
        listVehicleTableView.tableFooterView = UIView(frame: CGRect.zero)
//
//        searchVehicleContraint.constant = 0
//        listViewContraint.constant = view.frame.size.height - 80 - 255
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == listVehicleTableView {
            return numberOfVehcle
        }
        if tableView == listChargeMoneyTypeTableView {
            return listChargeMoneyString.count
        }
        if tableView == listMonth2PayTableView {
            return numberOfMonth.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell2Return: UITableViewCell!
        switch tableView {
        case listVehicleTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellForlistVehicle", for: indexPath) as! ListVehicleCustumCell
            if numberOfVehcle != 0 {
                cell.lblRegNo.text = vehicleInformations[indexPath.row].regNo
                cell.lblName.text = vehicleInformations[indexPath.row].factory
                cell.imgIcon.image = UIImage(named: self.motorStateImage[vehicleInformations[indexPath.row].state])
            }
            cell2Return = cell
        case listChargeMoneyTypeTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellForChargeMoney", for: indexPath) as! ChargeMoneyCell
            cell.lblChargeMoneyStype.text = listChargeMoneyString[indexPath.row]
            cell2Return = cell
        case listMonth2PayTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellForChooseMonth", for: indexPath) as! ListMonthToPayCell
            cell.lblNumberOfMonth.text = String(numberOfMonth[indexPath.row]) + " Tháng"
            cell2Return = cell
        default:
            break
        }
        return cell2Return
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView {
        case listVehicleTableView:
            vehicle2showIndex = indexPath.row
            vehicle2show = vehicleInformations[indexPath.row]
            if vehicle2show.isForbidden == true {
                lblIForbiddenAlert.text = "SmartMotor: Dịch vụ giám sát hành trình xe " + vehicle2show.regNo + " của Quý khách đang tạm ngừng. Tài khoản của Quý khách đã hết, vui lòng nạp thêm tiền để tiếp tục sử dụng dịch vụ. Chi tiết liên hệ 198 (Miễn phí). Trân trọng."
                backgroundChargeMoneyView.isHidden = false
                listChargeMoneyView.isHidden = true
                isForbiddenAlertView.isHidden = false
                return
            }
            gotoMapView()
        case listChargeMoneyTypeTableView:
            if indexPath.row == 0 {
                listChargeMoneyView.isHidden = true
                chargeCardCodeView.isHidden = false
                txtChargeCode.text = ""
                txtCaptchaCardCode.text = ""
                lblVehiclePhoneNumber.text = "SĐT thiết bị:  " + vehicle2show.sim
                urlSessionDelegate()
            }else {
                lblPayMoneyInfo.text = ""
                txtCaptchaPayMoney.text = ""
                getInfo2PayMoney()
            }
        case listMonth2PayTableView:
            
            chooseNumberOfMonthToPayView.isHidden = true
            monthPay = String(numberOfMonth[indexPath.row])
            let money = monthlyFee*numberOfMonth[indexPath.row]
            moneyPay = String(money)
            btnChooseNumberOfMonth2Pay.titleLabel?.text = monthPay + " Tháng"
            //txtMoneyPay.text = moneyPay + " VNĐ"
            txtMoneyPay.text = money.stringWithSepator + " VNĐ"
        default:
            break
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if endOfListFlag != false {
            return
        }
        if scrollView != listVehicleTableView {
            return
        }
        //
        if scrollView.contentOffset.y + scrollView.frame.size.height < scrollView.contentSize.height {
            return
        }
        if loadingDataInListVCFlag != false {
            return
        }
        loadingDataInListVCFlag = true
        pageIndex = pageIndex + 1
        loadListVehicle()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == listMonth2PayTableView {
            return 40
        }
        return 50
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {        
//        if scrollView.contentOffset.y < 0 {
////            searchVehicleContraint.constant = 50
//            listViewContraint.constant = view.frame.size.height - 80 - 255 - 50
//            UIView.animate(withDuration: 0.1) {
//                self.view.layoutIfNeeded()
//            }
//        }else if scrollView.contentOffset.y > 10 {
////            searchVehicleContraint.constant = 0
//            listViewContraint.constant = view.frame.size.height - 80 - 255
//            UIView.animate(withDuration: 0.1) {
//                self.view.layoutIfNeeded()
//            }
//        }
    }
    
}





