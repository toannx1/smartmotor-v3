//
//  MapVC.swift
//  Smart Motor
//
//  Created by vietnv2 on 20/10/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit
import MessageUI
import Darwin
import GoogleMaps
import Mapbox
import MapboxGeocoder
class MapVC: UIViewController {
    @IBOutlet weak var gmapView: GMSMapView!
    @IBOutlet weak var vtmapView: UIView!
    @IBOutlet weak var titleMapView: UIView!
    @IBOutlet weak var lockUnlockContainerView: UIView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var sliderView: UIView!
    //@IBOutlet weak var selectStartStopDateView: UIView!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var selectDateTimeView: UIView!
    @IBOutlet weak var selectDateTimeViewContraint: NSLayoutConstraint!
    @IBOutlet weak var btnChangeMapView: UIButton!
    @IBOutlet weak var btnHistoryMode: UIButton!
    @IBOutlet weak var btnSetting: UIButton!
    @IBOutlet weak var btnMyLocation: UIButton!
    @IBOutlet weak var btnLock: UIButton!
    @IBOutlet weak var btnUnlock: UIButton!
    @IBOutlet weak var btnCall2Vehicle: UIButton!
    @IBOutlet weak var lblVehicleAdress: UILabel!
    @IBOutlet weak var lblVehicleType: UILabel!
    @IBOutlet weak var lblVehicleStatus: UILabel!
    @IBOutlet weak var btnSelectedDateStartSetTitle: UIButton!
    @IBOutlet weak var btnSelectedStartTimeSetTitle: UIButton!
    @IBOutlet weak var btnSelectedDateEndSetTitle: UIButton!
    @IBOutlet weak var btnSelectedEndTimeSetTitle: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var sliderConfig: UISlider!
    @IBOutlet weak var lblDatePlay: UILabel!
    @IBOutlet weak var lblTimePlay: UILabel!
    @IBOutlet weak var lblSpeedPlay: UILabel!
    @IBOutlet weak var btnPlayHistoryTrack: UIButton!
    @IBOutlet weak var lblRegNoTitle: UILabel!
    @IBOutlet weak var btnConfirmLoadHistoryTrack: UIButton!
    @IBOutlet weak var btnCancelLoadHistoryTrack: UIButton!
    @IBOutlet weak var btnShowHideButton: UIButton!
    @IBOutlet weak var btnSpeedPlay: UIButton!
    @IBOutlet weak var btnChangeGmapType: UIButton!
    @IBOutlet weak var confirmSendLockView: UIView!
    @IBOutlet weak var lblConfirmLock: UILabel!
    @IBOutlet weak var confirmSendUnlockView: UIView!
    @IBOutlet weak var lblConfirmUnlock: UILabel!
    @IBOutlet weak var confirmCallVehicleView: UIView!
    @IBOutlet weak var lblConfirmCallVehicle: UILabel!
    @IBOutlet weak var warningWrongLocation: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var contraintStatusView: NSLayoutConstraint!
    @IBOutlet weak var btnDetail: UIButton!
    let motorStateImage:[String] = ["ic_motor_no_infor",
                                           "ic_motor_run",
                                           "ic_motor_stop",
                                           "ic_motor_park",
                                           "ic_motor_lost_gps",
                                           "ic_motor_hibernate",
                                           "ic_motor_lost_gprs",
                                           "ic_motor_no_infor"]
    var displayVehicleStatusString:[String]! = ["Không có thông tin",
                                         "Trạng thái: Dừng",
                                         "Trạng thái: Đỗ",
                                         "Trạng thái: Mất GPS",
                                         "Trạng thái: Ngủ đông",
                                         "Trạng thái: Mất GPRS",
                                         "Không xác định"]
    
    enum parseJson2GetHistoryAnnotationString:String {
        case gpsDate = "gpsDate"
        case gpsSpeed = "gpsSpeed"
        case id = "id"
        case lat = "lat"
        case lng = "lng"
        case motoSpeed = "motoSpeed"
        case motorState = "motorState"
        case state = "state"
        case timeState = "timeState"
    }
    
    let vehicleAdressString:String = "Vị trí xe: "
    let vehicleType:String = "Loại xe: "
    let statusString:String = "Trạng thái: "
    let speedString:String = "Tốc độ: "
    let kmPerHourString:String = " km/h"
    let noConfirmString:String = "Không xác định"
    let lockString:String = "LOCK"
    let unLockString:String = "UNLOCK"
    let noVehicleInfomationString:String = "Không xác định được thông tin của phương tiện!"
    let wrongTimeOrNoDataThisTime:String! = "Không có dữ liệu trong khoảng thời gian này!"
    let cannotLoadData:String! = "Không tải được dữ liệu!"
    let noDataInThisTime:String = "No data in this time"
    let lblTimeString :String = "Thời gian :"
    let iconMotorImage:UIImage = UIImage(named: "icon motobike.png")!
    let startFlag:UIImage! = UIImage(named: "Start Flag-35.png")
    let finishFlag:UIImage! = UIImage(named: "Finish Flag-35.png")
    let pauseImage:UIImage! = UIImage(named: "btn_pause_media.png")
    let playImage:UIImage! = UIImage(named: "btn_resume_media.png")
    let originDateFormat:String = "yyyy-MM-dd'T'HH:mm:ssZ"
    
    let convertDateFormat:String = "dd/MM/yyyy HH:mm:ss"
    let dateFormatForDisplay:String = "dd/MM/yyyy"
    let timeFormatForSendRequest:String = "HH:mm"
    var dateStart2SendRequest:String!
    var dateEnd2SendRequest:String!
    var timeStart2SendRequest:String!
    var timeEnd2SendRequest:String!
    
    var vehicle2show_currentAnnotation = CLLocationCoordinate2D()
    var lastAnnotationDisplay:CLLocationCoordinate2D!
    var lastAnnotationForRotation = CLLocationCoordinate2D()
    var allAnnotations:Array = [Station]()
    let gmMarkerForMotor = GMSMarker()
    var vtMarkerForMotor = MGLPointAnnotation()
    var vtStartPointMotor = MGLPointAnnotation()
    var vtEndPointMotor = MGLPointAnnotation()
    var vtMap = MGLMapView()
    
    var timerForVehicleCurrentLocation:Timer!
    var timerForSlider:Timer!
    var timerForSliderRunningFlag:Bool! = false
    //var timerForAlert:Timer!
    var alertController:UIAlertController!
    var playHistoryFlag:Bool! = false
    var viewHistoryModeFlag:Bool = false
    var statusViewFlag:Bool = false
    var flagHideButton:Bool = false
    var sliderValue:Float = 0
    var selectedStartStopDateIndex:Int = 0
    var viewCurrentLocationFlag:Bool = true
    var dateStart:Date!
    var dateEnd:Date!
    var checkInvalidDateToShowHistory:Int!
    var timeValueForSliderPlay:Double!
    var updateTimeValueForSliderPlay:Double!
    var changeGmapTypeFlag:Bool! = false
    var vtMapDisplayFlag:Bool! = true
    //var centerMarkerInGmapFlag:Bool! = true
    var locationManager = CLLocationManager()
    var valueLocation:CLLocationCoordinate2D!
    var timer: Timer?
    var polylineSource: MGLShapeSource?
    var currentIndex = 1
    var shapeSource : MGLShapeSource?
    var shapeLayer : MGLSymbolStyleLayer!
    var geocoder: Geocoder!
    var geocodingDataTask: URLSessionDataTask?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initAllMapView()
        initMapVC()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }

    func initAllMapView() {
        sliderValue = 0
        timeValueForSliderPlay = 0.2
        updateTimeValueForSliderPlay = timeValueForSliderPlay
        initVTMap()
        initGMap()
        initMyLocation()
        initTapInConfirmBackgroundView()
        initTapToCloseDatePicker()
    }
    
    func initMapVC() {
        titleMapView.addBackground(width: UIScreen.main.bounds.size.width,height: titleViewHeight, image: titleViewImage)
        viewHistoryModeFlag = false
        closeAllViewUpperBackground()
        flagHideButton = false
        playHistoryFlag = false
        flagHideButton = true 
        hideMainButton(false)
        flagHideButton = true
        datePickerView.isHidden = true
        statusView.isHidden = false
        if #available(iOS 13.4, *) {
               datePicker.preferredDatePickerStyle = .wheels
               datePicker.sizeToFit()
           }
        sliderView.isHidden = true
        if vtMapDisplayFlag ==  true {
            vtmapView.isHidden = false
            gmapView.isHidden = true
        }else {
            gmapView.isHidden = false
            vtmapView.isHidden = true
        }
        initSlider()
        initDateTime()
        
        lblRegNoTitle.text = "Vị trí xe " + vehicle2show.regNo
        timerReloadCurrentLocationRun(true)
        saveCurrentLocation()
        saveLastLocation()
        displayVehicleCurrentLocation(withZoomIn: true)
        if widthScreen < 350 {
            lblVehicleAdress.font = UIFont.systemFont(ofSize: 13)
            lblVehicleType.font = UIFont.systemFont(ofSize: 13)
            lblVehicleStatus.font = UIFont.systemFont(ofSize: 13)
        }
    }
    
    func closeAllViewUpperBackground() {
        confirmSendLockView.isHidden = true
        confirmCallVehicleView.isHidden = true
        confirmSendUnlockView.isHidden = true
        warningWrongLocation.isHidden = true
        selectDateTimeView.isHidden = true
        datePickerView.isHidden = true
        backgroundView.isHidden = true
    }
    
    func initSlider() {
        sliderConfig.setThumbImage(UIImage(named: "icon_seek.jpg"), for: UIControlState.normal)
        sliderConfig.setThumbImage(UIImage(named: "icon_seek.jpg"), for: UIControlState.highlighted)
        closeSlider()
    }
    
    func initDatePicker() {
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date())
//        datePicker.backgroundColor = UIColor.white
        // 24h
        datePicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale
        //12h
        //datePicker.locale = NSLocale(localeIdentifier: "en_US") as Locale
    }
    
    func initDateTime() {
        let date = Date()
        let formatter = DateFormatter();
        //init first time format value send to webservice
        formatter.dateFormat = dateFormatForDisplay
        dateStart2SendRequest = formatter.string(from: date);
        dateEnd2SendRequest = formatter.string(from: date);
        timeStart2SendRequest = "00:00"
        timeEnd2SendRequest = "23:59"
        //
        //init first time display on button
        formatter.dateFormat = dateFormatForDisplay
        let strDate = formatter.string(from: date)
        dateStart = formatter.date(from: strDate)
        dateEnd = dateStart
        checkInvalidDateToShowHistory = 0
        btnSelectedDateStartSetTitle.setTitle(strDate, for: .normal)
        btnSelectedDateEndSetTitle.setTitle(strDate, for: .normal)
        btnSelectedStartTimeSetTitle.setTitle("00:00", for: .normal)
        btnSelectedEndTimeSetTitle.setTitle("23:59", for: .normal)
        initDatePicker()
    }
    
    // Mark:
    @IBAction func btnBackToListVehicleTapped(_ sender: AnyObject) {
        //getVehicleLocation()
        if viewCurrentLocationFlag != true {
            btnShowHideButton.isHidden = false
            sliderConfig.value = 0
            sliderValue = 0
            playHistoryFlag = false
            btnSpeedPlay.setTitle("x1", for: .normal)
            btnPlayHistoryTrack.setImage(playImage, for: .normal)
            backToViewCurrentLocation()
            return
        }
        timerReloadCurrentLocationRun(false)
        gotoListVehicleView()
    }
    
    @IBAction func btnShowHideButtonTapped(_ sender: AnyObject) {
        if viewCurrentLocationFlag != true {
            return
        }
        if flagHideButton != true {
            hideMainButton(false)
            flagHideButton = true
            return
        }
        hideMainButton(true)
        flagHideButton = false
    }
    
    @IBAction func btnLockTapped(_ sender: AnyObject) {
        let allow = checkAllowControl()
        if allow == false {
            return
        }
        backgroundView.isHidden = false
        confirmSendLockView.isHidden = false
        lblConfirmLock.text = "Bạn muốn LOCK xe \"" + vehicle2show.regNo + "\" bằng SMS?"
    }
    
    @IBAction func btnConfirmLockTapped(_ sender: Any) {
        confirmSendLockView.isHidden = true
        backgroundView.isHidden = true
        sendMessage(phoneNumber: vehicle2show.sim, content: lockString)
    }
    
    @IBAction func btnCancelLockTapped(_ sender: Any) {
        confirmSendLockView.isHidden = true
        backgroundView.isHidden = true
    }
    
    @IBAction func btnUnlockTapped(_ sender: AnyObject) {
        let allow = checkAllowControl()
        if allow == false {
            return
        }
        confirmSendUnlockView.isHidden = false
        backgroundView.isHidden = false
        lblConfirmUnlock.text = "Bạn muốn UNLOCK xe \"" + vehicle2show.regNo + "\" bằng SMS?"
    }
    
    @IBAction func btnConfirmUnlockTapped(_ sender: Any) {
        confirmSendUnlockView.isHidden = true
        backgroundView.isHidden = true
        sendMessage(phoneNumber: vehicle2show.sim, content: unLockString)
    }
    
    @IBAction func btnCancelUnlockTapped(_ sender: Any) {
        confirmSendUnlockView.isHidden = true
        backgroundView.isHidden = true
    }
    
    
    @IBAction func btnCall2VehicleTapped(_ sender: Any) {
        let allow = checkAllowControl()
        if allow == false {
            return
        }
        confirmCallVehicleView.isHidden = false
        backgroundView.isHidden = false
        lblConfirmCallVehicle.text = "Bạn muốn tìm xe \"" + vehicle2show.regNo + "\" bằng cuộc gọi?"
    }
    
    @IBAction func btnConfirmCall2VehicleTapped(_ sender: Any) {
        confirmCallVehicleView.isHidden = true
        backgroundView.isHidden = true
        call2APhone()
    }
    
    @IBAction func btnCancelCall2VehicleTapped(_ sender: Any) {
        confirmCallVehicleView.isHidden = true
        backgroundView.isHidden = true
    }
    

    @IBAction func btnSettingTapped(_ sender: AnyObject) {
        let allow = checkAllowControl()
        if allow == false {
            return
        }
        if viewCurrentLocationFlag != true {
            initMapVC()
            return
        }
        timerReloadCurrentLocationRun(false)
        gotoSettingView()
    }
    
    func checkAllowControl() -> Bool {
        if groupRootSave == 1 {
            Alert.alertAutoDismiss(delegate: self, message: "Không thể điều khiển xe này!")
            return false
        }
        if vehicle2show.deviceType == 0 {
            Alert.alertAutoDismiss(delegate: self, message: "Không thể điều khiển xe này!")
            return false
        }
        return true
    }
    
    @IBAction func btnChangeMapViewTapped(_ sender: AnyObject) {
        func changeMapButtonImage() {
            if vtMapDisplayFlag != true {
                vtMapDisplayFlag = true
                btnChangeMapView.setImage( #imageLiteral(resourceName: "btn googlemap.png"), for: .normal)
                return
            }
            vtMapDisplayFlag = false
            btnChangeMapView.setImage( #imageLiteral(resourceName: "btn viettelmap.png"), for: .normal)
        }
        //centerMarkerInGmapFlag = true
        clearEverythingOnMap()
        changeMapButtonImage()
        initMapVC()
    }
    
    @IBAction func btnChangeGmapTypeTapped(_ sender: Any) {
        gmapView.mapType = .hybrid
        if changeGmapTypeFlag == true {
            changeGmapTypeFlag = false
            gmapView.mapType = .normal
            btnChangeGmapType.setImage( #imageLiteral(resourceName: "gmap02.png"), for: .normal)
        }else {
            changeGmapTypeFlag = true
            gmapView.mapType = .hybrid
            btnChangeGmapType.setImage( #imageLiteral(resourceName: "gmap01.png"), for: .normal)
        }
        
    }
    
    
    // Mark:
    @IBAction func btnMyLocationTapped(_ sender: Any) {
        backgroundView.isHidden = false
        warningWrongLocation.isHidden = false
    }
    
    @IBAction func btnConfirmViewMyLocationTapped(_ sender: Any) {
        backgroundView.isHidden = true
        warningWrongLocation.isHidden = true
        viewMyLocation()
    }
    
    @IBAction func btnCancelViewMyLocationTapped(_ sender: Any) {
        backgroundView.isHidden = true
        warningWrongLocation.isHidden = true
    }
    
    let markerMyLocation = GMSMarker()
    func viewMyLocation() {
        gmapView.isMyLocationEnabled = true
        gmapView.camera = GMSCameraPosition.camera(withLatitude: valueLocation.latitude, longitude: valueLocation.longitude, zoom: 15)
        markerMyLocation.position = CLLocationCoordinate2D(latitude: valueLocation.latitude,longitude: valueLocation.longitude)
//        markerMyLocation.title = "Current Location"
//        markerMyLocation.snippet = String(valueLocation.latitude) + " + " + String(valueLocation.longitude)//"XXX"
//        markerMyLocation.map = self.gmapView
        locationManager.startUpdatingLocation()
        hideMainButton(true)
        fitMyLocationAndMotorInGmap()
        
       // locationManager.stopUpdatingLocation()
    }
    
    func fitMyLocationAndMotorInGmap() {
        var bounds = GMSCoordinateBounds()
        bounds = bounds.includingCoordinate(markerMyLocation.position)
        bounds = bounds.includingCoordinate(gmMarkerForMotor.position)
        gmapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 80))
    }
    
    // Mark:
    @IBAction func btnHistoryModeTapped(_ sender: AnyObject) {
        //initDateTime()
        if vehicle2show.state == 0 {
            return
        }
        backgroundView.isHidden = false
        selectDateTimeView.isHidden = false
        sliderView.isHidden = true
        selectDateTimeViewContraint.constant = (view.frame.size.height - selectDateTimeView.frame.size.height)/2
        btnSelectedDateStartSetTitle.layer.borderWidth = 0.5
        btnSelectedDateStartSetTitle.layer.borderColor = UIColor.lightGray.cgColor
        btnSelectedDateStartSetTitle.backgroundColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 0.3)
        btnSelectedStartTimeSetTitle.layer.borderWidth = 0.5
        btnSelectedStartTimeSetTitle.layer.borderColor = UIColor.lightGray.cgColor
        btnSelectedStartTimeSetTitle.backgroundColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 0.3)
        btnSelectedDateEndSetTitle.layer.borderWidth = 0.5
        btnSelectedDateEndSetTitle.layer.borderColor = UIColor.lightGray.cgColor
        btnSelectedDateEndSetTitle.backgroundColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 0.3)
        btnSelectedEndTimeSetTitle.layer.borderWidth = 0.5
        btnSelectedEndTimeSetTitle.layer.borderColor = UIColor.lightGray.cgColor
        btnSelectedEndTimeSetTitle.backgroundColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 0.3)
        
    }
    
    @IBAction func btnDatePickerOKTapped(_ sender: AnyObject) {
        datePickerView.isHidden = true
    }
    
    @IBAction func btnDatePickerCancelTapped(_ sender: AnyObject) {
        datePickerView.isHidden = true
    }
    
    func checkDateTimeTapped() {
        if (view.frame.size.height - datePickerView.frame.size.height) < (selectDateTimeViewContraint.constant + selectDateTimeView.frame.size.height + 60) {
            //let contraint = selectDateTimeViewContraint.constant - (datePickerView.frame.size.height - selectDateTimeViewContraint.constant) - 50
            let contraint = (view.frame.size.height - datePickerView.frame.size.height - selectDateTimeViewContraint.constant)/2
            if contraint < 0 {
                selectDateTimeViewContraint.constant = 0
            }else {
                selectDateTimeViewContraint.constant = contraint
            }
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func daysBetweenDates(startDate: Date, endDate: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day!
    }
    
    @objc func dataPickerScrolled(sender: AnyObject) {
        let dateFormatter = DateFormatter()
        switch selectedStartStopDateIndex {
        case 0:
            dateFormatter.dateFormat = dateFormatForDisplay
            let strDate = dateFormatter.string(from: datePicker.date)
            btnSelectedDateStartSetTitle.setTitle(strDate, for: UIControlState())
            dateStart2SendRequest = dateFormatter.string(from: datePicker.date)
            //
            dateStart = dateFormatter.date(from: strDate) as Date!
            checkInvalidDateToShowHistory = daysBetweenDates(startDate: dateStart as Date, endDate: dateEnd as Date)
        case 1:
            dateFormatter.dateFormat = timeFormatForSendRequest
            let date24 = dateFormatter.string(from: datePicker.date)
            btnSelectedStartTimeSetTitle.setTitle(date24, for: UIControlState())
            timeStart2SendRequest = dateFormatter.string(from: datePicker.date)
        case 2:
            dateFormatter.dateFormat = dateFormatForDisplay
            let strDate = dateFormatter.string(from: datePicker.date)
            btnSelectedDateEndSetTitle.setTitle(strDate, for: UIControlState())
            dateEnd2SendRequest = dateFormatter.string(from: datePicker.date)
            //
            dateEnd = dateFormatter.date(from: strDate)
            checkInvalidDateToShowHistory = daysBetweenDates(startDate: dateStart as Date, endDate: dateEnd as Date)
        case 3:
            dateFormatter.dateFormat = timeFormatForSendRequest
            let date24 = dateFormatter.string(from: datePicker.date)
            btnSelectedEndTimeSetTitle.setTitle(date24, for: UIControlState())
            timeEnd2SendRequest = dateFormatter.string(from: datePicker.date)
        default:
            break
        }
    }
    
    @IBAction func btnSelectedDateStartHistoryTrackTapped(_ sender: AnyObject) {
        datePicker.datePickerMode = UIDatePickerMode.date
        datePickerView.isHidden = false
        selectedStartStopDateIndex = 0
        checkDateTimeTapped()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormatForDisplay
        let strDate = dateFormatter.date(from: (btnSelectedDateStartSetTitle.titleLabel?.text)!)
        datePicker.setDate(strDate!, animated: true)
        
        btnSelectedDateStartSetTitle.layer.borderWidth = 2
        btnSelectedDateStartSetTitle.layer.borderColor = UIColor(red: 30/255, green: 154/255, blue: 195/255, alpha: 1).cgColor
        btnSelectedStartTimeSetTitle.layer.borderWidth = 0.5
        btnSelectedStartTimeSetTitle.layer.borderColor = UIColor.clear.cgColor
        btnSelectedDateEndSetTitle.layer.borderWidth = 0.5
        btnSelectedDateEndSetTitle.layer.borderColor = UIColor.clear.cgColor
        btnSelectedEndTimeSetTitle.layer.borderWidth = 0.5
        btnSelectedEndTimeSetTitle.layer.borderColor = UIColor.clear.cgColor
        
        datePicker.addTarget(self, action: #selector(dataPickerScrolled(sender:)), for: .valueChanged)
    }
    
    @IBAction func btnSelectedStartTimeHistoryTrackTapped(_ sender: AnyObject) {
        datePicker.datePickerMode = UIDatePickerMode.time
        datePickerView.isHidden = false
        selectedStartStopDateIndex = 1
        checkDateTimeTapped()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = timeFormatForSendRequest
        let strDate = dateFormatter.date(from: (btnSelectedStartTimeSetTitle.titleLabel?.text)!)
        datePicker.setDate(strDate!, animated: true)
        
        btnSelectedDateStartSetTitle.layer.borderWidth = 0.5
        btnSelectedDateStartSetTitle.layer.borderColor = UIColor.clear.cgColor
        btnSelectedStartTimeSetTitle.layer.borderWidth = 2
        btnSelectedStartTimeSetTitle.layer.borderColor = UIColor(red: 30/255, green: 154/255, blue: 195/255, alpha: 1).cgColor
        btnSelectedDateEndSetTitle.layer.borderWidth = 0.5
        btnSelectedDateEndSetTitle.layer.borderColor = UIColor.clear.cgColor
        btnSelectedEndTimeSetTitle.layer.borderWidth = 0.5
        btnSelectedEndTimeSetTitle.layer.borderColor = UIColor.clear.cgColor
        
        datePicker.addTarget(self, action: #selector(dataPickerScrolled(sender:)), for: .valueChanged)
    }
    
    @IBAction func btnSelectedDateEndHistoryTrackTapped(_ sender: AnyObject) {
        datePicker.datePickerMode = UIDatePickerMode.date
        datePickerView.isHidden = false
        selectedStartStopDateIndex = 2
        checkDateTimeTapped()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormatForDisplay
        let strDate = dateFormatter.date(from: (btnSelectedDateEndSetTitle.titleLabel?.text)!)
        datePicker.setDate(strDate!, animated: true)
        
        btnSelectedDateStartSetTitle.layer.borderWidth = 0.5
        btnSelectedDateStartSetTitle.layer.borderColor = UIColor.clear.cgColor
        btnSelectedStartTimeSetTitle.layer.borderWidth = 0.5
        btnSelectedStartTimeSetTitle.layer.borderColor = UIColor.clear.cgColor
        btnSelectedDateEndSetTitle.layer.borderWidth = 2
        btnSelectedDateEndSetTitle.layer.borderColor = UIColor(red: 30/255, green: 154/255, blue: 195/255, alpha: 1).cgColor
        btnSelectedEndTimeSetTitle.layer.borderWidth = 0.5
        btnSelectedEndTimeSetTitle.layer.borderColor = UIColor.clear.cgColor
        
        datePicker.addTarget(self, action: #selector(dataPickerScrolled(sender:)), for: .valueChanged)
    }
    
    @IBAction func btnSelectedEndTimeHistoryTrackTapped(_ sender: AnyObject) {
        datePicker.datePickerMode = UIDatePickerMode.time
        datePickerView.isHidden = false
        selectedStartStopDateIndex = 3
        checkDateTimeTapped()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = timeFormatForSendRequest
        let strDate = dateFormatter.date(from: (btnSelectedEndTimeSetTitle.titleLabel?.text)!)
        datePicker.setDate(strDate!, animated: true)
        
        btnSelectedDateStartSetTitle.layer.borderWidth = 0.5
        btnSelectedDateStartSetTitle.layer.borderColor = UIColor.clear.cgColor
        btnSelectedStartTimeSetTitle.layer.borderWidth = 0.5
        btnSelectedStartTimeSetTitle.layer.borderColor = UIColor.clear.cgColor
        btnSelectedDateEndSetTitle.layer.borderWidth = 0.5
        btnSelectedDateEndSetTitle.layer.borderColor = UIColor.clear.cgColor
        btnSelectedEndTimeSetTitle.layer.borderWidth = 2
        btnSelectedEndTimeSetTitle.layer.borderColor = UIColor(red: 30/255, green: 154/255, blue: 195/255, alpha: 1).cgColor
        
        datePicker.addTarget(self, action: #selector(dataPickerScrolled(sender:)), for: .valueChanged)
    }
    
    @IBAction func btnConfirmLoadHistoryTrackTapped(_ sender: AnyObject) {
        if checkInvalidDateToShowHistory > 1 {
            Alert.alertAutoDismiss(delegate: self, message: "Khoảng thời gian chỉ được tối đa 2 ngày!")
            return
        }else if checkInvalidDateToShowHistory < 0 {
            Alert.alertAutoDismiss(delegate: self, message: "Thời gian bắt đầu phải nhỏ hơn thời gian kết thúc!")
            return
        }
        locationManager.stopUpdatingLocation()
        viewCurrentLocationFlag = false
        timerReloadCurrentLocationRun(false)
        closeAllViewUpperBackground()
        hideMainButton(true)
        getMapAnnotations()
    
    }
    
    @IBAction func btnCancelLoadHistoryTrackTapped(_ sender: AnyObject) {
        closeAllViewUpperBackground()
    }
    

    @IBAction func sliderToShowHistoryTrack(_ sender: UISlider) {
        playHistoryTrack(index: sliderConfig.value)
        sliderValue = sliderConfig.value
    }
    
    @IBAction func btnPlayHistoryTrackTapped(_ sender: UIButton) {
        if sliderConfig.value < sliderConfig.maximumValue {
            sliderValue = sliderConfig.value
        }else {
            sliderConfig.value = 0
            sliderValue = 0
        }
        if playHistoryFlag == false {
            playHistoryFlag = true
            btnPlayHistoryTrack.setImage(pauseImage, for: .normal)
            timerForAutoSlider(true)
            return
        }else {
            playHistoryFlag = false
            btnPlayHistoryTrack.setImage(playImage, for: .normal)
            timerForAutoSlider(false)
        }
    }
    
   
    
    @IBAction func btnSpeedPlayTapped(_ sender: Any) {
        if let text = btnSpeedPlay.titleLabel?.text {
            switch text {
            case "x1":
                btnSpeedPlay.setTitle("x2", for: .normal)
                updateTimeValueForSliderPlay = 0.1
            case "x2":
                btnSpeedPlay.setTitle("x4", for: .normal)
                updateTimeValueForSliderPlay = 0.05
            case "x4":
                btnSpeedPlay.setTitle("x1", for: .normal)
                updateTimeValueForSliderPlay = 0.2
            default:
                break
            }
        }
    }
    
    // Mark: Map
    func clearEverythingOnMap() {
        closeSlider()
        if vtMapDisplayFlag == true {
//            vtMap.clear()
            return
        }
        gmapView.clear()
    }
    
    func hideMainButton(_ displayFlag: Bool) {
        btnChangeMapView.isHidden = displayFlag
        btnHistoryMode.isHidden = displayFlag
        btnSetting.isHidden = displayFlag
        btnMyLocation.isHidden = displayFlag
    }
    
    func backToViewCurrentLocation() {
        viewCurrentLocationFlag = true
        lockUnlockContainerView.isHidden = false
        getVehicleLocation()
        initAllMapView()
        initMapVC()
        timerReloadCurrentLocationRun(true)
    }
    
    // Mark: Marker
    func vtCustomMarker(annotation : CLLocationCoordinate2D, img : UIImage, title : String, subtitle : String) {
        vtMap.setCenter(CLLocationCoordinate2D(latitude: self.valueLocation.latitude, longitude: self.valueLocation.longitude),
                        zoomLevel: 14, animated: false)
    }
    
    func vtCustomMarkerForMotor( annotation : CLLocationCoordinate2D , sourceIdentifier : String! ,
                                 styleIdentifier : String! , image : UIImage, forname : String) {
        self.vtMap.style?.removeLayer(self.shapeLayer)
        self.vtMap.style?.removeSource(self.shapeSource!)
        self.vtMarkerForMotor.coordinate = annotation
        
//        marker-source
        self.shapeSource = MGLShapeSource(identifier: sourceIdentifier, shape: self.vtMarkerForMotor, options: nil)
        self.shapeLayer = MGLSymbolStyleLayer(identifier: styleIdentifier, source: self.shapeSource!)
        self.vtMap.style?.setImage(image, forName: forname)
        
        self.shapeLayer.iconImageName = NSExpression(forConstantValue: forname)
        self.vtMap.style?.addSource(self.shapeSource!)
        self.vtMap.style?.addLayer(self.shapeLayer!)
    }
    func addStartAndEndPoint(shape : MGLPointAnnotation, annotation:CLLocationCoordinate2D ,
                             image :UIImage,sourceIndentifier : String , layerIndentifier : String,foraname : String){
        shape.coordinate = annotation
        //"",""
        let shapeSource = MGLShapeSource(identifier: sourceIndentifier , shape: shape, options: nil)
        let shapeLayer = MGLSymbolStyleLayer(identifier: layerIndentifier, source: shapeSource)
       
        
            self.vtMap.style?.setImage(image, forName: foraname)
        
        shapeLayer.iconImageName = NSExpression(forConstantValue: foraname)
        
        // Add the source and style layer to the map
        self.vtMap.style?.addSource(shapeSource)
        self.vtMap.style?.addLayer(shapeLayer)
    }
    
    func gmCustomMarkerForMotor(annotation:CLLocationCoordinate2D,img: UIImage,title: String,subtitle: String) {
        gmMarkerForMotor.position = CLLocationCoordinate2D(latitude: annotation.latitude,
                                                           longitude: annotation.longitude)
        gmMarkerForMotor.title = title
        gmMarkerForMotor.snippet = subtitle
        gmMarkerForMotor.icon = img
        gmMarkerForMotor.groundAnchor = CGPoint(x: 0.5, y: 0.5)  // center
        gmMarkerForMotor.map = self.gmapView
    }
    
    func gmCustomMarker(annotation : CLLocationCoordinate2D, img : UIImage, title : String, subtitle : String){
        let gmMarker = GMSMarker()
        gmMarker.position = CLLocationCoordinate2D(latitude: annotation.latitude,longitude: annotation.longitude)
        gmMarker.title = title
        gmMarker.snippet = subtitle
        gmMarker.icon = img
        gmMarker.map = self.gmapView
    }
    
    func displayMarkerMotor(_ annotation: CLLocationCoordinate2D,_ img: UIImage) {
        if vtMapDisplayFlag == false {
            self.gmCustomMarkerForMotor(annotation: annotation, img: img, title: "", subtitle: "")
            return
        }
        vtCustomMarkerForMotor(annotation: annotation, sourceIdentifier: "marker-source",
                               styleIdentifier: "marker-style", image: img, forname: "home-symbol")
//        vtMap.setCenter(VMSLatLng.init(lat: annotation.latitude, lng: annotation.longitude), refresh: true)
        
        vtMap.setCenter(CLLocationCoordinate2D(latitude: annotation.latitude, longitude: annotation.longitude)
        , zoomLevel: 14, animated: false)
        }
    
    func displayVehicleCurrentLocation(withZoomIn: Bool) {
        if viewHistoryModeFlag == true {
            return
        }
        if (vehicle2show.state < 1 || vehicle2show.state > 6) {
            DispatchQueue.main.async {
                Alert.alertAutoDismiss(delegate: self, message: self.noVehicleInfomationString)
            }
            return
        }
        if vtMapDisplayFlag == false {
            DispatchQueue.main.async {
                self.displayMarkerMotor(self.vehicle2show_currentAnnotation, self.iconMotorImage)
            }
            //displayMarkerMotor(vehicle2show_currentAnnotation, iconMotorImage)
            //if centerMarkerInGmapFlag == true {
                //centerMarkerInGmapFlag = false
                gmapView.camera = GMSCameraPosition.camera(withLatitude: vehicle2show_currentAnnotation.latitude, longitude: vehicle2show_currentAnnotation.longitude, zoom: 15)
            //}
            if vehicle2show_currentAnnotation.latitude == lastAnnotationForRotation.latitude && vehicle2show_currentAnnotation.longitude == lastAnnotationForRotation.longitude {
                return
            }
            gmMarkerForMotor.rotation = caculateAngleWithCoordiante().angle(firstLocation: lastAnnotationForRotation, secondLocation: vehicle2show_currentAnnotation)
            return
        }
        displayMarkerMotor(vehicle2show_currentAnnotation, iconMotorImage)
        if vehicle2show_currentAnnotation.latitude == lastAnnotationForRotation.latitude && vehicle2show_currentAnnotation.longitude == lastAnnotationForRotation.longitude {
            return
        }
    }
    
    func tapToMapMarkerForDisplayStatusView() {
        if viewHistoryModeFlag == true {
            return
        }
        if statusViewFlag == true {
            statusViewFlag = false
            statusView.isHidden = true
            return
        }
        statusViewFlag = true
        statusView.isHidden = false
        if vtMapDisplayFlag == false {
            getAddressFromGmap(vehicle2show_currentAnnotation)
            return
        }
        getAddressFromVTMap(vehicle2show_currentAnnotation)
    }
    
    // Mark: Play history
    func openSlider() {
        lblRegNoTitle.text = "Hành trình xe " + vehicle2show.regNo
        sliderView.isHidden = false
        statusView.isHidden = false
        lockUnlockContainerView.isHidden = true
        initSliderConfig(initValue: 1,min: 1,max: self.allAnnotations.count)
        viewHistoryModeFlag = true
        btnShowHideButton.isHidden = true
    }
    
    func closeSlider() {
        timerForAutoSlider(false)
        sliderView.isHidden = true
    }

    func initSliderConfig(initValue: Int,min: Int,max: Int) {
        self.sliderConfig.minimumValue = Float(min)
        self.sliderConfig.maximumValue = Float(max - 1)
        self.sliderConfig.value = Float(initValue)
        let time = convertDateFormatForSliderPlay(string: self.allAnnotations[0].gpsDate)
        let speed = "0"
        displayDateTimeSpeedHistory(dateTime: time, speed: speed)
        lastAnnotationDisplay = allAnnotations.first?.coordinate
        sliderValue = 0
        if vtMapDisplayFlag == true {
            vtCustomMarkerForMotor(annotation : allAnnotations.first!.coordinate , sourceIdentifier: "marker-source", styleIdentifier : "marker-style", image : iconMotorImage, forname : "home-symbol")
            return
        }
        gmCustomMarkerForMotor( annotation : allAnnotations.first!.coordinate,img : iconMotorImage,title : "",subtitle : "")
    }
    
    func playHistoryTrack(index: Float) {
        let index = Int(index)
        let nameImage = self.motorStateImage[self.allAnnotations[index].state]
        let image = UIImage(named: nameImage)
        func displayMarker(_ index: Int) {
            if vtMapDisplayFlag == true {
//                self.vtCustomMarkerForMotor(annotation: self.allAnnotations[index].coordinate, img: self.iconMotorImage, title: "", subtitle: "")
                
                vtCustomMarkerForMotor(annotation: self.allAnnotations[index].coordinate, sourceIdentifier: "marker-source",
                                       styleIdentifier: "marker-style", image: image ?? iconMotorImage , forname: "home-symbol")
                self.getAddressFromVTMap(self.allAnnotations[index].coordinate)
//                  let camera = MGLMapCamera(lookingAtCenter: self.allAnnotations[index].coordinate, altitude: 1500, pitch: 15, heading: 0)
//                self.vtMap.setCamera(camera, animated: true)
                
//                vtMap.refresh()
                return
            }
            gmCustomMarkerForMotor(annotation: allAnnotations[index].coordinate,img: image ?? iconMotorImage,title: "",subtitle: "")
        }
        func rotationMarker(_ index:Int) {
            if index >= allAnnotations.count - 1 {
                return
            }
            if allAnnotations[index].coordinate.latitude == allAnnotations[index+1].coordinate.latitude {
                return
            }
            if vtMapDisplayFlag == false {
                gmMarkerForMotor.rotation = caculateAngleWithCoordiante().angle(firstLocation: allAnnotations[index].coordinate,
                                                                                secondLocation: allAnnotations[index+1].coordinate)
                return
            }
            self.shapeLayer.iconRotation = NSExpression(forConstantValue: CGFloat(caculateAngleWithCoordiante().angle(firstLocation: self.allAnnotations[index].coordinate, secondLocation: self.allAnnotations[index+1].coordinate)))
//            vtMap.setCenter(self.allAnnotations[index].coordinate, animated: true)
        }
        func zoomInLocation(annotation: CLLocationCoordinate2D) {
            if vtMapDisplayFlag == false {
                self.gmapView.animate(toLocation : CLLocationCoordinate2D(latitude : annotation.latitude, longitude : annotation.longitude))
                return
            }
            //            self.vtMap.move(to: VMSLatLng.init(lat: annotation.latitude, lng: annotation.longitude))
            //            vtMap.refresh()
        }
        displayMarker(index)
        rotationMarker(index)
        lastAnnotationDisplay = allAnnotations[index].coordinate
        let time = convertDateFormatForSliderPlay(string: allAnnotations[index].gpsDate)
        let speed = String(allAnnotations[index].gpsSpeed)
        displayDateTimeSpeedHistory(dateTime: time, speed: speed)
    }
    
    
    func displayDateTimeSpeedHistory(dateTime:String,speed:String) {
        lblDatePlay.text = trimString(string: dateTime, start: 0, offSet: 10)
        lblTimePlay.text = trimString(string: dateTime, start: 11, offSet: 8)
        lblSpeedPlay.text = speed + "km/h"
    }

    func trimString(string:String, start:Int,offSet:Int) -> String {
        let tldStartIndex1 = string.index(string.startIndex, offsetBy: start)
        let tldEndIndex1 = string.index(tldStartIndex1, offsetBy: offSet)
        let range1 = Range(uncheckedBounds: (lower: tldStartIndex1, upper: tldEndIndex1))
        return String(string[range1])
    }
    
    func timerForAutoSlider(_ isRun: Bool) {
        if isRun == false {
            if timerForSlider != nil {
                timerForSliderRunningFlag = false
                timerForSlider.invalidate()
                timerForSlider = nil
            }
            return
        }
        if timerForSliderRunningFlag == true {
            return
        }
        timerForSliderRunningFlag = true
        timerForSlider = Timer.scheduledTimer(timeInterval: timeValueForSliderPlay, target : self, selector: #selector(playHistoryTrackWithTimer), userInfo: nil, repeats: true)
    }
    
    func convertDateFormatForSliderPlay(string: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = originDateFormat
        let date = dateFormatter.date(from: string)
        //
        dateFormatter.dateFormat = convertDateFormat
        let stringWithRequestedDateFormat: String = dateFormatter.string(from: date!)
        return stringWithRequestedDateFormat
    }
    
    @objc func playHistoryTrackWithTimer() {
        if timeValueForSliderPlay != updateTimeValueForSliderPlay {
            timeValueForSliderPlay = updateTimeValueForSliderPlay
            if timerForSliderRunningFlag == true {
                timerForAutoSlider(false)
                timerForAutoSlider(true)
            }
        }
        if sliderValue >= sliderConfig.maximumValue {
            timerForAutoSlider(false)
            playHistoryFlag = false
            sliderValue = 0
            btnPlayHistoryTrack.setImage(playImage, for: .normal)
            return
        }
        
        sliderValue = sliderValue + 2
        if sliderValue >= sliderConfig.maximumValue {
            sliderValue = sliderConfig.maximumValue
        }
        sliderConfig.value = sliderValue
        playHistoryTrack(index: sliderConfig.value)
    }
    
    func drawPolyLineOnMap(annotations:[Station]) {
        if vtMapDisplayFlag == false {
            if annotations.first != nil {
                DispatchQueue.main.async {
                    self.gmCustomMarkerForMotor(annotation: (annotations.first?.coordinate)!,
                                                img: self.iconMotorImage, title: "", subtitle: "")
                }
                self.gmCustomMarker(annotation: (annotations.first?.coordinate)!,img: startFlag,title: "",subtitle: "")
                self.gmCustomMarker(annotation: (annotations.last?.coordinate)!,img: finishFlag,title: "",subtitle: "")
                self.gmapView.camera = GMSCameraPosition.camera(withLatitude: (allAnnotations.first?.lat)!,
                                                                longitude: (allAnnotations.first?.lng)!, zoom: 12)
            }else {
                Alert.alertAutoDismiss(delegate: self, message: noDataInThisTime)
            }
            let path = GMSMutablePath()
            var bounds = GMSCoordinateBounds()
            for annotation in annotations {
                path.add(annotation.coordinate)
                //
                bounds = bounds.includingCoordinate(annotation.coordinate)
                gmapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 80))
            }
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 2.0
            polyline.strokeColor = UIColor.blue
            polyline.geodesic = true
            polyline.map = self.gmapView
            openSlider()
            return
        }
        if annotations.first != nil {
            if(annotations.count>0){
                self.fitLocattionVtMap()
                openSlider()
                addPolyline(to: vtMap.style!)
                animatePolyline()
                
            }
             let padđing = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            guard let annotations = vtMap.annotations else {return} // get all annotations(markers on the mapview)
            vtMap.showAnnotations(annotations, edgePadding: padđing , animated: true, completionHandler: nil)
            //            self.vtCustomMarkerForMotor(annotation: (annotations.first?.coordinate)!, img: iconMotorImage, title: "", subtitle: "")
            //            self.vtCustomMarker(annotation: (annotations.first?.coordinate)!, img: startFlag, title: "", subtitle: "")
            //            self.vtCustomMarker(annotation: (annotations.last?.coordinate)!, img: finishFlag, title: "", subtitle: "")
            //            self.vtMap.setCenter(VMSLatLng.init(lat: (annotations.first?.lat)!, lng: (annotations.first?.lng)!), refresh: true)
            
        }else {
            Alert.alertAutoDismiss(delegate: self, message: self.noDataInThisTime)
        }
        let polylineOption = VMSPolylineOptions.init()
        for annotation in annotations {
            polylineOption.addPoint(VMSLatLng.init(lat:  annotation.lat, lng:  annotation.lng))
        }
        polylineOption.strokeColor = UIColor.blue
        polylineOption.strokeWidth = 2
//        vtMap.addPolyline(by: polylineOption)
//        let polyline = vtMap.addPolyline(by: polylineOption)
//        vtMap.fitBounds(polyline?.boundary)
//        vtMap.refresh()
        openSlider()
    }
    func fitLocattionVtMap(){
        var arrCoodinate = [CLLocationCoordinate2D]()
        for item in self.allAnnotations{
            arrCoodinate.append(item.coordinate)
        }
        self.vtMap.setVisibleCoordinates(arrCoodinate, count: UInt(arrCoodinate.count), edgePadding: UIEdgeInsets.init(top: 50, left: 50, bottom: 250, right: 50), animated: false)
    }
    // Mark: Current Location
    func timerReloadCurrentLocationRun(_ reloadFlag: Bool) {
        if reloadFlag == false {
            if timerForVehicleCurrentLocation != nil {
                timerForVehicleCurrentLocation.invalidate()
                timerForVehicleCurrentLocation = nil
            }
            return
        }else {
            if timerForVehicleCurrentLocation != nil {
                return
            }
            timerForVehicleCurrentLocation = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(getVehicleLocation), userInfo: nil, repeats: true)
        }
    }
    
    func saveCurrentLocation() {
        vehicle2show_currentAnnotation.latitude = vehicle2show.lat
        vehicle2show_currentAnnotation.longitude = vehicle2show.lng
    }
    
    func saveLastLocation() {
        lastAnnotationForRotation = vehicle2show_currentAnnotation
    }
    
    func convertDateSend2Service(inputString:String) -> String {
        var converted:String!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormatForDisplay + " " + timeFormatForSendRequest
        let dateStart = dateFormatter.date(from: inputString)
        let convertDateStartToMinlisecond = Int64((dateStart?.timeIntervalSince1970)!*1000)
        converted = String(convertDateStartToMinlisecond)
        return converted
    }
    
    
    // Mark: Request data
    func getMapAnnotations() {
        if vehicle2show.state == 0 {
            return
        }
        Loading().showLoading()
        var annotations:Array = [Station]()
        self.clearEverythingOnMap()
        let convertStartDate = convertDateSend2Service(inputString: dateStart2SendRequest + " " + timeStart2SendRequest)
        let convertEndDate = convertDateSend2Service(inputString: dateEnd2SendRequest + " " + timeEnd2SendRequest)
        let urlString = smartmotorUrl + reviewtransportUrl + tokenSave + "/" + userIdSave + "/" + String(vehicle2show.id) + "/" + convertStartDate + "/" + convertEndDate
        let url = URL(string: urlString)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        //
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        //
        let task = defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
            Loading().hideLoading()
            if data == nil || response == nil {
                Alert.alertAutoDismiss(delegate: self, message: self.wrongTimeOrNoDataThisTime)
                self.backToViewCurrentLocation()
                return
            }
            if error != nil {
                Alert.alertAutoDismiss(delegate: self, message: self.cannotLoadData)
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    Alert.alertAutoDismiss(delegate: self, message: self.cannotLoadData)
                    self.backToViewCurrentLocation()
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let jsonData:NSArray! = jsonResult["data"] as! NSArray
                    //Loading().hideLoading()
                    if jsonData == nil {
                        Alert.alertAutoDismiss(delegate: self, message: self.cannotLoadData)
                        self.backToViewCurrentLocation()
                        return
                    }
                    for element in jsonData{
                        let result = element as AnyObject
                        let lat = result.value(forKeyPath: "lat") as! Double
                        let lng = result.value(forKeyPath: "lng") as! Double
                        if lat != 0 && lng != 0 {
                            annotations.append(self.parseJson2GetHistoryAnnotation(result: result))
                        }
                    }
                    if annotations.count == 0 {
                        Alert.alertAutoDismiss(delegate: self, message: self.wrongTimeOrNoDataThisTime)
                        self.backToViewCurrentLocation()
                        return
                    }
                    self.allAnnotations = annotations
                    self.drawPolyLineOnMap(annotations: annotations)
                    //Loading().hideLoading()
                }catch {
                    Alert.alertAutoDismiss(delegate: self, message: "Lỗi kết nối. Vui lòng thử lại sau!")
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    func getAddressFromGmap(_ annotation: CLLocationCoordinate2D) {
        if vehicle2show.state == 0 {
            return
        }
        var address2Label:String! = ""
        var count:Int! = 0
        //
        let urlString = googleMapsURL + "\(String(annotation.latitude)),\(String(annotation.longitude))&key=" + googlePlaceAPI
        let url = URL(string: urlString)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        let jsonQuery = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if data == nil {
                DispatchQueue.main.async {
                    self.lblVehicleAdress.text = self.vehicleAdressString + self.noConfirmString
//                    self.lblVehicleType.text = self.vehicleType + vehicle2show.type
                    self.lblVehicleStatus.text = self.statusString
                        + self.noConfirmString
                }
                return
            }
            if error != nil {
                DispatchQueue.main.async {
                    self.lblVehicleAdress.text = self.vehicleAdressString + self.noConfirmString
//                    self.lblVehicleType.text = self.vehicleType + vehicle2show.type
                    self.lblVehicleStatus.text = self.statusString + self.noConfirmString
                }
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let jsonData = jsonResult.value(forKey: "results") as! NSArray!
                    if jsonData?.count == 0 {
                        DispatchQueue.main.async {
                            self.lblVehicleAdress.text = self.vehicleAdressString + self.noConfirmString
                            self.lblVehicleType.text = self.vehicleType + vehicle2show.type
                            self.lblVehicleStatus.text = self.statusString + self.noConfirmString
                        }
                        return
                    }
                    let result = (jsonData?[1] as AnyObject).value(forKey: "address_components") as! NSArray
                    for add in result {
                        let add_ex = add as AnyObject
                        address2Label = address2Label + (add_ex.value(forKey: "long_name") as! String)
                        count = count + 1
                        if count < result.count {
                            address2Label = address2Label + ", "
                        }
                    }
                    DispatchQueue.main.async {
                        self.lblVehicleAdress.text = self.vehicleAdressString + address2Label
//                        self.lblVehicleType.text = self.vehicleType + vehicle2show.type
                        if vehicle2show.state == 1 {
                            self.lblVehicleStatus.text = self.speedString + String(vehicle2show.motoSpeed) + self.kmPerHourString
                        }else {
                            self.lblVehicleStatus.text = self.displayVehicleStatusString[vehicle2show.state - 1]
                        }
                    }
                }catch {
                    fatalError("Failure\(error)")
                }
            }
        }
        jsonQuery.resume()
    }
    
    @objc func getVehicleLocation() {
        if vehicle2show.state == 0 {
            return
        }
        let regName = registerNo2Query.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)! as String
        let url = URL(string: smartmotorUrl + listTransportUrl + tokenSave + "/" + userIdSave + listTransportRegNoUrl + regName + listTransportPageIndexUrl + String(pageIndex))
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        //
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        //
        let task = defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
            if data == nil {
                return
            }
            if error != nil {
                Alert.alertAutoDismiss(delegate: self, message: self.cannotLoadData)
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    Alert.alertAutoDismiss(delegate: self, message: self.cannotLoadData)
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)) as! NSDictionary
                    let jsonData:NSArray! = jsonResult["data"] as! NSArray
                    if jsonData == nil {
                        return
                    }
                    self.saveLastLocation()
                    for element in jsonData {
                        let result = element as AnyObject
                        vehicle2show = self.parseJson2GetVehicleLocation(result: result)
                    }
                    self.saveCurrentLocation()
                    self.displayVehicleCurrentLocation(withZoomIn: true)
                    if self.vtMapDisplayFlag == true {
                        self.getAddressFromVTMap(self.vehicle2show_currentAnnotation)
                        return
                    }
                    self.getAddressFromGmap(self.vehicle2show_currentAnnotation)
                }catch {
                    Alert.alertAutoDismiss(delegate: self, message: "Lỗi kết nối. Vui lòng thử lại sau!")
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    
    // Mark: Parse data
    func parseJson2GetHistoryAnnotation(result:AnyObject) -> Station {
        var resultReturn:Station
        let gpsDate =  result.value(forKeyPath: parseJson2GetHistoryAnnotationString.gpsDate.rawValue) as! String
        let gpsSpeed = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.gpsSpeed.rawValue) as! Int
        let id = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.id.rawValue) as! Int
        let lat = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.lat.rawValue) as! Double
        let lng = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.lng.rawValue) as! Double
        let motoSpeed = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.motoSpeed.rawValue) as! Double
        let motorState = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.motorState.rawValue) as! Int
        let state = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.state.rawValue) as! Int
        let timeState = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.timeState.rawValue) as! Int
        resultReturn = Station(gpsDate:gpsDate,gpsSpeed:gpsSpeed,id:id,lat:lat,lng:lng,motoSpeed:motoSpeed,motorState: motorState,state:state,timeState:timeState)
        return resultReturn
    }
    
    func parseJson2GetVehicleLocation(result:AnyObject) -> vehicleInformation{
        var resultReturn:vehicleInformation!
        var gpsDate:String! = ""
        var isForbidden:Bool! = false
        let accIllegalState = result.value(forKeyPath: vehicleInfoString.accIllegalState.rawValue) as! Int
        let devicePin = result.value(forKeyPath: vehicleInfoString.devicePin.rawValue) as! String
        let deviceType = result.value(forKeyPath: vehicleInfoString.deviceType.rawValue) as! Int
        let deviceTypeIdOrginal = result.value(forKeyPath: vehicleInfoString.deviceTypeIdOrginal.rawValue) as! Int
        let factory = result.value(forKeyPath: vehicleInfoString.factory.rawValue) as! String
        if let _gpsDate = result.value(forKeyPath: vehicleInfoString.gpsDate.rawValue) as? String {
            gpsDate = _gpsDate
        }
        let gpsSpeed = result.value(forKeyPath: vehicleInfoString.gpsSpeed.rawValue) as! Int
        let gpsState = result.value(forKeyPath: vehicleInfoString.gpsState.rawValue) as! Int
        let groupsCode = result.value(forKeyPath: vehicleInfoString.groupsCode.rawValue) as! String
        let id = result.value(forKeyPath: vehicleInfoString.id.rawValue) as! Int
        let illegalMoveState = result.value(forKeyPath: vehicleInfoString.illegalMoveState.rawValue) as! Int
        let lat = result.value(forKeyPath: vehicleInfoString.lat.rawValue) as! Double
        let lng = result.value(forKeyPath: vehicleInfoString.lng.rawValue) as! Double
        let lowBatteryState = result.value(forKeyPath: vehicleInfoString.lowBatteryState.rawValue) as! Int
        let motoSpeed = result.value(forKeyPath: vehicleInfoString.motoSpeed.rawValue) as! Int
        let offPowerState = result.value(forKeyPath: vehicleInfoString.offPowerState.rawValue) as! Int
        let regNo = result.value(forKeyPath: vehicleInfoString.regNo.rawValue) as! String
        let sim = result.value(forKeyPath: vehicleInfoString.sim.rawValue) as! String
        let sosState = result.value(forKeyPath: vehicleInfoString.sosState.rawValue) as! Int
        let state = result.value(forKeyPath: vehicleInfoString.state.rawValue) as! Int
        let timeState = result.value(forKeyPath: vehicleInfoString.timeState.rawValue) as! Int
        let type = result.value(forKeyPath: vehicleInfoString.type.rawValue) as! String
        let vibrationState = result.value(forKeyPath: vehicleInfoString.vibrationState.rawValue) as! Int
        if let _isForbidden = result.value(forKeyPath: vehicleInfoString.isForbidden.rawValue) as? Bool {
            isForbidden = _isForbidden
        }
        resultReturn = vehicleInformation(accIllegalState:accIllegalState,devicePin:devicePin,deviceType:deviceType,deviceTypeIdOrginal:deviceTypeIdOrginal,factory:factory,gpsDate:gpsDate,gpsSpeed:gpsSpeed,gpsState:gpsState,groupsCode:groupsCode,id:id,illegalMoveState:illegalMoveState,lat:lat,lng:lng,lowBatteryState:lowBatteryState,motoSpeed:motoSpeed,offPowerState:offPowerState,regNo:regNo,sim:sim,sosState:sosState,state:state,timeState:timeState,type:type,vibrationState:vibrationState,isForbidden:isForbidden)
        return resultReturn
    }

    // Mark:
    func gotoListVehicleView() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "listVehicle") as! ListVehicleVC
        timerReloadCurrentLocationRun(false)
        loginFlag = false
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }

    func gotoSettingView() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "setting") as! SettingVC
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    // Mark: Call to vehicle
    func call2APhone() {
        let phoneUrl = "tel://" + vehicle2show.sim    //"tel://\(19008198)"
        let url:URL = URL(string: phoneUrl)!
        UIApplication.shared.openURL(url)
    }
    
    // Mark: Tap on background to close warning
    func initTapInConfirmBackgroundView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(MapVC.handleConfirmBackgroundViewTap(_:)))
        backgroundView.addGestureRecognizer(tap)
    }
    
    @objc func handleConfirmBackgroundViewTap(_ sender: UITapGestureRecognizer) {
        closeViewUpperBackground()
    }
    
    func closeViewUpperBackground() {
        confirmSendLockView.isHidden = true
        confirmCallVehicleView.isHidden = true
        confirmSendUnlockView.isHidden = true
        warningWrongLocation.isHidden = true        
        if selectDateTimeView.isHidden == false && datePickerView.isHidden == false {
            datePickerView.isHidden = true
            selectDateTimeViewContraint.constant = (view.frame.size.height - selectDateTimeView.frame.size.height)/2
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            return
        }else {
            selectDateTimeView.isHidden = true
        }
        backgroundView.isHidden = true
    }
    
    func initTapToCloseDatePicker() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(MapVC.handleCloseDatePickerTap(_:)))
        selectDateTimeView.addGestureRecognizer(tap)
    }
    
    @objc func handleCloseDatePickerTap(_ sender: UITapGestureRecognizer) {
        if selectDateTimeView.isHidden == false && datePickerView.isHidden == false {
            datePickerView.isHidden = true
            selectDateTimeViewContraint.constant = (view.frame.size.height - selectDateTimeView.frame.size.height)/2
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            return
        }
    }
    
    @IBAction func onDetail(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DetailHistoryViewController") as! DetailHistoryViewController
        if  (self.allAnnotations.count > 0){
            nextViewController.arrAnotation = self.allAnnotations
            let navigation = UINavigationController(rootViewController: nextViewController)
            navigation.modalPresentationStyle = .fullScreen
            self.present(navigation, animated: true, completion: nil)
        }
    }
}

extension MapVC: GMSMapViewDelegate {
    func initGMap() {
        gmapView.clear()
        gmapView.delegate = self
        gmapView.settings.compassButton = false
        gmapView.settings.rotateGestures = false
        gmapView.camera = GMSCameraPosition.camera(withLatitude: self.vehicle2show_currentAnnotation.latitude,longitude: self.vehicle2show_currentAnnotation.longitude,zoom: 15)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        tapToMapMarkerForDisplayStatusView()
        return true
    }
    
}

extension MapVC: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sendMessage(phoneNumber: String, content: String) {
        let messageVC = MFMessageComposeViewController()
        messageVC.messageComposeDelegate = self
        messageVC.body = content
        messageVC.recipients = [phoneNumber]
        messageVC.modalPresentationStyle = .fullScreen
        self.present(messageVC, animated: false, completion: nil)
    }
    
}

extension MapVC:VMSMapDelegate,VMSMapObjectSingleTabDelegate {
    func initVTMap() {
        let mapCGRect = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        vtMap = MGLMapView(frame: mapCGRect, styleURL: MGLStyle.streetsStyleURL)
        vtMap.delegate = self
        vtmapView.addSubview(vtMap)
       
    }
    

    func singleTap(at pt: CGPoint, latLng: VMSLatLng!) -> Bool {
        //tapToMapMarkerForDisplayStatusView()
        return true
    }
    
    func singleTabMapObject(_ mapObject: VMSMapObject!, point pt: CGPoint, latLng: VMSLatLng!) -> Bool {
        tapToMapMarkerForDisplayStatusView()
        return true
    }
    
    func singleTabMapObject1(_ mapObject: VMSMapObject!, point pt: CGPoint, latLng: VMSLatLng!, viewController: UIViewController!, mapView: VMSMapView!) -> Bool {
        return false
    }
}

extension MapVC {
    func getAddressFromVTMap(_ annotation: CLLocationCoordinate2D) {
        
        let VTMapAccessToken = "baa64fae6b1efc8df706d7ffcdd49deb"
        
        self.geocoder = Geocoder(accessToken:VTMapAccessToken
        )
        let options =  ForwardGeocodeOptions(query: "getReguest")
        options.allowedISOCountryCodes = ["VN"]
        options.focalLocation = CLLocation(latitude: annotation.latitude, longitude: annotation.longitude)
        geocodingDataTask = geocoder.geocode(options) { [unowned self]  (placemarks, attribution, error) in
            guard let placemark = placemarks?.first else {
                return
            }
            
            
        }
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(annotation.latitude)")!
        //21.228124
        let lon: Double = Double("\(annotation.longitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if placemarks == nil{
                    return
                }
                let pm = placemarks! as [CLPlacemark]
                if pm.count > 0 {
                    let pm = placemarks![0]

                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                        DispatchQueue.main.async {
                            self.lblVehicleAdress.text = self.vehicleAdressString + addressString
//                            self.lblVehicleType.text = self.vehicleType + vehicle2show.type
//                            print("%l",vehicle2show.timeState)
                            let dateTime = Date(timeIntervalSince1970: Double(vehicle2show.timeState))
                            let dateFormatter = DateFormatter()
                            dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
                            dateFormatter.locale = NSLocale.current
                            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ" //Specify your format that you want
                            let strDate = dateFormatter.string(from: dateTime)
                            guard let date = dateFormatter.date(from: strDate) else {
                                   return
                               }
                             dateFormatter.dateFormat = "yyyy"
                             let year = dateFormatter.string(from: date)
                            dateFormatter.dateFormat = "MM"
                            let month = dateFormatter.string(from: date)
                            dateFormatter.dateFormat = "dd"
                            let day = dateFormatter.string(from: date)
                            dateFormatter.dateFormat = "HH"
                            let hour = dateFormatter.string(from: date)
                            dateFormatter.dateFormat = "mm"
                            let munite = dateFormatter.string(from: date)
                            dateFormatter.dateFormat = "ss"
                            let seconds = dateFormatter.string(from: date)
                            print(String.init(format: " year : %@, month : %@,day : %@ , hour : %@ , munites : %@ ", year,month,day,hour,munite))
                            if  hour.count > 0 {
                                let hourInt = Int(hour) ?? 0
                                if  (hourInt <= 1){
                                    self.lblVehicleType.text =  String.init(format:"%@ %@ phút %@ giây", self.lblTimeString, munite,seconds)
                                }else{
                                    self.lblVehicleType.text =  String.init(format:"%@ %@ giờ %@ phút", self.lblTimeString, hour,munite)
                                }
                                
                            }

                            
                            if vehicle2show.state == 1 {
                                self.lblVehicleStatus.text = self.speedString + String(vehicle2show.motoSpeed) + self.kmPerHourString
                            }else {
                                self.lblVehicleStatus.text = self.displayVehicleStatusString[vehicle2show.state - 1]
                            }
                        }
//                    })
                    
                    
                    print(addressString)
                }
        })
        
        
    }
    
}

extension DateFormatter{
    
    func formatDateTimeToSting(time:String)->String{
        let dateTime = Date(timeIntervalSince1970: Double(vehicle2show.timeState))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "dd-MM-yyyy " //Specify your format that you want
        let strDate = dateFormatter.string(from: dateTime)
        return strDate
    }
}



extension MapVC: URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
    
}

extension MapVC: CLLocationManagerDelegate {
    func initMyLocation() {
        //user location stuff
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //print("Error" + error.description)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        valueLocation = locations.last!.coordinate
        locationManager.stopUpdatingLocation()
    }
}

class caculateAngleWithCoordiante {
    func DegreesToRadians(degrees: Double) -> Double {
        return degrees * M_PI / 180.0
    }
    
    func RadiansToDegrees(radians: Double) -> Double {
        return radians * 180.0/M_PI
    }
    
    func angle(firstLocation: CLLocationCoordinate2D, secondLocation: CLLocationCoordinate2D) -> Double {
        let lat1 = DegreesToRadians(degrees: firstLocation.latitude)
        let lon1 = DegreesToRadians(degrees: firstLocation.longitude)
        let lat2 = DegreesToRadians(degrees: secondLocation.latitude)
        let lon2 = DegreesToRadians(degrees: secondLocation.longitude)
        let dLon = lon2 - lon1
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        var radiansBearing = atan2(y, x)
        if(radiansBearing < 0.0) {
            radiansBearing += 2*M_PI
        }
        let degreesBearing = RadiansToDegrees(radians: radiansBearing)
        return degreesBearing
    }
    
    
}
extension MapVC : MGLMapViewDelegate{
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        // Wait for the map to load before initiating the first camera movement.
        
        // Create a camera that rotates around the same center point, rotating 180°.
        // 'fromDistance:' is meters above mean sea level that an eye would have to be in order to see what the map view is showing.
//        let camera = MGLMapCamera(lookingAtCenter: self.vehicle2show_currentAnnotation, altitude: 3000, pitch: 15, heading: 180)
//
//        // Animate the camera movement over 5 seconds.
//        mapView.setCamera(camera, animated: true)
        
        
        //        mapView.setCamera(camera, withDuration: 5, animationTimingFunction: CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut))
    }
    func addPolyline(to style: MGLStyle) {
        // Add an empty MGLShapeSource, we’ll keep a reference to this and add points to this later.
        let source = MGLShapeSource(identifier: "polyline", shape: nil, options: nil)
        style.addSource(source)
        polylineSource = source
        
        // Add a layer to style our polyline.
        let layer = MGLLineStyleLayer(identifier: "polyline", source: source)
        layer.lineJoin = NSExpression(forConstantValue: "round")
        layer.lineCap = NSExpression(forConstantValue: "round")
        layer.lineColor = NSExpression(forConstantValue: UIColor.blue)
        
        // The line width should gradually increase based on the zoom level.
        layer.lineWidth = NSExpression(format: "mgl_interpolate:withCurveType:parameters:stops:($zoomLevel, 'linear', nil, %@)",
                                       [14: 5, 18: 20])
        style.addLayer(layer)
        addStartAndEndPoint(shape : self.vtStartPointMotor, annotation : (self.allAnnotations.first?.coordinate)!, image : self.startFlag, sourceIndentifier : "marker-startSource", layerIndentifier : "marker-startStyle", foraname : "start-symbol")
        addStartAndEndPoint(shape : self.vtEndPointMotor, annotation : (self.allAnnotations.last?.coordinate)!, image : self.finishFlag, sourceIndentifier : "marker-endSource", layerIndentifier : "marker-endStyle", foraname: "end-symbol")
    }
    
    func animatePolyline() {
        currentIndex = 1
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(tick), userInfo: nil, repeats: true)
    }
    
    @objc func tick() {
        if currentIndex > allAnnotations.count {
            timer?.invalidate()
            timer = nil
            return
        }
        
        var coordinatesAnotation = [CLLocationCoordinate2D]()
        for item in self.allAnnotations{
            coordinatesAnotation.append(item.coordinate)
        }
        // Update our MGLShapeSource with the current locations.
        updatePolylineWithCoordinates(coordinates: coordinatesAnotation)
        
        currentIndex += 1
    }
    
    func updatePolylineWithCoordinates(coordinates: [CLLocationCoordinate2D]) {
        var mutableCoordinates = coordinates
        
        let polyline = MGLPolylineFeature(coordinates : &mutableCoordinates ,count : UInt(mutableCoordinates.count))
        
        polylineSource?.shape = polyline
    }
    func mapView(_ mapView: MGLMapView, didFinishLoading style: MGLStyle) {
        
        vtCustomMarkerForMotor(annotation :  self.vehicle2show_currentAnnotation, sourceIdentifier : "marker-source", styleIdentifier : "marker-style", image : iconMotorImage, forname : "home-symbol")
        
        
        
    }
}

//DispatchQueue.main.async {
//
//}

//OperationQueue.main.addOperation {
//
//}







