//
//  ListVehicleCustumCell.swift
//  Smart Motor
//
//  Created by vietnv2 on 26/10/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

class ListVehicleCustumCell: UITableViewCell {
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblRegNo: UILabel!
    @IBOutlet weak var lblName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
