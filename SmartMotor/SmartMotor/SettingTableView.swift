//
//  SettingTableView.swift
//  SmartMotor
//
//  Created by vietnv2 on 12/5/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit
import Alamofire
import CryptoSwift
extension SettingVC: UITableViewDelegate, UITableViewDataSource,PassDataDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.isScrollEnabled = false
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRow:Int!
        if tableView == settingTableView {
            numberOfRow = settingArray.count
        }else if tableView == alarmTableView {
            numberOfRow = alarmSettingArray.count
        }
        return numberOfRow
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell2Return: UITableViewCell!
        if tableView == settingTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell", for: indexPath) as! SettingCustumCell
            cell.lblSetting.text = settingArray[indexPath.row]
            cell.imgIcon.image = settingImageArray[indexPath.row]
            
            
            cell2Return = cell
        }else if tableView == alarmTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "alarmCell", for: indexPath) as! AlarmSettingCell
            cell.lblAlarm.text = alarmSettingArray[indexPath.row]
            cell.imgAlarm.image = alarmImageArray[indexPath.row]
            cell2Return = cell
        }
        return cell2Return
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        backGroundSettingView.isHidden = false
     
        
        if tableView == settingTableView {
            switch indexPath.row {
            case 0:vehicleRegistryView.isHidden = false
            case 1:listAlarmSettingView.isHidden = false
            case 2:deviceFormatView.isHidden = false
            case 3 :
//                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SettingPoupVC") as! SettingPoupVC
                let nextViewController = SettingPoupVC()
                nextViewController.modalPresentationStyle = .overFullScreen
                nextViewController.modalTransitionStyle = .crossDissolve
                nextViewController.delegate = self
                self.present(nextViewController, animated: true, completion: nil)
                
                
            case 4:changePasswordView.isHidden = false
            case 5:deviceRebootView.isHidden = false
            default:break
            }
            
        }else if tableView == alarmTableView {
            listAlarmSettingView.isHidden = true
            switch indexPath.row {
            case 0:accAlarmSettingView.isHidden = false
            case 1:vibrateAlarmSettingView.isHidden = false
            case 2:fenceAlarmSettingView.isHidden = false
            case 3:speedAlarmSettingView.isHidden = false
            default:break
            }
        }
    }
    func passData(_ olddPass: String, _ newPass: String) {
        backGroundSettingView.isHidden = true
        Loading().showLoading()
        
         let encrypKeyForLogin = "PHANNANGDUYBKAHN"
        let header : HTTPHeaders = ["Content-Type":"application/json",
                                    "token": tokenSave
        ]
        let parameters : [String: Any] = ["userId":userIdSave,
                                          "oldpassword" : try! olddPass.encrypt(cipher: AES(key: encrypKeyForLogin, iv: "")).uppercased(),
                          "newpassword" : try! newPass.encrypt(cipher: AES(key: encrypKeyForLogin, iv: "")).uppercased()
        ]
        print(try! olddPass.encrypt(cipher: AES(key: encrypKeyForLogin, iv: "")))
        print(try! newPass.encrypt(cipher: AES(key: encrypKeyForLogin, iv: "")))
        let url =  "http://smartmotor.viettel.vn/mtapi/rest/auth/changePassword"
        AF.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header, interceptor: nil, requestModifier: nil).responseJSON { (res) in
            switch(res.result){
            case .success(let resurtData):
                let listData: Dictionary<String, Any> =  resurtData as! Dictionary<String, Any>
                let resultCode = listData["resultCode"] as! Int
                switch resultCode {
                case 1:
                    Alert.alertAutoDismiss(delegate: self, message: "Mã captcha không chính xác!")
                    break
                case -1,-2:
                    Alert.alertAutoDismiss(delegate: self, message: "Thông tin tài khoản ko hợp lệ")
                    break
                case -5 :
                    Alert.alertAutoDismiss(delegate: self, message: "Mật khẩu hiện tại không đúng")
                    break
                case -6,-7,-8,-9:
                    Alert.alertAutoDismiss(delegate: self, message: "Mật khẩu mới không hợp lệ, mật khẩu phải không dấu, mật khẩu phải chứa ít nhất 8 ký tự, nhiều nhất 20 kí tự, gồm: Số, chữ thường, chữ hoa, ký tự đặc biệt")
                    break
                default:
                    Alert.alertAutoDismiss(delegate: self, message: "Mật khẩu mới trùng mật khẩu cũ")
                    break
                }
                
                break
                Loading().hideLoading()
            case .failure(let error):
                print("res data: \(error)");
//                completionHandler(nil, error as NSError?)
                break
                Loading().hideLoading()
                
            }
        }
//            { (res) in
//
//
//
//            print(res.data)
//            print(res.response)
//            print(res.result)
//            print(res.error)
//
        Loading().hideLoading()
      }
//
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat!
        if tableView == settingTableView {
            height = 60
        }else if tableView == alarmTableView {
            height = 45
        }
        return height
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = UIColor.lightGray
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = .clear
    }
}





