//
//  Enum.swift
//  SmartMotor
//
//  Created by Toan on 6/2/21.
//  Copyright © 2021 vietnv2. All rights reserved.
//

import UIKit


enum userGender  : String {
    case male = "male"
    case female = "female"
    case another = "other"
}
