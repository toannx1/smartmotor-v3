//
//  MapVCCall.swift
//  SmartMotor
//
//  Created by Vietnv2 on 3/11/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import UIKit

extension MapVC {
    // Mark: Call to vehicle
    func call2APhone() {
        let phoneUrl = "tel://" + vehicle2show.sim // "tel://\(19008198)"
        let url: URL = URL(string: phoneUrl)!
        UIApplication.shared.openURL(url)
    }
}
