//
//  DetailPointTableView.swift
//  SmartMotor
//
//  Created by Vietnv2 on 3/30/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import UIKit

extension MapVC: UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    // MARK: - Pick time
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerTimeRoute {
            return pickerData.count
        } else {
            return pickerData2.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerTimeRoute {
            return pickerData[row]
        } else {
            return pickerData2[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerTimeRoute {
            print(pickerData[row])
            strTimeTB = pickerData[row]
        } else {
            print(pickerData2[row])
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 30))
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 18, weight: .semibold)
        label.add(Border: .bottom, withColor: .lightGray)
        if pickerView == pickerNumber {
            label.text = pickerData2[row]
        } else {
            label.text = pickerData[row]
        }
        label.sizeToFit()
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
    
    // MARK: - Chi tiết hành trình
    
    @objc func goDetailRoute() {
        detailViewPointFlag = true
        // stop play history
        playHistoryFlag = false
        btnPlayHistoryTrack.setImage(#imageLiteral(resourceName: "btn_resume_media"), for: .normal)
        timerForAutoSlider(false)
        removeAddressView()
        displayDetailPointView()
        
        setDatePickerTBFilter()
        
        dataTotalTime = dataTotal
        dataTableRoute = dataTotalTime
        setupViewRoute()
        routeTableView.reloadData()
        pickerTimeRoute.selectRow(0, inComponent: 0, animated: true)
    }
    
    func displayDetailPointView() {
        removeAddressView()
        detailPointView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(detailPointView)
        detailPointTableView.reloadData()
        routeTableView.reloadData()
        
        // lblDistanceJourney.text = String(format: "%@ %.2f Km", txtContent03DetailPoint[languageChooseSave], distanceJourney)
        let strKM = String(format: "%.2f", distanceJourney)
        let attrs1 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17), NSAttributedString.Key.foregroundColor: UIColor.black]
        let attrs2 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor.black]
        let attributedString1 = NSMutableAttributedString(string: "Tổng khoảng cách: ", attributes: attrs1)
        let attributedString2 = NSMutableAttributedString(string: strKM, attributes: attrs2)
        let attributedString3 = NSMutableAttributedString(string: " (Km)", attributes: attrs1)
        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        lblDistanceJourney.attributedText = attributedString1
        // lblDistanceJourney.text = "Tổng khoảng cách: \(distanceJourney) Km"
        
        lblTitleDetailPoint.text = txtTitleDetailPoint[languageChooseSave].uppercased()
        lblContent01DetailPoint.text = txtContent01DetailPoint[languageChooseSave]
        lblContent02DetailPoint.text = txtContent02DetailPoint[languageChooseSave]
    }
    
    func removeDetailPointView() {
        detailPointView.removeFromSuperview()
    }
    
    func displayAddressView() {
        removeDetailPointView()
        if viewCurrentLocationFlag == true || detailViewPointFlag == true || playHistoryFlag == true {
            return
        }
        addressView.frame = CGRect(x: 0, y: view.frame.size.height - 130, width: view.frame.size.width, height: 0) // 80 + 50
        view.addSubview(addressView)
    }
    
    func removeAddressView() {
        addressView.removeFromSuperview()
        lblContentAddressView.text = String(format: "%@...", txtStatus01[languageChooseSave])
        // lbInfomationAddress.text = String(format:"%@...", txtStatus01[languageChooseSave]) // lv thông tin view map
    }
    
    @objc func closeHistoryMapview() {
        viewSelectStatusRouteTB.isHidden = true
        viewSelectDateRouteTB.isHidden = true
        removeDetailPointView()
        detailViewPointFlag = false
        if vtMapDisplayFlag == false {
            getAddressFromGmap(allAnnotations[Int(sliderValue)].coordinate)
            displayAddressView()
            return
        }
        getAddressFromVTMap(allAnnotations[Int(sliderValue)].coordinate)
        displayAddressView()
    }
    
    // MARK: - Back to map
    
    @IBAction func btnBackToMapViewTapped(_ sender: Any) {
        closeHistoryMapview()
        if viewCurrentLocationFlag == false {
            showHideInfo()
            sliderConfig.value = 0
            sliderValue = 0
            playHistoryFlag = false
            timerForAutoSlider(false)
            sliderSpeed = 1
            btnSpeedPlay.setImage(#imageLiteral(resourceName: "Speedx1"), for: .normal)
            btnPlayHistoryTrack.setImage(#imageLiteral(resourceName: "btn_resume_media"), for: .normal)
            removeSliderView()
            removeAddressView()
            
            backToViewCurrentLocation()
            return
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // return 1
        var numOfSection: NSInteger = 0
        let noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: routeTableView.bounds.size.width, height: routeTableView.bounds.size.height))
        noDataLabel.padding = .init(top: 0, left: 0, bottom: 0, right: 0)
        noDataLabel.text = "Không có dữ liệu"
        noDataLabel.textColor = UIColor.black
        noDataLabel.textAlignment = NSTextAlignment.center
        
        if dataTableRoute.count != 0 {
            if dataTableRoute.count == 1 {
                if dataTableRoute[0].count == 0 {
                    routeTableView.backgroundView = noDataLabel
                    return numOfSection
                }
            } else {
                if dataTableRoute[0].count == 0, dataTableRoute[1].count == 0 {
                    routeTableView.backgroundView = noDataLabel
                    return numOfSection
                }
            }
            routeTableView.backgroundView = nil
            numOfSection = dataTableRoute.count
        } else {
            routeTableView.backgroundView = noDataLabel
        }
        return numOfSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case listChargeMoneyTypeTableView:
            return 2 // listChargeMoneyString.count
//        case listMonth2PayTableView:
//            return numberOfMonth.count
        case routeTableView:
            // return allAnnotations.count
            return dataTableRoute[section].count
        default:
            return allAnnotations.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if dataTableRoute[section].count == 0 {
            return UIView()
        }
        let dateN = dataTableRoute[section][0].gpsDate
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: dateN)!
        dateFormatter.dateFormat = "dd/MM/yyyy"
        // dateFormatter.dateFormat = "HH:mm:ss"
        let time = dateFormatter.string(from: date)
        
        // let dateString = Date(timeIntervalSince1970: dateN / 1000).toDateString("EEEE, dd MMMM yyyy")
        let header = UILabel(text: "    " + time, font: .systemFont(ofSize: 17, weight: .bold), textColor: .black, textAlignment: .left)
        header.backgroundColor = #colorLiteral(red: 0.9402173162, green: 0.9692956805, blue: 0.9904792905, alpha: 1)
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case listChargeMoneyTypeTableView:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellForChargeMoney", for: indexPath) as! ChargeMoneyCell
            
            if indexPath.row == 0 {
                cell.lblChargeMoneyStype.text = listChargeMoney_PaymentViaCard[languageChooseSave]
            } else {
                cell.lblChargeMoneyStype.text = listChargeMoney_PaymentViaPhoneBalance[languageChooseSave]
            }
            return cell
            
        case routeTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "routeCell", for: indexPath) as! RouteTableViewCell
            cell.selectionStyle = .none
            let row = indexPath.row
            let indexSection = indexPath.section
            cell.lbStt.text = row.description
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            let date = dateFormatter.date(from: dataTableRoute[indexSection][row].gpsDate)!
            // dateFormatter.dateFormat = "dd-MM-yyyy"
            dateFormatter.dateFormat = "HH:mm:ss"
            let time = dateFormatter.string(from: date)
            cell.lbTime.text = time
            
            if (row % 2) == 0 {
                cell.backgroundColor = UIColor.white
            } else {
                cell.backgroundColor = #colorLiteral(red: 0.8979603648, green: 0.8980897069, blue: 0.8979322314, alpha: 1)
            }
            
//            let state = allAnnotations[indexPath.row].state
            let state = dataTableRoute[indexSection][row].state
            switch state {
            case 1: // run
                cell.lbStatus.text = txtStatus_DetailPoint_Run[languageChooseSave]
                let speedCar = Int(dataTableRoute[indexSection][row].gpsSpeed)
                // Int(allAnnotations[indexPath.row].gpsSpeed)
                cell.lbStatus.text = speedCar.description + " Km/h"
                cell.lbStatus.backgroundColor = #colorLiteral(red: 0.004271617159, green: 0.4011737704, blue: 0.0002964493178, alpha: 1)
                cell.lbStatus.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            case 2: // stop
                cell.lbStatus.text = txtStatus_DetailPoint_Stop[languageChooseSave]
                cell.lbStatus.backgroundColor = #colorLiteral(red: 0.8430630565, green: 0.8431850076, blue: 0.8430364728, alpha: 1)
                cell.lbStatus.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            case 3: // park
                cell.lbStatus.text = txtStatus_DetailPoint_Park[languageChooseSave]
                cell.lbStatus.backgroundColor = #colorLiteral(red: 0.999996841, green: 0.8015163541, blue: 0.002458711388, alpha: 1)
                cell.lbStatus.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            case 4: // lost gps
                cell.lbStatus.text = txtStatus_DetailPoint_LossGPS[languageChooseSave]
                cell.lbStatus.backgroundColor = #colorLiteral(red: 0.8603786826, green: 0.0003520670871, blue: 0.001728266943, alpha: 1)
                cell.lbStatus.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            case 5: // hibernate
                cell.lbStatus.text = txtStatus_DetailPoint_Hibernate[languageChooseSave]
                cell.lbStatus.backgroundColor = #colorLiteral(red: 0.4195685685, green: 0.4196329713, blue: 0.4195545018, alpha: 1)
                cell.lbStatus.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            case 6: // lost gps
                cell.lbStatus.text = txtStatus_DetailPoint_LossGPRS[languageChooseSave]
                cell.lbStatus.backgroundColor = #colorLiteral(red: 0.8646463752, green: 0, blue: 6.92601534e-05, alpha: 1)
                cell.lbStatus.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            default: // no info
                cell.lbStatus.text = txtStatus_DetailPoint_NoInfo[languageChooseSave]
                cell.lbStatus.backgroundColor = #colorLiteral(red: 0.004271617159, green: 0.4011737704, blue: 0.0002964493178, alpha: 1)
            }
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as! DetailPointViewCell
            //
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            let date = dateFormatter.date(from: allAnnotations[indexPath.row].gpsDate)!
            //
            dateFormatter.dateFormat = "dd-MM-yyyy"
            cell.lblDate.text = dateFormatter.string(from: date)
            //
            dateFormatter.dateFormat = "HH:mm:ss"
            let time = dateFormatter.string(from: date)
            cell.lblTime.text = time
            //
            if (indexPath.row % 2) == 0 {
                cell.backgroundColor = UIColor.white
            } else {
                cell.backgroundColor = UIColor(red: 220 / 255, green: 220 / 255, blue: 220 / 255, alpha: 0.7)
            }
            
            func calculateTime() -> String {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                if indexPath.row < allAnnotations.count - 1 {
                    let date1 = dateFormatter.date(from: allAnnotations[indexPath.row].gpsDate)!
                    let date2 = dateFormatter.date(from: allAnnotations[indexPath.row + 1].gpsDate)!
                    
                    let interval = date2.timeIntervalSince(date1)
                    return stringFromTimeInterval(interval: interval)
                } else {
                    let date1 = dateFormatter.date(from: allAnnotations[indexPath.row].gpsDate)!
                    // change format date "dd/MM/yyyy" -> "yyyy-MM-dd"
                    let dateFormatter1 = DateFormatter()
                    dateFormatter1.dateFormat = "dd/MM/yyyy"
                    let dt = dateFormatter1.date(from: dateEnd2SendRequest)
                    dateFormatter1.dateFormat = "yyyy-MM-dd"
                    let date2String = dateFormatter1.string(from: dt!)
                    // format date
                    let date2 = dateFormatter.date(from: date2String + "T" + timeEnd2ViewDetailPoint + "+07:00")
                    //
                    let currentDate = Date()
                    let dateFormatter2 = DateFormatter()
                    dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                    let date3String = dateFormatter2.string(from: currentDate)
                    let date3 = dateFormatter2.date(from: date3String)
                    // check last time
                    let checkLastTime = date3?.timeIntervalSince(date2!)
                    var end: Date!
                    if Int(checkLastTime!) > 0 {
                        end = date2
                    } else {
                        end = date3
                    }
                    //
                    let interval = end?.timeIntervalSince(date1)
                    return stringFromTimeInterval(interval: interval!)
                }
                // return ""
            }
            let state = allAnnotations[indexPath.row].state
            switch state {
            case 1: // run
                // let color = UIColor(red: 0x69/0xFF, green: 0xaf/0xFF, blue: 0x11/0xFF, alpha: 1)
                let color = UIColor(red: 0x35 / 0xFF, green: 0x87 / 0xFF, blue: 0x00 / 0xFF, alpha: 1)
                cell.lblState.text = txtStatus_DetailPoint_Run[languageChooseSave]
                cell.lblStateDetail.text = String(allAnnotations[indexPath.row].gpsSpeed) + " Km/h"
                cell.lblState.textColor = color
                cell.lblStateDetail.textColor = color
                cell.lblDate.textColor = color
                cell.lblTime.textColor = color
            case 2: // stop
                // let color = UIColor(red: 0x06/0xFF, green: 0x71/0xFF, blue: 0x6D/0xFF, alpha: 1)
                let color = UIColor(red: 0x00 / 0xFF, green: 0x76 / 0xFF, blue: 0xAA / 0xFF, alpha: 1)
                cell.lblState.text = txtStatus_DetailPoint_Stop[languageChooseSave]
                cell.lblStateDetail.text = calculateTime()
                cell.lblState.textColor = color
                cell.lblStateDetail.textColor = color
                cell.lblDate.textColor = color
                cell.lblTime.textColor = color
            case 3: // park
                // let color = UIColor(red: 0xcb/0xFF, green: 0x9b/0xFF, blue: 0x1b/0xFF, alpha: 1)
                let color = UIColor(red: 0xFF / 0xFF, green: 0xB6 / 0xFF, blue: 0x00 / 0xFF, alpha: 1)
                cell.lblState.text = txtStatus_DetailPoint_Park[languageChooseSave]
                cell.lblStateDetail.text = calculateTime()
                cell.lblState.textColor = color
                cell.lblStateDetail.textColor = color
                cell.lblDate.textColor = color
                cell.lblTime.textColor = color
            case 4: // lost gps
                // let color = UIColor(red: 0xDD/0xFF, green: 0x70/0xFF, blue: 0x09/0xFF, alpha: 1)
                let color = UIColor(red: 0xFF / 0xFF, green: 0x6D / 0xFF, blue: 0x00 / 0xFF, alpha: 1)
                cell.lblState.text = txtStatus_DetailPoint_LossGPS[languageChooseSave]
                cell.lblStateDetail.text = calculateTime()
                cell.lblState.textColor = color
                cell.lblStateDetail.textColor = color
                cell.lblDate.textColor = color
                cell.lblTime.textColor = color
            case 5: // hiberbate
                // let color = UIColor(red: 0x57/0xFF, green: 0x57/0xFF, blue: 0x57/0xFF, alpha: 1)
                let color = UIColor(red: 0x51 / 0xFF, green: 0x51 / 0xFF, blue: 0x51 / 0xFF, alpha: 1)
                cell.lblState.text = txtStatus_DetailPoint_Hibernate[languageChooseSave]
                cell.lblStateDetail.text = calculateTime()
                cell.lblState.textColor = color
                cell.lblStateDetail.textColor = color
                cell.lblDate.textColor = color
                cell.lblTime.textColor = color
            case 6: // lost gps
                // let color = UIColor(red: 0xA9/0xFF, green: 0x0F/0xFF, blue: 0x09/0xFF, alpha: 1)
                let color = UIColor(red: 0xFF / 0xFF, green: 0x17 / 0xFF, blue: 0x17 / 0xFF, alpha: 1)
                cell.lblState.text = txtStatus_DetailPoint_LossGPRS[languageChooseSave]
                cell.lblStateDetail.text = calculateTime()
                cell.lblState.textColor = color
                cell.lblStateDetail.textColor = color
                cell.lblDate.textColor = color
                cell.lblTime.textColor = color
            default: // no info
                // let color = UIColor(red: 0xa9/0xFF, green: 0x0f/0xFF, blue: 0x09/0xFF, alpha: 1)
                let color = UIColor(red: 0x77 / 0xFF, green: 0x1C / 0xFF, blue: 0xF9 / 0xFF, alpha: 1)
                cell.lblState.text = txtStatus_DetailPoint_NoInfo[languageChooseSave]
                cell.lblStateDetail.text = ""
                cell.lblState.textColor = color
                cell.lblDate.textColor = color
                cell.lblTime.textColor = color
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView {
        case listChargeMoneyTypeTableView:
            if indexPath.row == 0 {
                removeListChargeMoneyView()
                displayChargeCardCodeContainerView()
                txtChargeCode.text = ""
                txtCaptchaCardCode.text = ""
                
                urlSessionDelegate()
            } else {
                if connection == 1 {
                    showBannerNotifi(text: txtAlert01PayMoney[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
                    // alertCustom(message: txtAlert01PayMoney[languageChooseSave], delay: 3)
                    return
                } else if connection == 0 {
                    showBannerNotifi(text: "Lỗi kết nối mạng. Vui lòng thử lại sau!", background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!)
                    // alertCustom(message: txtAlert02PayMoney[languageChooseSave], delay: 3)
                    return
                }
                txtCaptchaPayMoney.text = ""
                getIPAddress()
            }
        case routeTableView:
            detailViewPointFlag = false
            // stop play history
            playHistoryFlag = false
            btnPlayHistoryTrack.setImage(#imageLiteral(resourceName: "btn_resume_media"), for: .normal)
            timerForAutoSlider(false)
            // display address view
            displayAddressView()
            // set value
            for i in 0...allAnnotations.count - 1 {
                if dataTableRoute[indexPath.section][indexPath.row].gpsDate == allAnnotations[i].gpsDate {
                    sliderValue = Float(i)
                }
            }
            // sliderValue = Float(indexPath.row)
            sliderConfig.value = sliderValue
            // print(sliderConfig.value)
            playHistoryTrack(index: sliderConfig.value)
            removeDetailPointView()
            //
            if vtMapDisplayFlag == false {
//                getAddressFromGmap(allAnnotations[indexPath.row].coordinate)
                getAddressFromGmap(dataTableRoute[indexPath.section][indexPath.row].coordinate)
                return
            }
            // getAddressFromVTMap(allAnnotations[indexPath.row].coordinate)
            getAddressFromVTMap(dataTableRoute[indexPath.section][indexPath.row].coordinate)
        default:
            detailViewPointFlag = false
            // stop play history
            playHistoryFlag = false
            btnPlayHistoryTrack.setImage(#imageLiteral(resourceName: "btn_resume_media"), for: .normal)
            timerForAutoSlider(false)
            // display address view
            displayAddressView()
            // set value
            sliderValue = Float(indexPath.row)
            sliderConfig.value = sliderValue
            // print(sliderConfig.value)
            playHistoryTrack(index: sliderConfig.value)
            removeDetailPointView()
            //
            if vtMapDisplayFlag == false {
                getAddressFromGmap(allAnnotations[indexPath.row].coordinate)
                return
            }
            getAddressFromVTMap(allAnnotations[indexPath.row].coordinate)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if tableView == listMonth2PayTableView {
//            return 40
//        }
        return 44
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        let string: String!
        let interval = Int(interval)
        let seconds = (interval % 60)
        let minutes = (interval / 60) % 60
        let hours = (interval / 3600)
        if languageChooseSave == 0 {
            if hours > 0 {
                string = String(format: "%02d %@ %02d %@", hours, txtHour[languageChooseSave], minutes, txtMinute[languageChooseSave])
            } else if hours == 0, minutes > 0 {
                string = String(format: "%02d %@ %02d %@", minutes, txtMinute[languageChooseSave], seconds, txtSecond[languageChooseSave])
            } else {
                string = String(format: "%02d %@", seconds, txtSecond[languageChooseSave])
            }
        } else {
            if hours > 0 {
                if hours > 1 {
                    if minutes > 1 {
                        string = String(format: "%02d %@s %02d %@s", hours, txtHour[languageChooseSave], minutes, txtMinute[languageChooseSave])
                    } else {
                        string = String(format: "%02d %@s %02d %@", hours, txtHour[languageChooseSave], minutes, txtMinute[languageChooseSave])
                    }
                } else {
                    if minutes > 1 {
                        string = String(format: "%02d %@ %02d %@s", hours, txtHour[languageChooseSave], minutes, txtMinute[languageChooseSave])
                    } else {
                        string = String(format: "%02d %@ %02d %@", hours, txtHour[languageChooseSave], minutes, txtMinute[languageChooseSave])
                    }
                }
            } else if hours == 0, minutes > 0 {
                if minutes > 1 {
                    if seconds > 1 {
                        string = String(format: "%02d %@s %02d %@s", minutes, txtMinute[languageChooseSave], seconds, txtSecond[languageChooseSave])
                    } else {
                        string = String(format: "%02d %@s %02d %@", minutes, txtMinute[languageChooseSave], seconds, txtSecond[languageChooseSave])
                    }
                } else {
                    if seconds > 1 {
                        string = String(format: "%02d %@ %02d %@s", minutes, txtMinute[languageChooseSave], seconds, txtSecond[languageChooseSave])
                    } else {
                        string = String(format: "%02d %@ %02d %@", minutes, txtMinute[languageChooseSave], seconds, txtSecond[languageChooseSave])
                    }
                }
            } else {
                if seconds > 1 {
                    string = String(format: "%02d %@s", seconds, txtSecond[languageChooseSave])
                } else {
                    string = String(format: "%02d %@", seconds, txtSecond[languageChooseSave])
                }
            }
        }
        
        return string
    }
}
