//
//  PlayHistory.swift
//  SmartMotor
//
//  Created by Vietnv2 on 3/11/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import GoogleMaps
import UIKit
import Mapbox
extension MapVC {
    // MARK: - SLIDER VIEW
    
    func displaySliderView() {
        initSlider()
        sliderView.frame = CGRect(x: 0, y: view.frame.size.height - 220, width: view.frame.size.width, height: 220)
        view.addSubview(sliderView)
        btnSpeedPlay.setImage(#imageLiteral(resourceName: "Speedx1"), for: .normal)
        btnPlayHistoryTrack.setImage(#imageLiteral(resourceName: "btn_resume_media"), for: .normal)
        lblDatePlay.text = "00/00/0000"
        lblTimePlay.text = "00:00:00"
        lblSpeedPlay.text = "0 Km/h"
        lblSpeedPlay.backgroundColor = #colorLiteral(red: 0.8941176471, green: 0.8941176471, blue: 0.8941176471, alpha: 1)
        playHistoryFlag = false
    }
    
    func removeSliderView() {
        sliderView.removeFromSuperview()
    }
    
    func initSlider() {
        sliderValue = 0
        timeValueForSliderPlay = 0.25
        updateTimeValueForSliderPlay = timeValueForSliderPlay
        sliderConfig.setThumbImage(#imageLiteral(resourceName: "icon_seek"), for: UIControl.State.normal)
        sliderConfig.setThumbImage(#imageLiteral(resourceName: "icon_seek"), for: UIControl.State.highlighted)
        closeSlider()
    }
    
    // MARK: - Play history
    
    func openSlider() {
        displaySliderView()
        removeStatusView()
        removeLockUnlockContainerView()
        initSliderConfig(initValue: 0, min: 0, max: allAnnotations.count)
        viewHistoryModeFlag = true
    }
    
    func closeSlider() {
        timerForAutoSlider(false)
        removeSliderView()
    }
    
    func initSliderConfig(initValue: Int, min: Int, max: Int) {
        sliderConfig.minimumValue = Float(min)
        sliderConfig.maximumValue = Float(max - 1)
        sliderConfig.value = Float(initValue)
        lastAnnotationDisplay = allAnnotations.first?.coordinate
        sliderValue = 0
        if vtMapDisplayFlag == true {
//            vtCustomMarkerForMotor(annotation: allAnnotations.first!.coordinate, img: iconMotorStateImageVT[allAnnotations.first!.state], title: "", subtitle: "")
            return
        }
        gmCustomMarkerForMotor(annotation: allAnnotations.first!.coordinate, img: iconMotorStateImage[allAnnotations.first!.state], title: "", subtitle: "")
    }
    
    func playHistoryTrack(index: Float) {
        let index = Int(index)
        
        // MARK: filter noise
        
        func filterNoise() -> Bool {
            if Float(index) < sliderConfig.maximumValue - 1 {
                let angle = getBearingFrom(firstLocation: allAnnotations[index].coordinate, secondLocation: allAnnotations[index + 1].coordinate)
                // let angle = CalculateAngle().angle(firstLocation: allAnnotations[index].coordinate, secondLocation: allAnnotations[index + 1].coordinate)
                if oldAngle == nil {
                    oldAngle = angle
                }
                if abs(angle - oldAngle) > 90 {
                    let nextAngle = getBearingFrom(firstLocation: allAnnotations[index + 1].coordinate, secondLocation: allAnnotations[index + 2].coordinate)
                    // let nextAngle = CalculateAngle().angle(firstLocation: allAnnotations[index + 1].coordinate, secondLocation: allAnnotations[index + 2].coordinate)
                    if abs(angle - nextAngle) > 90 {
                        oldAngle = angle
                        return false
                    }
                }
                oldAngle = angle
                locationDegrees = angle
                // return true
            }
            return true
        }
        func displayMarker(_ index: Int) {
            let sizeBounds = CGRect(x: 10, y: 10, width: view.frame.size.width - 20, height: view.frame.size.height - 160)
            if vtMapDisplayFlag == true {
//                getAddressFromVTMap(allAnnotations[Int(sliderValue)].coordinate)
                var arrCoodinate = [CLLocationCoordinate2D]()
                for item in self.allAnnotations{
                    arrCoodinate.append(item.coordinate)
                }
                vtMap.setVisibleCoordinates(arrCoodinate, count: UInt(arrCoodinate.count), edgePadding: UIEdgeInsets.init(top: 20, left: 20, bottom: 320, right: 20), animated: true)
                vtCustomMarkerForMotor(annotation: allAnnotations[Int(sliderValue)].coordinate, sourceIdentifier: self.makersource, styleIdentifier: self.styleIndetifier, image: iconMotorStateImage[allAnnotations[Int(sliderValue)].state], forname: self.vtMapForname)
//                self.vtCustomMarkerForMotor(annotation: self.allAnnotations[index].coordinate, img: iconMotorStateImageVT[allAnnotations[index].state], title: "", subtitle: "")
//                vtmapView.refresh()
//                // check outside view
//                let coord = VMSLatLng.init(lat: allAnnotations[index].coordinate.latitude, lng: allAnnotations[index].coordinate.longitude)
//
//                let isPointInFrameVTmap = sizeBounds.contains(vtmapView.projection.toViewPixel(coord))
//                if isPointInFrameVTmap == false {
//                    vtmapView.setCenter(VMSLatLng.init(lat: allAnnotations[index].coordinate.latitude, lng: allAnnotations[index].coordinate.longitude), refresh: true)
//                }
                return
            }
            gmCustomMarkerForMotor(annotation: allAnnotations[index].coordinate, img: iconMotorStateImage[allAnnotations[index].state], title: "", subtitle: "")
            // check outside view
            let isPointInFrameGmap = sizeBounds.contains(gmMarkerForMotor.accessibilityActivationPoint)
            if isPointInFrameGmap == false {
                gmapView.animate(toLocation: CLLocationCoordinate2D(latitude: allAnnotations[index].coordinate.latitude, longitude: allAnnotations[index].coordinate.longitude))
            }
        }
        func rotationMarker(_ index: Int) {
            if index >= allAnnotations.count - 1 {
                return
            }
            if allAnnotations[index].coordinate.latitude == allAnnotations[index + 1].coordinate.latitude {
                return
            }
            if vtMapDisplayFlag == false {
                if filterNoise() == false {
                    gmMarkerForMotor.rotation = locationDegrees
                } else {
                    gmMarkerForMotor.rotation = getBearingFrom(firstLocation: allAnnotations[index].coordinate, secondLocation: allAnnotations[index + 1].coordinate)
                    // gmMarkerForMotor.rotation = CalculateAngle().angle(firstLocation: allAnnotations[index].coordinate, secondLocation: allAnnotations[index+1].coordinate)
                }
                return
            }else{
                self.shapeLayer.iconRotation = NSExpression(forConstantValue:getBearingFrom(firstLocation: allAnnotations[index].coordinate, secondLocation: allAnnotations[index + 1].coordinate))
            }
//            if filterNoise() == false {
//                vtMarkerForMotor?.rotate = Int32(locationDegrees)
//            } else {
//                vtMarkerForMotor?.rotate = Int32(getBearingFrom(firstLocation: self.allAnnotations[index].coordinate, secondLocation: self.allAnnotations[index+1].coordinate))
//                //vtMarkerForMotor?.rotate = Int32(CalculateAngle().angle(firstLocation: self.allAnnotations[index].coordinate, secondLocation: self.allAnnotations[index+1].coordinate))
//            }
//            vtmapView.refresh()
        }
        
        func zoomInLocation(annotation: CLLocationCoordinate2D) {
            if vtMapDisplayFlag == false {
                gmapView.animate(toLocation: CLLocationCoordinate2D(latitude: annotation.latitude, longitude: annotation.longitude))
                return
            }
//            self.vtmapView.move(to: VMSLatLng.init(lat: annotation.latitude, lng: annotation.longitude))
//            vtmapView.refresh()
        }
        
        displayMarker(index)
        rotationMarker(index)
        lastAnnotationDisplay = allAnnotations[index].coordinate
        let time = convertDateFormatForSliderPlay(string: allAnnotations[index].gpsDate)
        let speed: String!
        var color: UIColor!
        var colorLabel: UIColor!
        let state = allAnnotations[index].state
        switch state {
        case 1: // run = Chạy
            let sp = Int(allAnnotations[index].gpsSpeed)
            speed = sp.description + " Km/h"
            color = #colorLiteral(red: 0.004271617159, green: 0.4011737704, blue: 0.0002964493178, alpha: 1) // UIColor(red: 0x35/0xFF, green: 0x87/0xFF, blue: 0x00/0xFF, alpha: 1)
            colorLabel = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        case 2: // speed = "Dừng"
            speed = txtStatus_DetailPoint_Stop[languageChooseSave]
            color = #colorLiteral(red: 0.9450153708, green: 0.945151031, blue: 0.9449856877, alpha: 1) // UIColor(red: 0x00/0xFF, green: 0x76/0xFF, blue: 0xAA/0xFF, alpha: 1)
            colorLabel = #colorLiteral(red: 0.2281919718, green: 0.2473481894, blue: 0.2687590122, alpha: 1)
        case 3: // speed = "Đỗ"
            speed = txtStatus_DetailPoint_Park[languageChooseSave]
            color = #colorLiteral(red: 0.9314132929, green: 0.7468139529, blue: 0.01102155726, alpha: 1) // UIColor(red: 0xFF/0xFF, green: 0xB6/0xFF, blue: 0x00/0xFF, alpha: 1)
            colorLabel = #colorLiteral(red: 0.2478776276, green: 0.2668866813, blue: 0.2926428318, alpha: 1)
        case 4: // speed = "Mất GPS"
            speed = txtStatus_DetailPoint_LossGPS[languageChooseSave]
            color = #colorLiteral(red: 0.8603786826, green: 0.0003520670871, blue: 0.001728266943, alpha: 1) // UIColor(red: 0xFF/0xFF, green: 0x6D/0xFF, blue: 0x00/0xFF, alpha: 1)
            colorLabel = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        case 5: // speed = "Ngủ đông"
            speed = txtStatus_DetailPoint_Hibernate[languageChooseSave]
            color = #colorLiteral(red: 0.3842773139, green: 0.384337008, blue: 0.3842643201, alpha: 1) // UIColor(red: 0x51/0xFF, green: 0x51/0xFF, blue: 0x51/0xFF, alpha: 1)
            colorLabel = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        case 6: // speed = "Mất GPRS"
            speed = txtStatus_DetailPoint_LossGPRS[languageChooseSave]
            color = #colorLiteral(red: 0.8603786826, green: 0.0003520670871, blue: 0.001728266943, alpha: 1) // UIColor(red: 0xFF/0xFF, green: 0x17/0xFF, blue: 0x17/0xFF, alpha: 1)
            colorLabel = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        default:
            speed = ""
            color = #colorLiteral(red: 0.004271617159, green: 0.4011737704, blue: 0.0002964493178, alpha: 1) // UIColor(red: 0x77/0xFF, green: 0x1C/0xFF, blue: 0xF9/0xFF, alpha: 1)
            colorLabel = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        displayDateTimeSpeedHistory(dateTime: time, speed: speed, color: color, colorLabel: colorLabel)
    }
    
    // MARK: - Display date tme speed history
    
    func displayDateTimeSpeedHistory(dateTime: String, speed: String, color: UIColor, colorLabel: UIColor) {
        lblDatePlay.text = trimString(string: dateTime, start: 0, offSet: 10)
        lblTimePlay.text = trimString(string: dateTime, start: 11, offSet: 8)
        lblSpeedPlay.text = speed
        lblSpeedPlay.textColor = colorLabel
        lblSpeedPlay.backgroundColor = color
    }
    
    func trimString(string: String, start: Int, offSet: Int) -> String {
        let tldStartIndex1 = string.index(string.startIndex, offsetBy: start)
        let tldEndIndex1 = string.index(tldStartIndex1, offsetBy: offSet)
        let range1 = Range(uncheckedBounds: (lower: tldStartIndex1, upper: tldEndIndex1))
        return String(string[range1])
    }
    
    func timerForAutoSlider(_ isRun: Bool) {
        if isRun == false {
            if timerForSlider != nil {
                timerForSliderRunningFlag = false
                timerForSlider.invalidate()
                timerForSlider = nil
            }
            return
        }
        if timerForSliderRunningFlag == true {
            return
        }
        timerForSliderRunningFlag = true
        timerForSlider = Timer.scheduledTimer(timeInterval: timeValueForSliderPlay, target: self, selector: #selector(playHistoryTrackWithTimer), userInfo: nil, repeats: true)
    }
    
    func convertDateFormatForSliderPlay(string: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = originDateFormat
        let date = dateFormatter.date(from: string)
        //
        dateFormatter.dateFormat = convertDateFormat
        let stringWithRequestedDateFormat: String = dateFormatter.string(from: date!)
        return stringWithRequestedDateFormat
    }
    
    // MARK: - Play history with timer
    
    @objc func playHistoryTrackWithTimer() {
        if timeValueForSliderPlay != updateTimeValueForSliderPlay {
            timeValueForSliderPlay = updateTimeValueForSliderPlay
            if timerForSliderRunningFlag == true {
                timerForAutoSlider(false)
                timerForAutoSlider(true)
            }
        }
        if sliderValue >= sliderConfig.maximumValue {
            timerForAutoSlider(false)
            playHistoryFlag = false
            sliderValue = 0
            btnPlayHistoryTrack.setImage(#imageLiteral(resourceName: "btn_resume_media"), for: .normal)
            return
        }
        
        sliderValue = sliderValue + 1
        if sliderValue >= sliderConfig.maximumValue {
            sliderValue = sliderConfig.maximumValue
        }
        sliderConfig.value = sliderValue
        playHistoryTrack(index: sliderConfig.value)
        
    }
    
    // MARK: - draw polyline
    
    func drawPolyLineOnMap(annotations: [Station]) {
        if vtMapDisplayFlag == false {
            initMap()
            //
            getAddressFromGmap((allAnnotations.first?.coordinate)!)
            if allAnnotations.first?.coordinate.latitude == 0, allAnnotations.first?.coordinate.longitude == 0 {
//                print("Aaaaaaaaaaaaa")
//                self.lbInfomationAddress.text = "Vị trí xe không xác định"
            }
            if annotations.first != nil {
                DispatchQueue.main.async {
                    self.gmCustomMarkerForMotor(annotation: (annotations.first?.coordinate)!, img: self.iconMotorStateImage[(annotations.first?.state)!], title: "", subtitle: "")
                }
                gmCustomMarker(annotation: (annotations.first?.coordinate)!, img: #imageLiteral(resourceName: "icon_StartFlag"), title: "", subtitle: "")
                gmCustomMarker(annotation: (annotations.last?.coordinate)!, img: #imageLiteral(resourceName: "icon_FinishFlag"), title: "", subtitle: "")
                gmapView.camera = GMSCameraPosition.camera(withLatitude: (allAnnotations.first?.lat)!, longitude: (allAnnotations.first?.lng)!, zoom: 11)
            } else {
                showBannerNotifi(text: txtWrongTimeOrNoDataThisTime[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            }
            let path = GMSMutablePath()
            var bounds = GMSCoordinateBounds()
            for annotation in annotations {
                path.add(annotation.coordinate)
                //
                bounds = bounds.includingCoordinate(annotation.coordinate)
                gmapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 80))
            }
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 5
            polyline.strokeColor = UIColor.blue
            let solidPurple = GMSStrokeStyle.solidColor(#colorLiteral(red: 0.3343915343, green: 0.3729723394, blue: 0.9430476427, alpha: 1))
            let redBlueLight = GMSStrokeStyle.gradient(from: #colorLiteral(red: 0.3343915343, green: 0.3729723394, blue: 0.9430476427, alpha: 1), to: #colorLiteral(red: 0.5240387917, green: 0.9039016366, blue: 0.8640653491, alpha: 1))
            polyline.spans = [GMSStyleSpan(style: solidPurple),
                              GMSStyleSpan(style: solidPurple),
                              GMSStyleSpan(style: redBlueLight)]
            polyline.geodesic = true
            polyline.map = gmapView
            openSlider()
            hideMainButton(true)
            
            return
        }else{
            openSlider()
            hideMainButton(true)
            if(annotations.count>0){
                
                addPolyline(to: self.vtMap.style!)
//                animatePolyline()
                if vtMapDisplayFlag == true{
                    var arrCoodinate = [CLLocationCoordinate2D]()
                    for item in self.allAnnotations{
                        arrCoodinate.append(item.coordinate)
                    }
                    vtMap.setVisibleCoordinates(arrCoodinate, count: UInt(arrCoodinate.count), edgePadding: UIEdgeInsets.init(top: 20, left: 20, bottom: 320, right: 20), animated: true)
                }
                let padđing = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                guard let annotations = vtMap.annotations else {return} // get all annotations(markers on the mapview)
                vtMap.showAnnotations(annotations, edgePadding: padđing , animated: true, completionHandler: nil)
                self.vtmapView.showAnnotations(annotations, edgePadding: padđing , animated: true, completionHandler: nil)
            }
        }
        
        //        getAddressFromVTMap((allAnnotations.first?.coordinate)!)
//        if annotations.first != nil {
//            self.vtCustomMarkerForMotor(annotation: (annotations.first?.coordinate)!, img: iconMotorStateImageVT[(annotations.first?.state)!], title: "", subtitle: "")
//            self.vtCustomMarker(annotation: (annotations.first?.coordinate)!, img: #imageLiteral(resourceName: "icon_StartFlag"), title: "", subtitle: "")
//            self.vtCustomMarker(annotation: (annotations.last?.coordinate)!, img: #imageLiteral(resourceName: "icon_FinishFlag"), title: "", subtitle: "")
//            self.vtmapView.setCenter(VMSLatLng.init(lat: (annotations.first?.lat)!, lng: (annotations.first?.lng)!), refresh: true)
//        }else {
//            alertCustom(message: txtWrongTimeOrNoDataThisTime[languageChooseSave], delay: 2)
//        }
//        let polylineOption = VMSPolylineOptions.init()
//        for annotation in annotations {
//            polylineOption.addPoint(VMSLatLng.init(lat:  annotation.lat, lng:  annotation.lng))
//        }
//        polylineOption.strokeColor = UIColor.blue
//        polylineOption.strokeWidth = 3.0
//        vtmapView.addPolyline(by: polylineOption)
//        let polyline = vtmapView.addPolyline(by: polylineOption)
//        vtmapView.fitBounds(polyline?.boundary)
//        vtmapView.refresh()
//        playHistoryTrack(index: 0)
//        openSlider()
    }
    func addPolyline(to style: MGLStyle) {
        // Add an empty MGLShapeSource, we’ll keep a reference to this and add points to this later.
        let randomIdentifier = randomString(length: 8)
           print("random identifier \(randomIdentifier)")
        let source = MGLShapeSource(identifier: randomIdentifier, shape: nil, options: nil)
        style.addSource(source)
        polylineSource = source
        
        // Add a layer to style our polyline.
        let layer = MGLLineStyleLayer(identifier: randomIdentifier, source: source)
        layer.lineJoin = NSExpression(forConstantValue: "round")
        layer.lineCap = NSExpression(forConstantValue: "round")
        layer.lineColor = NSExpression(forConstantValue: UIColor.blue)
        
        // The line width should gradually increase based on the zoom level.
        layer.lineWidth = NSExpression(format: "mgl_interpolate:withCurveType:parameters:stops:($zoomLevel, 'linear', nil, %@)",
                                       [14: 5, 18: 20])
        style.addLayer(layer)
        let randomStartPointId = randomString(length: 8)
        addStartAndEndPoint(shape : self.vtStartPointMotor, annotation : (self.allAnnotations.first?.coordinate)!, image : self.startFlag, sourceIndentifier : randomStartPointId, layerIndentifier : randomStartPointId, foraname : randomStartPointId)
         let randomEndPointId = randomString(length: 8)
        addStartAndEndPoint(shape : self.vtEndPointMotor, annotation : (self.allAnnotations.last?.coordinate)!, image : self.finishFlag, sourceIndentifier : randomEndPointId, layerIndentifier : randomEndPointId, foraname: randomEndPointId)
    }
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    func addStartAndEndPoint(shape : MGLPointAnnotation, annotation:CLLocationCoordinate2D ,
                             image :UIImage,sourceIndentifier : String , layerIndentifier : String,foraname : String){
        shape.coordinate = annotation
        //"",""
        let shapeSource = MGLShapeSource(identifier: sourceIndentifier , shape: shape, options: nil)
        let shapeLayer = MGLSymbolStyleLayer(identifier: layerIndentifier, source: shapeSource)
       
        
            self.vtMap.style?.setImage(image, forName: foraname)
        
        shapeLayer.iconImageName = NSExpression(forConstantValue: foraname)
        
        // Add the source and style layer to the map
        self.vtMap.style?.addSource(shapeSource)
        self.vtMap.style?.addLayer(shapeLayer)
        var coordinatesAnotation = [CLLocationCoordinate2D]()
        for item in self.allAnnotations{
            coordinatesAnotation.append(item.coordinate)
        }
        
        let polyline = MGLPolylineFeature(coordinates : &coordinatesAnotation ,count : UInt(coordinatesAnotation.count))
        
        polylineSource?.shape = polyline
    }
    func animatePolyline() {
           currentIndex = 1
           timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(tick), userInfo: nil, repeats: true)
        
       }
       
       @objc func tick() {
           if currentIndex > allAnnotations.count {
               timer?.invalidate()
               timer = nil
               return
           }
           
           var coordinatesAnotation = [CLLocationCoordinate2D]()
           for item in self.allAnnotations{
               coordinatesAnotation.append(item.coordinate)
           }
           // Update our MGLShapeSource with the current locations.
           updatePolylineWithCoordinates(coordinates: coordinatesAnotation)
           
           currentIndex += 1
       }
       
       func updatePolylineWithCoordinates(coordinates: [CLLocationCoordinate2D]) {
           var mutableCoordinates = coordinates
           
           let polyline = MGLPolylineFeature(coordinates : &mutableCoordinates ,count : UInt(mutableCoordinates.count))
           
           polylineSource?.shape = polyline
       }
    
    
}
