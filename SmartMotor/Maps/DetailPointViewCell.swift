//
//  DetailViewCell.swift
//  SmartMotor
//
//  Created by Vietnv2 on 3/30/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import UIKit

class DetailPointViewCell: UITableViewCell {
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblState: UILabel!
    @IBOutlet var lblStateDetail: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
