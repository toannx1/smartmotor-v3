//
//  GmapView.swift
//  SmartMotor
//
//  Created by Vietnv2 on 3/9/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import GoogleMaps
import UIKit

extension MapVC: GMSMapViewDelegate {
    func displayGoogleMap() {
        removeViettelMaps()
        guard gmapView == nil else {
//            gmapView.removeFromSuperview()
//            gmapView = nil
            return
        }
        gmapView = GMSMapView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        self.mapView.addSubview(gmapView)
        gmapView.clear()
        gmapView.delegate = self
        gmapView.settings.compassButton = false
        gmapView.settings.rotateGestures = false
        gmapView.camera = GMSCameraPosition.camera(withLatitude: 21.028417, longitude: 105.853707, zoom: 12.0)
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
            case .authorizedAlways, .authorizedWhenInUse:
                gmapView.isMyLocationEnabled = true
            }
        } else {
            print("Location services are not enabled")
        }
        
//        btnChangeGmapType.isHidden = false
    }
    
    func removeGoogleMaps() {
        if gmapView != nil {
            gmapView.removeFromSuperview()
            gmapView = nil
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        tapToMapMarkerForDisplayStatusView()
        
        return true
    }
}

extension MapVC {
    func gmCustomMarkerForMotor(annotation: CLLocationCoordinate2D, img: UIImage, title: String, subtitle: String) {
        gmMarkerForMotor.position = CLLocationCoordinate2D(latitude: annotation.latitude, longitude: annotation.longitude)
        gmMarkerForMotor.title = title
        gmMarkerForMotor.snippet = subtitle
        gmMarkerForMotor.icon = self.imageWithImage(image: img, scaledToSize: CGSize(width: 60.0, height: 60.0))
        gmMarkerForMotor.groundAnchor = CGPoint(x: 0.5, y: 0.5) // center
        gmMarkerForMotor.map = self.gmapView
    }
    
    func imageWithImage(image: UIImage, scaledToSize newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func gmCustomMarker(annotation: CLLocationCoordinate2D, img: UIImage, title: String, subtitle: String) {
        let gmMarker = GMSMarker()
        gmMarker.position = CLLocationCoordinate2D(latitude: annotation.latitude, longitude: annotation.longitude)
        gmMarker.title = title
        gmMarker.snippet = subtitle
        gmMarker.icon = img
        gmMarker.map = self.gmapView
    }
    
    func viewMyLocation() {
        initMyLocation()
        gmapView.isMyLocationEnabled = true
    }
    
    func fitMyLocationAndMotorInGmap() {
        var bounds = GMSCoordinateBounds()
        bounds = bounds.includingCoordinate(markerMyLocation.position)
        bounds = bounds.includingCoordinate(gmMarkerForMotor.position)
        gmapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 80))
    }
    
    func displayDistanceMyLocationAndMotor() {
        let point1 = CLLocation(latitude: vehicle2show.lat, longitude: vehicle2show.lng)
        let point2 = CLLocation(latitude: valueLocation.latitude, longitude: valueLocation.longitude)
        var distance = point1.distance(from: point2)
//        var distance = VMSGeometryUtils.length(fromPoint: VMSLatLng.init(lat: vehicle2show.lat, lng: vehicle2show.lng), toPoint: VMSLatLng.init(lat: valueLocation.latitude, lng: valueLocation.longitude))
        distance = distance / 1000
        alertCustom(message: String(format: "%@ %.2f Km", txtUserLocationDistance[languageChooseSave], distance), delay: 3)
    }
}
