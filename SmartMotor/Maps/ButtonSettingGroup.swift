//
//  ButtonSettingGroup.swift
//  SmartMotor
//
//  Created by Vietnv2 on 3/11/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import GoogleMaps
import UIKit
import Mapbox
extension MapVC {
    // MARK: - WARNING ALLOW ACCESS LOCATION VIEW
    
    func displayWarningAllowAccessLocationView() {
        warningAllowAccessLocationView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(warningAllowAccessLocationView)
        lblTitleWarningAllowAccessLocation.text = txtTitleWarningAllowAccessLocation[languageChooseSave].uppercased()
        lblContentWarningAllowAccessLocation.text = txtContentWarningAllowAccessLocation[languageChooseSave]
        btnConfirmWarningAllowAccessLocation.setTitle(txtButtonOkWarningAllowAccessLocation[languageChooseSave], for: .normal)
        // btnCancelWarningAllowAccessLocation.setTitle(txtButtonCancelWarningAllowAccessLocation[languageChooseSave], for: .normal)
    }
    
    func removeWarningAllowAccessLocationView() {
        warningAllowAccessLocationView.removeFromSuperview()
    }
    
    @IBAction func closeWarningAllowAccessLocationViewTapped(_ sender: Any) {
        removeWarningAllowAccessLocationView()
    }
    
    // MARK: - WARNING WRONG LOCATION VIEW
    
    func displayWarningWrongLocationView() {
        warningWrongLocationView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(warningWrongLocationView)
        lblTitleWarningWrongLocation.text = txtTitleWarningWrongLocation[languageChooseSave].uppercased()
        lblContentWarningWrongLocation.text = txtContentWarningWrongLocation[languageChooseSave]
        btnOkWarningWrongLocation.setTitle(txtButtonOkWarningWrongLocation[languageChooseSave], for: .normal)
        // btnCancelWarningWrongLocation.setTitle(txtButtonCancelWarningWrongLocation[languageChooseSave], for: .normal)
    }
    
    func removeWarningWrongLocationView() {
        warningWrongLocationView.removeFromSuperview()
    }
    
    @IBAction func closeWarningWrongLocationView(_ sender: Any) {
        removeWarningWrongLocationView()
    }
    
    @IBAction func btnShowHideButtonTapped(_ sender: AnyObject) {
        if idleFlag == false {
            return
        }
        idleFlag = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            idleFlag = true
        }
        if viewCurrentLocationFlag != true {
            return
        }
        flagHideButton = !flagHideButton
        hideMainButton(flagHideButton)
    }
    
    @IBAction func btnSettingTapped(_ sender: AnyObject) {
        if idleFlag == false {
            return
        }
        idleFlag = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            idleFlag = true
        }
        let allow = checkAllowControl()
        if allow == false {
            return
        }
        if viewCurrentLocationFlag != true {
            initMapVC()
            return
        }
        timerReloadCurrentLocationRun(false)
        gotoSettingView()
    }
    
    func checkAllowControl() -> Bool {
        if groupRootSave == 1 {
            showBannerNotifi(text: txtAlert04MapVC[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
            return false
        }
        if vehicle2show.deviceType == 0 {
            showBannerNotifi(text: txtAlert04MapVC[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
            return false
        }
        return true
    }
    
    @IBAction func btnChangeMapViewTapped(_ sender: AnyObject) {
        getVehicleLocation()
        if idleFlag == false {
            return
        }
        idleFlag = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            idleFlag = true
        }
//        if vtMapDisplayFlag != true {
//            vtMapDisplayFlag = true
//            LoginVC().saveMapType2Sqlite(mapType: "VTMap")
//            btnChangeMapView.setImage(#imageLiteral(resourceName: "btn_googlemap"), for: .normal)
//            lblButtonSetting_Map.text = "GMap"
        ////            settingContainerViewHeightContraint.constant = 225
//            initMap()
//            return
//        }
        vtMapDisplayFlag = false
        LoginVC().saveMapType2Sqlite(mapType: "GMap")
        btnChangeMapView.setImage(#imageLiteral(resourceName: "btn_googlemap"), for: .normal)
//        lblButtonSetting_Map.text = "VTMap"
//        settingContainerViewHeightContraint.constant = 305
        initMap()
    }
    @IBAction func btnChangeVtMapViewTapped(_ sender: AnyObject){
        getVehicleLocation()
        if idleFlag == false {
            return
        }
        idleFlag = false
        vtMapDisplayFlag = true
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            idleFlag = true
        }
        if (vtMapDisplayFlag == false){
            return
        }
        
         LoginVC().saveMapType2Sqlite(mapType: "VTMap")
        initMap()
//        if changeGmapTypeFlag
        
    }
    @IBAction func btnChangeMapTypeTapped(_ sender: Any) {
        if idleFlag == false {
            return
        }
        idleFlag = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            idleFlag = true
        }
        if vtMapDisplayFlag == true {
//            vtMap.styleURL = MGLStyle.satelliteStyleURL
            if changeGmapTypeFlag == true {
                
                vtMap.styleURL = MGLStyle.streetsStyleURL
                btnChangeGmapType.setImage(UIImage(named: "Gmap_Type02"), for: .normal)
            } else {
                changeGmapTypeFlag = true
                vtMap.styleURL = MGLStyle.satelliteStyleURL
                btnChangeGmapType.setImage(UIImage(named: "Gmap_Type01"), for: .normal)
            }
            
        }else{
            gmapView.mapType = .hybrid
            if changeGmapTypeFlag == true {
                changeGmapTypeFlag = false
                gmapView.mapType = .normal
                btnChangeGmapType.setImage(UIImage(named: "Gmap_Type02"), for: .normal)
            } else {
                changeGmapTypeFlag = true
                gmapView.mapType = .hybrid
                btnChangeGmapType.setImage(UIImage(named: "Gmap_Type01"), for: .normal)
            }
        }
    }
    
    // MARK: - My location tapped
    
    @IBAction func btnMyLocationTapped(_ sender: Any) {
        if idleFlag == false {
            return
        }
        idleFlag = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            idleFlag = true
        }
        UIView.animate(withDuration: 0.3) {
            self.bottomLayoutInfo.constant = -295
            self.infoShow = false
            self.btnHideInfo.setImage(UIImage(named: "ic_up"), for: .normal)
        }
        checkAllowLocationAccess()
    }
    
    @IBAction func btnConfirmAllowAccessLocationTapped(_ sender: Any) {
        removeWarningAllowAccessLocationView()
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                initMyLocation()
            case .restricted, .denied:
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            case .authorizedAlways, .authorizedWhenInUse:
                displayWarningWrongLocationView()
            }
        } else {}
    }
    
    @IBAction func btnCancelAllowAccessLocationTapped(_ sender: Any) {
        removeWarningAllowAccessLocationView()
    }
    
    @IBAction func btnConfirmViewMyLocationTapped(_ sender: Any) {
        removeWarningWrongLocationView()
        viewMyLocation()
    }
    
    @IBAction func btnCancelViewMyLocationTapped(_ sender: Any) {
        removeWarningWrongLocationView()
    }
    
    func checkAllowLocationAccess() {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                displayWarningAllowAccessLocationView()
            case .authorizedAlways, .authorizedWhenInUse:
                displayWarningWrongLocationView()
            }
        } else {}
    }
    
    func hideMainButton(_ displayFlag: Bool) {
        flagHideButton = displayFlag
        lblButtonSetting_Journey.text = txtButtonJourney[languageChooseSave]
//        lblButtonSetting_Setting.text = txtButtonSetting[languageChooseSave]
//        lblButtonSetting_Map.text = vtMapDisplayFlag == true ? "GMap":"VTMap"
//        lblButtonSetting_MyLocation.text = txtButtonMyLocation[languageChooseSave]
        
//        settingContainerView.isHidden = displayFlag
//        settingContainerViewHeightContraint.constant = 305
//        if vtMapDisplayFlag == true {
//            settingContainerViewHeightContraint.constant = 225
//            return
//        }
    }
}

extension MapVC: CLLocationManagerDelegate {
    func initMyLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {}
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        valueLocation = locations.last!.coordinate
        
        gmapView.camera = GMSCameraPosition.camera(withLatitude: valueLocation.latitude, longitude: valueLocation.longitude, zoom: 12)
        markerMyLocation.position = CLLocationCoordinate2D(latitude: valueLocation.latitude, longitude: valueLocation.longitude)
        locationManager.startUpdatingLocation()
        
        fitMyLocationAndMotorInGmap()
        displayDistanceMyLocationAndMotor()
        
        locationManager.stopUpdatingLocation()
    }
}
