//
//  ChooseHistoryDateTime.swift
//  SmartMotor
//
//  Created by Vietnv2 on 3/11/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import UIKit
import MapKit
extension MapVC {
    enum selectedStartStopDate: String {
        case historyDateStart
        case historyDateEnd
        case historyTimeStart
        case historyTimeEnd
    }
    
    //MARK: - picker select time
    @IBAction func pickerSelectTime(_ sender: Any) {
        if selectBenzel {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            lbTimeBenzel1.text = formatter.string(from: pickerTime.date)
            timeStart2SendRequest = formatter.string(from: pickerTime.date)
            // dateStart = formatter.date(from: dateStart2SendRequest)
            
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            lbTimeBenzel2.text = formatter.string(from: pickerTime.date)
            timeEnd2SendRequest = formatter.string(from: pickerTime.date)
            // dateEnd = formatter.date(from: dateEnd2SendRequest)
        }
    }
    
    @IBAction func pickerSelectDate(_ sender: Any) {
        if selectBenzel {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            lbDateBenzel1.text = formatter.string(from: pickerDate.date)
            dateStart2SendRequest = formatter.string(from: pickerDate.date)
            dateStart = formatter.date(from: dateStart2SendRequest)
            
            // set min Date
            let someDateMin = formatter.date(from: lbDateBenzel1.text!)!
            let timeIntervalMin = someDateMin.timeIntervalSince1970
            let dateD = Date(timeIntervalSince1970: timeIntervalMin)
            var componentTimeMin = Calendar.current.dateComponents([.day, .month, .year], from: dateD)
            
            let calendar = Calendar.current
            let dateCr = Date()
            let components = calendar.dateComponents([.year, .month, .day], from: dateCr)
            let year = components.year
            let month = components.month
            let day = components.day!
            
            if day == componentTimeMin.day, month == componentTimeMin.month, year == componentTimeMin.year {
                lbDateBenzel2.text = formatter.string(from: pickerDate.date)
                dateEnd2SendRequest = formatter.string(from: pickerDate.date)
                dateEnd = formatter.date(from: dateEnd2SendRequest)
            } else {
                componentTimeMin.day = componentTimeMin.day! + 1
                let dateMin = Calendar.current.date(from: componentTimeMin)!
                lbDateBenzel2.text = formatter.string(from: dateMin)
                dateEnd2SendRequest = formatter.string(from: dateMin)
                dateEnd = formatter.date(from: dateEnd2SendRequest)
            }
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            lbDateBenzel2.text = formatter.string(from: pickerDate.date)
            dateEnd2SendRequest = formatter.string(from: pickerDate.date)
            dateEnd = formatter.date(from: dateEnd2SendRequest)
            
            // set min Date
            let someDateMin = formatter.date(from: lbDateBenzel2.text!)!
            let timeIntervalMin = someDateMin.timeIntervalSince1970
            let dateD = Date(timeIntervalSince1970: timeIntervalMin)
            var componentTimeMin = Calendar.current.dateComponents([.day, .month, .year], from: dateD)
            componentTimeMin.day = componentTimeMin.day! - 1
            let dateMax = Calendar.current.date(from: componentTimeMin)!
            lbDateBenzel1.text = formatter.string(from: dateMax)
            
            dateStart2SendRequest = formatter.string(from: dateMax)
            dateStart = formatter.date(from: dateEnd2SendRequest)
        }
    }
    // MARK: - Select date time View - View Route
    
    func displaySelectDateTimeView() {
        initDateTime()
        selectDateTimeView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(selectDateTimeView)
        selectDateTimeViewHeightOrigin = selectDateTimeView.frame.size.height
        lblTitleSelectDateTime.text = txtTitleSelectDateTime[languageChooseSave].uppercased()
        lblContent01SelectDateTime.text = txtContent01SelectDateTime[languageChooseSave]
        lblContent02SelectDateTime.text = txtContent02SelectDateTime[languageChooseSave]
        
        selectBenzel = true
        btnViewDate = ButtonBenzel(frame: viewButton.frame, mode: 1)
        viewButton.addSubview(btnViewDate!)
        viewButton.isHidden = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.btnViewDate!.removeFromSuperview()
            self.btnViewDate = ButtonBenzel(frame: self.viewButton.frame, mode: 1)
            self.viewButton.addSubview(self.btnViewDate!)
            self.view.layoutIfNeeded()
            self.viewButton.isHidden = false
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = event?.allTouches?.first {
            let loc: CGPoint = touch.location(in: touch.view)
            if bolViewDate {
                if btnViewDate!.path.contains(loc) {
                    btnViewDate!.removeFromSuperview()
                    btnViewDate = ButtonBenzel(frame: viewButton.frame, mode: 1)
                    viewButton.addSubview(btnViewDate!)
                    
                    selectBenzel = true
                    lbTimeBenzel1.textColor = #colorLiteral(red: 0, green: 0.4470588235, blue: 0.8, alpha: 1)
                    lbTimeBenzel2.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    lbDateBenzel1.textColor = #colorLiteral(red: 0, green: 0.4470588235, blue: 0.8, alpha: 1)
                    lbDateBenzel2.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "HH:mm"
                    let someDate = dateFormatter.date(from: lbTimeBenzel1.text!)!
                    let timeInterval = someDate.timeIntervalSince1970
                    let datet = Date(timeIntervalSince1970: timeInterval)
                    let componentTime = Calendar.current.dateComponents([.hour, .minute], from: datet)
                    let dates = Calendar.current.date(from: componentTime)!
                    pickerTime.setDate(dates, animated: true)
                    
                    let dateFormatter2 = DateFormatter()
                    dateFormatter2.dateFormat = "dd/MM/yyyy"
                    let someDate2 = dateFormatter2.date(from: lbDateBenzel1.text!)!
                    let timeInterval2 = someDate2.timeIntervalSince1970
                    let datet2 = Date(timeIntervalSince1970: timeInterval2)
                    let componentTime2 = Calendar.current.dateComponents([.day, .month, .year], from: datet2)
                    let dates2 = Calendar.current.date(from: componentTime2)!
                    pickerDate.setDate(dates2, animated: true)
                    
                    // set min Date
                    let someDateMin = dateFormatter2.date(from: "01/01/1990")!
                    let timeIntervalMin = someDateMin.timeIntervalSince1970
                    let dateD = Date(timeIntervalSince1970: timeIntervalMin)
                    let componentTimeMin = Calendar.current.dateComponents([.day, .month, .year], from: dateD)
                    let dateMin = Calendar.current.date(from: componentTimeMin)!
                    pickerDate.minimumDate = dateMin
                    
                } else if btnViewDate!.path2.contains(loc) {
                    btnViewDate!.removeFromSuperview()
                    btnViewDate = ButtonBenzel(frame: viewButton.frame, mode: 2)
                    viewButton.addSubview(btnViewDate!)
                    selectBenzel = false
                    lbTimeBenzel1.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    lbTimeBenzel2.textColor = #colorLiteral(red: 0, green: 0.4470588235, blue: 0.8, alpha: 1)
                    lbDateBenzel1.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                    lbDateBenzel2.textColor = #colorLiteral(red: 0, green: 0.4470588235, blue: 0.8, alpha: 1)
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "HH:mm"
                    let someDate = dateFormatter.date(from: lbTimeBenzel2.text!)!
                    let timeInterval = someDate.timeIntervalSince1970
                    let datet = Date(timeIntervalSince1970: timeInterval)
                    let componentTime = Calendar.current.dateComponents([.hour, .minute], from: datet)
                    let dates = Calendar.current.date(from: componentTime)!
                    pickerTime.setDate(dates, animated: true)
                    
                    let dateFormatter2 = DateFormatter()
                    dateFormatter2.dateFormat = "dd/MM/yyyy"
                    let someDate2 = dateFormatter2.date(from: lbDateBenzel2.text!)!
                    let timeInterval2 = someDate2.timeIntervalSince1970
                    let datet2 = Date(timeIntervalSince1970: timeInterval2)
                    let componentTime2 = Calendar.current.dateComponents([.day, .month, .year], from: datet2)
                    let dates2 = Calendar.current.date(from: componentTime2)!
                    pickerDate.setDate(dates2, animated: true)
                    
                    // set min Date
                    let someDateMin = dateFormatter2.date(from: "01/01/1990")!
                    let timeIntervalMin = someDateMin.timeIntervalSince1970
                    let dateD = Date(timeIntervalSince1970: timeIntervalMin)
                    let componentTimeMin = Calendar.current.dateComponents([.day, .month, .year], from: dateD)
                    // componentTimeMin.day = componentTimeMin.day! - 1
                    let dateMin = Calendar.current.date(from: componentTimeMin)!
                    pickerDate.minimumDate = dateMin
//                    pickerDate.maximumDate = dates2
                }
            }
        }
    }
    
    func removeSelectDateTimeView() {
        selectDateTimeView.removeFromSuperview()
        view.endEditing(true)
        bolViewDate = false
    }
    
    @IBAction func closeSelectDateTimeViewTapped(_ sender: Any) {
        removeSelectDateTimeView()
    }
    
    func initDateTime() {
        let date = Date()
        let formatter = DateFormatter()
        // init first time format value send to webservice
        formatter.dateFormat = "dd/MM/yyyy"
        dateStart2SendRequest = formatter.string(from: date)
        dateEnd2SendRequest = dateStart2SendRequest
        //
        // init first time display on button
        formatter.dateFormat = "dd/MM/yyyy"
        let strDate = formatter.string(from: date)
        dateStart = formatter.date(from: strDate)
        //
        dateEnd = dateStart
        checkInvalidDateToShowHistory = 0
        btnSelectedDateStartSetTitle.text = strDate
        btnSelectedDateEndSetTitle.text = strDate
        //
        timeStart2SendRequest = "00:00"
        //
        timeEnd2SendRequest = "23:59"
        //
        timeEnd2ViewDetailPoint = "23:59:59"
        //
        btnSelectedStartTimeSetTitle.text = timeStart2SendRequest
        // btnSelectedEndTimeSetTitle.text = timeEnd2SendRequest
        initDatePicker()
    }
    
    func initDatePicker() {
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date())
        datePicker.backgroundColor = UIColor.white
        // 24h
        datePicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale
        // 12h
        // datePicker.locale = NSLocale(localeIdentifier: "en_US") as Locale
    }
    
    // MARK: - Click Route
    
    func routeViewHistory() {
        if vehicle2show.state == 0 {
            return
        }
        displaySelectDateTimeView()
        lbTimeBenzel1.textColor = #colorLiteral(red: 0, green: 0.4470588235, blue: 0.8, alpha: 1)
        lbTimeBenzel2.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        lbDateBenzel1.textColor = #colorLiteral(red: 0, green: 0.4470588235, blue: 0.8, alpha: 1)
        lbDateBenzel2.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        lbTimeBenzel1.text = "00:00"
        lbDateBenzel1.text = formatter.string(from: date)
        lbTimeBenzel2.text = "23:59"
        lbDateBenzel2.text = formatter.string(from: date)
        bolViewDate = true
        
        lbBKS.text = BKS
        lbBKSinfo.text = BKS
        viewSelectDateRouteTB.isHidden = true
        viewSelectStatusRouteTB.isHidden = true
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let someDate = dateFormatter.date(from: lbTimeBenzel1.text!)!
        let timeInterval = someDate.timeIntervalSince1970
        let datet = Date(timeIntervalSince1970: timeInterval)
        let componentTime = Calendar.current.dateComponents([.hour, .minute], from: datet)
        let dates = Calendar.current.date(from: componentTime)!
        pickerTime.setDate(dates, animated: true)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "dd/MM/yyyy"
        let someDate2 = dateFormatter2.date(from: lbDateBenzel1.text!)!
        let timeInterval2 = someDate2.timeIntervalSince1970
        let datet2 = Date(timeIntervalSince1970: timeInterval2)
        let componentTime2 = Calendar.current.dateComponents([.day, .month, .year], from: datet2)
        let dates2 = Calendar.current.date(from: componentTime2)!
        pickerDate.setDate(dates2, animated: true)
    }
    
    @IBAction func btnHistoryModeTapped(_ sender: AnyObject) {
        routeViewHistory()
    }
    
    @objc func goFindView() {
        routeViewHistory()
    }
    
    @IBAction func btnDatePickerOKTapped(_ sender: AnyObject) {
        removeSelectDateTimeView()
        view.endEditing(true)
    }
    
    @IBAction func btnDatePickerCancelTapped(_ sender: AnyObject) {
        removeSelectDateTimeView()
        view.endEditing(true)
    }
    
    func btnSelectDateTimeFormat() {
        btnSelectedDateStartSetTitle.layer.borderWidth = 0.5
        btnSelectedDateStartSetTitle.layer.borderColor = UIColor.lightGray.cgColor
        btnSelectedDateStartSetTitle.backgroundColor = UIColor(red: 200 / 255, green: 200 / 255, blue: 200 / 255, alpha: 0.3)
        btnSelectedStartTimeSetTitle.layer.borderWidth = 0.5
        btnSelectedStartTimeSetTitle.layer.borderColor = UIColor.lightGray.cgColor
        btnSelectedStartTimeSetTitle.backgroundColor = UIColor(red: 200 / 255, green: 200 / 255, blue: 200 / 255, alpha: 0.3)
        btnSelectedDateEndSetTitle.layer.borderWidth = 0.5
        btnSelectedDateEndSetTitle.layer.borderColor = UIColor.lightGray.cgColor
        btnSelectedDateEndSetTitle.backgroundColor = UIColor(red: 200 / 255, green: 200 / 255, blue: 200 / 255, alpha: 0.3)
        btnSelectedEndTimeSetTitle.layer.borderWidth = 0.5
        btnSelectedEndTimeSetTitle.layer.borderColor = UIColor.lightGray.cgColor
        btnSelectedEndTimeSetTitle.backgroundColor = UIColor(red: 200 / 255, green: 200 / 255, blue: 200 / 255, alpha: 0.3)
    }
    
    func checkDateTimeTapped() {
        selectDateTimeView.frame.size.height = selectDateTimeViewHeightOrigin - datePickerView.frame.size.height
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func daysBetweenDates(startDate: Date, endDate: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day!
    }
    
    @objc func dataPickerScrolled(sender: AnyObject) {
        let dateFormatter = DateFormatter()
        switch selectedStartStopDateString {
        case selectedStartStopDate.historyDateStart.rawValue:
            dateFormatter.dateFormat = "dd/MM/yyyy"
            dateStart2SendRequest = dateFormatter.string(from: datePicker.date)
            
            // Check invalid date to send request
            dateStart = dateFormatter.date(from: dateStart2SendRequest)
            checkInvalidDateToShowHistory = daysBetweenDates(startDate: dateStart as Date, endDate: dateEnd as Date)
            
        case selectedStartStopDate.historyTimeStart.rawValue:
            dateFormatter.dateFormat = "HH:mm"
            _ = dateFormatter.string(from: datePicker.date)
            timeStart2SendRequest = dateFormatter.string(from: datePicker.date)
        case selectedStartStopDate.historyDateEnd.rawValue:
            dateFormatter.dateFormat = "dd/MM/yyyy"
            dateEnd2SendRequest = dateFormatter.string(from: datePicker.date)
            btnSelectedDateEndSetTitle.text = dateEnd2SendRequest
            // Check invalid date to send request
            dateEnd = dateFormatter.date(from: dateEnd2SendRequest)
            checkInvalidDateToShowHistory = daysBetweenDates(startDate: dateStart as Date, endDate: dateEnd as Date)
        case selectedStartStopDate.historyTimeEnd.rawValue:
            dateFormatter.dateFormat = "HH:mm"
            let date24 = dateFormatter.string(from: datePicker.date)
            btnSelectedEndTimeSetTitle.text = date24
            timeEnd2SendRequest = dateFormatter.string(from: datePicker.date)
            
            dateFormatter.dateFormat = "HH:mm:ss"
            timeEnd2ViewDetailPoint = dateFormatter.string(from: datePicker.date)
        default:
            break
        }
    }
    
    @IBAction func btnSelectedDateStartHistoryTrackTapped(_ sender: AnyObject) {
        selectedStartStopDateString = selectedStartStopDate.historyDateStart.rawValue
        borderColorForSelectDateTime()
        datePicker.datePickerMode = UIDatePicker.Mode.date
//        displayDatePickerView()
        checkDateTimeTapped()
        //
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
//        let strDate = dateFormatter.date(from: (btnSelectedDateStartSetTitle.titleLabel?.text)!)
//        datePicker.setDate(strDate!, animated: true)
        datePicker.addTarget(self, action: #selector(dataPickerScrolled(sender:)), for: .valueChanged)
    }
    
    @IBAction func btnSelectedStartTimeHistoryTrackTapped(_ sender: AnyObject) {
        selectedStartStopDateString = selectedStartStopDate.historyTimeStart.rawValue
        borderColorForSelectDateTime()
        datePicker.datePickerMode = UIDatePicker.Mode.time
//        displayDatePickerView()
        checkDateTimeTapped()
        //
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
//        let strDate = dateFormatter.date(from: (btnSelectedStartTimeSetTitle.titleLabel?.text)!)
//        datePicker.setDate(strDate!, animated: true)
        datePicker.addTarget(self, action: #selector(dataPickerScrolled(sender:)), for: .valueChanged)
    }
    
    @IBAction func btnSelectedDateEndHistoryTrackTapped(_ sender: AnyObject) {
        selectedStartStopDateString = selectedStartStopDate.historyDateEnd.rawValue
        borderColorForSelectDateTime()
        datePicker.datePickerMode = UIDatePicker.Mode.date
//        displayDatePickerView()
        checkDateTimeTapped()
        //
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
//        let strDate = dateFormatter.date(from: (btnSelectedDateEndSetTitle.titleLabel?.text)!)
//        datePicker.setDate(strDate!, animated: true)
        datePicker.addTarget(self, action: #selector(dataPickerScrolled(sender:)), for: .valueChanged)
    }
    
    @IBAction func btnSelectedEndTimeHistoryTrackTapped(_ sender: AnyObject) {
        selectedStartStopDateString = selectedStartStopDate.historyTimeEnd.rawValue
        borderColorForSelectDateTime()
        datePicker.datePickerMode = UIDatePicker.Mode.time
//        displayDatePickerView()
        checkDateTimeTapped()
        //
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
//        let strDate = dateFormatter.date(from: (btnSelectedEndTimeSetTitle.titleLabel?.text)!)
//        datePicker.setDate(strDate!, animated: true)
        datePicker.addTarget(self, action: #selector(dataPickerScrolled(sender:)), for: .valueChanged)
    }
    
    func borderColorForSelectDateTime() {
        btnSelectDateTimeFormat()
        switch selectedStartStopDateString {
        case selectedStartStopDate.historyDateStart.rawValue:
            btnSelectedDateStartSetTitle.layer.borderWidth = 2
            btnSelectedDateStartSetTitle.layer.borderColor = UIColor(red: 30 / 255, green: 154 / 255, blue: 195 / 255, alpha: 1).cgColor
        case selectedStartStopDate.historyTimeStart.rawValue:
            btnSelectedStartTimeSetTitle.layer.borderWidth = 2
            btnSelectedStartTimeSetTitle.layer.borderColor = UIColor(red: 30 / 255, green: 154 / 255, blue: 195 / 255, alpha: 1).cgColor
        case selectedStartStopDate.historyDateEnd.rawValue:
            btnSelectedDateEndSetTitle.layer.borderWidth = 2
            btnSelectedDateEndSetTitle.layer.borderColor = UIColor(red: 30 / 255, green: 154 / 255, blue: 195 / 255, alpha: 1).cgColor
        case selectedStartStopDate.historyTimeEnd.rawValue:
            btnSelectedEndTimeSetTitle.layer.borderWidth = 2
            btnSelectedEndTimeSetTitle.layer.borderColor = UIColor(red: 30 / 255, green: 154 / 255, blue: 195 / 255, alpha: 1).cgColor
        default:
            break
        }
    }
    
    // MARK: - Button Xem hành trình - View Lựa chọn thời gian
    
    @IBAction func btnConfirmLoadHistoryTrackTapped(_ sender: AnyObject) {
        // Calculator date
        checkInvalidDateToShowHistory = daysBetweenDates(startDate: dateStart as Date, endDate: dateEnd as Date)
        if checkInvalidDateToShowHistory > 1 {
            showBannerNotifi(text: txtAlertTimeMax2Date[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            // alertCustom(message: txtAlertTimeMax2Date[languageChooseSave], delay: 2)
            return
        } else if checkInvalidDateToShowHistory < 0 {
            showBannerNotifi(text: txtDateTimeInvalid[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            // alertCustom(message: txtDateTimeInvalid[languageChooseSave], delay: 2)
            return
        }
        if checkInvalidDateToShowHistory == 0 {
            if timeStart2SendRequest > timeEnd2SendRequest {
                showBannerNotifi(text: txtDateTimeInvalid[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
                // alertCustom(message: txtDateTimeInvalid[languageChooseSave], delay: 2)
                return
            }
        }
        statusViewFlag = false
        // removeStatusView() //Ẩn Bảng menu
        viewInfo.isHidden = true
        btnMyLocation.isHidden = true
        locationManager.stopUpdatingLocation()
        timerReloadCurrentLocationRun(false)
//        hideMainButton(true)
        removeSelectDateTimeView()
        getMapAnnotations()
        
        btnFindView.isHidden = false
        btnDetailRoute.isHidden = false
        strDateTB = lbDateBenzel1.text!
        maxDateTB = lbDateBenzel2.text!
        minDateTB = lbDateBenzel1.text!
        pickerTimeRoute.selectRow(0, inComponent: 0, animated: true)
    }
    
    @IBAction func btnCancelLoadHistoryTrackTapped(_ sender: AnyObject) {
        removeSelectDateTimeView()
    }
    
    @IBAction func sliderToShowHistoryTrack(_ sender: UISlider) {
        playHistoryTrack(index: sliderConfig.value)
        sliderValue = sliderConfig.value
        removeAddressView()
    }
    
    @IBAction func btnPlayHistoryTrackTapped(_ sender: UIButton) {
        if sliderConfig.value < sliderConfig.maximumValue {
            sliderValue = sliderConfig.value
        } else {
            sliderConfig.value = 0
            sliderValue = 0
        }
        if playHistoryFlag == false {
            playHistoryFlag = true
            btnPlayHistoryTrack.setImage(#imageLiteral(resourceName: "btn_pause_media"), for: .normal)
            timerForAutoSlider(true)
            removeAddressView()
            return
        } else {
            playHistoryFlag = false
            btnPlayHistoryTrack.setImage(#imageLiteral(resourceName: "btn_resume_media"), for: .normal)
            timerForAutoSlider(false)
            if vtMapDisplayFlag == false {
                getAddressFromGmap(allAnnotations[Int(sliderValue)].coordinate)
                // displayAddressView()
                return
            }
            getAddressFromVTMap(allAnnotations[Int(sliderValue)].coordinate)
            displayAddressView()
        }
    }
    
    @IBAction func btnSpeedPlayTapped(_ sender: Any) {
        sliderSpeed = sliderSpeed + 1
        if sliderSpeed > 3 {
            sliderSpeed = 1
        }
        switch sliderSpeed {
        case 2:
            btnSpeedPlay.setImage(#imageLiteral(resourceName: "Speedx2"), for: .normal)
            updateTimeValueForSliderPlay = 0.1
        case 3:
            btnSpeedPlay.setImage(#imageLiteral(resourceName: "Speedx4"), for: .normal)
            updateTimeValueForSliderPlay = 0.05
        case 1:
            btnSpeedPlay.setImage(#imageLiteral(resourceName: "Speedx1"), for: .normal)
            updateTimeValueForSliderPlay = 0.25
        default:
            break
        }
    }
}
