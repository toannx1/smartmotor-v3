//
//  PickerDateCustomButton.swift
//  SmartMotor
//
//  Created by nguyen hoan on 9/17/19.
//  Copyright © 2019 EPR. All rights reserved.
//

import UIKit

//MARK: - Create custom button pick date
class CustomButton: UIView {
    var path: UIBezierPath!
    var path2: UIBezierPath!
    var mode = 1
    
    init(frame: CGRect, mode: Int) {
        self.mode = mode
        super.init(frame: frame)
        self.backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        if mode == 1 {
            createShape(width1: 4, width2: 0)
        } else {
            createShape(width1: 0, width2: 4)
        }
        
        #colorLiteral(red: 0.9450980392, green: 0.9764705882, blue: 1, alpha: 1).setFill()
        path.fill()
        path2.fill()
        // Specify a border (stroke) color.
        #colorLiteral(red: 0, green: 0.4470588235, blue: 0.8039215686, alpha: 1).setStroke()
        path.stroke()
        path2.stroke()
    }
    
    func createShape(width1: CGFloat, width2: CGFloat) {
        path = UIBezierPath()
        path.lineWidth = width1
        
        path2 = UIBezierPath()
        path2.lineWidth = width2
        let point1 = CGPoint(x: 0, y: 0)
        let point2 = CGPoint(x: frame.width / 2 - 15, y: 0)
        let point3 = CGPoint(x: frame.width / 2 + 15, y: frame.height / 2)
        let point4 = CGPoint(x: frame.width / 2 - 15, y: frame.height)
        let point5 = CGPoint(x: 0, y: frame.height)
        
        if mode == 1 {
            let pointx1 = CGPoint(x: frame.width / 4 + 8, y: frame.height)
            let pointx2 = CGPoint(x: frame.width / 4, y: frame.height - 12)
            let pointx3 = CGPoint(x: frame.width / 4 - 8, y: frame.height)
            
            path.move(to: point1)
            path.addLine(to: point2)
            path.addLine(to: point3)
            path.addLine(to: point4)
            path.addLine(to: pointx1)
            path.addLine(to: pointx2)
            path.addLine(to: pointx3)
            path.addLine(to: point5)
            path.close()
            
            path2 = UIBezierPath()
            path2.lineWidth = width2
            path2.move(to: point2)
            path2.addLine(to: CGPoint(x: frame.width, y: 0))
            path2.addLine(to: CGPoint(x: frame.width, y: frame.height))
            path2.addLine(to: point4)
            path2.addLine(to: point3)
            path2.close()
        } else {
            let pointx1 = CGPoint(x: frame.width * 3 / 4 + 8, y: frame.height)
            let pointx2 = CGPoint(x: frame.width * 3 / 4, y: frame.height - 12)
            let pointx3 = CGPoint(x: frame.width * 3 / 4 - 8, y: frame.height)
            
            path.move(to: point1)
            path.addLine(to: point2)
            path.addLine(to: point3)
            path.addLine(to: point4)
            path.addLine(to: point5)
            path.close()
            
            path2.move(to: point2)
            path2.addLine(to: CGPoint(x: frame.width, y: 0))
            path2.addLine(to: CGPoint(x: frame.width, y: frame.height))
            path2.addLine(to: pointx1)
            path2.addLine(to: pointx2)
            path2.addLine(to: pointx3)
            path2.addLine(to: point4)
            path2.addLine(to: point3)
            path2.close()
        }
    }
}
