//
//  RouteTableViewCell.swift
//  SmartMotor
//
//  Created by Hoang Linh on 9/27/19.
//  Copyright © 2019 EPR. All rights reserved.
//

import UIKit

class RouteTableViewCell: UITableViewCell {
    @IBOutlet var lbStt: UILabel!
    @IBOutlet var lbTime: UILabel!
    @IBOutlet var lbStatus: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
