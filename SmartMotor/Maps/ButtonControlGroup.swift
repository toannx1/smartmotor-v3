//
//  ButtonControlGroup.swift
//  SmartMotor
//
//  Created by Vietnv2 on 3/11/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import UIKit

extension MapVC {
    // MARK: - check vehicle state
    
    func checkVehicleState() -> Bool {
        switch vehicle2show.state {
        case 5:
            showBannerNotifi(text: txtAlert01MapVC[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            // alertCustom(message: txtAlert01MapVC[languageChooseSave], delay: 2)
            return false
        case 6:
            showBannerNotifi(text: txtAlert02MapVC[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            // alertCustom(message: txtAlert02MapVC[languageChooseSave], delay: 2)
            return false
        case 0:
            showBannerNotifi(text: txtAlert03MapVC[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            // alertCustom(message: txtAlert03MapVC[languageChooseSave], delay: 2)
            return false
        default:
            break
        }
        return true
    }
    
    // MARK: - Lock unlock vehicle
    
    func displayLockUnlockContainerView() {
        lockUnlockContainerView.frame = CGRect(x: 0, y: view.frame.size.height - 70, width: view.frame.size.width, height: 70)
        view.addSubview(lockUnlockContainerView)
        lblButtonControl_Lock.text = txtButtonLock[languageChooseSave]
        lblButtonControl_Call.text = txtButtonCall[languageChooseSave]
        lblButtonControl_UnLock.text = txtButtonUnLock[languageChooseSave]
    }
    
    func removeLockUnlockContainerView() {
        lockUnlockContainerView.removeFromSuperview()
    }
    
    // MARK: - LOCK & Confirm Send Lock
    
    func displayConfirmSendLockView() {
        confirmSendLockView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(confirmSendLockView)
//        lblTitleConfirmLock.text = txtTitleConfirmLockVehicle[languageChooseSave].uppercased()
        lblTitleConfirmLock.text = "Khoá xe".uppercased()
        lblContentConfirmLock.text = String(format: txtContentConfirmLockVehicle[languageChooseSave], vehicle2show.regNo)
        viewContainerLockBtn.layer.borderColor = UIColor.darkGray.cgColor
        viewContainerLockBtn.layer.borderWidth = 1
//        btnOKConfirmLock.setTitle(txtButtonOKConfirmLockVehicle[languageChooseSave], for: .normal)
//        btnCancelConfirmLock.setTitle(txtButtonCancelConfirmLockVehicle[languageChooseSave], for: .normal)
        
        btnSendByGPRS_ConfirmSendLock.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
        btnSendBySMS_ConfirmSendLock.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        sendBySmsFlag = false
        btnSendByGPRS_ConfirmSendLock.titleLabel?.font = .boldSystemFont(ofSize: 18)
        btnSendByGPRS_ConfirmSendLock.setTitleColor(#colorLiteral(red: 0.01627634279, green: 0.3547099531, blue: 0.6935865879, alpha: 1), for: .normal)
        btnSendBySMS_ConfirmSendLock.titleLabel?.font = .systemFont(ofSize: 18)
        btnSendBySMS_ConfirmSendLock.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
    }
    
    func removeConfirmSendLockView() {
        confirmSendLockView.removeFromSuperview()
    }
    
    @IBAction func closeConfirmSendLockViewTapped(_ sender: Any) {
        removeConfirmSendLockView()
    }
    
    @IBAction func btnLockTapped(_ sender: AnyObject) {
        if idleFlag == false {
            return
        }
        idleFlag = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            idleFlag = true
        }
        let allow = checkAllowControl()
        if allow == false {
            return
        }
        displayConfirmSendLockView()
    }
    
    @IBAction func btnConfirmLockTapped(_ sender: Any) {
        removeConfirmSendLockView()
        if sendBySmsFlag == true {
            sendMessage(phoneNumber: vehicle2show.sim, content: lockString)
        } else {
            SoapHandShaking(commandIndex: "5", inputContent: "LOCK")
        }
    }
    
    @IBAction func btnCancelLockTapped(_ sender: Any) {
        removeConfirmSendLockView()
    }
    
    @IBAction func btnSendBySMS_ConfirmSendLockTapped(_ sender: Any) {
        btnSendBySMS_ConfirmSendLock.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
        btnSendByGPRS_ConfirmSendLock.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        sendBySmsFlag = true
        btnSendBySMS_ConfirmSendLock.titleLabel?.font = .boldSystemFont(ofSize: 18)
        btnSendBySMS_ConfirmSendLock.setTitleColor(#colorLiteral(red: 0.01627634279, green: 0.3547099531, blue: 0.6935865879, alpha: 1), for: .normal)
        btnSendByGPRS_ConfirmSendLock.titleLabel?.font = .systemFont(ofSize: 18)
        btnSendByGPRS_ConfirmSendLock.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
    }
    
    @IBAction func btnSendByGPRS_ConfirmSendLockTapped(_ sender: Any) {
        btnSendByGPRS_ConfirmSendLock.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
        btnSendBySMS_ConfirmSendLock.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        sendBySmsFlag = false
        btnSendByGPRS_ConfirmSendLock.titleLabel?.font = .boldSystemFont(ofSize: 18)
        btnSendByGPRS_ConfirmSendLock.setTitleColor(#colorLiteral(red: 0.01627634279, green: 0.3547099531, blue: 0.6935865879, alpha: 1), for: .normal)
        btnSendBySMS_ConfirmSendLock.titleLabel?.font = .systemFont(ofSize: 18)
        btnSendBySMS_ConfirmSendLock.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
    }
    
    //
    
    // MARK: - UNLOCK & Confirm Send UnLock
    
    func displayConfirmSendUnlockView() {
        confirmSendUnlockView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(confirmSendUnlockView)
        lblTitleConfirmUnLock.text = "Mở khoá xe".uppercased()
        lblContentConfirmUnLock.text = String(format: txtContentConfirmUnLockVehicle[languageChooseSave], vehicle2show.regNo)
        sendBySmsFlag = false
    }
    
    func removeConfirmSendUnlockView() {
        confirmSendUnlockView.removeFromSuperview()
    }
    
    @IBAction func closeConfirmSendUnlockView(_ sender: Any) {
        removeConfirmSendUnlockView()
    }
    
    @IBAction func btnUnlockTapped(_ sender: AnyObject) {
        if idleFlag == false {
            return
        }
        idleFlag = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            idleFlag = true
        }
        let allow = checkAllowControl()
        if allow == false {
            return
        }
        
        displayConfirmSendUnlockView()
    }
    
    @IBAction func btnConfirmUnlockTapped(_ sender: Any) {
        removeConfirmSendUnlockView()
        if sendBySmsFlag == true {
            sendMessage(phoneNumber: vehicle2show.sim, content: unLockString)
        } else {
            SoapHandShaking(commandIndex: "6", inputContent: "UNLOCK")
        }
    }
    
    @IBAction func btnCancelUnlockTapped(_ sender: Any) {
        removeConfirmSendUnlockView()
    }
    
    @IBAction func btnSendBySMS_ConfirmSendUnLockTapped(_ sender: Any) {
        btnSendBySMS_ConfirmSendUnLock.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
        btnSendByGPRS_ConfirmSendUnLock.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        sendBySmsFlag = true
    }
    
    @IBAction func btnSendByGPRS_ConfirmSendUnLockTapped(_ sender: Any) {
        btnSendByGPRS_ConfirmSendUnLock.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
        btnSendBySMS_ConfirmSendUnLock.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        sendBySmsFlag = false
    }
    
    //
    
    // MARK: - CALL to Vehicle & CONFIRM Send UnLock
    
    func displayConfirmCallVehicleView() {
        confirmCallVehicleView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(confirmCallVehicleView)
//        lblTitleConfirmCall2Vehicle.text = txtTitleConfirmCall2Vehicle[languageChooseSave].uppercased()
        lblContentConfirmCall2Vehicle.text = String(format: txtContentConfirmCall2Vehicle[languageChooseSave], String(vehicle2show.regNo))
//        btnOKConfirmCall2Vehicle.setTitle(txtButtonOKConfirmCall2Vehicle[languageChooseSave], for: .normal)
//        btnCancelConfirmCall2Vehicle.setTitle(txtButtonCancelConfirmCall2Vehicle[languageChooseSave], for: .normal)
    }
    
    func removeConfirmCallVehicleView() {
        confirmCallVehicleView.removeFromSuperview()
    }
    
    @IBAction func closeConfirmCallVehicleView(_ sender: Any) {
        removeConfirmCallVehicleView()
    }
    
    @IBAction func btnCall2VehicleTapped(_ sender: Any) {
        if idleFlag == false {
            return
        }
        idleFlag = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            idleFlag = true
        }
        let allow = checkAllowControl()
        if allow == false {
            return
        }
        displayConfirmCallVehicleView()
    }
    
    @IBAction func btnConfirmCall2VehicleTapped(_ sender: Any) {
        removeConfirmCallVehicleView()
        call2APhone()
    }
    
    @IBAction func btnCancelCall2VehicleTapped(_ sender: Any) {
        removeConfirmCallVehicleView()
    }
    
    //
    //
    func displayUnlockByCallView() {
        unlockByCallView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(unlockByCallView)
        lblTitleUnlockByCallView.text = txtTitleUnlockByCall[languageChooseSave].uppercased()
        lblContentUnlockByCall.text = txtContentUnlockByCall[languageChooseSave]
        btnConfirmCall2VehicleUnlock.setTitle(txtButtonOKUnlockByCall[languageChooseSave], for: .normal)
        btnCancelCall2VehicleUnlock.setTitle(txtButtonCancelUnlockByCall[languageChooseSave], for: .normal)
    }
    
    func removeUnlockByCallView() {
        unlockByCallView.removeFromSuperview()
    }
    
    @IBAction func btnConfirmCall2VehicleUnlockTapped(_ sender: Any) {
        removeUnlockByCallView()
        call2APhone()
    }
    
    @IBAction func btnCancelCall2VehicleUnlockTapped(_ sender: Any) {
        removeUnlockByCallView()
    }
    
    @IBAction func btnLockUnlockClicked(_ sender: Any) {
        if idleFlag == false {
            return
        }
        idleFlag = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            idleFlag = true
        }
        let allow = checkAllowControl()
        if allow == false {
            return
        }
        displayConfirmSendLockView()
    }
    
    //
    func displayLockByCallView() {
        lockByCallView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(lockByCallView)
        lblTitleLockByCallView.text = txtTitleLockByCall[languageChooseSave].uppercased()
        lblContentLockByCall.text = txtContentLockByCall[languageChooseSave]
        btnConfirmCall2VehicleLock.setTitle(txtButtonOKLockByCall[languageChooseSave], for: .normal)
        btnCancelCall2VehicleLock.setTitle(txtButtonCancelLockByCall[languageChooseSave], for: .normal)
    }
    
    func removeLockByCallView() {
        lockByCallView.removeFromSuperview()
    }
    
    @IBAction func btnConfirmCall2VehicleLockTapped(_ sender: Any) {
        removeLockByCallView()
        call2APhone()
    }
    
    @IBAction func btnCancelCall2VehicleLockTapped(_ sender: Any) {
        removeLockByCallView()
    }
    
    // MARK: - Account Info
    
    func removeAccountInfoView() {
        accountInfoView.removeFromSuperview()
    }
    
    @IBAction func closeAccountInfoView(_ sender: Any) {
        accountInfoView.removeFromSuperview()
    }
    
    @IBAction func btnCancelAccountInfoTapped(_ sender: Any) {
//        removeListChargeMoneyView()
        removeAccountInfoView()
    }
    
    @IBAction func btnInfomationTapped(_ sender: Any) {
        if idleFlag == false {
            return
        }
        idleFlag = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            idleFlag = true
        }
        accountInfo()
    }
    
    func accountInfo() {
        Loading().showLoading()
        if tokenSave == nil || userIdSave == nil {
            gotoLoginView()
        }
        let url = URL(string: smartmotorUrl + getAccountInfoUrl + userIdSave + "/" + vehicle2show.sim)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        request.addValue(tokenSave, forHTTPHeaderField: "user-token")
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        let task = defaultSession.dataTask(with: request as URLRequest) { data, response, error in
            Loading().hideLoading(delay: 0)
            if error != nil {
                // self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
                self.showBannerNotifi(text: connectionFail[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    // self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
                    self.showBannerNotifi(text: connectionFail[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!)
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let resultCode = jsonResult["resultCode"] as! Int
                    if resultCode != 1 {
                        self.showBannerNotifi(text: txtAlertAccountInfo[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
                        // self.alertCustom(message: txtAlertAccountInfo[languageChooseSave], delay: 2)
                        return
                    }
                    self.lblSmsPromotion.text = jsonResult["smsPromotion"] as? String
                    self.lblBasicBalance.text = (jsonResult["basicBalance"] as? String)
                    self.lblGprsFreePromotion.text = jsonResult["gprsFreePromotion"] as? String
                    self.lblMoneyPromotion.text = jsonResult["moneyPromotion"] as? String
                    self.lblSmsNgoaiMang.text = jsonResult["smsNgoaiMang"] as? String
                    self.lblTimeCallNgoaiMang.text = jsonResult["timePromotion"] as? String
                    self.displayAccountInfoView()
                } catch {
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    func displayAccountInfoView() {
        accountInfoView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(accountInfoView)
        lblTitleAccountInfo.text = txtTitleAccountInfo[languageChooseSave].uppercased()
        lblContent01AccountInfo.text = txtContent01AccountInfo[languageChooseSave]
        lblContent02AccountInfo.text = txtContent02AccountInfo[languageChooseSave]
        lblContent03AccountInfo.text = txtContent03AccountInfo[languageChooseSave]
        lblContent04AccountInfo.text = txtContent04AccountInfo[languageChooseSave]
        lblContent05AccountInfo.text = txtContent05AccountInfo[languageChooseSave]
        lblContent06AccountInfo.text = txtContent06AccountInfo[languageChooseSave]
        btnCancelAccountInfo.setTitle(txtButtonCancelAccountInfo[languageChooseSave], for: .normal)
    }
    
    // MARK: - Go to other VC
    
    func gotoLoginView() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "login") as! LoginVC
        logoutFlag = true
        nextViewController.modalPresentationStyle = .fullScreen
        present(nextViewController, animated: false, completion: nil)
    }
}
