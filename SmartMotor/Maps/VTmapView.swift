//
//  VTmapView.swift
//  SmartMotor
//
//  Created by Vietnv2 on 3/9/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import CoreLocation
import UIKit
import Mapbox

extension MapVC : MGLMapViewDelegate{
    
    func displayVTMap() {
        removeGoogleMaps()
//        guard vtmapView == nil else {
//            vtmapView.removeFromSuperview()
//            vtmapView = nil
//            return
//        }
        let mapCGRect = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        self.vtMap = MGLMapView(frame: mapCGRect, styleURL: MGLStyle.streetsStyleURL)
        vtmapView = vtMap
        vtmapView.delegate = self
        mapView.addSubview(vtmapView)
//        //vtmapView.zoomLevel = 11
//        vtmapView.zoomLevel = 15
//        vtmapView.setMarkerTabDelegate(self)
//        vtmapView.uiSetting.mapTypeControl = false

    }
    
    func removeViettelMaps() {
        if vtmapView != nil {
            vtmapView.removeFromSuperview()
            vtmapView = nil
        }
    }
    
//    func singleTap(at pt: CGPoint, latLng: VMSLatLng!) -> Bool {
    ////        vtmapView.refresh()
//        return true
//    }
    
//    func singleTabMapObject(_ mapObject: VMSMapObject!, point pt: CGPoint, latLng: VMSLatLng!) -> Bool {
//        tapToMapMarkerForDisplayStatusView()
//        return true
//    }
    
//    func singleTabMapObject1(_ mapObject: VMSMapObject!, point pt: CGPoint, latLng: VMSLatLng!, viewController: UIViewController!, mapView: VMSMapView!) -> Bool {
//        return false
//    }
}

extension MapVC /* VMSGeoServiceDelegate */ {
    func getAddressFromVTMap(_ annotation: CLLocationCoordinate2D) {
        //     displayMarkerMotor(vehicle2show_currentAnnotation, iconMotorStateImage[vehicle2show.state])
        let ceo = CLGeocoder()
        
        let loc: CLLocation = CLLocation(latitude:annotation.latitude, longitude: annotation.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    DispatchQueue.main.async {
                        self.lblVehicleAdress.text = String(format: txtStatus01[languageChooseSave], txtStatus_DetailPoint_NoInfo[languageChooseSave])
                        self.lblVehicleTimeState.text = String(format: "%@ %@", txtStatus02, txtStatus_DetailPoint_NoInfo[languageChooseSave])
                        self.lblVehicleStatus.text = txtStatus_DetailPoint_NoInfo[languageChooseSave]
                        self.lblVehicleStatus.backgroundColor = UIColor(red: 0x77 / 0xFF, green: 0x1C / 0xFF, blue: 0xF9 / 0xFF, alpha: 1)
                    }
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                        let pm = placemarks![0]

                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    let attrs1 : [NSAttributedString.Key : Any] = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.black]
                    let attrs2 : [NSAttributedString.Key : Any] = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor.black]
                    let attributedString4 = NSMutableAttributedString(string: "Vị trí hiện tại: ", attributes: attrs1)
                    let attributedString1 = NSMutableAttributedString(string: "Vị trí xe: ", attributes: attrs1)
                    let attributedString2 = NSMutableAttributedString(string: addressString, attributes: attrs2)
                    let attributedString3 = NSMutableAttributedString(string: " (\(annotation.latitude),\(annotation.longitude))", attributes: attrs1)
                    attributedString4.append(attributedString2)
                    attributedString4.append(attributedString3)
                    attributedString1.append(attributedString2)
                    attributedString1.append(attributedString3)
                 self.lblVehicleAdress.attributedText = attributedString4
                self.lbInfomationAddress.attributedText = attributedString1
                    
                }
            })
        var string: String!
        var color: UIColor!
        switch vehicle2show.state {
        case 1:
            string = txtStatus_DetailPoint_Run[languageChooseSave]
            color = UIColor(red: 0x35 / 0xFF, green: 0x87 / 0xFF, blue: 0x00 / 0xFF, alpha: 1)
        case 2:
            string = txtStatus_DetailPoint_Stop[languageChooseSave]
            color = UIColor(red: 0x00 / 0xFF, green: 0x76 / 0xFF, blue: 0xAA / 0xFF, alpha: 1)
        case 3:
            string = txtStatus_DetailPoint_Park[languageChooseSave]
            color = UIColor(red: 0xFF / 0xFF, green: 0xB6 / 0xFF, blue: 0x00 / 0xFF, alpha: 1)
        case 4:
            string = txtStatus_DetailPoint_LossGPS[languageChooseSave]
            color = UIColor(red: 0xFF / 0xFF, green: 0x6D / 0xFF, blue: 0x00 / 0xFF, alpha: 1)
        case 5:
            string = txtStatus_DetailPoint_Hibernate[languageChooseSave]
            color = UIColor(red: 0x51 / 0xFF, green: 0x51 / 0xFF, blue: 0x51 / 0xFF, alpha: 1)
        case 6: //txtStatus03[languageChooseSave]
            string = txtStatus_DetailPoint_LossGPRS[languageChooseSave]
            color = UIColor(red: 0xFF / 0xFF, green: 0x17 / 0xFF, blue: 0x17 / 0xFF, alpha: 1)
        default:
            string = txtStatus_DetailPoint_NoInfo[languageChooseSave]
            color = UIColor(red: 0x77 / 0xFF, green: 0x1C / 0xFF, blue: 0xF9 / 0xFF, alpha: 1)
        }
        // self.lblVehicleStatus.text = string
        // self.lblVehicleStatus.textColor = color
        if vehicle2show.state > 0, vehicle2show.state <= 6 {
            if languageChooseSave == 0 {
                self.setColorInLabel(label: self.lblVehicleStatus, string: string, fontName: "Georgia", fontSize: self.lblVehicleStatus.font.pointSize, color: color)
            } else {
                self.setColorInLabel(label: self.lblVehicleStatus, string: string, fontName: "Georgia", fontSize: self.lblVehicleStatus.font.pointSize, color: color)
            }
        } else {
            self.setColorInLabel(label: self.lblVehicleStatus, string: string, fontName: "Georgia", fontSize: self.lblVehicleStatus.font.pointSize, color: color)
        }
        //
        if vehicle2show.state == 1 {
            self.lblVehicleTimeState.text = txtStatus04[languageChooseSave] + String(vehicle2show.motoSpeed) + self.kmPerHourString
        } else {
            self.lblVehicleTimeState.text = self.calculateVehicleTimeState()
        }
        //
        // self.lbInfomationAddress.text = self.lblVehicleAdress.text // lb thông tin map view
        self.lblContentAddressView.text = self.lblVehicleAdress.text
        self.displayAddressView()
        vtMap.setCenter(annotation, zoomLevel: 14, animated: true)
        vtCustomMarkerForMotor(annotation: annotation, sourceIdentifier: self.makersource, styleIdentifier: self.styleIndetifier, image: iconMotorStateImage[vehicle2show.state], forname: self.vtMapForname)
    }
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        
    }
    func vtCustomMarkerForMotor( annotation : CLLocationCoordinate2D , sourceIdentifier : String! ,
                                 styleIdentifier : String! , image : UIImage, forname : String) {
        self.vtMap.style?.removeLayer(self.shapeLayer)
        self.vtMap.style?.removeSource(self.shapeSource!)
        self.vtMarkerForMotor.coordinate = annotation
        
        //        marker-source
        self.shapeSource = MGLShapeSource(identifier: sourceIdentifier, shape: self.vtMarkerForMotor, options: nil)
        self.shapeLayer = MGLSymbolStyleLayer(identifier: styleIdentifier, source: self.shapeSource!)
        self.vtMap.style?.setImage(image, forName: forname)
        
        self.shapeLayer.iconImageName = NSExpression(forConstantValue: forname)
        self.vtMap.style?.addSource(self.shapeSource!)
        self.vtMap.style?.addLayer(self.shapeLayer!)
    }
    //    func geoServiceWillStart(_ type: VMSGeoServiceType) {
    //    }
    
//    func geoServiceDidComplete(_ result: VMSGeoServiceResult!, of type: VMSGeoServiceType) {
//        if result.status != VMSStatusOK {
//            return
//        }
//        switch (type) {
//        case VMSGetPointAddress:
//            self.lblVehicleAdress.text = String(format: txtStatus01[languageChooseSave], String(result.getItem(0).address + " (\(annotationVTDisplay.latitude),\(annotationVTDisplay.longitude))"))
//            // display address view
//            self.lblContentAddressView.text = self.lblVehicleAdress.text
//            var string:String!
//            var color:UIColor!
//            switch vehicle2show.state {
//            case 1:
//                string = String(format: txtStatus03[languageChooseSave], txtStatus_DetailPoint_Run[languageChooseSave])
//                color = UIColor(red: 0x35/0xFF, green: 0x87/0xFF, blue: 0x00/0xFF, alpha: 1)
//            case 2:
//                string = String(format: txtStatus03[languageChooseSave], txtStatus_DetailPoint_Stop[languageChooseSave])
//                color = UIColor(red: 0x00/0xFF, green: 0x76/0xFF, blue: 0xAA/0xFF, alpha: 1)
//            case 3:
//                string = String(format: txtStatus03[languageChooseSave], txtStatus_DetailPoint_Park[languageChooseSave])
//                color = UIColor(red: 0xFF/0xFF, green: 0xB6/0xFF, blue: 0x00/0xFF, alpha: 1)
//            case 4:
//                string = String(format: txtStatus03[languageChooseSave], txtStatus_DetailPoint_LossGPS[languageChooseSave])
//                color = UIColor(red: 0xFF/0xFF, green: 0x6D/0xFF, blue: 0x00/0xFF, alpha: 1)
//            case 5:
//                string = String(format: txtStatus03[languageChooseSave], txtStatus_DetailPoint_Hibernate[languageChooseSave])
//                color = UIColor(red: 0x51/0xFF, green: 0x51/0xFF, blue: 0x51/0xFF, alpha: 1)
//            case 6:
//                string = String(format: txtStatus03[languageChooseSave], txtStatus_DetailPoint_LossGPRS[languageChooseSave])
//                color = UIColor(red: 0xFF/0xFF, green: 0x17/0xFF, blue: 0x17/0xFF, alpha: 1)
//            default:
//                string = txtStatus_DetailPoint_NoInfo[languageChooseSave]
//                color = UIColor(red: 0x77/0xFF, green: 0x1C/0xFF, blue: 0xF9/0xFF, alpha: 1)
//            }
//            if vehicle2show.state > 0 && vehicle2show.state <= 6 {
//                if languageChooseSave == 0 {
//                    self.setColorInLabel(label: self.lblVehicleStatus, string: string, fontName: "Georgia", fontSize: self.lblVehicleStatus.font.pointSize, color: color, location: 11, length: string.count - 11)
//                } else {
//                    self.setColorInLabel(label: self.lblVehicleStatus, string: string, fontName: "Georgia", fontSize: self.lblVehicleStatus.font.pointSize, color: color, location: 7, length: string.count - 7)
//                }
//            }else {
//                self.setColorInLabel(label: self.lblVehicleStatus, string: string, fontName: "Georgia", fontSize: self.lblVehicleStatus.font.pointSize, color: color, location: 0, length: string.count)
//            }
//            //
//            if vehicle2show.state == 1 {
//                self.lblVehicleTimeState.text = txtStatus04[languageChooseSave] + String(vehicle2show.motoSpeed) + self.kmPerHourString
//            }else {
//                self.lblVehicleTimeState.text = calculateVehicleTimeState()
//            }
//            displayAddressView()
//            break;
//        case VMSGetPointsAddress:
//            break;
//        default:
//            break;
//        }
//    }
    
    func calculateVehicleTimeState() -> String {
        let string: String!
        let days = vehicle2show.timeState / 86400
        let hours = (vehicle2show.timeState % 86400) / 3600
        let minutes = (vehicle2show.timeState % 3600) / 60
        let seconds = vehicle2show.timeState % 60
        if languageChooseSave == 0 {
            if days > 0 {
                string = String(format: "\(txtStatus02[languageChooseSave]) %d %@ %d %@", days, txtDay[languageChooseSave], hours, txtHour[languageChooseSave])
            } else {
                if hours > 0 {
                    string = String(format: "\(txtStatus02[languageChooseSave]) %d %@ %d %@", hours, txtHour[languageChooseSave], minutes, txtMinute[languageChooseSave])
                } else {
                    if minutes > 0 {
                        string = String(format: "\(txtStatus02[languageChooseSave]) %d %@ %d %@", minutes, txtMinute[languageChooseSave], seconds, txtSecond[languageChooseSave])
                    } else {
                        string = String(format: "\(txtStatus02[languageChooseSave]) %d %@", seconds, txtSecond[languageChooseSave])
                    }
                }
            }
        } else {
            if days > 0 {
                if days > 1 {
                    if hours > 1 {
                        string = String(format: "\(txtStatus02[languageChooseSave]) %d %@s %d %@s", days, txtDay[languageChooseSave], hours, txtHour[languageChooseSave])
                    } else {
                        string = String(format: "\(txtStatus02[languageChooseSave]) %d %@s %d %@", days, txtDay[languageChooseSave], hours, txtHour[languageChooseSave])
                    }
                } else {
                    if hours > 1 {
                        string = String(format: "\(txtStatus02[languageChooseSave]) %d %@ %d %@s", days, txtDay[languageChooseSave], hours, txtHour[languageChooseSave])
                    } else {
                        string = String(format: "\(txtStatus02[languageChooseSave]) %d %@ %d %@", days, txtDay[languageChooseSave], hours, txtHour[languageChooseSave])
                    }
                }
            } else {
                if hours > 0 {
                    if hours > 1 {
                        if minutes > 1 {
                            string = String(format: "\(txtStatus02[languageChooseSave]) %d %@s %d %@s", hours, txtHour[languageChooseSave], minutes, txtMinute[languageChooseSave])
                        } else {
                            string = String(format: "\(txtStatus02[languageChooseSave]) %d %@s %d %@", hours, txtHour[languageChooseSave], minutes, txtMinute[languageChooseSave])
                        }
                    } else {
                        if minutes > 1 {
                            string = String(format: "\(txtStatus02[languageChooseSave]) %d %@ %d %@s", hours, txtHour[languageChooseSave], minutes, txtMinute[languageChooseSave])
                        } else {
                            string = String(format: "\(txtStatus02[languageChooseSave]) %d %@ %d %@", hours, txtHour[languageChooseSave], minutes, txtMinute[languageChooseSave])
                        }
                    }
                } else {
                    if minutes > 0 {
                        if minutes > 1 {
                            if seconds > 1 {
                                string = String(format: "\(txtStatus02[languageChooseSave]) %d %@s %d %@s", minutes, txtMinute[languageChooseSave], seconds, txtSecond[languageChooseSave])
                            } else {
                                string = String(format: "\(txtStatus02[languageChooseSave]) %d %@s %d %@", minutes, txtMinute[languageChooseSave], seconds, txtSecond[languageChooseSave])
                            }
                        } else {
                            if seconds > 1 {
                                string = String(format: "\(txtStatus02[languageChooseSave]) %d %@ %d %@s", minutes, txtMinute[languageChooseSave], seconds, txtSecond[languageChooseSave])
                            } else {
                                string = String(format: "\(txtStatus02[languageChooseSave]) %d %@ %d %@", minutes, txtMinute[languageChooseSave], seconds, txtSecond[languageChooseSave])
                            }
                        }
                    } else {
                        if seconds > 1 {
                            string = String(format: "\(txtStatus02[languageChooseSave]) %d %@s", seconds, txtSecond[languageChooseSave])
                        } else {
                            string = String(format: "\(txtStatus02[languageChooseSave]) %d %@", seconds, txtSecond[languageChooseSave])
                        }
                    }
                }
            }
        }
        return string
    }
}

extension MapVC {
    func vtCustomMarker(annotation: CLLocationCoordinate2D, img: UIImage, title: String, subtitle: String) {
//        let position = VMSLatLng.init(lat: annotation.latitude, lng: annotation.longitude)
//        let marker = VMSMarker.init(position: position)
//        vtmapView.add(marker)
//        marker?.zIndex = 10
//        marker?.isDraggable = true
//        marker?.icon = img
//        marker?.anchor = CGPoint(x: img.size.width/2, y: img.size.height/2)
    }
    
//    func vtCustomMarkerForMotor(annotation: CLLocationCoordinate2D, img: UIImage, title: String, subtitle: String) {
//        vtMarkerForMotor?.remove()
//        let position = VMSLatLng.init(lat: annotation.latitude, lng: annotation.longitude)
//        vtMarkerForMotor = VMSMarker.init(position: position)
//        if vtmapView == nil {
//            displayVTMap()
//        }
//        vtmapView.add(vtMarkerForMotor)
//        vtMarkerForMotor?.zIndex = 10
//        vtMarkerForMotor?.isDraggable = true
//
//        vtMarkerForMotor?.icon = img
//        vtMarkerForMotor?.anchor = CGPoint(x: img.size.width/2, y: img.size.height/2)
//
//    }
}
