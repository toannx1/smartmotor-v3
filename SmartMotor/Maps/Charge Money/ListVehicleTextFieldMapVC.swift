//
//  ListVehicleTextField.swift
//  SmartMotor
//
//  Created by vietnv2 on 12/5/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

extension MapVC: UITextFieldDelegate {
    func initTextField() {
        txtChargeCode.keyboardType = UIKeyboardType.numberPad
        txtCaptchaCardCode.keyboardType = UIKeyboardType.default
        txtChargeCode.delegate = self
        txtCaptchaCardCode.delegate = self
        txtCaptchaPayMoney.delegate = self
        txtChargeCode.keyboardDistanceFromTextField = 238
        txtCaptchaCardCode.keyboardDistanceFromTextField = 153
        txtCaptchaPayMoney.keyboardDistanceFromTextField = 81
        
        btnSelectedDateStartSetTitle.tintColor = .clear
        btnSelectedStartTimeSetTitle.tintColor = .clear
        btnSelectedDateEndSetTitle.tintColor = .clear
        btnSelectedEndTimeSetTitle.tintColor = .clear
        
        btnSelectedDateStartSetTitle.delegate = self
        btnSelectedStartTimeSetTitle.delegate = self
        btnSelectedDateEndSetTitle.delegate = self
        btnSelectedEndTimeSetTitle.delegate = self
        showDatePicker()
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == btnSelectedDateStartSetTitle || textField == btnSelectedDateStartSetTitle || textField == btnSelectedDateStartSetTitle || textField == btnSelectedDateStartSetTitle {
//            return false
//        }
//    }
    
    func showDatePicker() {
        // ToolBar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(cancelDatePicker))
        
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        
//        btnSelectedDateStartSetTitle.inputAccessoryView = toolbar
        btnSelectedDateStartSetTitle.inputView = datePicker
//        btnSelectedStartTimeSetTitle.inputAccessoryView = toolbar
        btnSelectedStartTimeSetTitle.inputView = datePicker
//        btnSelectedDateEndSetTitle.inputAccessoryView = toolbar
        btnSelectedDateEndSetTitle.inputView = datePicker
//        btnSelectedEndTimeSetTitle.inputAccessoryView = toolbar
        btnSelectedEndTimeSetTitle.inputView = datePicker
        
        btnSelectedDateStartSetTitle.delegate = self
        btnSelectedStartTimeSetTitle.delegate = self
        btnSelectedDateEndSetTitle.delegate = self
        btnSelectedEndTimeSetTitle.delegate = self
        
        datePicker.addTarget(self, action: #selector(valueChanged(datePicker:)), for: .valueChanged)
    }
    
    @objc func valueChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        if btnSelectedDateStartSetTitle.isFirstResponder {
            dateFormatter.dateFormat = "dd/MM/yyyy"
            btnSelectedDateStartSetTitle.text = dateFormatter.string(from: datePicker.date)
            btnSelectedDateStartSetTitle.selectedTextRange = btnSelectedDateStartSetTitle.textRange(from: btnSelectedDateStartSetTitle.beginningOfDocument, to: btnSelectedDateStartSetTitle.endOfDocument)
            dateStart2SendRequest = dateFormatter.string(from: datePicker.date)
            
            // Check invalid date to send request
            dateStart = dateFormatter.date(from: dateStart2SendRequest)
            checkInvalidDateToShowHistory = daysBetweenDates(startDate: dateStart as Date, endDate: dateEnd as Date)
        }
        if btnSelectedStartTimeSetTitle.isFirstResponder {
            dateFormatter.dateFormat = "HH:mm"
            btnSelectedStartTimeSetTitle.text = dateFormatter.string(from: datePicker.date)
            btnSelectedStartTimeSetTitle.selectedTextRange = btnSelectedStartTimeSetTitle.textRange(from: btnSelectedStartTimeSetTitle.beginningOfDocument, to: btnSelectedStartTimeSetTitle.endOfDocument)
            timeStart2SendRequest = dateFormatter.string(from: datePicker.date)
        }
        if btnSelectedDateEndSetTitle.isFirstResponder {
            dateFormatter.dateFormat = "dd/MM/yyyy"
            btnSelectedDateEndSetTitle.text = dateFormatter.string(from: datePicker.date)
            btnSelectedDateEndSetTitle.selectedTextRange = btnSelectedDateEndSetTitle.textRange(from: btnSelectedDateEndSetTitle.beginningOfDocument, to: btnSelectedDateEndSetTitle.endOfDocument)
            
            dateEnd2SendRequest = dateFormatter.string(from: datePicker.date)
            dateEnd = dateFormatter.date(from: dateEnd2SendRequest)
            checkInvalidDateToShowHistory = daysBetweenDates(startDate: dateStart as Date, endDate: dateEnd as Date)
        }
        if btnSelectedEndTimeSetTitle.isFirstResponder {
            dateFormatter.dateFormat = "HH:mm"
            btnSelectedEndTimeSetTitle.text = dateFormatter.string(from: datePicker.date)
            btnSelectedEndTimeSetTitle.selectedTextRange = btnSelectedEndTimeSetTitle.textRange(from: btnSelectedEndTimeSetTitle.beginningOfDocument, to: btnSelectedEndTimeSetTitle.endOfDocument)
            
            timeEnd2SendRequest = dateFormatter.string(from: datePicker.date)
            dateFormatter.dateFormat = "HH:mm:ss"
            timeEnd2ViewDetailPoint = dateFormatter.string(from: datePicker.date)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case btnSelectedDateStartSetTitle:
            btnSelectedDateStartSetTitle.selectedTextRange = btnSelectedDateStartSetTitle.textRange(from: btnSelectedDateStartSetTitle.beginningOfDocument, to: btnSelectedDateStartSetTitle.endOfDocument)
            datePicker.datePickerMode = .date
        case btnSelectedStartTimeSetTitle:
            datePicker.datePickerMode = .time
            btnSelectedStartTimeSetTitle.selectedTextRange = btnSelectedStartTimeSetTitle.textRange(from: btnSelectedStartTimeSetTitle.beginningOfDocument, to: btnSelectedStartTimeSetTitle.endOfDocument)
        case btnSelectedDateEndSetTitle:
            btnSelectedDateEndSetTitle.selectedTextRange = btnSelectedDateEndSetTitle.textRange(from: btnSelectedDateEndSetTitle.beginningOfDocument, to: btnSelectedDateEndSetTitle.endOfDocument)
            datePicker.datePickerMode = .date
        case btnSelectedEndTimeSetTitle:
            btnSelectedEndTimeSetTitle.selectedTextRange = btnSelectedEndTimeSetTitle.textRange(from: btnSelectedEndTimeSetTitle.beginningOfDocument, to: btnSelectedEndTimeSetTitle.endOfDocument)
            datePicker.datePickerMode = .time
        case txtCaptchaCardCode:
            if txtChargeCode.text?.count != 13, txtChargeCode.text?.count != 15 {
                lblErrorCardCodeCard.text = "Vui lòng nhập Mã thẻ cào chứa 13 hoặc 15 kí tự số"
                lblErrorCardCodeCard.isHidden = false
            }
        default:
            break
        }
    }
    
    @objc func donedatePicker() {
        let dateFormatter = DateFormatter()
        if btnSelectedDateStartSetTitle.isFirstResponder {
            dateFormatter.dateFormat = "dd/MM/yyyy"
            btnSelectedDateStartSetTitle.text = dateFormatter.string(from: datePicker.date)
            dateStart2SendRequest = dateFormatter.string(from: datePicker.date)
            
            // Check invalid date to send request
            dateStart = dateFormatter.date(from: dateStart2SendRequest)
            checkInvalidDateToShowHistory = daysBetweenDates(startDate: dateStart as Date, endDate: dateEnd as Date)
        }
        if btnSelectedStartTimeSetTitle.isFirstResponder {
            dateFormatter.dateFormat = "HH:mm"
            btnSelectedStartTimeSetTitle.text = dateFormatter.string(from: datePicker.date)
            timeStart2SendRequest = dateFormatter.string(from: datePicker.date)
        }
        if btnSelectedDateEndSetTitle.isFirstResponder {
            dateFormatter.dateFormat = "dd/MM/yyyy"
            btnSelectedDateEndSetTitle.text = dateFormatter.string(from: datePicker.date)
            dateEnd2SendRequest = dateFormatter.string(from: datePicker.date)
            dateEnd = dateFormatter.date(from: dateEnd2SendRequest)
            checkInvalidDateToShowHistory = daysBetweenDates(startDate: dateStart as Date, endDate: dateEnd as Date)
        }
        if btnSelectedEndTimeSetTitle.isFirstResponder {
            dateFormatter.dateFormat = "HH:mm"
            btnSelectedEndTimeSetTitle.text = dateFormatter.string(from: datePicker.date)
            timeEnd2SendRequest = dateFormatter.string(from: datePicker.date)
            dateFormatter.dateFormat = "HH:mm:ss"
            timeEnd2ViewDetailPoint = dateFormatter.string(from: datePicker.date)
        }
        view.endEditing(true)
    }
    
    @objc func cancelDatePicker() {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtChargeCode, txtChargeCode.text == "" {
        } else if textField == txtCaptchaCardCode {
            btnConfirmChargeCardCodeTap()
        }
        if textField == txtCaptchaPayMoney {
//            alertCustom(message: txtEnterCaptcha[languageChooseSave], delay: 2)
            confirmPayMoney()
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.selectedTextRange = nil
        if textField.text == "" {
            switch textField {
            case txtChargeCode:
                lblErrorCardCodeCard.isHidden = false
            case txtCaptchaCardCode:
                lblErrorCardCodeVerify.isHidden = false
            case txtCaptchaPayMoney:
                lblErrorPayMoneyCaptcha.isHidden = false
            default: break
            }
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        switch textField {
        case txtChargeCode:
            lblErrorCardCodeCard.isHidden = false
        case txtCaptchaCardCode:
            lblErrorCardCodeVerify.isHidden = false
        case txtCaptchaPayMoney:
            lblErrorPayMoneyCaptcha.isHidden = false
        default: break
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 15
        
        let text = textField.text
        let newLength = (text?.count)! + string.count - range.length
        let flag: Bool!
        flag = !(newLength > 0)
        
        switch textField {
        case txtChargeCode:
            lblErrorCardCodeCard.isHidden = !flag
        case txtCaptchaCardCode:
            lblErrorCardCodeVerify.isHidden = !flag
        case txtCaptchaPayMoney:
            lblErrorPayMoneyCaptcha.isHidden = !flag
        default: break
        }
        
        if textField.text!.count < 15 {
            if textField == txtChargeCode {
                // dont allow special character
                let set = NSCharacterSet(charactersIn: "0123456789").inverted
                return string.rangeOfCharacter(from: set) == nil
            }
        }
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}

extension UILabel {
    private struct AssociatedKeys {
        static var padding = UIEdgeInsets()
    }
    
    public var padding: UIEdgeInsets? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.padding) as? UIEdgeInsets
        }
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &AssociatedKeys.padding, newValue as UIEdgeInsets?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
}
