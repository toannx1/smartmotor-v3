//
//  ListVehicleTableView.swift
//  SmartMotor
//
//  Created by vietnv2 on 12/5/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import SwiftEntryKit
import UIKit

extension MapVC {
    enum dataConnection: String {
        case noConnection
        case wifiConnection
        case otherConnection = "2g3gConnection"
    }
    
    func initTableView() {
        listChargeMoneyTypeTableView.tableFooterView = UIView(frame: CGRect.zero)
        listChargeMoneyTypeTableView.isScrollEnabled = false
//        listMonth2PayTableView.tableFooterView = UIView(frame: CGRect.zero)
//        listMonth2PayTableView.isScrollEnabled = false
        let footer = UIView(frame: CGRect.zero)
        footer.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
//        displaySearchBar()
    }
    
    func image(_ image: UIImage, withSize newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!.withRenderingMode(.automatic)
    }
    
    // var scrollFlag:Bool! = false
//    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
    ////        if scrollView.contentOffset.y < 0 && listVehicleContraint.constant == 0 {
    ////            listVehicleContraint.constant = 50
    ////        }
//        UIView.animate(withDuration: 0.5) {
//            self.view.layoutIfNeeded()
//        }
//    }
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
    ////        print(page)
//
//    }
    
//    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    ////        if scrollView.contentOffset.y > 1 && listVehicleContraint.constant == 50 {
    ////            listVehicleContraint.constant = 0
    ////        }
//        UIView.animate(withDuration: 0.2) {
//            self.view.layoutIfNeeded()
//        }
//
//    }
}
