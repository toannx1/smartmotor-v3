//
//  MapVC.swift
//  Smart Motor
//
//  Created by vietnv2 on 20/10/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import Darwin
import GoogleMaps
import Reachability
import UIKit
import Mapbox

class MapVC: UIViewController {
    @IBOutlet var viewChargeSuccess: UIView!
    @IBOutlet var viewInfo: UIView!
    
    @IBOutlet var btnCardCode: UIButton!
    @IBOutlet var btnPayMoney: UIButton!
    @IBOutlet var viewContainerChargeMoney: UIView!
    
    @IBOutlet var btn1Month: UIButton!
    @IBOutlet var btn2Month: UIButton!
    @IBOutlet var btn3Month: UIButton!
    @IBOutlet var btn6Month: UIButton!
    let datePicker = UIDatePicker()
    
    @IBOutlet var lblErrorPayMoneyCaptcha: UILabel!
    @IBOutlet var lblErrorCardCodeVerify: UILabel!
    @IBOutlet var lblErrorCardCodeCard: UILabel!
    @IBOutlet var heightTbvMonthConstraint: NSLayoutConstraint!
    @IBOutlet var imgCaptchaChargeCode: UIImageView!
    @IBOutlet var imgCaptchaPayMoney: UIImageView!
    @IBOutlet var imgRefressCaptchaChargeCode: UIImageView!
    @IBOutlet var imgRefressCaptchaPayMoney: UIImageView!
    @IBOutlet var btnCloseListChargeMoneyType: UIButton!
    @IBOutlet var btnChooseNumberOfMonth2Pay: UIButton!
    @IBOutlet var txtCaptchaCardCode: UITextField!
    @IBOutlet var txtCaptchaPayMoney: UITextField!
    @IBOutlet var txtChargeCode: UITextField!
    @IBOutlet var txtMoneyPay: UILabel!
    @IBOutlet var payMoneyViewHeightContraint: NSLayoutConstraint!
    @IBOutlet var listChargeMoneyTypeTableView: UITableView!
    @IBOutlet var listMonth2PayTableView: UITableView!
    @IBOutlet var lblContentConfirmChargeCardCode: VerticalTopAlignLabel!
    @IBOutlet var lblTitleListChargeMoney: UILabel!
    @IBOutlet var lblTitleChargeCardCode: UILabel!
    //    @IBOutlet weak var lblContent01ChargeCardCode: UILabel!
    @IBOutlet var lblContent02ChargeCardCode: UILabel!
    @IBOutlet var lblContent03ChargeCardCode: UILabel!
    @IBOutlet var lblContent04ChargeCardCode: UILabel!
    @IBOutlet var lblContent05ChargeCardCode: UILabel!
    @IBOutlet var btnConfirmChargeCardCode: UIButton!
    @IBOutlet var btnCancelChargeCardcode: UIButton!
    @IBOutlet var lblTitleConfirmChargeCardCode: UILabel!
    @IBOutlet var btnOKConfirmChargeCardCode: UIButton!
    @IBOutlet var btnCancelConfirmChargeCardCode: UIButton!
    @IBOutlet var lblTitlePayMoney: UILabel!
    @IBOutlet var lblContent01PayMoney: UILabel!
    @IBOutlet var lblContent02PayMoney: UILabel!
    @IBOutlet var lblContent03PayMoney: UILabel!
    @IBOutlet var btnConfirmPayMoney: UIButton!
    @IBOutlet var btnCancelPayMoney: UIButton!
    
    @IBOutlet var lblTitleConfirmPayMoney: UILabel!
    @IBOutlet var lblContentConfirmPayMoney: UILabel!
    @IBOutlet var btnOKConfirmPayMoney: UIButton!
    @IBOutlet var btnCancelConfirmPayMoney: UIButton!
    
    @IBOutlet var viewInfoContainer: UIView!
    var infoShow = false
    @IBOutlet var bottomLayoutInfo: NSLayoutConstraint!
    @IBOutlet var btnHideInfo: UIButton!
    @IBOutlet var mapView: UIView!
    var gmapView: GMSMapView!
    var vtmapView: MGLMapView!
    var vtMap : MGLMapView!
    var currentIndex = 1
    var timer: Timer?
    var vtStartPointMotor = MGLPointAnnotation()
     var vtEndPointMotor = MGLPointAnnotation()
    var shapeLayer : MGLSymbolStyleLayer!
    var shapeSource : MGLShapeSource?
    var polylineSource: MGLShapeSource!
    let finishFlag:UIImage! = UIImage(named: "FinishFlag")
    let startFlag:UIImage! = UIImage(named: "StartFlag")
    var vtMarkerForMotor = MGLPointAnnotation()
    @IBOutlet var titleMapView: UIView!
    
    @IBOutlet var btnChangeMapView: UIButton!
    @IBOutlet var btnHistoryMode: UIButton!
//    @IBOutlet weak var btnSetting: UIButton!
    @IBOutlet var btnMyLocation: UIButton!
    
    @IBOutlet var btnLock: UIButton!
    @IBOutlet var btnUnlock: UIButton!
    @IBOutlet var btnCall2Vehicle: UIButton!
    
    @IBOutlet var lblVehicleAdress: UILabel!
    @IBOutlet var lblVehicleTimeState: UILabel!
    
    @IBOutlet var lblVehicleStatus: PaddingLabel!
    @IBOutlet var btnSelectedDateStartSetTitle: UITextField!
    @IBOutlet var btnSelectedStartTimeSetTitle: UITextField!
    @IBOutlet var btnSelectedDateEndSetTitle: UITextField!
    @IBOutlet var btnSelectedEndTimeSetTitle: UITextField!
//    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet var sliderConfig: UISlider!
    @IBOutlet var lblDatePlay: UILabel!
    @IBOutlet var lblTimePlay: UILabel!
    @IBOutlet var lblSpeedPlay: UILabel!
    @IBOutlet var btnPlayHistoryTrack: UIButton!
    @IBOutlet var lblRegNoTitle: UILabel!
//    @IBOutlet weak var btnShowHideButton: UIButton!
    @IBOutlet var btnSpeedPlay: UIButton!
    @IBOutlet var btnChangeGmapType: UIButton!
    
    @IBOutlet weak var btnChangeVtMap: UIButton!
    //    @IBOutlet weak var settingContainerView: UIView!
//    @IBOutlet weak var settingContainerViewHeightContraint: NSLayoutConstraint!
    @IBOutlet var lblButtonSetting_Journey: UILabel!
//    @IBOutlet weak var lblButtonSetting_Setting: UILabel!
    @IBOutlet var lblButtonSetting_Map: UILabel!
    @IBOutlet var lblButtonSetting_MyLocation: UILabel!
    
    @IBOutlet var lockUnlockContainerView: UIView!
    @IBOutlet var lblButtonControl_Lock: UILabel!
    @IBOutlet var lblButtonControl_UnLock: UILabel!
    @IBOutlet var lblButtonControl_Call: UILabel!
    
    @IBOutlet var viewContainerUnlockBtn: UIView!
    @IBOutlet var viewContainerLockBtn: UIView!
    @IBOutlet var btnControlLockUnlock: UIButton!
    
    @IBOutlet var confirmSendLockView: UIView!
    @IBOutlet var btnSendBySMS_ConfirmSendLock: UIButton!
    @IBOutlet var btnSendByGPRS_ConfirmSendLock: UIButton!
    @IBOutlet var confirmSendUnlockView: UIView!
    @IBOutlet var btnSendBySMS_ConfirmSendUnLock: UIButton!
    @IBOutlet var btnSendByGPRS_ConfirmSendUnLock: UIButton!
    @IBOutlet var confirmCallVehicleView: UIView!
    @IBOutlet var sliderView: UIView!
    @IBOutlet var datePickerView: UIView!
    @IBOutlet var warningAllowAccessLocationView: UIView!
    @IBOutlet var warningWrongLocationView: UIView!
    
    // MARK: Custom view button benzel
    
    var selectBenzel = true
    @IBOutlet var selectDateTimeView: UIView!
    var btnViewDate: ButtonBenzel!
    var bolViewDate = false
    @IBOutlet var viewButton: UIView!
    @IBOutlet var pickerTime: UIDatePicker!
    @IBOutlet var pickerDate: UIDatePicker!
    
    @IBOutlet var lbTimeBenzel1: UILabel!
    @IBOutlet var lbTimeBenzel2: UILabel!
    @IBOutlet var lbDateBenzel1: UILabel!
    @IBOutlet var lbDateBenzel2: UILabel!
    //
    
    @IBOutlet var lblTitleSelectDateTime: UILabel!
    @IBOutlet var lblContent01SelectDateTime: UILabel!
    @IBOutlet var lblContent02SelectDateTime: UILabel!
    @IBOutlet var btnConfirmLoadHistoryTrack: UIButton!
    @IBOutlet var btnCancelLoadHistoryTrack: UIButton!
    
    @IBOutlet var lblTitleWarningAllowAccessLocation: UILabel!
    @IBOutlet var lblContentWarningAllowAccessLocation: UILabel!
    @IBOutlet var btnConfirmWarningAllowAccessLocation: UIButton!
    @IBOutlet var btnCancelWarningAllowAccessLocation: UIButton!
    
    @IBOutlet var lblTitleWarningWrongLocation: UILabel!
    @IBOutlet var lblContentWarningWrongLocation: UILabel!
    @IBOutlet var btnOkWarningWrongLocation: UIButton!
    @IBOutlet var btnCancelWarningWrongLocation: UIButton!
    
    @IBOutlet var lblTitleConfirmLock: UILabel!
    @IBOutlet var lblContentConfirmLock: UILabel!
    @IBOutlet var btnOKConfirmLock: UIButton!
    @IBOutlet var btnCancelConfirmLock: UIButton!
    
    @IBOutlet var lblTitleConfirmUnLock: UILabel!
    @IBOutlet var lblContentConfirmUnLock: UILabel!
    @IBOutlet var btnOKConfirmUnLock: UIButton!
    @IBOutlet var btnCancelConfirmUnLock: UIButton!
    
    @IBOutlet var lblTitleConfirmCall2Vehicle: UILabel!
    @IBOutlet var lblContentConfirmCall2Vehicle: UILabel!
    @IBOutlet var btnOKConfirmCall2Vehicle: UIButton!
    @IBOutlet var btnCancelConfirmCall2Vehicle: UIButton!
    
    @IBOutlet var lblShowDetailPoint: UILabel!
    @IBOutlet var detailPointView: UIView!
    
    // MARK: Route TableView
    
    var pickerData: [String] = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"]
    var pickerData2: [String] = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"]
    
    @IBOutlet var pickerNumber: UIPickerView!
    
    @IBOutlet var viewStDontTap: UIView!
    @IBOutlet var pickerDateRoute: UIDatePicker!
    @IBOutlet var pickerTimeRoute: UIPickerView!
    @IBAction func pickerDateRouteClicked(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        strDateTB = formatter.string(from: pickerDateRoute.date)
        dateStart2SendRequest = formatter.string(from: pickerDateRoute.date)
    }
    
    @IBOutlet var lbBKS: UILabel!
    @IBOutlet var btnFindView: UIButton!
    @IBOutlet var btnDetailRoute: UIButton!
    @IBOutlet var btnMenuTB: UIButton!
    @IBOutlet var btnFindTB: UIButton!
    @IBOutlet var btnStatusPark: UIButton!
    @IBOutlet var btnStatusLostGPS: UIButton!
    @IBOutlet var btnStatusHibernate: UIButton!
    @IBOutlet var btnStatusRun: UIButton!
    @IBOutlet var btnStatusStop: UIButton!
    @IBOutlet var btnConfirmRouteTBView: UIButton!
    @IBOutlet var btnCancelRouteTBView: UIButton!
    @IBOutlet var viewSelectStatusRouteTB: UIView!
    @IBOutlet var viewSelectDateRouteTB: UIView!
    @IBOutlet var lbStatusRouteTB: UILabel!
    @IBOutlet var lbTimeRouteTB: UILabel!
    @IBOutlet var routeTableView: UITableView!
    @IBOutlet var detailPointTableView: UITableView!
    @IBOutlet var lblTitleDetailPoint: UILabel!
    @IBOutlet var lblContent01DetailPoint: UILabel!
    @IBOutlet var lblContent02DetailPoint: UILabel!
    @IBOutlet var lblDistanceJourney: UILabel!
    var distanceJourney: Double! = 0
    
    @IBOutlet var viewBG: UIView!
    @IBOutlet var lbBKSinfo: UILabel!
    @IBOutlet var lbInfomationAddress: UILabel!
    @IBOutlet var addressView: UIView!
    @IBOutlet var lblContentAddressView: UILabel!
    
    @IBOutlet var unlockByCallView: UIView!
    @IBOutlet var lblTitleUnlockByCallView: UILabel!
    @IBOutlet var lblContentUnlockByCall: UILabel!
    @IBOutlet var btnConfirmCall2VehicleUnlock: UIButton!
    @IBOutlet var btnCancelCall2VehicleUnlock: UIButton!
    
    @IBOutlet var lockByCallView: UIView!
    @IBOutlet var lblTitleLockByCallView: UILabel!
    @IBOutlet var lblContentLockByCall: UILabel!
    @IBOutlet var btnConfirmCall2VehicleLock: UIButton!
    @IBOutlet var btnCancelCall2VehicleLock: UIButton!
    
    @IBOutlet var lblSmsPromotion: UILabel!
    @IBOutlet var lblBasicBalance: UILabel!
    @IBOutlet var lblGprsFreePromotion: UILabel!
    @IBOutlet var lblMoneyPromotion: UILabel!
    @IBOutlet var lblSmsNgoaiMang: UILabel!
    @IBOutlet var lblTimeCallNgoaiMang: UILabel!
    @IBOutlet var lblTitleAccountInfo: UILabel!
    @IBOutlet var lblContent01AccountInfo: UILabel!
    @IBOutlet var lblContent02AccountInfo: UILabel!
    @IBOutlet var lblContent03AccountInfo: UILabel!
    @IBOutlet var lblContent04AccountInfo: UILabel!
    @IBOutlet var lblContent05AccountInfo: UILabel!
    @IBOutlet var lblContent06AccountInfo: UILabel!
    @IBOutlet var btnCancelAccountInfo: UIButton!
    @IBOutlet var accountInfoView: UIView!
    
    enum parseJson2GetHistoryAnnotationString: String {
        case gpsDate
        case gpsSpeed
        case id
        case lat
        case lng
        case motoSpeed
        case motorState
        case state
        case timeState
    }
    
    let kmPerHourString: String = " Km/h"
    let lockString: String = "LOCK"
    let unLockString: String = "UNLOCK"
    let originDateFormat: String = "yyyy-MM-dd'T'HH:mm:ssZ"
    let convertDateFormat: String = "dd/MM/yyyy HH:mm:ss"
    var dateStart2SendRequest: String!
    var dateEnd2SendRequest: String!
    var timeStart2SendRequest: String!
    var timeEnd2SendRequest: String!
    var timeEnd2ViewDetailPoint: String!
    var vehicle2show_currentAnnotation = CLLocationCoordinate2D()
    var lastAnnotationDisplay: CLLocationCoordinate2D!
    var lastAnnotationForRotation = CLLocationCoordinate2D()
    var allAnnotations: Array = [Station]()
    var annotationVTDisplay: CLLocationCoordinate2D!
    let gmMarkerForMotor = GMSMarker()
//    var vtMarkerForMotor = VMSMarker()
    var timerForVehicleCurrentLocation: Timer!
    var timerForSlider: Timer!
    var timerForSliderRunningFlag: Bool! = false
    var alertController: UIAlertController!
    var playHistoryFlag: Bool! = false
    var viewHistoryModeFlag: Bool = false
    var statusViewFlag: Bool = true
    var flagHideButton: Bool = true
    var sliderValue: Float = 0
    var viewCurrentLocationFlag: Bool! = true
    var dateStart: Date!
    var dateEnd: Date!
    
    var checkInvalidDateToShowHistory: Int!
    var timeValueForSliderPlay: Double!
    var updateTimeValueForSliderPlay: Double!
    var changeGmapTypeFlag: Bool! = false
    var locationManager = CLLocationManager()
    var valueLocation: CLLocationCoordinate2D!
    var selectDateTimeViewHeightOrigin: CGFloat!
    var sendBySmsFlag: Bool = true
    var mutableData: NSMutableData = NSMutableData()
    var currentElementName: NSString = ""
    var markerMyLocation = GMSMarker()
    var selectedStartStopDateString: String!
    var oldAngle: Double!
    var keepAngle: Double!
    var detailViewPointFlag: Bool! = false
    var sliderSpeed: Int! = 1
    var locationDegrees: CLLocationDegrees!
    
    let numberOfMonth: [Int] = [1, 2, 3, 6, 12]
    var monthPay: String! = "1"
    var moneyPay: String! = "30000"
    var loadingDataInListVCFlag: Bool! = false
    var searchVehicleFlag: Bool = false
    var endOfListFlag: Bool = false
    var requestedPhone: String! = ""
    var deviceCode: String!
    var deviceSim: String!
    var monthlyFee: Int! = 0
    var registerNo: String!
    var cookieString: String! = ""
    var session = URLSession()
    var totalOfVehicle: Int! = 0
    var chargeCardCodeViewHeight: CGFloat!
    var payMoneyViewHeight: CGFloat!
    var location = CGPoint(x: 0, y: 0)
    var timerReloadListVehicle: Timer!
    let makersource = "marker-source"
    let styleIndetifier = "marker-style"
    let vtMapForname = "home-symbol"
    let iconMotorStateImage: [UIImage] = [UIImage(named: "ic_motor_no_infor")!,
                                          UIImage(named: "ic_motor_run")!,
                                          UIImage(named: "ic_motor_stop")!,
                                          UIImage(named: "ic_motor_park")!,
                                          UIImage(named: "ic_motor_lost_gps")!,
                                          UIImage(named: "ic_motor_hibernate")!,
                                          UIImage(named: "ic_motor_lost_gprs")!,
                                          UIImage(named: "ic_motor_no_infor")!]
    let iconMotorStateImageVT: [UIImage] = [UIImage(named: "ic_motor_no_infor")!,
                                            UIImage(named: "ic_motor_run")!,
                                            UIImage(named: "ic_motor_stop")!,
                                            UIImage(named: "ic_motor_park")!,
                                            UIImage(named: "ic_motor_lost_gps")!,
                                            UIImage(named: "ic_motor_hibernate")!,
                                            UIImage(named: "ic_motor_lost_gprs")!,
                                            UIImage(named: "ic_motor_no_infor")!]
    
    let reachability = Reachability()!
    var connection: Int = 0
    
    // MARK: Main Viewdidload
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initMapVC()
        initURLSession()
        initTextField()
        getVehicleLocation()
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do {
            try reachability.startNotifier()
        } catch {
            print("noConnection")
        }
        
        pickerNumber.isUserInteractionEnabled = false
        
        setupViewRoute()
        actionViewRouteTable()
    }
    
    var bolRun = true
    var bolStop = true
    var bolHibernate = true
    var bolPark = true
    var bolLostGPS = true
    var bolLostGPRS = true
    var BKS = ""
    
    var strDateTB = ""
    var strTimeTB = ""
    var maxDateTB = ""
    var minDateTB = ""
    var dataTotal = [[Station]]()
    var dataTotalTime = [[Station]]()
    var dataTableRoute = [[Station]]()
    
    var startLocation: CGPoint?
    @IBOutlet var statusView: UIView!
    @IBOutlet var listChargeMoneyView: UIView!
    @IBOutlet var chargeCardCodeContainerView: UIView!
    @IBOutlet var confirmChargeCardCodeView: UIView!
    @IBOutlet var payMoneyContainerView: UIView!
    @IBOutlet var confirmPayMoneyView: UIView!
}
