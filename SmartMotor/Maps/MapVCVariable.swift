//
//  MapVCVariable.swift
//  SmartMotor
//
//  Created by nguyen hoan on 10/5/19.
//  Copyright © 2019 EPR. All rights reserved.
//

import Reachability
import UIKit

extension MapVC {
    func setupViewRoute() {
        btnStatusRun.contentHorizontalAlignment = .left
        btnStatusRun.titleEdgeInsets = .init(top: 0, left: 16, bottom: 0, right: 0)
        btnStatusRun.contentEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
        btnStatusRun.setImage(UIImage(named: "iconChecked"), for: .normal)
        btnStatusRun.imageView?.contentMode = .scaleAspectFit
        btnStatusRun.imageEdgeInsets = .init(top: 6, left: 0, bottom: 6, right: 16)
        
        btnStatusStop.contentHorizontalAlignment = .left
        btnStatusStop.titleEdgeInsets = .init(top: 0, left: 16, bottom: 0, right: 0)
        btnStatusStop.contentEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
        btnStatusStop.setImage(UIImage(named: "iconChecked"), for: .normal)
        btnStatusStop.imageView?.contentMode = .scaleAspectFit
        btnStatusStop.imageEdgeInsets = .init(top: 6, left: 0, bottom: 6, right: 16)
        
        btnStatusPark.contentHorizontalAlignment = .left
        btnStatusPark.titleEdgeInsets = .init(top: 0, left: 16, bottom: 0, right: 0)
        btnStatusPark.contentEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
        btnStatusPark.setImage(UIImage(named: "iconChecked"), for: .normal)
        btnStatusPark.imageView?.contentMode = .scaleAspectFit
        btnStatusPark.imageEdgeInsets = .init(top: 6, left: 0, bottom: 6, right: 16)
        
        btnStatusLostGPS.contentHorizontalAlignment = .left
        btnStatusLostGPS.titleEdgeInsets = .init(top: 0, left: 16, bottom: 0, right: 0)
        btnStatusLostGPS.contentEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
        btnStatusLostGPS.setImage(UIImage(named: "iconChecked"), for: .normal)
        btnStatusLostGPS.imageView?.contentMode = .scaleAspectFit
        btnStatusLostGPS.imageEdgeInsets = .init(top: 6, left: 0, bottom: 6, right: 16)
        
        btnStatusHibernate.contentHorizontalAlignment = .left
        btnStatusHibernate.titleEdgeInsets = .init(top: 0, left: 16, bottom: 0, right: 0)
        btnStatusHibernate.contentEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
        btnStatusHibernate.setImage(UIImage(named: "iconChecked"), for: .normal)
        btnStatusHibernate.imageView?.contentMode = .scaleAspectFit
        btnStatusHibernate.imageEdgeInsets = .init(top: 6, left: 0, bottom: 6, right: 16)
        
        bolRun = true
        bolStop = true
        bolHibernate = true
        bolPark = true
        bolLostGPS = true
        bolLostGPRS = true
    }
    
    func actionViewRouteTable() {
        routeTableView.separatorStyle = .none
        btnFindView.isHidden = true
        btnDetailRoute.isHidden = true
        
        let date = Date()
        pickerDate.maximumDate = date
        
        let tapViewStatus = UITapGestureRecognizer(target: self, action: #selector(viewStatusClicked))
        viewSelectStatusRouteTB.addGestureRecognizer(tapViewStatus)
        let tapViewStatus2 = UITapGestureRecognizer(target: self, action: #selector(viewStatusClicked2))
        viewStDontTap.addGestureRecognizer(tapViewStatus2)
        let taplbStatusTB = UITapGestureRecognizer(target: self, action: #selector(tapLabelStatusTB))
        lbStatusRouteTB.addGestureRecognizer(taplbStatusTB)
        let tapTimeTB = UITapGestureRecognizer(target: self, action: #selector(tapLabelTimeTB))
        lbTimeRouteTB.addGestureRecognizer(tapTimeTB)
        btnCancelRouteTBView.addTarget(self, action: #selector(cancelRouteTBView), for: .touchUpInside)
        btnConfirmRouteTBView.addTarget(self, action: #selector(confirmRouteTBView), for: .touchUpInside)
        btnStatusRun.addTarget(self, action: #selector(statusRunClicked), for: .touchUpInside)
        btnStatusStop.addTarget(self, action: #selector(statusStopClicked), for: .touchUpInside)
        btnStatusPark.addTarget(self, action: #selector(statusParkClicked), for: .touchUpInside)
        btnStatusHibernate.addTarget(self, action: #selector(statusHibernateClicked), for: .touchUpInside)
        btnStatusLostGPS.addTarget(self, action: #selector(statusLostGPSClicked), for: .touchUpInside)
        
        btnDetailRoute.addTarget(self, action: #selector(goDetailRoute), for: .touchUpInside)
        btnFindView.addTarget(self, action: #selector(goFindView), for: .touchUpInside)
        btnMenuTB.addTarget(self, action: #selector(closeHistoryMapview), for: .touchUpInside)
        btnFindTB.addTarget(self, action: #selector(goFindView), for: .touchUpInside)
    }
    
    @objc func cancelRouteTBView() {
        viewSelectDateRouteTB.isHidden = true
    }
    
    @objc func confirmRouteTBView() {
        viewSelectDateRouteTB.isHidden = true
        
        let dateStr = strDateTB + " " + strTimeTB
        print(dateStr)
        let dict = groupedByHours(allAnnotations)
        // let dict = self.groupedTripsByDate(self.allAnnotations)
        let dictSort = dict.keys.sorted(by: <)
        var data = [[Station]]()
        dictSort.forEach { key in
            let values = dict[key]
            data.append(values ?? [])
        }
        
        for item in data {
            for i in item {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                let date = dateFormatter.date(from: i.gpsDate)!
                // dateFormatter.dateFormat = "dd-MM-yyyy"
                dateFormatter.dateFormat = "dd/MM/yyyy HH"
                let time = dateFormatter.string(from: date)
                if dateStr == time {
                    dataTotalTime.removeAll()
                    dataTotalTime.append(item)
                    filterByStatus()
                    routeTableView.reloadData()
                    return
                }
            }
        }
        dataTotalTime.removeAll()
        filterByStatus()
        routeTableView.reloadData()
    }
    
    // filter time
    @objc func tapLabelTimeTB(sender: UITapGestureRecognizer) {
        viewSelectDateRouteTB.isHidden = false
        // setDatePickerTBFilter()
    }
    
    func setDatePickerTBFilter() {
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "dd/MM/yyyy"
        let someDate2 = dateFormatter2.date(from: minDateTB)!
        let timeInterval2 = someDate2.timeIntervalSince1970
        let datet2 = Date(timeIntervalSince1970: timeInterval2)
        var componentTime2 = Calendar.current.dateComponents([.day, .month, .year], from: datet2)
        let dates2 = Calendar.current.date(from: componentTime2)!
        pickerDateRoute.setDate(dates2, animated: true)
        pickerDateRoute.minimumDate = dates2
        
        if maxDateTB != minDateTB {
            componentTime2.day = componentTime2.day! + 1
        }
        let maxDate = Calendar.current.date(from: componentTime2)!
        pickerDateRoute.maximumDate = maxDate
    }
    
    // filter status
    @objc func tapLabelStatusTB(sender: UIGestureRecognizer) {
        viewSelectStatusRouteTB.isHidden = false
    }
    
    @objc func viewStatusClicked(sender: UITapGestureRecognizer) {
        viewSelectStatusRouteTB.isHidden = true
        filterByStatus()
    }
    
    @objc func viewStatusClicked2(recognizer: UITapGestureRecognizer) {
        // Dont tap
    }
    
    // 1 chạy 2 dừng 3 dỗ 4 mất gps 5 ngủ đông 6 mất GPRS
    @objc func statusRunClicked() {
        if bolRun {
            btnStatusRun.setImage(UIImage(named: "iconUncheck"), for: .normal)
        } else {
            btnStatusRun.setImage(UIImage(named: "iconChecked"), for: .normal)
        }
        bolRun = !bolRun
    }
    
    @objc func statusStopClicked() {
        if bolStop {
            btnStatusStop.setImage(UIImage(named: "iconUncheck"), for: .normal)
        } else {
            btnStatusStop.setImage(UIImage(named: "iconChecked"), for: .normal)
        }
        bolStop = !bolStop
    }
    
    @objc func statusHibernateClicked() {
        if bolHibernate {
            btnStatusHibernate.setImage(UIImage(named: "iconUncheck"), for: .normal)
        } else {
            btnStatusHibernate.setImage(UIImage(named: "iconChecked"), for: .normal)
        }
        bolHibernate = !bolHibernate
    }
    
    @objc func statusLostGPSClicked() {
        if bolLostGPS {
            btnStatusLostGPS.setImage(UIImage(named: "iconUncheck"), for: .normal)
        } else {
            btnStatusLostGPS.setImage(UIImage(named: "iconChecked"), for: .normal)
        }
        bolLostGPS = !bolLostGPS
    }
    
    @objc func statusParkClicked() {
        if bolPark {
            btnStatusPark.setImage(UIImage(named: "iconUncheck"), for: .normal)
        } else {
            btnStatusPark.setImage(UIImage(named: "iconChecked"), for: .normal)
        }
        bolPark = !bolPark
    }
    
    // 1 chạy 2 dừng 3 dỗ 4 mất gps 5 ngủ đông 6 mất GPRS
    func filterByStatus() {
        print(bolHibernate)
        dataTableRoute = dataTotalTime
        if !bolRun {
            dataTableRoute = [dataTableRoute.joined().filter { $0.state != 1 }]
        }
        if !bolStop {
            dataTableRoute = [dataTableRoute.joined().filter { $0.state != 2 }]
        }
        if !bolPark {
            dataTableRoute = [dataTableRoute.joined().filter { $0.state != 3 }]
        }
        if !bolLostGPS {
            dataTableRoute = [dataTableRoute.joined().filter { $0.state != 4 }]
        }
        if !bolHibernate {
            dataTableRoute = [dataTableRoute.joined().filter { $0.state != 5 }]
        }
        
//        if !bolLostGPRS {
//            if itm.state == 6 {
//                item.remove(at: item.index(of: itm)!)
//            }
//        }
        
        routeTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .wifi:
            connection = 1
        case .cellular:
            connection = 2
        default:
            connection = 0
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initMapVC() {
        smsSettingContext = ""
        viewHistoryModeFlag = false
        playHistoryFlag = false
        hideMainButton(false)
        timerReloadCurrentLocationRun(true)
        saveCurrentLocation()
        saveLastLocation()
        initMap()
//        displayLockUnlockContainerView()
        btnChangeGmapType.isHidden = true
        btnChangeGmapType.setImage(UIImage(named: "gmap02"), for: .normal)
        viewInfo.drawShadow(offset: CGSize(width: 0, height: 3))
        btnHideInfo.addTarget(self, action: #selector(showHideInfo), for: .touchUpInside)
        viewInfoContainer.isUserInteractionEnabled = true
        shadowOnviewWithcornerRadius(customView: viewInfoContainer, cornerRadius: 30)
        shadowOnviewWithcornerRadius(customView: viewBG, cornerRadius: 30)
        btnHideInfo.drawShadow(offset: CGSize(width: 0, height: -3))
        let pangesture = UIPanGestureRecognizer(target: self, action: #selector(swipeInfo))
        viewInfoContainer.addGestureRecognizer(pangesture)
        let pangesture1 = UIPanGestureRecognizer(target: self, action: #selector(swipeInfo))
        btnHideInfo.addGestureRecognizer(pangesture1)
        
        lblVehicleAdress.add(Border: .bottom, withColor: .lightGray, andHeight: 0.5)
        
        if widthScreen < 350 {
            lblVehicleAdress.font = UIFont.systemFont(ofSize: 13)
            lblVehicleTimeState.font = UIFont.systemFont(ofSize: 13)
            lblVehicleStatus.font = UIFont.systemFont(ofSize: 13)
        }
        btnMyLocation.setImage(UIImage(named: "location_highlight"), for: .highlighted)
    }
    
    @objc func swipeInfo(_ sender: UIPanGestureRecognizer) {
        let currentLocation = sender.location(in: viewInfoContainer)
        if sender.state == .began {
            startLocation = currentLocation
        }
        if sender.state == .changed {
            let dy = currentLocation.y - startLocation!.y
            if (bottomLayoutInfo.constant - dy) <= -90 {
                UIView.animate(withDuration: 0.01) {
                    self.bottomLayoutInfo.constant -= dy
                    self.view.layoutIfNeeded()
                }
            }
        }
        if sender.state == .ended {
            let distance = sender.translation(in: viewInfoContainer)
            
            if distance.y > 0 {
                UIView.animate(withDuration: 0.3) {
                    self.bottomLayoutInfo.constant = -295
                    self.btnHideInfo.setImage(UIImage(named: "ic_up"), for: .normal)
                    self.infoShow = false
                    self.view.layoutIfNeeded()
                }
            } else {
                UIView.animate(withDuration: 0.3) {
                    self.bottomLayoutInfo.constant = -90
                    self.btnHideInfo.setImage(UIImage(named: "ic_down"), for: .normal)
                    self.infoShow = true
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    @objc func showHideInfo() {
        if infoShow {
            btnHideInfo.setImage(UIImage(named: "ic_up"), for: .normal)
        } else {
            btnHideInfo.setImage(UIImage(named: "ic_down"), for: .normal)
        }
        
        UIView.animate(withDuration: 0.5) {
            if self.infoShow {
                self.bottomLayoutInfo.constant = -295
            } else {
                self.bottomLayoutInfo.constant = -90
            }
            self.view.layoutIfNeeded()
        }
        
        infoShow = !infoShow
    }
    
    func initMap() {
        vtMapDisplayFlag = (mapTypeSave == "VTMap") ? true:false
//        vtMapDisplayFlag = false
        if vtMapDisplayFlag == true {
            displayVTMap()
            hideMainButton(false)
            getAddressFromVTMap(vehicle2show_currentAnnotation)
        } else {
            displayGoogleMap()
            hideMainButton(false)
            getAddressFromGmap(vehicle2show_currentAnnotation)
        }
        displayVehicleCurrentLocation(withZoomIn: true)
//        displayStatusView()
    }
    
    // MARK: Back click
    
    @IBAction func btnBackToListVehicleTapped(_ sender: AnyObject) {
        if viewCurrentLocationFlag == false {
            showHideInfo()
            sliderConfig.value = 0
            sliderValue = 0
            playHistoryFlag = false
            timerForAutoSlider(false)
            sliderSpeed = 1
            btnSpeedPlay.setImage(#imageLiteral(resourceName: "Speedx1"), for: .normal)
            btnPlayHistoryTrack.setImage(#imageLiteral(resourceName: "btn_resume_media"), for: .normal)
            removeSliderView()
            removeAddressView()
            
            backToViewCurrentLocation()
            return
        }
        timerReloadCurrentLocationRun(false)
        loginFlag = false
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - STATUS VIEW
    
    func displayStatusView() {
        statusView.frame = CGRect(x: 0, y: view.frame.size.height - 80 - lockUnlockContainerView.frame.height, width: view.frame.size.width, height: 80)
        view.addSubview(statusView)
    }
    
    func removeStatusView() {
        // statusView.removeFromSuperview()
        viewInfoContainer.isHidden = true
        btnHideInfo.isHidden = true
    }
    
    //
    
    func clearEverythingOnMap() {
        closeSlider()
        if vtMapDisplayFlag == true {
//            vtmapView.clear()
            return
        }
        gmapView.clear()
    }
    
    func backToViewCurrentLocation() {
        if (vtMapDisplayFlag == false){
            gmapView.clear()
            btnFindView.isHidden = true
            btnDetailRoute.isHidden = true
            
            viewCurrentLocationFlag = true
            getVehicleLocation()
            initMapVC()
            viewInfo.isHidden = false
            viewInfoContainer.isHidden = false
            btnHideInfo.isHidden = false
            btnMyLocation.isHidden = false
            timerReloadCurrentLocationRun(true)
        }else{
            btnFindView.isHidden = true
            btnDetailRoute.isHidden = true
            viewCurrentLocationFlag = true
            getVehicleLocation()
            initMapVC()
            viewInfo.isHidden = false
            viewInfoContainer.isHidden = false
            btnHideInfo.isHidden = false
            btnMyLocation.isHidden = false
            timerReloadCurrentLocationRun(true)
        }
   
    }
    
    func gotoListVehicleView() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "listVehicle") as! ListVehicleVC
        // nextViewController.modalTransitionStyle = .crossDissolve
        timerReloadCurrentLocationRun(false)
        loginFlag = false
        nextViewController.modalPresentationStyle = .fullScreen
        present(nextViewController, animated: false, completion: nil)
    }
    
    func gotoSettingView() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "setting") as! SettingVC
        nextViewController.modalTransitionStyle = .coverVertical
        nextViewController.modalPresentationStyle = .fullScreen
        present(nextViewController, animated: true, completion: nil)
    }
    
    // MARK: Alert custom
    
    func alertCustom(message: String, delay: Double) {
        AlertCustom().Alert(delegate: self, message: message, textAlignment: "", delay: delay)
    }
    
    //
    func setColorInLabel(label: UILabel, string: String, fontName: String, fontSize: CGFloat, color: UIColor) {
        label.textColor = .white
        label.font = UIFont(name: fontName, size: fontSize)
        label.backgroundColor = color
        label.text = string
    }
    
    // MARK: - Charge money
    
    @IBAction func btnChangeTypePayClicked(_ sender: UIButton) {
        if sender == btnCardCode {
            btnCardCode.add(Border: .top, withColor: #colorLiteral(red: 0, green: 0.3568627451, blue: 0.8274509804, alpha: 1), andHeight: 4)
            for subview in btnPayMoney.subviews {
                if subview.tag == 1000 {
                    subview.removeFromSuperview()
                }
            }
            btnCardCode.backgroundColor = .white
            btnPayMoney.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
            btnCardCode.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
            btnPayMoney.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            displayChargeCardCodeContainerView()
        } else {
            btnPayMoney.add(Border: .top, withColor: #colorLiteral(red: 0, green: 0.3568627451, blue: 0.8274509804, alpha: 1), andHeight: 4)
            for subview in btnCardCode.subviews {
                if subview.tag == 1000 {
                    subview.removeFromSuperview()
                }
            }
            btnPayMoney.backgroundColor = .white
            btnCardCode.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
            btnPayMoney.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
            btnCardCode.titleLabel?.font = UIFont.systemFont(ofSize: 15)
            
            displayPayMoneyContainerView()
        }
    }
    
    func displayListChargeMoneyView() {
        listChargeMoneyView.frame = CGRect(x: 0, y: view.frame.size.height, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(listChargeMoneyView)
        btnCardCode.add(Border: .top, withColor: #colorLiteral(red: 0, green: 0.3568627451, blue: 0.8274509804, alpha: 1), andHeight: 4)
        for subview in btnPayMoney.subviews {
            if subview.tag == 1000 {
                subview.removeFromSuperview()
            }
        }
        btnCardCode.backgroundColor = .white
        btnPayMoney.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        btnCardCode.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        btnPayMoney.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        
        displayChargeCardCodeContainerView()
        
        UIView.animate(withDuration: 0.5, animations: {
            self.listChargeMoneyView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        })
    }
    
    func removeListChargeMoneyView() {
        listChargeMoneyView.removeFromSuperview()
    }
    
    @IBAction func closeListChargeMoneyView(_ sender: Any) {
        listChargeMoneyView.removeFromSuperview()
    }
    
    @IBAction func btnCloseListChargeMoneyTypeTapped(_ sender: Any) {
        var second: Double = 0.5
        view.endEditing(true)
        if txtChargeCode.isFirstResponder == true || txtCaptchaCardCode.isFirstResponder == true {
            second = 0.5
        }
        UIView.animate(withDuration: second, animations: {
            self.listChargeMoneyView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height)
            self.txtChargeCode.text = ""
            self.txtCaptchaCardCode.text = ""
        })
        DispatchQueue.main.asyncAfter(deadline: .now() + second) {
            self.removeListChargeMoneyView()
        }
    }
    
    @IBAction func chargeMoneyTapped(_ sender: Any) {
        if idleFlag == false {
            return
        }
        idleFlag = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            idleFlag = true
        }
        displayListChargeMoneyView()
    }
    
    // MARK: - Charge Card Code View
    
    func displayChargeCardCodeContainerView() {
        payMoneyContainerView.removeFromSuperview()
        viewContainerChargeMoney.addSubview(chargeCardCodeContainerView)
        chargeCardCodeContainerView.fillSuperview()
        
        imgRefressCaptchaChargeCode.isUserInteractionEnabled = true
        imgRefressCaptchaChargeCode.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(urlSessionDelegate)))
        lblErrorCardCodeCard.isHidden = true
        lblErrorCardCodeVerify.isHidden = true
        
        btnConfirmChargeCardCode.drawShadow(offset: CGSize(width: 0, height: 3))
        btnCancelChargeCardcode.drawShadow(offset: CGSize(width: 0, height: 3))
        urlSessionDelegate()
    }
    
    func removeChargeCardCodeContainerView() {
        chargeCardCodeContainerView.removeFromSuperview()
    }
    
    @IBAction func closeChargeCardCodeContainerView(_ sender: Any) {
        chargeCardCodeContainerView.removeFromSuperview()
    }
    
    //
    
    // MARK: - Confirm Charge Card Code, thẻ đt
    
    func displayConfirmChargeCardCodeView() {
        confirmChargeCardCodeView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(confirmChargeCardCodeView)
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        let attrs1 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17), NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: style]
        let attrs2 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor.black]
        let attributedString1 = NSMutableAttributedString(string: "Nạp tiền qua thẻ cào cho số điện thoại ", attributes: attrs1)
        let attributedString2 = NSMutableAttributedString(string: vehicle2show.sim, attributes: attrs2)
        let attributedString3 = NSMutableAttributedString(string: "?", attributes: attrs1)
        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        
        lblContentConfirmChargeCardCode.attributedText = attributedString1
    }
    
    func removeConfirmChargeCardCodeView() {
        confirmChargeCardCodeView.removeFromSuperview()
    }
    
    @IBAction func closeConfirmChargeCardCodeView(_ sender: Any) {
        confirmChargeCardCodeView.removeFromSuperview()
    }
    
    // MARK: - Pay money View
    
    func displayPayMoneyContainerView() {
        chargeCardCodeContainerView.removeFromSuperview()
        viewContainerChargeMoney.addSubview(payMoneyContainerView)
        payMoneyContainerView.fillSuperview()
        
        btn1Month.imageView?.contentMode = .scaleAspectFit
        btn2Month.imageView?.contentMode = .scaleAspectFit
        btn3Month.imageView?.contentMode = .scaleAspectFit
        btn6Month.imageView?.contentMode = .scaleAspectFit
        
        imgRefressCaptchaPayMoney.isUserInteractionEnabled = true
        imgRefressCaptchaPayMoney.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(urlSessionDelegate)))
        btnConfirmPayMoney.drawShadow(offset: CGSize(width: 0, height: 3))
        btnCancelPayMoney.drawShadow(offset: CGSize(width: 0, height: 3))
        
        lblErrorPayMoneyCaptcha.isHidden = true
        if connection == 1 {
            showBannerNotifi(text: txtAlert01PayMoney[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
            // alertCustom(message: txtAlert01PayMoney[languageChooseSave], delay: 3)
            return
        } else if connection == 0 {
            // alertCustom(message: txtAlert02PayMoney[languageChooseSave], delay: 3)
            showBannerNotifi(text: "Lỗi kết nối mạng. Vui lòng thử lại sau!", background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!)
            return
        }
        txtCaptchaPayMoney.text = ""
        getIPAddress()
        urlSessionDelegate()
    }
    
    func moneyPayAttributedString(money: String) -> NSMutableAttributedString {
        let attrs1 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17), NSAttributedString.Key.foregroundColor: UIColor.black]
        let attrs2 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor.black]
        let attributedString1 = NSMutableAttributedString(string: money, attributes: attrs2)
        let attributedString2 = NSMutableAttributedString(string: " (đồng)", attributes: attrs1)
        
        attributedString1.append(attributedString2)
        return attributedString1
    }
    
    func borderGrayColor(button: UIButton) {
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.borderWidth = 0.5
    }
    
    @IBAction func btnChooseMonthTapped(_ sender: UIButton) {
        switch sender {
        case btn1Month:
            btn1Month.setImage(UIImage(named: "tick_icon"), for: .normal)
            btn2Month.setImage(UIImage(named: "tick_icon_clear"), for: .normal)
            btn3Month.setImage(UIImage(named: "tick_icon_clear"), for: .normal)
            btn6Month.setImage(UIImage(named: "tick_icon_clear"), for: .normal)
            btn1Month.layer.borderColor = colorBlueApp.cgColor
            btn1Month.layer.borderWidth = 4
            borderGrayColor(button: btn2Month)
            borderGrayColor(button: btn3Month)
            borderGrayColor(button: btn6Month)
            
            btn1Month.setTitleColor(colorBlueApp, for: .normal)
            btn2Month.setTitleColor(UIColor.black, for: .normal)
            btn3Month.setTitleColor(UIColor.black, for: .normal)
            btn6Month.setTitleColor(UIColor.black, for: .normal)
            
            monthPay = "1"
            let money = monthlyFee * 1
            moneyPay = String(money)
            txtMoneyPay.attributedText = moneyPayAttributedString(money: money.stringWithSepator)
            
        case btn2Month:
            btn2Month.setImage(UIImage(named: "tick_icon"), for: .normal)
            btn1Month.setImage(UIImage(named: "tick_icon_clear"), for: .normal)
            btn3Month.setImage(UIImage(named: "tick_icon_clear"), for: .normal)
            btn6Month.setImage(UIImage(named: "tick_icon_clear"), for: .normal)
            btn2Month.layer.borderColor = colorBlueApp.cgColor
            btn2Month.layer.borderWidth = 4
            borderGrayColor(button: btn1Month)
            borderGrayColor(button: btn3Month)
            borderGrayColor(button: btn6Month)
            
            btn2Month.setTitleColor(colorBlueApp, for: .normal)
            btn1Month.setTitleColor(UIColor.black, for: .normal)
            btn3Month.setTitleColor(UIColor.black, for: .normal)
            btn6Month.setTitleColor(UIColor.black, for: .normal)
            
            monthPay = "2"
            let money = monthlyFee * 2
            moneyPay = String(money)
            txtMoneyPay.attributedText = moneyPayAttributedString(money: money.stringWithSepator)
            
        case btn3Month:
            btn3Month.setImage(UIImage(named: "tick_icon"), for: .normal)
            btn2Month.setImage(UIImage(named: "tick_icon_clear"), for: .normal)
            btn1Month.setImage(UIImage(named: "tick_icon_clear"), for: .normal)
            btn6Month.setImage(UIImage(named: "tick_icon_clear"), for: .normal)
            btn3Month.layer.borderColor = colorBlueApp.cgColor
            btn3Month.layer.borderWidth = 4
            borderGrayColor(button: btn2Month)
            borderGrayColor(button: btn1Month)
            borderGrayColor(button: btn6Month)
            
            btn3Month.setTitleColor(colorBlueApp, for: .normal)
            btn2Month.setTitleColor(UIColor.black, for: .normal)
            btn1Month.setTitleColor(UIColor.black, for: .normal)
            btn6Month.setTitleColor(UIColor.black, for: .normal)
            
            monthPay = "3"
            let money = monthlyFee * 3
            moneyPay = String(money)
            txtMoneyPay.attributedText = moneyPayAttributedString(money: money.stringWithSepator)
            
        case btn6Month:
            btn6Month.setImage(UIImage(named: "tick_icon"), for: .normal)
            btn2Month.setImage(UIImage(named: "tick_icon_clear"), for: .normal)
            btn3Month.setImage(UIImage(named: "tick_icon_clear"), for: .normal)
            btn1Month.setImage(UIImage(named: "tick_icon_clear"), for: .normal)
            btn6Month.layer.borderColor = colorBlueApp.cgColor
            btn6Month.layer.borderWidth = 4
            borderGrayColor(button: btn2Month)
            borderGrayColor(button: btn3Month)
            borderGrayColor(button: btn1Month)
            
            btn6Month.setTitleColor(colorBlueApp, for: .normal)
            btn2Month.setTitleColor(UIColor.black, for: .normal)
            btn3Month.setTitleColor(UIColor.black, for: .normal)
            btn1Month.setTitleColor(UIColor.black, for: .normal)
            
            monthPay = "6"
            let money = monthlyFee * 6
            moneyPay = String(money)
            txtMoneyPay.attributedText = moneyPayAttributedString(money: money.stringWithSepator)
        default:
            break
        }
    }
    
    func removePayMoneyContainerView() {
        payMoneyContainerView.removeFromSuperview()
    }
    
    @IBAction func closePayMoneyContainerView(_ sender: Any) {
        payMoneyContainerView.removeFromSuperview()
    }
    
    @IBAction func btnChooseNumberOfMonth2PayTapped(_ sender: Any) {
        displayChooseNumberOfMonthToPayView()
    }
    
    //
    
    // MARK: Choose number of month to pay
    
    func displayChooseNumberOfMonthToPayView() {
        UIView.animate(withDuration: 0.5) {
            self.heightTbvMonthConstraint.constant = 200
            self.view.layoutIfNeeded()
        }
    }
    
    func removeChooseNumberOfMonthToPayView() {
        UIView.animate(withDuration: 0.5) {
            self.heightTbvMonthConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: Confirm Pay Money
    
    func displayConfirmPayMoneyView() {
        confirmPayMoneyView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(confirmPayMoneyView)
        
        let attrs1 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17), NSAttributedString.Key.foregroundColor: UIColor.black]
        let attrs2 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor.black]
        let attributedString1 = NSMutableAttributedString(string: "Cộng ", attributes: attrs1)
        let attributedString2 = NSMutableAttributedString(string: monthlyFee.stringWithSepator + "đ", attributes: attrs2)
        let attributedString3 = NSMutableAttributedString(string: " vào tài khoản SIM thiết bị ", attributes: attrs1)
        let attributedString4 = NSMutableAttributedString(string: vehicle2show.sim, attributes: attrs2)
        let attributedString5 = NSMutableAttributedString(string: ", xe \(vehicle2show.regNo!).\n\n", attributes: attrs1)
        let attributedString6 = NSMutableAttributedString(string: "Trừ ", attributes: attrs1)
        let attributedString7 = NSMutableAttributedString(string: monthlyFee.stringWithSepator + "đ", attributes: attrs2)
        let attributedString8 = NSMutableAttributedString(string: " từ tài khoản chính của thuê bao", attributes: attrs1)
        let attributedString9 = NSMutableAttributedString(string: requestedPhone + ".", attributes: attrs2)
        
        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        attributedString1.append(attributedString4)
        attributedString1.append(attributedString5)
        attributedString1.append(attributedString6)
        attributedString1.append(attributedString7)
        attributedString1.append(attributedString8)
        attributedString1.append(attributedString9)
        
        lblContentConfirmPayMoney.attributedText = attributedString1
    }
    
    func removeConfirmPayMoneyView() {
        confirmPayMoneyView.removeFromSuperview()
    }
    
    @IBAction func closeConfirmPayMoneyView(_ sender: Any) {
        confirmPayMoneyView.removeFromSuperview()
    }
}
