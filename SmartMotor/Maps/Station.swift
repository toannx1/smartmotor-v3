//
//  Station.swift
//  Smart Motor
//
//  Created by vietnv2 on 24/10/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import MapKit
import UIKit

class Station: NSObject, MKAnnotation {
    var gpsDate: String
    var gpsSpeed: Double
    var id: Int
    var lat: Double
    var lng: Double
    var motoSpeed: Double
    var motorState: Int
    var state: Int
    var timeState: Int

    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: lat, longitude: lng)
    }

    init(gpsDate: String, gpsSpeed: Double, id: Int, lat: Double, lng: Double, motoSpeed: Double, motorState: Int, state: Int, timeState: Int) {
        self.gpsDate = gpsDate
        self.gpsSpeed = gpsSpeed
        self.id = id
        self.lat = lat
        self.lng = lng
        self.motoSpeed = motoSpeed
        self.motorState = motorState
        self.state = state
        self.timeState = timeState
    }
}

struct routeStation: Decodable {
    var gpsDate: String?
    var gpsSpeed: Int?
    var id: Int?
    var lat: Double?
    var lng: Double?
    var motoSpeed: Double?
    var motoState: Int?
    var state: Int?
    var timeState: Int?
}
