//
//  MapVCSMS.swift
//  SmartMotor
//
//  Created by Vietnv2 on 3/11/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import MessageUI
import UIKit

extension MapVC: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }

    func sendMessage(phoneNumber: String, content: String) {
        let messageVC = MFMessageComposeViewController()
        messageVC.messageComposeDelegate = self
        messageVC.body = content
        messageVC.recipients = [phoneNumber]
        self.present(messageVC, animated: false, completion: nil)
    }
}
