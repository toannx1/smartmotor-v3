//
//  MapServer.swift
//  SmartMotor
//
//  Created by Vietnv2 on 3/7/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import CoreLocation
import GoogleMaps
import UIKit

extension MapVC: NSURLConnectionDelegate, XMLParserDelegate {
    func convertDateSend2Service(inputString: String) -> String {
        var converted: String!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        let dateStart = dateFormatter.date(from: inputString)
        let convertDateStartToMinlisecond = Int64((dateStart?.timeIntervalSince1970)! * 1000)
        converted = String(convertDateStartToMinlisecond)
        return converted
    }
    
    // MARK: - get Map annotation
    
    func getMapAnnotations() {
        if vehicle2show.state == 0 {
            return
        }
        Loading().showLoading()
        var annotations: Array = [Station]()
        
        clearEverythingOnMap()
        
        print(dateStart2SendRequest)
        print(dateEnd2SendRequest)
        print(timeStart2SendRequest)
        print(timeEnd2SendRequest)
        
        let convertStartDate = convertDateSend2Service(inputString: dateStart2SendRequest + " " + timeStart2SendRequest)
        let convertEndDate = convertDateSend2Service(inputString: dateEnd2SendRequest + " " + timeEnd2SendRequest)
        let urlString = smartmotorUrl + reviewtransportUrl + userIdSave + "/" + String(vehicle2show.id) + "/" + convertStartDate + "/" + convertEndDate
        let url = URL(string: urlString)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        request.addValue(tokenSave, forHTTPHeaderField: "user-token")
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        //
        let task = defaultSession.dataTask(with: request as URLRequest) { data, response, error in
            Loading().hideLoading(delay: 0)
            if data == nil || response == nil {
                // self.alertCustom(message: txtConnectServerFail[languageChooseSave], delay: 2)
                self.showBannerNotifi(text: txtConnectServerFail[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                self.backToViewCurrentLocation()
                return
            }
            if error != nil {
                // self.alertCustom(message: txtConnectServerFail[languageChooseSave], delay: 2)
                self.showBannerNotifi(text: txtConnectServerFail[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    self.showBannerNotifi(text: txtloadDataUnSuccess[languageChooseSave], background: colorSuccess, textColor: .white, imageTitle: UIImage(named: "checked")!, des: "")
                    // self.alertCustom(message: txtloadDataUnSuccess[languageChooseSave], delay: 2)
                    self.backToViewCurrentLocation()
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let jsonData: NSArray! = jsonResult["data"] as? NSArray
                    if jsonData == nil {
                        // self.alertCustom(message: txtloadDataUnSuccess[languageChooseSave], delay: 2)
                        self.showBannerNotifi(text: txtloadDataUnSuccess[languageChooseSave], background: colorSuccess, textColor: .white, imageTitle: UIImage(named: "checked")!, des: "")
                        self.backToViewCurrentLocation()
                        return
                    }
                    
                    var coordinateFirst: CLLocation!
                    var coordinateSecond: CLLocation!
                    self.distanceJourney = 0
                    for element in jsonData {
                        let result = element as AnyObject
                        let lat = result.value(forKeyPath: "lat") as! Double
                        let lng = result.value(forKeyPath: "lng") as! Double
                        annotations.append(self.parseJson2GetHistoryAnnotation(result: result))
                        if coordinateFirst == nil {
//                            coordinateFirst = VMSLatLng.init(lat: lat, lng: lng)
                            coordinateFirst = CLLocation(latitude: lat, longitude: lng)
                        }
                        if lat != 0, lng != 0 {
//                            coordinateSecond = VMSLatLng.init(lat: lat, lng: lng)
                            coordinateSecond = CLLocation(latitude: lat, longitude: lng)
//                            self.distanceJourney = self.distanceJourney + VMSGeometryUtils.length(fromPoint: coordinateFirst, toPoint: coordinateSecond)
                            self.distanceJourney += coordinateFirst.distance(from: coordinateSecond)
                            coordinateFirst = coordinateSecond
                        }
                    }
                    self.distanceJourney = (self.distanceJourney / 1000)
                    //
                    if annotations.count == 0 {
                        // self.alertCustom(message: txtWrongTimeOrNoDataThisTime[languageChooseSave], delay: 2)
                        self.showBannerNotifi(text: txtWrongTimeOrNoDataThisTime[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                        self.backToViewCurrentLocation()
                        return
                    }
                    self.viewCurrentLocationFlag = false
                    
                    self.allAnnotations.removeAll()
                    self.allAnnotations = annotations
                    self.drawPolyLineOnMap(annotations: annotations)
                    
                    // let dict = self.groupedByDay(self.allAnnotations)
                    let dict = self.groupedTripsByDate(self.allAnnotations)
                    let dictSort = dict.keys.sorted(by: <)
                    self.dataTableRoute.removeAll()
                    var data = [[Station]]()
                    dictSort.forEach { key in
                        let values = dict[key]
                        data.append(values ?? [])
                    }
                    
                    // group by date
                    self.dataTableRoute = data
                    self.dataTotal = data
                    self.dataTotalTime = data
                    
                } catch {
                    // self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
                    self.showBannerNotifi(text: connectionFail[languageChooseSave], background: colorSuccess, textColor: .white, imageTitle: UIImage(named: "checked")!, des: "")
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    // MARK: - Group data
    
    func groupedTripsByDate(_ trips: [Station]) -> [Date: [Station]] {
        let empty: [Date: [Station]] = [:]
        return trips.reduce(into: empty) { acc, cur in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            let someDate = dateFormatter.date(from: cur.gpsDate)!
            let timeInterval = someDate.timeIntervalSince1970
            let datet = Date(timeIntervalSince1970: timeInterval)
            let components = Calendar.current.dateComponents([.year, .month, .day], from: datet)
            let date = Calendar.current.date(from: components)!
            let existing = acc[date] ?? []
            acc[date] = existing + [cur]
        }
    }
    
    func groupedByHours(_ trips: [Station]) -> [Date: [Station]] {
        let empty: [Date: [Station]] = [:]
        return trips.reduce(into: empty) { acc, cur in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            let someDate = dateFormatter.date(from: cur.gpsDate)!
            let timeInterval = someDate.timeIntervalSince1970
            let datet = Date(timeIntervalSince1970: timeInterval)
            let components = Calendar.current.dateComponents([.year, .month, .day, .hour], from: datet)
            let date = Calendar.current.date(from: components)!
            let existing = acc[date] ?? []
            acc[date] = existing + [cur]
        }
    }
    
    // MARK: display marker, vehicle icon
    
    func displayMarkerMotor(_ annotation: CLLocationCoordinate2D, _ img: UIImage) {
        if vtMapDisplayFlag == false {
            gmCustomMarkerForMotor(annotation: annotation, img: img, title: "", subtitle: "")
            return
        }
    }
    
    func displayVehicleCurrentLocation(withZoomIn: Bool) {
        if viewHistoryModeFlag == true {
            return
        }
        if vehicle2show.state < 1 || vehicle2show.state > 6 {
            DispatchQueue.main.async {
//                self.alertCustom(message: txtNoVehicleInfomationString[languageChooseSave], delay: 2)
                self.showBannerNotifi(text: txtNoVehicleInfomationString[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
            }
            return
        }
        if vtMapDisplayFlag == false {
            if gmapView == nil {
                displayGoogleMap()
            }
            // displayMarkerMotor(vehicle2show_currentAnnotation, #imageLiteral(resourceName: "icon motobike"))
            displayMarkerMotor(vehicle2show_currentAnnotation, iconMotorStateImage[vehicle2show.state])
            gmapView.camera = GMSCameraPosition.camera(withLatitude: vehicle2show_currentAnnotation.latitude, longitude: vehicle2show_currentAnnotation.longitude, zoom: gmapView.camera.zoom)
            if vehicle2show_currentAnnotation.latitude == lastAnnotationForRotation.latitude, vehicle2show_currentAnnotation.longitude == lastAnnotationForRotation.longitude {
                return
            }
            gmMarkerForMotor.rotation = getBearingFrom(firstLocation: lastAnnotationForRotation, secondLocation: vehicle2show_currentAnnotation)
            // gmMarkerForMotor.rotation = CalculateAngle().angle(firstLocation: lastAnnotationForRotation, secondLocation: vehicle2show_currentAnnotation)
            return
        }
        // displayMarkerMotor(vehicle2show_currentAnnotation, #imageLiteral(resourceName: "icon motobike"))
//        displayMarkerMotor(vehicle2show_currentAnnotation, iconMotorStateImageVT[vehicle2show.state])
//        vtmapView.setCenter(VMSLatLng.init(lat: vehicle2show_currentAnnotation.latitude, lng: vehicle2show_currentAnnotation.longitude), refresh: true)
        
//        if vehicle2show_currentAnnotation.latitude == lastAnnotationForRotation.latitude && vehicle2show_currentAnnotation.longitude == lastAnnotationForRotation.longitude {
//            return
//        }
        // let angle = CalculateAngle().angle(firstLocation: lastAnnotationForRotation, secondLocation: vehicle2show_currentAnnotation)
//        let angle = getBearingFrom(firstLocation: lastAnnotationForRotation, secondLocation: vehicle2show_currentAnnotation)
//        self.vtMarkerForMotor?.rotate = Int32(angle)
//        vtmapView.setCenter(VMSLatLng.init(lat: vehicle2show_currentAnnotation.latitude, lng: vehicle2show_currentAnnotation.longitude), refresh: true)
    }
    
    // MARK: - calculate angle marker
    
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
    func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }
    
    func getBearingFrom(firstLocation: CLLocationCoordinate2D, secondLocation: CLLocationCoordinate2D) -> Double {
        let lat1 = degreesToRadians(degrees: firstLocation.latitude)
        let lon1 = degreesToRadians(degrees: firstLocation.longitude)
        
        let lat2 = degreesToRadians(degrees: secondLocation.latitude)
        let lon2 = degreesToRadians(degrees: secondLocation.longitude)
        
        let dLon = lon2 - lon1
        
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
        
        return radiansToDegrees(radians: radiansBearing)
    }
    
    func tapToMapMarkerForDisplayStatusView() {
        if viewHistoryModeFlag == true {
            return
        }
        
        if vtMapDisplayFlag == false {
            getAddressFromGmap(vehicle2show_currentAnnotation)
            return
        }
        getAddressFromVTMap(vehicle2show_currentAnnotation)
    }
    
    // MARK: - get address from map
    
    func getAddressFromGmap(_ annotation: CLLocationCoordinate2D) {
        if vehicle2show.state == 0 {
            return
        }
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(annotation) { response, _ in
            if response == nil {
                DispatchQueue.main.async {
                    self.lblVehicleAdress.text = String(format: txtStatus01[languageChooseSave], txtStatus_DetailPoint_NoInfo[languageChooseSave])
                    self.lblVehicleTimeState.text = String(format: "%@ %@", txtStatus02, txtStatus_DetailPoint_NoInfo[languageChooseSave])
                    self.lblVehicleStatus.text = txtStatus_DetailPoint_NoInfo[languageChooseSave]
                    self.lblVehicleStatus.backgroundColor = UIColor(red: 0x77 / 0xFF, green: 0x1C / 0xFF, blue: 0xF9 / 0xFF, alpha: 1)
                }
                return
            }
            // Add this line
            if let address = response!.firstResult() {
                var address2Label: String! = ""
                let lines = address.lines! as [String]
                for i in 0...(lines.count - 1) {
                    address2Label = address2Label + " " + lines[i] + ","
                }
                
                let attrs1 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.black]
                let attrs2 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor.black]
                let attributedString4 = NSMutableAttributedString(string: "Vị trí hiện tại: ", attributes: attrs1)
                let attributedString1 = NSMutableAttributedString(string: "Vị trí xe: ", attributes: attrs1)
                let attributedString2 = NSMutableAttributedString(string: address2Label, attributes: attrs2)
                let attributedString3 = NSMutableAttributedString(string: " (\(annotation.latitude),\(annotation.longitude))", attributes: attrs1)
                attributedString1.append(attributedString2)
                attributedString1.append(attributedString3)
                
                attributedString4.append(attributedString2)
                attributedString4.append(attributedString3)
                
                self.lblVehicleAdress.attributedText = attributedString4
                self.lbInfomationAddress.attributedText = attributedString1
                
                // self.lblVehicleAdress.text = String(format: txtStatus01[languageChooseSave], address2Label + " (\(annotation.latitude),\(annotation.longitude))")
                
                // display address view
                var string: String!
                var color: UIColor!
                switch vehicle2show.state {
                case 1:
                    string = txtStatus_DetailPoint_Run[languageChooseSave]
                    color = UIColor(red: 0x35 / 0xFF, green: 0x87 / 0xFF, blue: 0x00 / 0xFF, alpha: 1)
                case 2:
                    string = txtStatus_DetailPoint_Stop[languageChooseSave]
                    color = UIColor(red: 0x00 / 0xFF, green: 0x76 / 0xFF, blue: 0xAA / 0xFF, alpha: 1)
                case 3:
                    string = txtStatus_DetailPoint_Park[languageChooseSave]
                    color = UIColor(red: 0xFF / 0xFF, green: 0xB6 / 0xFF, blue: 0x00 / 0xFF, alpha: 1)
                case 4:
                    string = txtStatus_DetailPoint_LossGPS[languageChooseSave]
                    color = UIColor(red: 0xFF / 0xFF, green: 0x6D / 0xFF, blue: 0x00 / 0xFF, alpha: 1)
                case 5:
                    string = txtStatus_DetailPoint_Hibernate[languageChooseSave]
                    color = UIColor(red: 0x51 / 0xFF, green: 0x51 / 0xFF, blue: 0x51 / 0xFF, alpha: 1)
                case 6: //txtStatus03[languageChooseSave]
                    string = txtStatus_DetailPoint_LossGPRS[languageChooseSave]
                    color = UIColor(red: 0xFF / 0xFF, green: 0x17 / 0xFF, blue: 0x17 / 0xFF, alpha: 1)
                default:
                    string = txtStatus_DetailPoint_NoInfo[languageChooseSave]
                    color = UIColor(red: 0x77 / 0xFF, green: 0x1C / 0xFF, blue: 0xF9 / 0xFF, alpha: 1)
                }
                // self.lblVehicleStatus.text = string
                // self.lblVehicleStatus.textColor = color
                if vehicle2show.state > 0, vehicle2show.state <= 6 {
                    if languageChooseSave == 0 {
                        self.setColorInLabel(label: self.lblVehicleStatus, string: string, fontName: "Georgia", fontSize: self.lblVehicleStatus.font.pointSize, color: color)
                    } else {
                        self.setColorInLabel(label: self.lblVehicleStatus, string: string, fontName: "Georgia", fontSize: self.lblVehicleStatus.font.pointSize, color: color)
                    }
                } else {
                    self.setColorInLabel(label: self.lblVehicleStatus, string: string, fontName: "Georgia", fontSize: self.lblVehicleStatus.font.pointSize, color: color)
                }
                //
                if vehicle2show.state == 1 {
                    self.lblVehicleTimeState.text = txtStatus04[languageChooseSave] + String(vehicle2show.motoSpeed) + self.kmPerHourString
                } else {
                    self.lblVehicleTimeState.text = self.calculateVehicleTimeState()
                }
                //
                // self.lbInfomationAddress.text = self.lblVehicleAdress.text // lb thông tin map view
                self.lblContentAddressView.text = self.lblVehicleAdress.text
                self.displayAddressView()
            }
        }
    }
    
    // MARK: - Current Location
    
    func timerReloadCurrentLocationRun(_ reloadFlag: Bool) {
        if reloadFlag == false {
            if timerForVehicleCurrentLocation != nil {
                timerForVehicleCurrentLocation.invalidate()
                timerForVehicleCurrentLocation = nil
            }
            return
        } else {
            if timerForVehicleCurrentLocation != nil {
                return
            }
            timerForVehicleCurrentLocation = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(getVehicleLocation), userInfo: nil, repeats: true)
        }
    }
    
    func saveCurrentLocation() {
        vehicle2show_currentAnnotation.latitude = vehicle2show.lat
        vehicle2show_currentAnnotation.longitude = vehicle2show.lng
    }
    
    func saveLastLocation() {
        lastAnnotationForRotation = vehicle2show_currentAnnotation
    }
    
    // MARK: - get vehicle location
    
    @objc func getVehicleLocation() {
        if vehicle2show.state == 0 {
            lblVehicleAdress.text = String(format: txtStatus01[languageChooseSave], txtUnknownVehicleLocation[languageChooseSave])
            lblContentAddressView.text = lblVehicleAdress.text
            // self.lbInfomationAddress.text = self.lblVehicleAdress.text // lb thông tin view map
            lblVehicleStatus.text = txtStatus_DetailPoint_NoInfo[languageChooseSave]
            lblVehicleStatus.backgroundColor = UIColor(red: 0x77 / 0xFF, green: 0x1C / 0xFF, blue: 0xF9 / 0xFF, alpha: 1)
            lblVehicleTimeState.text = String(format: "Thời gian: 0 %@", txtSecond[languageChooseSave]) // "Thời gian: 0 giây"
            return
        }
        let regName = registerNo2Query.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)! as String
        // let url = URL(string: smartmotorUrl + listTransportUrl + userIdSave + listTransportRegNoUrl + regName + listTransportPageIndexUrl + String(pageIndex))
        let url1 = smartmotorUrl + listTransportUrl + userIdSave + listTransportRegNoUrl
        let url2 = regName + listTransportPageIndexUrl + String(1)
        let url = URL(string: url1 + url2)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        request.addValue(tokenSave, forHTTPHeaderField: "user-token")
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        //
        let task = defaultSession.dataTask(with: request as URLRequest) { data, response, error in
            if data == nil {
                return
            }
            if error != nil {
//                self.alertCustom(message: txtloadDataUnSuccess[languageChooseSave], delay: 2)
                self.showBannerNotifi(text: txtloadDataUnSuccess[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
//                    self.alertCustom(message: txtloadDataUnSuccess[languageChooseSave], delay: 2)
                    self.showBannerNotifi(text: txtloadDataUnSuccess[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)) as! NSDictionary
                    let jsonData: NSArray! = jsonResult["data"] as? NSArray
                    if jsonData == nil {
                        return
                    }
                    self.saveLastLocation()
                    for element in jsonData {
                        let result = element as AnyObject
                        vehicle2show = self.parseJson2GetVehicleLocation(result: result)
                    }
                    self.saveCurrentLocation()
                    self.displayVehicleCurrentLocation(withZoomIn: false)
                    if vtMapDisplayFlag == true {
                        self.getAddressFromVTMap(self.vehicle2show_currentAnnotation)
                        return
                    }
                    self.getAddressFromGmap(self.vehicle2show_currentAnnotation)
                } catch {
//                    self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
                    self.showBannerNotifi(text: connectionFail[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    // MARK: - Parse data
    
    func parseJson2GetHistoryAnnotation(result: AnyObject) -> Station {
        var resultReturn: Station
        let gpsDate = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.gpsDate.rawValue) as! String
        let gpsSpeed = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.gpsSpeed.rawValue) as! Double
        let id = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.id.rawValue) as! Int
        let lat = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.lat.rawValue) as! Double
        let lng = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.lng.rawValue) as! Double
        let motoSpeed = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.motoSpeed.rawValue) as! Double
        let motorState = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.motorState.rawValue) as! Int
        let state = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.state.rawValue) as! Int
        let timeState = result.value(forKeyPath: parseJson2GetHistoryAnnotationString.timeState.rawValue) as! Int
        resultReturn = Station(gpsDate: gpsDate, gpsSpeed: gpsSpeed, id: id, lat: lat, lng: lng, motoSpeed: motoSpeed, motorState: motorState, state: state, timeState: timeState)
        return resultReturn
    }
    
    func parseJson2GetVehicleLocation(result: AnyObject) -> vehicleInformation {
        var resultReturn: vehicleInformation!
        var gpsDate: String! = ""
        var isForbidden: Bool! = false
        let accIllegalState = result.value(forKeyPath: vehicleInfoString.accIllegalState.rawValue) as! Int
        let devicePin = result.value(forKeyPath: vehicleInfoString.devicePin.rawValue) as! String
        let deviceType = result.value(forKeyPath: vehicleInfoString.deviceType.rawValue) as! Int
        let deviceTypeIdOrginal = result.value(forKeyPath: vehicleInfoString.deviceTypeIdOrginal.rawValue) as! Int
        let factory = result.value(forKeyPath: vehicleInfoString.factory.rawValue) as! String
        if let _gpsDate = result.value(forKeyPath: vehicleInfoString.gpsDate.rawValue) as? String {
            gpsDate = _gpsDate
        }
        let gpsSpeed = result.value(forKeyPath: vehicleInfoString.gpsSpeed.rawValue) as! Int
        let gpsState = result.value(forKeyPath: vehicleInfoString.gpsState.rawValue) as! Int
        let groupsCode = result.value(forKeyPath: vehicleInfoString.groupsCode.rawValue) as! String
        let id = result.value(forKeyPath: vehicleInfoString.id.rawValue) as! Int
        let illegalMoveState = result.value(forKeyPath: vehicleInfoString.illegalMoveState.rawValue) as! Int
        let lat = result.value(forKeyPath: vehicleInfoString.lat.rawValue) as! Double
        let lng = result.value(forKeyPath: vehicleInfoString.lng.rawValue) as! Double
        let lowBatteryState = result.value(forKeyPath: vehicleInfoString.lowBatteryState.rawValue) as! Int
        let motoSpeed = result.value(forKeyPath: vehicleInfoString.motoSpeed.rawValue) as! Int
        let offPowerState = result.value(forKeyPath: vehicleInfoString.offPowerState.rawValue) as! Int
        let regNo = result.value(forKeyPath: vehicleInfoString.regNo.rawValue) as! String
        let sim = result.value(forKeyPath: vehicleInfoString.sim.rawValue) as! String
        let sosState = result.value(forKeyPath: vehicleInfoString.sosState.rawValue) as! Int
        let state = result.value(forKeyPath: vehicleInfoString.state.rawValue) as! Int
        let timeState = result.value(forKeyPath: vehicleInfoString.timeState.rawValue) as! Int
        let type = result.value(forKeyPath: vehicleInfoString.type.rawValue) as! String
        let vibrationState = result.value(forKeyPath: vehicleInfoString.vibrationState.rawValue) as! Int
        if let _isForbidden = result.value(forKeyPath: vehicleInfoString.isForbidden.rawValue) as? Bool {
            isForbidden = _isForbidden
        }
        resultReturn = vehicleInformation(accIllegalState: accIllegalState, devicePin: devicePin, deviceType: deviceType, deviceTypeIdOrginal: deviceTypeIdOrginal, factory: factory, gpsDate: gpsDate, gpsSpeed: gpsSpeed, gpsState: gpsState, groupsCode: groupsCode, id: id, illegalMoveState: illegalMoveState, lat: lat, lng: lng, lowBatteryState: lowBatteryState, motoSpeed: motoSpeed, offPowerState: offPowerState, regNo: regNo, sim: sim, sosState: sosState, state: state, timeState: timeState, type: type, vibrationState: vibrationState, isForbidden: isForbidden)
        return resultReturn
    }
    
    func SoapHandShaking(commandIndex: String, inputContent: String) {
        commandControlString = inputContent
        Loading().showLoading()
        let soapMessage = String(format: "<?xml version=\"1.0\" encoding=\"utf-8\"?><Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\"><Body><doCommand xmlns=\"http://mobilegateway.viettel.com/\"><token xmlns=\"\">%@</token><userId xmlns=\"\">%@</userId><transportId xmlns=\"\">%@</transportId><commandIndex xmlns=\"\">%@</commandIndex><inputContent xmlns=\"\">%@</inputContent></doCommand></Body></Envelope>", tokenSave, userIdSave, String(vehicle2show.id), commandIndex, inputContent)
        let urlString = smartmotorUrl + "/mgw/mobilegw"
        let url = URL(string: urlString)
        let theRequest = NSMutableURLRequest(url: url!)
        let msgLength = soapMessage.count
        theRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        theRequest.addValue("requestCommand", forHTTPHeaderField: "SOAPAction")
        theRequest.addValue(String(msgLength), forHTTPHeaderField: "Content-Length")
        theRequest.httpMethod = "POST"
        theRequest.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        theRequest.httpBody = soapMessage.data(using: String.Encoding.utf8, allowLossyConversion: false) // or false
        //
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        //
        let task = defaultSession.dataTask(with: theRequest as URLRequest) { data, response, error in
            if error != nil {
                Loading().hideLoading(delay: 0)
                self.printAlertUnSuccess()
                return
            }
            if data == nil {
                Loading().hideLoading(delay: 0)
                self.printAlertUnSuccess()
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    Loading().hideLoading(delay: 0)
                    self.printAlertUnSuccess()
                    return
                }
            }
            let xmlParser = XMLParser(data: data! as Data)
            xmlParser.delegate = self
            xmlParser.parse()
            xmlParser.shouldResolveExternalEntities = true
        }
        task.resume()
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String: String]) {
        currentElementName = elementName as NSString
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        if String(string) == "1" {
            countSendCommandControl = 0
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5) {
                self.SendCommandControl(inputContent: commandControlString)
            }
        } else {
            Loading().hideLoading(delay: 0)
            printAlertUnSuccess()
        }
    }
    
    func SendCommandControl(inputContent: String) {
        let url = URL(string: smartmotorUrl + "/mtapi/rest/mobile/deviceCommand/getDeviceParam/" + userIdSave + "/" + String(vehicle2show.id) + "/" + inputContent)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(tokenSave, forHTTPHeaderField: "user-token")
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        
        let task = defaultSession.dataTask(with: request as URLRequest) { data, response, error in
            if error != nil {
                Loading().hideLoading(delay: 0)
                self.printAlertUnSuccess()
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    Loading().hideLoading(delay: 0)
                    self.printAlertUnSuccess()
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    // print(jsonResult)
                    let resultCode = jsonResult.value(forKey: "resultCode") as! Int
                    if resultCode != 1 {
                        countSendCommandControl = countSendCommandControl + 1
                        if countSendCommandControl < 3 {
                            // Recursion
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
                                self.SendCommandControl(inputContent: commandControlString)
                            }
                        } else {
                            countSendCommandControl = 0
                            Loading().hideLoading(delay: 0)
                            self.printAlertUnSuccess()
                        }
                        return
                    }
                    let jsonData = jsonResult.value(forKey: "data") as! NSDictionary
                    Loading().hideLoading(delay: 0)
                    let paramName = jsonData.value(forKey: "paramName") as! String
                    if paramName == "LOCK" || paramName == "UNLOCK" {
                        self.printAlertSuccess()
                    }
                } catch {
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    func printAlertSuccess() {
        showBannerNotifi(text: commandControlString + txtControlSuccess[languageChooseSave], background: colorSuccess, textColor: .white, imageTitle: UIImage(named: "checked")!, des: "")
        // alertCustom(message: commandControlString + txtControlSuccess[languageChooseSave], delay: 2)
    }
    
    func printAlertUnSuccess() {
        showBannerNotifi(text: commandControlString + txtControlUnSuccess[languageChooseSave], background: colorSuccess, textColor: .white, imageTitle: UIImage(named: "checked")!, des: "")
        // alertCustom(message: commandControlString + txtControlUnSuccess[languageChooseSave], delay: 2)
    }
}

extension MapVC: URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
}
