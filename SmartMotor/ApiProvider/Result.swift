//
//  Result.swift
//  SmartMotor
//
//  Created by Toan on 5/31/21.
//  Copyright © 2021 vietnv2. All rights reserved.
//

import UIKit

public enum Result <T>{
  case sucess(T)
  case faiure(Error)
    public var isSucess : Bool {
        switch self {
        case .sucess:
            return true
        case .faiure :
            return false
        }
    }
    public var error : Error?{
        switch self {
        case .sucess:
            return nil
        case .faiure (let errors):
            return errors
        }
    }
}
