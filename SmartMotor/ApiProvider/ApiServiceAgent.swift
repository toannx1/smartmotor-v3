//
//  ApiServiceAgent.swift
//  SmartMotor
//
//  Created by Toan on 5/31/21.
//  Copyright © 2021 vietnv2. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
enum ResponseCode : Int{
    case sucess = 1
    case sucess2 = 0
}
class ApiServiceAgent: NSObject {
    func startRequest ( request: DataRequest, completion: @escaping(JSON, NSError?) -> Void){
        
        request.responseJSON{ (_ response ) in
            switch response.result {
            case .success(let data) :
                let json = JSON(data)
                let message = json["message"].stringValue
                let responseCode = json["responseCode"].intValue
                if  responseCode == ResponseCode.sucess.rawValue || responseCode == ResponseCode.sucess2.rawValue{
                    completion(json,nil)
                }else{
                    let error = NSError.errorWith(code: responseCode, message: message)
                    completion(json,error)
                }
                break
            case .failure(let error as NSError) :
                if error.code == NSURLErrorNotConnectedToInternet ||
                    error.code == NSURLErrorNetworkConnectionLost {
                    let error = NSError.errorWith(code: error.code,
                                                  message: "Không có kết nối mạng")
                    completion(JSON.null, error)
                } else if error.code == NSURLErrorCancelled {
                    completion(JSON.null, NSError.errorWith(code: error.code, message: ""))
                } else {
                    completion(JSON.null, error)
                }
                break
            }
        }
    }
}
extension NSError {
    public static func errorWith(code errorCode: Int, message errorMessage: String) -> NSError {
        return NSError.init(domain: "com.viettel.smartmotor3", code: errorCode,
                            userInfo: [NSLocalizedDescriptionKey: errorMessage])
    }

}

