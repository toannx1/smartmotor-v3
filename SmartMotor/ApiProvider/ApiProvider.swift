//
//  ApiProvider.swift
//  SmartMotor
//
//  Created by Toan on 5/31/21.
//  Copyright © 2021 vietnv2. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Toast
class ApiProvider: NSObject {
    static let share = ApiProvider()
    let apiAgent = ApiServiceAgent()
    let baseUrl =  "https://smartmotor.toprate.io/"
//    var header  : HTTPHeaders {
//        get {
//            let header : HTTPHeaders = ["Content-Type":"application/json",
//                                        "token": tokenSave
//            ]
//            return header
//        }
//    }
    var alamoFireManager: SessionManager!
    
    override init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = 30 // seconds for testing
        alamoFireManager = Alamofire.SessionManager(configuration:configuration)
        
    }
    func SendOtp (phoneNumber : String, completion: @escaping (Result<OtpModel>) -> Void)  {
        let url = baseUrl.appending("mtapi/rest/auth/sendOtp/\(phoneNumber)")
        let request =  alamoFireManager.request(url, method: .get, parameters: [:], encoding: URLEncoding.default, headers: nil)
        apiAgent.startRequest(request: request) { (json, error) in
            if let err = error {
                completion(Result.faiure(err))
            }else{
                let model = OtpModel(json: json)
                completion(Result.sucess(model))
               
            }
        }
    }
    func RegisterAcount( name : String , password : String ,repeatPass : String ,phoneNumber : String, otp : String , username : String ,email :String ,completion : @escaping ((Result<JSON>) -> Void)){
        let url = baseUrl.appending("mtapi/rest/auth/register")
        var params =  [String :Any]()
        params["username"] = name
        params["password"] = password
        params["repeatPassword"] = repeatPass
        params["opt"] = otp
        params["userPhone"] = phoneNumber
        params["email"] = email
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        var param  : Parameters = params
        let request = alamoFireManager.request(url, method: .post, parameters: param,encoding: JSONEncoding.default, headers: headers)
        apiAgent.startRequest(request: request) { (json, error) in
            
           if let err = error {
                completion(Result.faiure(err))
            }else{
                completion(Result.sucess(json))

            }
        }
    }
    
}
