//
//  AppUpdateCheck.swift
//  SmartMotor
//
//  Created by Vietnv2 on 4/13/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import UIKit

class AppUpdateCheck {
    enum VersionError: Error {
        case invalidResponse, invalidBundleInfo
    }

    // Check version to update
    func isUpdateAvailable(completion: @escaping (Bool?, Error?) -> Void) throws -> URLSessionDataTask {
        guard let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            // let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=com.viettel.smartmotor3") else {
            throw VersionError.invalidBundleInfo
        }
        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            do {
                if let error = error { throw error }
                guard let data = data else { throw VersionError.invalidResponse }
                let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any]
                guard let result = (json?["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String else {
                    throw VersionError.invalidResponse
                }
                // self.defaults.set(currentVersion, forKey: "appVersion")

                completion(version != currentVersion, nil)
            } catch {
                completion(nil, error)
            }
        }
        task.resume()
        return task
    }

    func hasAppBeenUpdated() {
        _ = try? isUpdateAvailable { update, error in
            if let _ = error {
                // print(error)
                // return false
            } else if let _ = update {
                // print(update)
                // false is same version
                // true is different version
            }
        }
    }
}

// class AppUpdateCheck {
//
//    let defaults = UserDefaults.standard // gets instance of NSUserDefaults for persistent store
//    let currentAppVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String // gets current app version on user's device
//
//    func saveCurrentVersionToDefaults() {
//        // saves currentAppVersion to defaults
//        defaults.set(currentAppVersion, forKey: "appVersion")
//        defaults.synchronize()
//    }
//
//    func hasAppBeenUpdated() -> Bool {
//
//        var appUpdated = false // indicates whether app has been updated or not
//
//        let previousAppVersion = defaults.string(forKey: "appVersion") // checks stored app version in NSUserDefaults
//
//        if previousAppVersion == nil {
//            // first run, no appVersion has ever been saved to NSUserDefaults
//
//            saveCurrentVersionToDefaults()
//
//        } else if previousAppVersion != currentAppVersion {
//            // app versions are different, thus the app has been updated
//
//            saveCurrentVersionToDefaults()
//            appUpdated = true
//
//        }
//
//        print("The current app version is: \(currentAppVersion). \n The app has been updated: \(appUpdated)")
//        return appUpdated
//    }
//
// }
