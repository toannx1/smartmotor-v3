//
//  DefineVariable.swift
//  Smart Motor
//
//  Created by vietnv2 on 11/5/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import Foundation
import UIKit

let cornerRadius5: CGFloat! = 5
let borderWidth1: CGFloat! = 1
let cornerRadius10: CGFloat! = 10
let borderWidth05: CGFloat! = 0.5
let titleViewHeight: CGFloat! = 80
let getMethod: String! = "GET"
let postMethod: String! = "POST"
let contentTypeString: String = "Content-Type"
let applicationJsonString: String = "application/json"
let googleMapsURL: String = "https://maps.googleapis.com/maps/api/geocode/json?latlng="

// GOOGLE MAPS
// bundle ID: com.viettel.motosmartcare2
// let googleMapsAPI:String = "AIzaSyD-1x3fKYWPnouGgmVOWliidWsLsjgYlDA"
// let googlePlaceAPI:String = "AIzaSyAN0sNBz9Et6iQXEstR6-PdjbtKcEEEiAI"

// bundle ID: com.viettel.motosmartcare3
// let googleMapsAPI:String = "AIzaSyCuCdpPAJwu95pI_0Vt_M6scxNjyQt-4gU"
// let googlePlaceAPI:String = "AIzaSyAN0sNBz9Et6iQXEstR6-PdjbtKcEEEiAI"

// bundle ID: com.viettel.mtracking.v3
// let googleMapsAPI:String = "AIzaSyBbu_xIjPRvrct4wQV5D88V4Br29XQ3ubk"
// let googlePlaceAPI:String = "AIzaSyAN0sNBz9Et6iQXEstR6-PdjbtKcEEEiAI"

// bundle ID: com.viettel.smartmotor3
// let googleMapsAPI:String = "AIzaSyBxgPVQbb-h2Y7f0k-L9DO68j5ilsGHHOc"
let googleMapsAPI: String = "AIzaSyA9rTnxqBiap6yioOqcGREcyrcZno2ShGU"
let googlePlaceAPI: String = "AIzaSyAN0sNBz9Et6iQXEstR6-PdjbtKcEEEiAI"

// bundle ID: com.viettel.smartmotor4
// let googleMapsAPI:String = "AIzaSyDPI3eTRPYtxfLVEqUWK1-F2AY-F2xial0"
// let googlePlaceAPI:String = "AIzaSyDlEjbHOPbRVQTpOq66pZV3Zi1BW-dj9hg"

// VIETTEL MAPS
// bundle ID: com.viettel.motosmartcare2 && com.viettel.motosmartcare3
// let APP_VTMAP_KEY = "45b22516ef28a92764698a111f0245b9"

// bundle ID: com.viettel.motosmartcare2 app cu
// let APP_VTMAP_KEY = "c68b1593773100ac23933ef4ae79a2f1"

// bundle ID: com.viettel.mtracking.v3
let APP_VTMAP_KEY = "c72b7491e2526a69b21abdf8eb421798"

//
var smartmotorUrl: String!
let smartmotorUrl1: String = "https://smartmotor.viettel.vn"
let smartmotorUrl2: String = "http://203.190.173.70:9115"
let smartmotorUrl3: String = "https://10.60.158.90:9115"

let forgotPassUrl = "/mtapi/fogetpassword"
let newloginUrl: String = "/mtapi/rest/auth/newlogin/"
let getAccountInfoUrl: String = "/mtapi/rest/mobile/accountinfo/getAccountInfo/"
let logoutUrl: String = "/mtapi/rest/auth/logout/"
let chargingUrl: String = "/mtapi/rest/mobile/charging/"
let payMoneyUrl: String = "/mtapi/rest/mobile/charging/processCharging/"
let getInfoUrl: String = "/mtapi/rest/mobile/charging/getInfo/"
let reviewtransportUrl: String = "/mtapi/rest/mobile/reviewtransport/"
let captchaUrl: String = "/mtapi/jcaptcha.jpg"
let listTransportUrl: String = "/mtapi/rest/mobile/monitortransport/listTransport/"
let listTransportRegNoUrl: String = "/0/2?registerNo="
let listTransportPageIndexUrl: String = "&state=0123456&groupCode=&pageSize=10&pageIndex="
//
let ipAdressURL: String = "https://api.ipify.org?format=json"

let encrypKeyForLogin: String! = "PHANNANGDUYBKAHN"
//
let checkBoxImage: UIImage = #imageLiteral(resourceName: "Checkbox_Checked")
let uncheckBoxImage: UIImage = #imageLiteral(resourceName: "Checkbox_Unchecked")
var logoutFlag: Bool! = false
var keyboardHeight: CGFloat! = 215
var idleFlag: Bool! = true
var deviceIPAddress: String = ""
var tokenSave: String!
var userIdSave: String!
var userIdSave_ext: Int!
var groupRootSave: Int!
var loginFlag: Bool! = true
var heightScreen: CGFloat!
var widthScreen: CGFloat!
var commandControlString: String = ""
var countSendCommandControl: Int = 0
var vtMapDisplayFlag: Bool = true
var smsSettingContext: String = ""
let colorWarning: UIColor = UIColor(red: 255 / 255.0, green: 208 / 255.0, blue: 0 / 255.0, alpha: 1.0)
let colorError: UIColor = UIColor(red: 198 / 255.0, green: 0 / 255.0, blue: 0 / 255.0, alpha: 1.0)
let colorBlueTitle1: UIColor = UIColor(red: 21 / 255.0, green: 132 / 255.0, blue: 168 / 255.0, alpha: 1.0)
let colorBlueTitle2: UIColor = UIColor(red: 0 / 255.0, green: 156 / 255.0, blue: 222 / 255.0, alpha: 1.0)
let colorSuccess: UIColor = UIColor(red: 0 / 255.0, green: 161 / 255.0, blue: 0 / 255.0, alpha: 1.0)
let colorBlueApp: UIColor = UIColor(red: 0 / 255.0, green: 114 / 255.0, blue: 205 / 255.0, alpha: 1.0)
enum vehicleInfoString: String {
    case accIllegalState
    case devicePin
    case deviceType
    case deviceTypeIdOrginal
    case factory
    case gpsDate
    case gpsSpeed
    case gpsState
    case groupsCode
    case id
    case illegalMoveState
    case lat
    case lng
    case lowBatteryState
    case motoSpeed
    case offPowerState
    case regNo
    case sim
    case sosState
    case state
    case timeState
    case type
    case vibrationState
    case isForbidden
}

var mapTypeSave: String!

//
var languageChooseSave: Int!
var languageChooseIndex: Int! = 0
var languageChooseSaveFlag: Bool! = false
var txtEnterCaptcha: [String]!
var txtLoading: [String]!

// Mark: LOGIN VC
// Mark: LOGIN VC
var txtUserTextField_PlaceHolder: [String]!
var txtPasswordTextField_PlaceHolder: [String]!
var txtCaptchaTextField_PlaceHolder: [String]!
var txtButton_Title: [String]!
var txtForgotPassword_Title: [String]!
var txtServiceInfo_Title: [String]!
var txtHotline_title: [String]!

var txtAlertContent_UserEmpty: [String]!
var txtAlertContent_PasswordEmpty: [String]!
var txtAlertContent_LoginFail: [String]!
var txtAlertContent_CaptchaInvalid: [String]!
var txtAlertContent_CaptchaInvalid_SystemError: [String]!

var txtTitleHotlineConfirm: [String]!
var txtContentHotlineConfirm: [String]!
var txtOKHotlineConfirm: [String]!
var txtCancelHotlineConfirm: [String]!

var txtcannotConnect2Server: [String]!
var txtloginUnsuccess: [String]!
// ------------------------

// Mark: LIST VEHICLE VC
// Mark: LIST VEHICLE VC
var listVehicleVC_Title: [String]!
var listVehicleVC_Searching_PlaceHolder: [String]!
var listVehicleVC_RegNo_Choose: [String]!
var listVehicleVC_RegNo_Select: [String]!

var cannotLoadListVehicle: [String]!
var connectionFail: [String]!
var noDataString: [String]!

var listVehicleVC_BtnLogout_Title: [String]!
var txtButtonBalanceInfo: [String]!
var txtButtonHelp: [String]!
var txtButtonListChargeMoney: [String]!

var txtLogoutUnSuccessAlert: [String]!

var txtTitleConfirmLogout: [String]!
var txtContentConfirmLogout: [String]!
var txtButtonOKConfirmLogout: [String]!
var txtButtonCancelConfirmLogout: [String]!
// -----------------
var titleListChargeMoney: [String]!
var listChargeMoney_PaymentViaCard: [String]!
var listChargeMoney_PaymentViaPhoneBalance: [String]!

var txtButtonCloseListChargeMoney: [String]!
// ---ChargeCardCode--------------
var txtTitleChargeCardCode: [String]!
// var txtContent01ChargeCardCode:[String]!
var txtContent02ChargeCardCode: [String]!
var txtContent03ChargeCardCode: [String]!
var txtContent04ChargeCardCode: [String]!
var txtContent05ChargeCardCode: [String]!
var txtButtonConfirmChargeCardCode: [String]!
var txtButtonCancelChargeCardCode: [String]!
var txtAlertCardCode_ChargeCardCode: [String]!
var txtTitleConfirmChargeCardCode: [String]!
var txtContentConfirmChargeCardCode: [String]!
var txtButtonOKConfirmChargeCardCode: [String]!
var txtButtonCancelConfirmChargeCardCode: [String]!

var txtResult01_ChargeCardCode: [String]!

var txtResult02ChargeCardCode: [String]!
var txtResult03ChargeCardCode: [String]!
var txtResult04ChargeCardCode: [String]!
var txtResult05ChargeCardCode: [String]!
var txtResult06ChargeCardCode: [String]!
var txtResult07ChargeCardCode: [String]!
var txtResult08ChargeCardCode: [String]!
// ---SOS--------------
var txtTitleSOS: [String]!
var txtContentSOS: [String]!

var txtButtonCancelSOS: [String]!
var txtTitleConfirmCallHotline: [String]!
var txtContentConfirmCallHotline: [String]!
var txtButtonConfirmCallHotline: [String]!
var txtButtonCancelConfirmCallHotline: [String]!

// ----AccountInfo-----------------
var txtTitleAccountInfo: [String]!
var txtContent01AccountInfo: [String]!
var txtContent02AccountInfo: [String]!
var txtContent03AccountInfo: [String]!
var txtContent04AccountInfo: [String]!
var txtContent05AccountInfo: [String]!
var txtContent06AccountInfo: [String]!
var txtButtonCancelAccountInfo: [String]!
var txtAlertAccountInfo: [String]!
// ----PayMoney------------
var txtMonthPay: [String]!
var txtTitlePayMoney: [String]!
var txtContent01_PayMoney: [String]!
var txtContent02PayMoney: [String]!
var txtContent03PayMoney: [String]!
var txtContent04PayMoney: [String]!
var txtButtonOKPayMoney: [String]!
var txtButtonCancelPayMoney: [String]!

var txtTitleConfirmPayMoney: [String]!
var txtContentConfirmPayMoney: [String]!
var txtButtonOKConfirmPayMoney: [String]!
var txtButtonCancelConfirmPayMoney: [String]!

var txtPaymentSuccess: [String]!
var txtWaitAmomentAfterPaymentSuccess: [String]!
var txtPaymentUnSuccess01: [String]!
var txtPaymentUnSuccess02: [String]!
var txtPaymentUnSuccess03: [String]!
var txtPaymentUnSuccess04: [String]!
var txtAlert01PayMoney: [String]!
var txtAlert02PayMoney: [String]!
// -----ForBidden------------
var txtTitleAlertForBidden: [String]!
var txtButtonServicePaymentAlertForBidden: [String]!
var txtButtonCancelAlertForBidden: [String]!
var txtContent_AlertForBidden: [String]!
// ----------------
// Mark: MAP VC
// Mark: MAP VC
var txtTitle01MapVC: [String]!
var txtTitle02MapVC: [String]!
var txtTitleSelectDateTime: [String]!
var txtContent01SelectDateTime: [String]!
var txtContent02SelectDateTime: [String]!
var txtButtonConfirmSelectDateTime: [String]!
var txtButtonCancelSelectDateTime: [String]!

var txtTitleWarningAllowAccessLocation: [String]!
var txtContentWarningAllowAccessLocation: [String]!
var txtButtonOkWarningAllowAccessLocation: [String]!
var txtButtonCancelWarningAllowAccessLocation: [String]!

var txtTitleWarningWrongLocation: [String]!
var txtContentWarningWrongLocation: [String]!
var txtButtonOkWarningWrongLocation: [String]!
var txtButtonCancelWarningWrongLocation: [String]!

var txtTitleConfirmLockVehicle: [String]!
var txtContentConfirmLockVehicle: [String]!
var txtButtonOKConfirmLockVehicle: [String]!
var txtButtonCancelConfirmLockVehicle: [String]!

var txtTitleConfirmUnLockVehicle: [String]!
var txtContentConfirmUnLockVehicle: [String]!
var txtButtonOKConfirmUnLockVehicle: [String]!
var txtButtonCancelConfirmUnLockVehicle: [String]!

var txtTitleConfirmCall2Vehicle: [String]!
var txtContentConfirmCall2Vehicle: [String]!
var txtButtonOKConfirmCall2Vehicle: [String]!
var txtButtonCancelConfirmCall2Vehicle: [String]!

var txtTitleUnlockByCall: [String]!
var txtContentUnlockByCall: [String]!
var txtButtonOKUnlockByCall: [String]!
var txtButtonCancelUnlockByCall: [String]!

var txtTitleLockByCall: [String]!
var txtContentLockByCall: [String]!
var txtButtonOKLockByCall: [String]!
var txtButtonCancelLockByCall: [String]!

var txtStatus01: [String]!
var txtStatus02: [String]!
var txtStatus03: [String]!
var txtStatus04: [String]!
var txtUnknownVehicleLocation: [String]!

var txtYear: [String]!
var txtMonth: [String]!
var txtDay: [String]!
var txtHour: [String]!
var txtMinute: [String]!
var txtSecond: [String]!

var txtNoVehicleInfomationString: [String]!
var txtWrongTimeOrNoDataThisTime: [String]!
var txtloadDataUnSuccess: [String]!
var txtControlSuccess: [String]!
var txtControlUnSuccess: [String]!

var txtAlertTimeMax2Date: [String]!
var txtDateTimeInvalid: [String]!

var txtAlert01MapVC: [String]!
var txtAlert02MapVC: [String]!
var txtAlert03MapVC: [String]!
var txtAlert04MapVC: [String]!

var txtButtonLock: [String]!
var txtButtonUnLock: [String]!
var txtButtonCall: [String]!

var txtButtonJourney: [String]!
var txtButtonSetting: [String]!
var txtButtonMaps: [String]!
var txtButtonMyLocation: [String]!

var txtShowDetailPoint: [String]!
var txtTitleDetailPoint: [String]!
var txtContent01DetailPoint: [String]!
var txtContent02DetailPoint: [String]!
var txtContent03DetailPoint: [String]!

var txtStatus_DetailPoint_Run: [String]!
var txtStatus_DetailPoint_Stop: [String]!
var txtStatus_DetailPoint_Park: [String]!
var txtStatus_DetailPoint_LossGPS: [String]!
var txtStatus_DetailPoint_Hibernate: [String]!
var txtStatus_DetailPoint_LossGPRS: [String]!
var txtStatus_DetailPoint_NoInfo: [String]!

var txtConnectServerFail: [String]!
var txtUserLocationDistance: [String]!
// -----------------
// Mark: SETTING VC
// Mark: SETTING VC

var txtSettingArray_DeviceActive: [String]!
var txtSettingArray_ChangeAppPassword: [String]!
var txtSettingArray_ChangeDevicePassword: [String]!
var txtSettingArray_ChangeUserPhone: [String]!
var txtSettingArray_Reboot: [String]!
var txtSettingArray_ChangeConfig: [String]!
var txtSettingArray_FactoryReset: [String]!
var txtSettingArray_Feedback: [String]!
var txtSettingArray_Language: [String]!

var txtAlarmSettingArray_AccAlert: [String]!
var txtAlarmSettingArray_VibrateAlert: [String]!
var txtAlarmSettingArray_FenceAlert: [String]!
var txtAlarmSettingArray_OverSpeedAlert: [String]!

var txtTitleSettingView: [String]!
var txtAlert01SettingVC: [String]!
var txtAlert02SettingVC: [String]!
var txtAlert03SettingVC: [String]!
var txtAlert04SettingVC: [String]!

var txtTitleVehicleRegistry: [String]!
var txtContent01VehicleRegistry: [String]!
var txtContent02VehicleRegistry: [String]!
var txtContent03VehicleRegistry: [String]!
var txtContent04VehicleRegistry: [String]!
var txtButtonConfirmVehicleRegistry: [String]!
var txtButtonCancelVehicleRegistry: [String]!
var txtAlert01VehicleRegistry: [String]!
var txtAlert02VehicleRegistry: [String]!
var txtAlert03VehicleRegistry: [String]!
var txtAlert04VehicleRegistry: [String]!

var txtTitleChangeAppPassword: [String]!
var txtContent01ChangeAppPassword: [String]!
var txtContent02ChangeAppPassword: [String]!
var txtContent03ChangeAppPassword: [String]!
var txtButtonConfirmChangeAppPassword: [String]!
var txtButtonCancelChangeAppPassword: [String]!
var txtAlert01ChangeAppPassword: [String]!
var txtAlert02ChangeAppPassword: [String]!
var txtAlert03ChangeAppPassword: [String]!
var txtAlert04ChangeAppPassword: [String]!
var txtAlert05ChangeAppPassword: [String]!

var txtAlert06ChangeAppPassword: [String]!
var txtAlert07ChangeAppPassword: [String]!
var txtAlert08ChangeAppPassword: [String]!
var txtAlert09ChangeAppPassword: [String]!
var txtAlert10ChangeAppPassword: [String]!
var txtAlert11ChangeAppPassword: [String]!

var txtTitleChangeVehiclePassword: [String]!
var txtContent01ChangeVehiclePassword: [String]!
var txtContent02ChangeVehiclePassword: [String]!
var txtContent03ChangeVehiclePassword: [String]!
var txtButtonConfirmChangeVehiclePassword: [String]!
var txtButtonCancelChangeVehiclePassword: [String]!
var txtAlert01ChangeVehiclePassword: [String]!
var txtAlert02ChangeVehiclePassword: [String]!
var txtAlert03ChangeVehiclePassword: [String]!
var txtAlert04ChangeVehiclePassword: [String]!

var txtTitleChangeUserPhone: [String]!
var txtContent01ChangeUserPhone: [String]!
var txtContent02ChangeUserPhone: [String]!
var txtButtonConfirmChangeUserPhone: [String]!
var txtButtonCancelChangeUserPhone: [String]!
var txtAlert01ChangeUserPhone: [String]!
var txtAlert02ChangeUserPhone: [String]!

var txtTitleDeviceReboot: [String]!
var txtContentDeviceReboot: [String]!
var txtButtonConfirmDeviceReboot: [String]!
var txtButtonCancelDeviceReboot: [String]!

var txtTitleListAlarmSetting: [String]!
var txtButtonCancelListAlarmSetting: [String]!

var txtTitleAccAlarmSetting: [String]!
var txtContent01AccAlarmSetting: [String]!
var txtContent02AccAlarmSetting: [String]!
var txtContent03AccAlarmSetting: [String]!
var txtContent04AccAlarmSetting: [String]!
var txtButtonConfirmAccAlarmSetting: [String]!
var txtButtonCancelAccAlarmSetting: [String]!

var txtTitleVibrateAlarmSetting: [String]!
var txtContent01VibrateAlarmSetting: [String]!
var txtContent02VibrateAlarmSetting: [String]!
var txtContent03VibrateAlarmSetting: [String]!
var txtContent04VibrateAlarmSetting: [String]!
var txtButtonConfirmVibrateAlarmSetting: [String]!
var txtButtonCancelVibrateAlarmSetting: [String]!

var txtTitleFenceAlarmSetting: [String]!
var txtContent01FenceAlarmSetting: [String]!
var txtContent02FenceAlarmSetting: [String]!
var txtContent03FenceAlarmSetting: [String]!
var txtContent04FenceAlarmSetting: [String]!
var txtContent05FenceAlarmSetting: [String]!
var txtContent06FenceAlarmSetting: [String]!
var txtButtonConfirmFenceAlarmSetting: [String]!
var txtButtonCancelFenceAlarmSetting: [String]!
var txtAlertFenceAlarmSetting: [String]!

var txtTitleSpeedAlarmSetting: [String]!
var txtContent01SpeedAlarmSetting: [String]!
var txtContent02SpeedAlarmSetting: [String]!
var txtContent03SpeedAlarmSetting: [String]!
var txtContent04SpeedAlarmSetting: [String]!
var txtContent05SpeedAlarmSetting: [String]!
var txtButtonConfirmSpeedAlarmSetting: [String]!
var txtButtonCancelSpeedAlarmSetting: [String]!
var txtAlertSpeedAlarmSetting: [String]!

var txtTitleDeviceFormat: [String]!
var txtContentDeviceFormat: [String]!
var txtButtonConfirmDeviceFormat: [String]!
var txtButtonCancelDeviceFormat: [String]!

var commandControlStringReboot: [String]!
var commandControlStringChangeUserPhone: [String]!
var commandControlStringChangeVehiclePassword: [String]!

var commandControlStringSuccess: [String]!
var commandControlStringUnSuccess: [String]!

var txtButtonVietnamese_ChooseLanguage: [String]!
var txtButtonEnglish_ChooseLanguage: [String]!

var txtTitleChooseLanguage: [String]!
var txtButtonConfirmChooseLanguage: [String]!
var txtButtonCancelChooseLanguage: [String]!

var txtTitleAskPassword: [String]!
var txtContentAskPassword: [String]!
var txtButtonConfirmAskPassword: [String]!
var txtButtonCancelAskPassword: [String]!

var txtEmailSubject: [String]!
