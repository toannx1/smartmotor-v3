//
//  Person.swift
//  Smart Motor
//
//  Created by vietnv2 on 11/8/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import CoreData
import UIKit

class Person: NSManagedObject {
    @NSManaged var token: String?
    @NSManaged var userId: String?
    @NSManaged var groupRoot: String?
}
