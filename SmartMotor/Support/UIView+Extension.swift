//
//  UIView+Extension.swift
//  SmartMotor
//
//  Created by nguyen hoan on 8/27/19.
//  Copyright © 2019 EPR. All rights reserved.
//

import GoogleMaps
import UIKit

extension UIView {
    func roundCorners(cornerRadius: Double) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            layer.borderColor = newValue?.cgColor
        }
        get {
            guard let color = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: color)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    func applyShadowOnView(radius: CGFloat = 3) {
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOpacity = 1
        layer.shadowRadius = radius
        layer.shadowOffset = .zero
    }
    
    func drawShadow(radius: CGFloat = 3, offset: CGSize) {
        layer.shadowOffset = offset
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOpacity = 0.7
        layer.shadowRadius = radius
        layer.masksToBounds = false
    }
    
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            
            // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            if self.shadow == false {
                self.layer.masksToBounds = true
            }
        }
    }
    
    func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 1.0, height: 2.0),
                   shadowOpacity: Float = 0.4,
                   shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
    
    enum ViewBorder: String {
        case left, right, top, bottom
    }
    
    func add(Border border: ViewBorder, withColor color: UIColor, andHeight height: CGFloat = 0.5, left: CGFloat = 0) {
        let borderView = UIView()
        borderView.tag = 1000
        borderView.backgroundColor = color
        borderView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(borderView)
        NSLayoutConstraint.activate(self.getConstrainsFor(forView: borderView, WithBorderType: border, andWidth: height, left: left))
    }
    
    private func getConstrainsFor(forView borderView: UIView, WithBorderType border: ViewBorder, andWidth width: CGFloat, left: CGFloat) -> [NSLayoutConstraint] {
        let height = borderView.heightAnchor.constraint(equalToConstant: width)
        let widthAnchor = borderView.widthAnchor.constraint(equalToConstant: width)
        let leading = borderView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: left)
        let trailing = borderView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0)
        let top = borderView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0)
        let bottom = borderView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0)
        
        switch border {
        case .bottom:
            return [bottom, leading, trailing, height]
            
        case .top:
            return [top, leading, trailing, height]
            
        case .left:
            return [top, bottom, leading, widthAnchor]
            
        case .right:
            return [top, bottom, trailing, widthAnchor]
        }
    }
}

class PaddingLabel: UILabel {
    var topInset: CGFloat = 0.0
    var bottomInset: CGFloat = 0.0
    var leftInset: CGFloat = 5.0
    var rightInset: CGFloat = 5.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + self.leftInset + self.rightInset,
                      height: size.height + self.topInset + self.bottomInset)
    }
}

extension UIImage {
    func imageWithInsets(insetDimen: CGFloat) -> UIImage {
        return self.imageWithInset(insets: UIEdgeInsets(top: insetDimen, left: insetDimen, bottom: insetDimen, right: insetDimen))
    }
    
    func imageWithInset(insets: UIEdgeInsets) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: self.size.width + insets.left + insets.right,
                                                      height: self.size.height + insets.top + insets.bottom), false, self.scale)
        let origin = CGPoint(x: insets.left, y: insets.top)
        self.draw(at: origin)
        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return imageWithInsets!
    }
}

extension UITextField {
    func paddingRight(padding: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: padding, height: frame.size.height))
        rightView = paddingView
        rightViewMode = .always
    }
    
    func paddingLeft(padding: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: padding, height: frame.size.height))
        leftView = paddingView
        leftViewMode = .always
    }
    
    func setCursor(position: Int) {
        let position = self.position(from: beginningOfDocument, offset: position)!
        selectedTextRange = textRange(from: position, to: position)
    }
}

class VerticalTopAlignLabel: UILabel {
    override func drawText(in rect: CGRect) {
        guard let labelText = text else { return super.drawText(in: rect) }
        
        let attributedText = NSAttributedString(string: labelText, attributes: [NSAttributedString.Key.font: font])
        var newRect = rect
        newRect.size.height = attributedText.boundingRect(with: rect.size, options: .usesLineFragmentOrigin, context: nil).size.height
        
        if numberOfLines != 0 {
            newRect.size.height = min(newRect.size.height, CGFloat(numberOfLines) * font.lineHeight)
        }
        
        super.drawText(in: newRect)
    }
}

import MapKit
extension GMSCameraUpdate {
    static func fit(coordinate: CLLocationCoordinate2D, radius: Double) -> GMSCameraUpdate {
        var leftCoordinate = coordinate
        var rigthCoordinate = coordinate
        let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: radius, longitudinalMeters: radius)
        let span = region.span
        leftCoordinate.latitude = coordinate.latitude - span.latitudeDelta
        leftCoordinate.longitude = coordinate.longitude - span.longitudeDelta
        rigthCoordinate.latitude = coordinate.latitude + span.latitudeDelta
        rigthCoordinate.longitude = coordinate.longitude + span.longitudeDelta
        let bounds = GMSCoordinateBounds(coordinate: leftCoordinate, coordinate: rigthCoordinate)
        let update = GMSCameraUpdate.fit(bounds, withPadding: -15.0)
        return update
    }
}

import SwiftEntryKit
extension UIViewController {
    func shadowOnviewWithcornerRadius(customView: UIView, cornerRadius: CGFloat) {
        customView.layer.shadowColor = UIColor.darkGray.cgColor
        customView.layer.shadowOpacity = 1.0
        customView.layer.shadowRadius = 3
        customView.layer.shadowOffset = CGSize(width: 0, height: -3)
        customView.layer.masksToBounds = false
        customView.layer.cornerRadius = cornerRadius
        if #available(iOS 11.0, *) {
            customView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        } else {
            // Fallback on earlier versions
        }
        customView.layer.borderWidth = 0.3
        // customView.backgroundColor     = UIColor.white
    }
    
    func showBannerNotifi(text: String, background: UIColor, textColor: UIColor, imageTitle: UIImage, des: String = "") {
        let noti = NotificationView()
        noti.backgroundColor = background
        noti.title.textColor = textColor
        noti.imgContent.image = imageTitle
        noti.title.text = text
        if des == "" {
            noti.subTitle.isHidden = true
        } else {
            noti.subTitle.isHidden = false
            noti.subTitle.text = des
        }
        
        noti.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(noti)
        var constraint: NSLayoutConstraint?
        constraint = noti.topAnchor.constraint(equalTo: view.topAnchor, constant: -200)
        constraint!.isActive = true
        noti.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        noti.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            UIView.animate(withDuration: 0.5, animations: {
                constraint?.constant = 0
                self.view.layoutIfNeeded()
            })
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.5) {
            UIView.animate(withDuration: 0.5, animations: {
                constraint?.constant = -200
                self.view.layoutIfNeeded()
            })
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.5) {
            noti.removeFromSuperview()
        }
    }
}

extension UIButton {
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.setBackgroundImage(colorImage, for: forState)
    }
}

class NotificationView: UIView {
    let imgContent = UIImageView(image: UIImage(named: "icon_not_find"))
    let title = UILabel(text: "Nạp tiền thành công", font: UIFont.systemFont(ofSize: 15, weight: .semibold), textColor: .white, textAlignment: .left, numberOfLines: 0)
    let subTitle = UILabel(text: "Để kiểm tra tài khoản, vui lòng vào chức năng 'SIM thiết bị'.", font: UIFont.systemFont(ofSize: 14, weight: .medium), textColor: .white, textAlignment: .left, numberOfLines: 0)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.imgContent.contentMode = .scaleAspectFit
        let viewImg = UIView(backgroundColor: .clear)
        title.adjustsFontSizeToFitWidth = true
        self.title.minimumScaleFactor = 0.5
        
        hstack(viewImg.withSize(.init(width: 60, height: 60)),
               stack(self.title, self.subTitle, spacing: 0),
               spacing: 8).withMargins(.allSides(8))
        viewImg.addSubview(self.imgContent.withSize(.init(width: 40, height: 40)))
        self.imgContent.translatesAutoresizingMaskIntoConstraints = false
        self.imgContent.centerXAnchor.constraint(equalTo: viewImg.centerXAnchor).isActive = true
        self.imgContent.centerYAnchor.constraint(equalTo: viewImg.centerYAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
