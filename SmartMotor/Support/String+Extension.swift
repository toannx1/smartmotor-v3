//
//  String+Extension.swift
//  SmartMotor
//
//  Created by nguyen hoan on 9/9/19.
//  Copyright © 2019 EPR. All rights reserved.
//

import UIKit
extension String {
    func isVietNewPhone() -> Bool {
        if self.hasPrefix("+84") {
            let phone = self.replacingOccurrences(of: "+84", with: "0")
            // VINA 088 091 094
            if phone.hasPrefix("088") || phone.hasPrefix("091") || phone.hasPrefix("094") || phone.hasPrefix("083") || phone.hasPrefix("084") || phone.hasPrefix("085") || phone.hasPrefix("081") || phone.hasPrefix("082") {
                if phone.count == 10 {
                    return true
                }
            }
            // VIETTEL 10 So
            else if phone.hasPrefix("086") || phone.hasPrefix("096") || phone.hasPrefix("097") || phone.hasPrefix("098") || phone.hasPrefix("032") || phone.hasPrefix("033") || phone.hasPrefix("034") || phone.hasPrefix("035") || phone.hasPrefix("036") || phone.hasPrefix("037") || phone.hasPrefix("038") || phone.hasPrefix("039") {
                if phone.count == 10 {
                    return true
                }
            }
            // MOBI 10 so
            else if phone.hasPrefix("089") || phone.hasPrefix("090") || phone.hasPrefix("093") || phone.hasPrefix("070") || phone.hasPrefix("079") || phone.hasPrefix("077") || phone.hasPrefix("076") || phone.hasPrefix("078") {
                if phone.count == 10 {
                    return true
                }
            }
            // Vietnamobile 10 so
            else if phone.hasPrefix("092") || phone.hasPrefix("056") || phone.hasPrefix("058") {
                if phone.count == 10 {
                    return true
                }
            }
            // Gmobile 10 so
            else if phone.hasPrefix("099") || phone.hasPrefix("059") {
                if phone.count == 10 {
                    return true
                }
            }
        }
        else if self.hasPrefix("84") {
            let phone = self.replacingOccurrences(of: "84", with: "0")
            // VINA 088 091 094
            if phone.hasPrefix("088") || phone.hasPrefix("091") || phone.hasPrefix("094") || phone.hasPrefix("083") || phone.hasPrefix("084") || phone.hasPrefix("085") || phone.hasPrefix("081") || phone.hasPrefix("082") {
                if phone.count == 10 {
                    return true
                }
            }
            // VIETTEL 10 So
            else if phone.hasPrefix("086") || phone.hasPrefix("096") || phone.hasPrefix("097") || phone.hasPrefix("098") || phone.hasPrefix("032") || phone.hasPrefix("033") || phone.hasPrefix("034") || phone.hasPrefix("035") || phone.hasPrefix("036") || phone.hasPrefix("037") || phone.hasPrefix("038") || phone.hasPrefix("039") {
                if phone.count == 10 {
                    return true
                }
            }
            // MOBI 10 so
            else if phone.hasPrefix("089") || phone.hasPrefix("090") || phone.hasPrefix("093") || phone.hasPrefix("070") || phone.hasPrefix("079") || phone.hasPrefix("077") || phone.hasPrefix("076") || phone.hasPrefix("078") {
                if phone.count == 10 {
                    return true
                }
            }
            // Vietnamobile 10 so
            else if phone.hasPrefix("092") || phone.hasPrefix("056") || phone.hasPrefix("058") {
                if phone.count == 10 {
                    return true
                }
            }
            // Gmobile 10 so
            else if phone.hasPrefix("099") || phone.hasPrefix("059") {
                if phone.count == 10 {
                    return true
                }
            }
        }
        else if self.hasPrefix("0") {
            let phone = self
            // VINA 088 091 094
            if phone.hasPrefix("088") || phone.hasPrefix("091") || phone.hasPrefix("094") || phone.hasPrefix("083") || phone.hasPrefix("084") || phone.hasPrefix("085") || phone.hasPrefix("081") || phone.hasPrefix("082") {
                if phone.count == 10 {
                    return true
                }
            }
            // VIETTEL 10 So
            else if phone.hasPrefix("086") || phone.hasPrefix("096") || phone.hasPrefix("097") || phone.hasPrefix("098") || phone.hasPrefix("032") || phone.hasPrefix("033") || phone.hasPrefix("034") || phone.hasPrefix("035") || phone.hasPrefix("036") || phone.hasPrefix("037") || phone.hasPrefix("038") || phone.hasPrefix("039") {
                if phone.count == 10 {
                    return true
                }
            }
            // MOBI 10 so
            else if phone.hasPrefix("089") || phone.hasPrefix("090") || phone.hasPrefix("093") || phone.hasPrefix("070") || phone.hasPrefix("079") || phone.hasPrefix("077") || phone.hasPrefix("076") || phone.hasPrefix("078") {
                if phone.count == 10 {
                    return true
                }
            }
            // Vietnamobile 10 so
            else if phone.hasPrefix("092") || phone.hasPrefix("056") || phone.hasPrefix("058") {
                if phone.count == 10 {
                    return true
                }
            }
            // Gmobile 10 so
            else if phone.hasPrefix("099") || phone.hasPrefix("059") {
                if phone.count == 10 {
                    return true
                }
            }
        }
        return false
    }
}
