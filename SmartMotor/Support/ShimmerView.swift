//
//  ShimmerView.swift
//  SmartMotor
//
//  Created by nguyen hoan on 10/3/19.
//  Copyright © 2019 EPR. All rights reserved.
//

import UIKit

class ShimmerView: UIView {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var contentView: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        backgroundColor = .white
        Bundle.main.loadNibNamed("ShimmerView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
}
