import Reachability
import SystemConfiguration

public class CheckNetwork {
    public func checkConnection() -> Bool {
        let network = Reachability()!
        
        var status = true
        
        network.whenReachable = { reachability in
            switch reachability.connection {
            case .cellular:
                status = true
            case .wifi:
                status = true
            default:
                status = false
            }
        }
        
        network.whenUnreachable = { _ in
            status = false
        }
        return status
    }
    
    public func checkNetwork() -> String {
        let network = Reachability()!
        var isAvailable = "noConnection"
        network.whenReachable = { reachability in
            switch reachability.connection {
            case .cellular:
                isAvailable = "2g3gConnection"
            case .wifi:
                isAvailable = "wifiConnection"
            default:
                isAvailable = "noConnection"
            }
        }
        
        network.whenUnreachable = { _ in
            isAvailable = "noConnection"
        }
        return isAvailable
    }
    
    // MARK: Check internet available
    
    func isInternet() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) { zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
}
