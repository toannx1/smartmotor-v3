//
//  Loading.swift
//  Smart Motor
//
//  Created by vietnv2 on 24/10/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

var loadingFlag: Bool! = false

class Loading {
    var loadingView = UIView()
    var container = UIView()
    var activityIndicator = UIActivityIndicatorView()
    var content = UILabel()

    func showLoading() {
        if loadingFlag == true {
            return
        }
        loadingFlag = true
        content.text = txtLoading[languageChooseSave]
        if content.text != "" {
            content.textAlignment = NSTextAlignment.justified
            content.lineBreakMode = NSLineBreakMode.byWordWrapping
            content.numberOfLines = 0
            content.textColor = UIColor(red: 255 / 255, green: 255 / 255, blue: 255 / 255, alpha: 1)
            content.sizeToFit()
        }
        //
        let win: UIWindow = UIApplication.shared.delegate!.window!!
        loadingView = UIView(frame: win.frame)
        loadingView.tag = 1
        loadingView.backgroundColor = UIColor(red: 255 / 255, green: 255 / 255, blue: 255 / 255, alpha: 0)
        //
        if content.text != "" {
            container = UIView(frame: CGRect(x: 0, y: 0, width: content.frame.size.width + 60, height: win.frame.width / 5 + content.frame.size.height + 30))
        } else {
            container = UIView(frame: CGRect(x: 0, y: 0, width: win.frame.width / 3, height: win.frame.width / 3))
        }
        container.backgroundColor = UIColor(red: 0 / 255, green: 0 / 255, blue: 0 / 255, alpha: 0.7)
        container.layer.cornerRadius = 10.0
        container.layer.borderColor = UIColor.gray.cgColor
        container.layer.borderWidth = 0.5
        container.clipsToBounds = true
        container.center = loadingView.center
        //
        activityIndicator.frame = CGRect(x: 0, y: 0, width: win.frame.width / 5, height: win.frame.width / 5)
        activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge // .whiteLarge
        activityIndicator.startAnimating()
        activityIndicator.center = container.center
        //
        win.addSubview(loadingView)
        loadingView.addSubview(container)
        loadingView.addSubview(activityIndicator)
        if content.text != "" {
            loadingView.addSubview(content)
            content.frame.origin.x = container.frame.origin.x + (container.frame.size.width - content.frame.size.width) / 2
            content.frame.origin.y = container.frame.origin.y + container.frame.size.height * 3 / 4
        }
    }

    func hideLoading(delay: Double) {
        if loadingFlag == false {
            return
        }
        loadingFlag = false
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when) {
            UIView.animate(withDuration: 0.0, delay: 0.5, options: .curveEaseOut, animations: {
                self.container.alpha = 0.0
                self.loadingView.alpha = 0.0
                self.activityIndicator.stopAnimating()
            }, completion: { _ in
                self.content.removeFromSuperview()
                self.activityIndicator.removeFromSuperview()
                self.container.removeFromSuperview()
                self.loadingView.removeFromSuperview()
                let win: UIWindow = UIApplication.shared.delegate!.window!!
                let removeView = win.viewWithTag(1)
                removeView?.removeFromSuperview()
            })
        }
    }
}
