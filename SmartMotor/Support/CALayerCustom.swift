//
//  CALayerCustom.swift
//  SmartMotor
//
//  Created by vietnv2 on 12/23/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

extension CALayer {
    var shadowColorFromUIColor: UIColor? {
        set {
            shadowColor = newValue?.cgColor
        } get {
            return UIColor(cgColor: shadowColor ?? UIColor.clear.cgColor)
        }
    }

    var borderColorFromUIColor: UIColor? {
        set {
            borderColor = newValue?.cgColor
        } get {
            return UIColor(cgColor: borderColor ?? UIColor.clear.cgColor)
        }
    }
}
