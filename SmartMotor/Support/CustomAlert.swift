//
//  Alert.swift
//  SmartMotor
//
//  Created by vietnv2 on 12/16/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

// public class Alert: NSObject {
//    class func alertAutoDismiss(delegate: UIViewController,message:String,textAlignment:String,delay:Double) {
//        Loading().hideLoading(delay:0)
//        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertControllerStyle.alert)
//        //
//        let paragraphStyle = NSMutableParagraphStyle()
//        switch textAlignment {
//            case "justified":paragraphStyle.alignment = NSTextAlignment.justified
//            case "center":paragraphStyle.alignment = NSTextAlignment.center
//            case "left":paragraphStyle.alignment = NSTextAlignment.left
//            case "right":paragraphStyle.alignment = NSTextAlignment.right
//            default:break
//        }
//        let messageText = NSMutableAttributedString(
//            string: message,
//            attributes: [
//                NSParagraphStyleAttributeName: paragraphStyle,
//                NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)
//            ]
//        )
//        alert.setValue(messageText, forKey: "attributedMessage")
//        //
//        delegate.present(alert, animated: true, completion: nil)
//        let when = DispatchTime.now() + delay
//        DispatchQueue.main.asyncAfter(deadline: when){
//            alert.dismiss(animated: true, completion: nil)
//        }
//    }
//
// }

class AlertCustom {
    var backGroundView = UIView()
    var containerView = UIView()
    var content = UILabel()

    func Alert(delegate: UIViewController, message: String, textAlignment: String, delay: Double) {
        let win: UIWindow = UIApplication.shared.delegate!.window!!
        backGroundView = UIView(frame: win.frame)
        backGroundView.tag = 1
        backGroundView.backgroundColor = UIColor(red: 0 / 255, green: 0 / 255, blue: 0 / 255, alpha: 0.6)
        win.addSubview(backGroundView)
        containerView = UIView(frame: CGRect(x: 0, y: 0, width: win.frame.width * 0.9, height: 40))
        containerView.center = backGroundView.center
        containerView.backgroundColor = UIColor(red: 255 / 255, green: 255 / 255, blue: 255 / 255, alpha: 1)
        containerView.layer.cornerRadius = 10
        backGroundView.addSubview(containerView)

        // containerView.layer.masksToBounds = true

        content.frame.origin.x = 15
        content.frame.origin.y = 0
        content.frame.size.width = containerView.frame.size.width - 30
        content.text = message
        // content.textAlignment = NSTextAlignment.justified
        switch textAlignment {
            case "center": content.textAlignment = NSTextAlignment.center
            case "left": content.textAlignment = NSTextAlignment.left
            case "right": content.textAlignment = NSTextAlignment.right
            default: content.textAlignment = NSTextAlignment.justified
        }
        content.lineBreakMode = NSLineBreakMode.byWordWrapping
        content.numberOfLines = 0
        content.textColor = UIColor(red: 0 / 255, green: 0 / 255, blue: 0 / 255, alpha: 1)
        content.sizeToFit()
        containerView.addSubview(content)

        // correct area
        containerView.frame.size.height = content.frame.size.height + 40
        content.frame.origin.y = 20
        containerView.center = backGroundView.center

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delay) {
            self.backGroundView.removeFromSuperview()
        }
    }

//    func removeAlert() {
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5){
//            self.backGroundView.removeFromSuperview()
//            self.containerView.removeFromSuperview()
//            self.content.removeFromSuperview()
//            let win:UIWindow = UIApplication.shared.delegate!.window!!
//            let removeView  = win.viewWithTag(1)
//            removeView?.removeFromSuperview()
//        }
//    }
}
