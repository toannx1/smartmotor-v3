//
//  SettingVCSMS.swift
//  SmartMotor
//
//  Created by Home on 4/19/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import MessageUI
import UIKit

extension SettingVC: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }

    func sendMessage(phoneNumber: String, content: String) {
        let messageVC = MFMessageComposeViewController()
        messageVC.messageComposeDelegate = self
        messageVC.body = content
        messageVC.recipients = [phoneNumber]
        self.present(messageVC, animated: false, completion: nil)
    }
}
