//
//  SettingServer.swift
//  SmartMotor
//
//  Created by Vietnv2 on 3/7/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import CryptoSwift
import UIKit

extension SettingVC: NSURLConnectionDelegate, XMLParserDelegate {
    func SoapHandShaking(commandIndex: String, inputContent: String) {
        commandControlString = inputContent
        Loading().showLoading()
        let soapMessage = String(format: "<?xml version=\"1.0\" encoding=\"utf-8\"?><Envelope xmlns=\"http://schemas.xmlsoap.org/soap/envelope/\"><Body><doCommand xmlns=\"http://mobilegateway.viettel.com/\"><token xmlns=\"\">%@</token><userId xmlns=\"\">%@</userId><transportId xmlns=\"\">%@</transportId><commandIndex xmlns=\"\">%@</commandIndex><inputContent xmlns=\"\">%@</inputContent></doCommand></Body></Envelope>", tokenSave, userIdSave, String(vehicle2show.id), commandIndex, inputContent)
        
        let urlString = smartmotorUrl + "/mgw/mobilegw"
        let url = URL(string: urlString)
        let theRequest = NSMutableURLRequest(url: url!)
        let msgLength = soapMessage.count
        theRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        theRequest.addValue("requestCommand", forHTTPHeaderField: "SOAPAction")
        theRequest.addValue(String(msgLength), forHTTPHeaderField: "Content-Length")
        theRequest.httpMethod = "POST"
        theRequest.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        theRequest.httpBody = soapMessage.data(using: String.Encoding.utf8, allowLossyConversion: false) // or false
        
        //
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        //
        let task = defaultSession.dataTask(with: theRequest as URLRequest) { data, response, error in
            if error != nil {
                Loading().hideLoading(delay: 0)
                DispatchQueue.main.async {
                    self.printUnSuccess()
                }
                return
            }
            if data == nil {
                Loading().hideLoading(delay: 0)
                DispatchQueue.main.async {
                    self.printUnSuccess()
                }
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    Loading().hideLoading(delay: 0)
                    DispatchQueue.main.async {
                        self.printUnSuccess()
                    }
                    return
                }
            }
            let xmlParser = XMLParser(data: data! as Data)
            xmlParser.delegate = self
            xmlParser.parse()
            xmlParser.shouldResolveExternalEntities = true
        }
        task.resume()
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String: String]) {
        currentElementName = elementName as NSString
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        // print(string)
        commandControlString = commandControlString.components(separatedBy: "=")[0]
//        DispatchQueue.main.asyncAfter(deadline: (DispatchTime.now() + 6)){
//            self.SendCommandControl(inputContent: commandControlString)
//        }
        
        if String(string) == "1" {
            countSendCommandControl = 0
            if commandControlString == "REBOOT" {
                alertCustom(message: commandControlStringReboot[languageChooseSave] + commandControlStringSuccess[languageChooseSave], delay: 2)
                return
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 8) {
                self.SendCommandControl(inputContent: commandControlString)
            }
        } else {
            Loading().hideLoading(delay: 0)
            printUnSuccess()
        }
    }
    
//    func serverHandShaking(commandIndex:String,inputContent:String) {
//        commandControlString = inputContent
//        Loading().showLoading()
//        let request:NSMutableURLRequest!
//        let url = URL(string: smartmotorUrl +  "/mtapi/rest/mobile/deviceCommand/controlDevice")
//        request = NSMutableURLRequest(url: url!)
//        request.httpMethod = postMethod
//        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
//        request.addValue(tokenSave, forHTTPHeaderField: "user-token")
//        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
//        let dataString:[String: String] = ["userId" : userIdSave,"transportId" : String(vehicle2show.id), "commandIndex" : commandIndex, "inputContent": inputContent] as Dictionary
//        request.httpBody = try! JSONSerialization.data(withJSONObject: dataString, options: [])
//        //
//        let defaultConfigObject = URLSessionConfiguration.default
//        defaultConfigObject.timeoutIntervalForRequest = 30
//        defaultConfigObject.timeoutIntervalForResource = 30
//        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
//
//        let task = defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
//            if error != nil {
//                Loading().hideLoading(delay: 0)
//                self.printUnSuccess()
//                return
//            }
//            if let httpResponse = response as? HTTPURLResponse {
//                if httpResponse.statusCode != 200 {
//                    Loading().hideLoading(delay: 0)
//                    self.printUnSuccess()
//                    return
//                }
//                do {
//                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
//                    print(jsonResult)
//                    let resultCode = jsonResult.value(forKey: "resultCode") as! Int
//                    if resultCode == 1 {
//                        if commandControlString == "REBOOT" {
//                            Loading().hideLoading(delay: 0)
//                            countSendCommandControl = 0
//                            self.printSuccess()
//                            return
//                        }
//                        DispatchQueue.main.asyncAfter(deadline: (DispatchTime.now() + 6)){
//                            commandControlString = commandControlString.components(separatedBy: "=")[0]
//                            self.SendCommandControl(inputContent: commandControlString)
//                        }
//                    }else {
//                        Loading().hideLoading(delay:0)
//                        self.printUnSuccess()
//                    }
//
//                }catch {
//                    fatalError("Failure\(error)")
//                }
//            }
//        }
//        task.resume()
//    }
    
    func SendCommandControl(inputContent: String) {
        let url = URL(string: smartmotorUrl + "/mtapi/rest/mobile/deviceCommand/getDeviceParam/" + userIdSave + "/" + String(vehicle2show.id) + "/" + inputContent)
        
        //
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(tokenSave, forHTTPHeaderField: "user-token")
        
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        let task = defaultSession.dataTask(with: request as URLRequest) { data, response, error in
            if error != nil {
                Loading().hideLoading(delay: 0)
                DispatchQueue.main.async {
                    self.printUnSuccess()
                }
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    // print(httpResponse.statusCode)
                    Loading().hideLoading(delay: 0)
                    DispatchQueue.main.async {
                        self.printUnSuccess()
                    }
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    // print(jsonResult)
                    let resultCode = jsonResult.value(forKey: "resultCode") as! Int
                    if resultCode != 1 {
                        countSendCommandControl = countSendCommandControl + 1
                        if countSendCommandControl < 3 {
                            // Recursion
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
                                self.SendCommandControl(inputContent: commandControlString)
                            }
                        } else {
                            countSendCommandControl = 0
                            Loading().hideLoading(delay: 0)
                            DispatchQueue.main.async {
                                self.printUnSuccess()
                            }
                        }
                        return
                    }
                    let jsonData = jsonResult.value(forKey: "data") as! NSDictionary
                    let paramName = jsonData.value(forKeyPath: "paramName") as! String
                    Loading().hideLoading(delay: 0)
                    if paramName == "REBOOT" || paramName == "USER" || paramName == "PSW" {
                        self.printSuccess()
                    } else {
                        self.printUnSuccess()
                    }
                } catch {
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    func changeAppPassword() {
        Loading().showLoading()
        var request = NSMutableURLRequest()
        let iv = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] as [UInt8]
        let encryptedBase64_OldPassword = try! txtOldPassword_ChangeAppPassword.text!.encrypt(cipher: AES(key: encrypKeyForLogin.bytes, blockMode: .CBC(iv: iv)))
        let encryptedBase64_NewPassword = try! txtNewPassword_ChangeAppPassword.text!.encrypt(cipher: AES(key: encrypKeyForLogin.bytes, blockMode: .CBC(iv: iv)))
        let url = URL(string: smartmotorUrl + "/mtapi/rest/auth/changePassword/")
        request = NSMutableURLRequest(url: url!)
        request.httpMethod = postMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        request.addValue(tokenSave, forHTTPHeaderField: "user-token")
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let dataString  = ["userId": userIdSave, "oldpassword": encryptedBase64_OldPassword, "newpassword": encryptedBase64_NewPassword] as Dictionary
        request.httpBody = try! JSONSerialization.data(withJSONObject: dataString, options: [])
        //
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        
        let task = defaultSession.dataTask(with: request as URLRequest) { data, response, error in
            Loading().hideLoading(delay: 0)
            if error != nil {
                Loading().hideLoading(delay: 0)
                self.printUnSuccess()
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    Loading().hideLoading(delay: 0)
                    self.printUnSuccess()
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let resultCode = jsonResult.value(forKey: "resultCode") as! Int
                    switch resultCode {
                        case -5:
                            Loading().hideLoading(delay: 0)
                            
                            self.showBannerNotifi(text: txtAlert07ChangeAppPassword[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
                            // self.alertCustom(message: txtAlert07ChangeAppPassword[languageChooseSave], delay: 2)
                            return
                        case -6, -7, -8, -9:
                            Loading().hideLoading(delay: 0)
                            self.showBannerNotifi(text: txtAlert08ChangeAppPassword[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
                            // self.alertCustom(message: txtAlert08ChangeAppPassword[languageChooseSave], delay: 2)
                            return
                        case -11:
                            Loading().hideLoading(delay: 0)
                            self.showBannerNotifi(text: txtAlert09ChangeAppPassword[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
                            // self.alertCustom(message: txtAlert09ChangeAppPassword[languageChooseSave], delay: 2)
                            return
                        default: // 1
                            Loading().hideLoading(delay: 0)
                            self.showBannerNotifi(text: txtAlert11ChangeAppPassword[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
                            // self.alertCustom(message: txtAlert11ChangeAppPassword[languageChooseSave], delay: 3)
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
                                logoutFlag = true
                                self.gotoLoginView()
                            }
                    }
                    
                } catch {
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    func printSuccess() {
        var string: String = commandControlString.components(separatedBy: "=")[0]
        switch string {
            case "REBOOT": string = commandControlStringReboot[languageChooseSave] + commandControlStringSuccess[languageChooseSave]
            case "USER=", "USER": string = commandControlStringChangeUserPhone[languageChooseSave] + commandControlStringSuccess[languageChooseSave]
            case "PSW=", "PSW": string = commandControlStringChangeVehiclePassword[languageChooseSave] + commandControlStringSuccess[languageChooseSave]
            default:
                return
        }
        alertCustom(message: string, delay: 5)
    }
    
    func printUnSuccess() {
        var string: String = commandControlString.components(separatedBy: "=")[0]
        switch string {
            case "REBOOT": string = commandControlStringReboot[languageChooseSave] + commandControlStringUnSuccess[languageChooseSave]
            case "USER=", "USER": string = commandControlStringChangeUserPhone[languageChooseSave] + commandControlStringUnSuccess[languageChooseSave]
            case "PSW=", "PSW": string = commandControlStringChangeVehiclePassword[languageChooseSave] + commandControlStringUnSuccess[languageChooseSave]
            default:
                return
        }
        alertCustom(message: string, delay: 5)
    }
}

//                    let createDate = jsonData.value(forKeyPath: "createDate") as! String
//                    let gpsCreateDate = jsonData.value(forKeyPath: "gpsCreateDate") as! String
//                    let id = jsonData.value(forKeyPath: "id") as! Int
//                    let imei = jsonData.value(forKeyPath: "imei") as! String
//                    let paramName = jsonData.value(forKeyPath: "paramName") as! String
//                    let paramValue = jsonData.value(forKeyPath: "paramValue") as! String
//                    let sysdate = jsonData.value(forKeyPath: "sysdate") as! String
