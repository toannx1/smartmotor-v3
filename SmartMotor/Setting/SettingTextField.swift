//
//  SettingTextField.swift
//  SmartMotor
//
//  Created by vietnv2 on 12/5/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

extension SettingVC: UITextFieldDelegate {
    //MARK: - init textfield
    func initTextField() {
        // lblRegNoTitle.text = String(format: txtTitleSettingView[languageChooseSave], vehicle2show.regNo).uppercased()
        
        let paddingFortxtOldPassword_ChangeAppPassword = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 30))
        paddingFortxtOldPassword_ChangeAppPassword.image = UIImage(named: "")
        txtOldPassword_ChangeAppPassword.rightView = paddingFortxtOldPassword_ChangeAppPassword
        txtOldPassword_ChangeAppPassword.rightViewMode = UITextField.ViewMode.always
        
        let paddingFortxtNewPassword_ChangeAppPassword = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 30))
        paddingFortxtNewPassword_ChangeAppPassword.image = UIImage(named: "")
        txtNewPassword_ChangeAppPassword.rightView = paddingFortxtNewPassword_ChangeAppPassword
        txtNewPassword_ChangeAppPassword.rightViewMode = UITextField.ViewMode.always
        
        let paddingFortxtConfirmNewPassword_ChangeAppPassword = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 30))
        paddingFortxtConfirmNewPassword_ChangeAppPassword.image = UIImage(named: "")
        txtConfirmNewPassword_ChangeAppPassword.rightView = paddingFortxtConfirmNewPassword_ChangeAppPassword
        txtConfirmNewPassword_ChangeAppPassword.rightViewMode = UITextField.ViewMode.always
        
        let paddingFortxtOldPassword_ChangeVehiclePassword = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 30))
        paddingFortxtOldPassword_ChangeVehiclePassword.image = UIImage(named: "")
        txtConfirmNewPassword_ChangeAppPassword.rightView = paddingFortxtOldPassword_ChangeVehiclePassword
        txtConfirmNewPassword_ChangeAppPassword.rightViewMode = UITextField.ViewMode.always
        
        let paddingFortxtNewPassword_ChangeVehiclePassword = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 30))
        paddingFortxtNewPassword_ChangeVehiclePassword.image = UIImage(named: "")
        txtConfirmNewPassword_ChangeAppPassword.rightView = paddingFortxtNewPassword_ChangeVehiclePassword
        txtConfirmNewPassword_ChangeAppPassword.rightViewMode = UITextField.ViewMode.always
        
        let paddingFortxtConfirmNewPassword_ChangeVehiclePassword = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 30))
        paddingFortxtConfirmNewPassword_ChangeVehiclePassword.image = UIImage(named: "")
        txtConfirmNewPassword_ChangeAppPassword.rightView = paddingFortxtConfirmNewPassword_ChangeVehiclePassword
        txtConfirmNewPassword_ChangeAppPassword.rightViewMode = UITextField.ViewMode.always
        
        txtFence.keyboardType = UIKeyboardType.numberPad
        txtSpeed.keyboardType = UIKeyboardType.numberPad
        //
        txtVehiclePassword_ChangeUserPhoneNumberView.keyboardType = UIKeyboardType.numberPad
        txtNewUserPhoneNumber_ChangeUserPhoneNumber.keyboardType = UIKeyboardType.numberPad
        txtOldPassword_Registry.keyboardType = UIKeyboardType.numberPad
        txtNewPassword_Registry.keyboardType = UIKeyboardType.numberPad
        txtConfirmNewPassword_Registry.keyboardType = UIKeyboardType.numberPad
        txtVehiclePassword.keyboardType = UIKeyboardType.numberPad
        //
        txtOldPassword_ChangeVehiclePassword.delegate = self
        txtNewPassword_ChangeVehiclePassword.delegate = self
        txtConfirmNewPassword_ChangeVehiclePassword.delegate = self
        txtOldPassword_ChangeAppPassword.delegate = self
        txtNewPassword_ChangeAppPassword.delegate = self
        txtConfirmNewPassword_ChangeAppPassword.delegate = self
        txtVehiclePassword_ChangeUserPhoneNumberView.delegate = self
        txtNewUserPhoneNumber_ChangeUserPhoneNumber.delegate = self
        txtOldPassword_Registry.delegate = self
        txtNewPassword_Registry.delegate = self
        txtConfirmNewPassword_Registry.delegate = self
        //
        txtVehiclePassword.delegate = self
        txtSpeed.delegate = self
        txtFence.delegate = self
        _ = textFieldShouldReturn(txtOldPassword_ChangeVehiclePassword)
        _ = textFieldShouldReturn(txtNewPassword_ChangeVehiclePassword)
        _ = textFieldShouldReturn(txtConfirmNewPassword_ChangeVehiclePassword)
        
        _ = textFieldShouldReturn(txtOldPassword_ChangeAppPassword)
        _ = textFieldShouldReturn(txtNewPassword_ChangeAppPassword)
        _ = textFieldShouldReturn(txtConfirmNewPassword_ChangeAppPassword)
        
        _ = textFieldShouldReturn(txtVehiclePassword_ChangeUserPhoneNumberView)
        _ = textFieldShouldReturn(txtNewUserPhoneNumber_ChangeUserPhoneNumber)
        
        _ = textFieldShouldReturn(txtOldPassword_Registry)
        _ = textFieldShouldReturn(txtNewPassword_Registry)
        _ = textFieldShouldReturn(txtConfirmNewPassword_Registry)
        
        _ = textFieldShouldReturn(txtOldPassword_Registry)
        _ = textFieldShouldReturn(txtNewPassword_Registry)
        _ = textFieldShouldReturn(txtConfirmNewPassword_Registry)
        _ = textFieldShouldReturn(txtVehiclePassword)
        _ = textFieldShouldReturn(txtSpeed)
        _ = textFieldShouldReturn(txtFence)
        //txtFence.text = "200"
        //txtSpeed.text = "60"
        
        btnClearVehicleRegistry1.addTarget(self, action: #selector(clearTextField(sender:)), for: .touchUpInside)
        btnClearVehicleRegistry2.addTarget(self, action: #selector(clearTextField(sender:)), for: .touchUpInside)
        btnClearVehicleRegistry3.addTarget(self, action: #selector(clearTextField(sender:)), for: .touchUpInside)
        
        txtOldPassword_Registry.keyboardDistanceFromTextField = 200
        txtNewPassword_Registry.keyboardDistanceFromTextField = 150
        txtConfirmNewPassword_Registry.keyboardDistanceFromTextField = 106
        
        btnChangeVehiclePassClear1.addTarget(self, action: #selector(clearTextField(sender:)), for: .touchUpInside)
        btnChangeVehiclePassClear2.addTarget(self, action: #selector(clearTextField(sender:)), for: .touchUpInside)
        btnChangeVehiclePassClear3.addTarget(self, action: #selector(clearTextField(sender:)), for: .touchUpInside)
        btnClearVehiclePass_ChangeUserPhone.addTarget(self, action: #selector(clearTextField(sender:)), for: .touchUpInside)
        btnClearPassConfirmAlarm.addTarget(self, action: #selector(clearTextField(sender:)), for: .touchUpInside)
        btnClearVehiclePass.addTarget(self, action: #selector(clearTextField(sender:)), for: .touchUpInside)
        
        txtOldPassword_Registry.keyboardDistanceFromTextField = 265
        txtNewPassword_Registry.keyboardDistanceFromTextField = 177
        txtConfirmNewPassword_Registry.keyboardDistanceFromTextField = 89
        
        txtOldPassword_ChangeVehiclePassword.keyboardDistanceFromTextField = 150
        txtNewPassword_ChangeVehiclePassword.keyboardDistanceFromTextField = 205
        txtConfirmNewPassword_ChangeVehiclePassword.keyboardDistanceFromTextField = 104
        
        txtVehiclePassword_ChangeUserPhoneNumberView.keyboardDistanceFromTextField = 204
        txtNewUserPhoneNumber_ChangeUserPhoneNumber.keyboardDistanceFromTextField = 86
        
        btnSecureVehiclePass.addTarget(self, action: #selector(securePass(sender:)), for: .touchUpInside)
        txtVehiclePassword.keyboardDistanceFromTextField = 70
    }
    
    @objc func securePass(sender: UIButton) {
        if txtVehiclePassword.isSecureTextEntry {
            btnSecureVehiclePass.setImage(UIImage(named: "invisible_eye_dark"), for: .normal)
        } else {
            btnSecureVehiclePass.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
        }
        txtVehiclePassword.isSecureTextEntry = !txtVehiclePassword.isSecureTextEntry
    }
    
    @objc func clearTextField(sender: UIButton) {
        switch sender {
        case btnClearVehicleRegistry1:
            txtOldPassword_Registry.text = ""
            btnClearVehicleRegistry1.isHidden = true
            btnSecurityOldPasswordVehicleRegistry.isHidden = true
        case btnClearVehicleRegistry2:
            txtNewPassword_Registry.text = ""
            btnClearVehicleRegistry2.isHidden = true
            btnSecurityNewPasswordVehicleRegistry.isHidden = true
        case btnClearVehicleRegistry3:
            txtConfirmNewPassword_Registry.text = ""
            btnClearVehicleRegistry3.isHidden = true
            btnSecurityConfirmNewPasswordVehicleRegistry.isHidden = true
        case btnChangeVehiclePassClear1:
            txtOldPassword_ChangeVehiclePassword.text = ""
            btnChangeVehiclePassClear1.isHidden = true
            btnSecurityOldPassword_ChangeVehiclePassword.isHidden = true
        case btnChangeVehiclePassClear2:
            txtNewPassword_ChangeVehiclePassword.text = ""
            btnChangeVehiclePassClear2.isHidden = true
            btnSecurityNewPassword_ChangeVehiclePassword.isHidden = true
        case btnChangeVehiclePassClear3:
            txtConfirmNewPassword_ChangeVehiclePassword.text = ""
            btnChangeVehiclePassClear3.isHidden = true
            btnSecurityConfirmNewPassword_ChangeVehiclePassword.isHidden = true
        case btnClearVehiclePass_ChangeUserPhone:
            txtVehiclePassword_ChangeUserPhoneNumberView.text = ""
            btnClearVehiclePass_ChangeUserPhone.isHidden = true
            btnSecurityVehiclePassword_ChangeUser.isHidden = true
        case btnClearPassConfirmAlarm:
            txtVehiclePassword.text = ""
            btnClearPassConfirmAlarm.isHidden = true
            btnSecurityVehiclePassword.isHidden = true
            lbWarningError.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        case btnClearVehiclePass:
            btnClearVehiclePass.isHidden = true
            btnSecureVehiclePass.isHidden = true
            txtVehiclePassword.text = ""
        default:
            break
        }
    }
    
    // MARK: - Textfield Setting
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        // Register view
        case txtOldPassword_Registry:
            if txtOldPassword_Registry.text!.count > 0 {
                btnClearVehicleRegistry1.isHidden = false
                btnSecurityOldPasswordVehicleRegistry.isHidden = false
            }
            btnClearVehicleRegistry2.isHidden = true
            btnSecurityNewPasswordVehicleRegistry.isHidden = true
            btnClearVehicleRegistry3.isHidden = true
            btnSecurityConfirmNewPasswordVehicleRegistry.isHidden = true
            
        case txtNewPassword_Registry:
            if txtNewPassword_Registry.text!.count > 0 {
                btnClearVehicleRegistry2.isHidden = false
                btnSecurityNewPasswordVehicleRegistry.isHidden = false
            }
            btnClearVehicleRegistry1.isHidden = true
            btnSecurityOldPasswordVehicleRegistry.isHidden = true
            btnClearVehicleRegistry3.isHidden = true
            btnSecurityConfirmNewPasswordVehicleRegistry.isHidden = true
            
        case txtConfirmNewPassword_Registry:
            if txtConfirmNewPassword_Registry.text!.count > 0 {
                btnClearVehicleRegistry3.isHidden = false
                btnSecurityConfirmNewPasswordVehicleRegistry.isHidden = false
            }
            btnClearVehicleRegistry1.isHidden = true
            btnSecurityOldPasswordVehicleRegistry.isHidden = true
            btnClearVehicleRegistry2.isHidden = true
            btnSecurityNewPasswordVehicleRegistry.isHidden = true
            
        // Change Password device view
        case txtOldPassword_ChangeVehiclePassword:
            if txtOldPassword_ChangeVehiclePassword.text!.count > 0 {
                btnChangeVehiclePassClear1.isHidden = false
                btnSecurityOldPassword_ChangeVehiclePassword.isHidden = false
            }
            btnChangeVehiclePassClear2.isHidden = true
            btnSecurityNewPassword_ChangeVehiclePassword.isHidden = true
            btnChangeVehiclePassClear3.isHidden = true
            btnSecurityConfirmNewPassword_ChangeVehiclePassword.isHidden = true
            
        case txtNewPassword_ChangeVehiclePassword:
            if txtNewPassword_ChangeVehiclePassword.text!.count > 0 {
                btnChangeVehiclePassClear2.isHidden = false
                btnSecurityNewPassword_ChangeVehiclePassword.isHidden = false
            }
            btnChangeVehiclePassClear1.isHidden = true
            btnSecurityOldPassword_ChangeVehiclePassword.isHidden = true
            btnChangeVehiclePassClear3.isHidden = true
            btnSecurityConfirmNewPassword_ChangeVehiclePassword.isHidden = true
            
        case txtConfirmNewPassword_ChangeVehiclePassword:
            if txtConfirmNewPassword_ChangeVehiclePassword.text!.count > 0 {
                btnChangeVehiclePassClear3.isHidden = false
                btnSecurityConfirmNewPassword_ChangeVehiclePassword.isHidden = false
            }
            btnChangeVehiclePassClear2.isHidden = true
            btnSecurityNewPassword_ChangeVehiclePassword.isHidden = true
            btnChangeVehiclePassClear1.isHidden = true
            btnSecurityOldPassword_ChangeVehiclePassword.isHidden = true
            
        // Change User
        case txtVehiclePassword_ChangeUserPhoneNumberView:
            if txtVehiclePassword_ChangeUserPhoneNumberView.text!.count > 0 {
                btnClearVehiclePass_ChangeUserPhone.isHidden = false
                btnSecurityVehiclePassword_ChangeUser.isHidden = false
            }
        case txtNewUserPhoneNumber_ChangeUserPhoneNumber:
            btnClearVehiclePass_ChangeUserPhone.isHidden = true
            btnSecurityVehiclePassword_ChangeUser.isHidden = true
            
        // Setting Warning
        case txtVehiclePassword:
            if txtVehiclePassword.text!.count > 1 {
                btnClearVehiclePass.isHidden = false
                btnSecureVehiclePass.isHidden = false
            }
        default:
            break
        }
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool { // called when 'return' key pressed. return false to ignore.
        textField.resignFirstResponder()
        keyBoardHide()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textFieldCurrent = textField
        var viewHeight = view.frame.size.height - keyboardHeight
        if keyboardHeight > 216 {
            viewHeight = viewHeight + 44
        }
        switch textField {
        case txtFence:
//            fenceAlarmSettingView.frame.size.height = viewHeight
            flagStartEndEditForTextField = flagStartEndEditForTextField + 1
        case txtSpeed:
//            speedAlarmSettingView.frame.size.height = viewHeight
            flagStartEndEditForTextField = flagStartEndEditForTextField + 1
        case txtOldPassword_Registry, txtNewPassword_Registry, txtConfirmNewPassword_Registry:
//            vehicleRegistryView.frame.size.height = viewHeight
            flagStartEndEditForTextField = flagStartEndEditForTextField + 1
        case txtOldPassword_ChangeVehiclePassword, txtNewPassword_ChangeVehiclePassword, txtConfirmNewPassword_ChangeVehiclePassword:
//            changeVehiclePasswordView.frame.size.height = viewHeight
//            bottomScrollVehiclePassConstraint.constant = keyboardHeight
            flagStartEndEditForTextField = flagStartEndEditForTextField + 1
        case txtOldPassword_ChangeAppPassword, txtNewPassword_ChangeAppPassword, txtConfirmNewPassword_ChangeAppPassword:
//            changeAppPasswordView.frame.size.height = viewHeight
            flagStartEndEditForTextField = flagStartEndEditForTextField + 1
        case txtNewUserPhoneNumber_ChangeUserPhoneNumber:
//            changeUserPhoneNumberView.frame.size.height = viewHeight
            flagStartEndEditForTextField = flagStartEndEditForTextField + 1
        case txtVehiclePassword_ChangeUserPhoneNumberView:
//            changeVehiclePasswordView.frame.size.height = viewHeight
            flagStartEndEditForTextField = flagStartEndEditForTextField + 1
        case txtVehiclePassword:
//            askPasswordView.frame.size.height = viewHeight
            flagStartEndEditForTextField = flagStartEndEditForTextField + 1
        default:
            break
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        // change vehicle pass
        case txtOldPassword_ChangeVehiclePassword:
            if txtOldPassword_ChangeVehiclePassword.text!.count < 6 {
                lbChangeVehiclePassError1.backgroundColor = #colorLiteral(red: 0.9460067153, green: 0.5744947195, blue: 0.1601729095, alpha: 1)
                lbChangeVehiclePassError1.text = "Vui lòng nhập mật khẩu hiện tại gồm 6 ký tự số!"
            } else if txtOldPassword_ChangeVehiclePassword.text!.count == 6 {
                lbChangeVehiclePassError1.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
            
            btnChangeVehiclePassClear1.isHidden = true
            btnSecurityOldPassword_ChangeVehiclePassword.isHidden = true
        case txtNewPassword_ChangeVehiclePassword:
            if txtNewPassword_ChangeVehiclePassword.text!.count < 6 {
                lbChangeVehiclePassError2.backgroundColor = #colorLiteral(red: 0.9460067153, green: 0.5744947195, blue: 0.1601729095, alpha: 1)
                lbChangeVehiclePassError2.text = "Vui lòng nhập mật khẩu mới gồm 6 ký tự số!"
            } else if txtNewPassword_ChangeVehiclePassword.text!.count == 6 {
                lbChangeVehiclePassError2.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
            
            btnChangeVehiclePassClear2.isHidden = true
            btnSecurityNewPassword_ChangeVehiclePassword.isHidden = true
        case txtConfirmNewPassword_ChangeVehiclePassword:
            if txtConfirmNewPassword_ChangeVehiclePassword.text!.count < 6 {
                lbChangeVehiclePassError3.backgroundColor = #colorLiteral(red: 0.9460067153, green: 0.5744947195, blue: 0.1601729095, alpha: 1)
                lbChangeVehiclePassError3.text = "Vui lòng xác nhận mật khẩu mới!"
            } else if txtConfirmNewPassword_ChangeVehiclePassword.text!.count == 6 {
                lbChangeVehiclePassError3.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
            
            btnChangeVehiclePassClear3.isHidden = true
            btnSecurityConfirmNewPassword_ChangeVehiclePassword.isHidden = true
            
        // register device
        case txtOldPassword_Registry:
            if txtOldPassword_Registry.text!.count < 6 {
                lbRegistryError1.isHidden = false
                lbRegistryError1.text = "Vui lòng nhập mật khẩu hiện tại gồm 6 ký tự số!"
            } else if txtOldPassword_Registry.text!.count == 6 {
                lbRegistryError1.isHidden = true
            }
            btnClearVehicleRegistry1.isHidden = true
            btnSecurityOldPasswordVehicleRegistry.isHidden = true
        case txtNewPassword_Registry:
            if txtNewPassword_Registry.text!.count < 6 {
                lbRegistryError2.isHidden = false
                lbRegistryError2.text = "Vui lòng nhập mật khẩu mới gồm 6 ký tự số!"
            } else if txtNewPassword_Registry.text!.count == 6 {
                lbRegistryError2.isHidden = true
            }
            btnClearVehicleRegistry2.isHidden = true
            btnSecurityNewPasswordVehicleRegistry.isHidden = true
        case txtConfirmNewPassword_Registry:
            if txtConfirmNewPassword_Registry.text!.count < 6 {
                lbRegistryError3.isHidden = false
                lbRegistryError3.text = "Vui lòng xác nhận mật khẩu mới!"
            } else if txtConfirmNewPassword_Registry.text!.count == 6 {
                lbRegistryError3.isHidden = true
            }
            btnClearVehicleRegistry3.isHidden = true
            btnSecurityConfirmNewPasswordVehicleRegistry.isHidden = true
            
        // Change User permision
        case txtVehiclePassword_ChangeUserPhoneNumberView:
            if txtVehiclePassword_ChangeUserPhoneNumberView.text!.count < 6 {
                lblChangeUser_PassError.backgroundColor = #colorLiteral(red: 0.9460067153, green: 0.5744947195, blue: 0.1601729095, alpha: 1)
            } else if txtVehiclePassword_ChangeUserPhoneNumberView.text!.count == 6 {
                lblChangeUser_PassError.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
            btnSecurityVehiclePassword_ChangeUser.isHidden = true
            btnClearVehiclePass_ChangeUserPhone.isHidden = true
        case txtNewUserPhoneNumber_ChangeUserPhoneNumber:
            if txtNewUserPhoneNumber_ChangeUserPhoneNumber.text!.count < 10 {
                lblChangeUser_PhoneError.backgroundColor = #colorLiteral(red: 0.9460067153, green: 0.5744947195, blue: 0.1601729095, alpha: 1)
            } else if txtNewUserPhoneNumber_ChangeUserPhoneNumber.text!.count == 10 {
                lblChangeUser_PhoneError.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
            
        // Setting-Warning
        case txtVehiclePassword:
            if txtVehiclePassword.text!.count < 6 {
                lbWarningError.backgroundColor = #colorLiteral(red: 0.9460067153, green: 0.5744947195, blue: 0.1601729095, alpha: 1)
            } else {
                lbWarningError.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
            btnClearVehiclePass.isHidden = true
            btnSecureVehiclePass.isHidden = true
        default:
            break
        }
        
        switch textField {
        case txtFence:
            flagStartEndEditForTextField = flagStartEndEditForTextField - 1
            fenceAlarmSettingView.frame.size.height = view.frame.size.height
        case txtSpeed:
            flagStartEndEditForTextField = flagStartEndEditForTextField - 1
            speedAlarmSettingView.frame.size.height = view.frame.size.height
        case txtOldPassword_Registry, txtNewPassword_Registry, txtConfirmNewPassword_Registry:
            flagStartEndEditForTextField = flagStartEndEditForTextField - 1
            if flagStartEndEditForTextField == 0 {
                vehicleRegistryView.frame.size.height = view.frame.size.height
            }
        case txtOldPassword_ChangeVehiclePassword, txtNewPassword_ChangeVehiclePassword, txtConfirmNewPassword_ChangeVehiclePassword:
            flagStartEndEditForTextField = flagStartEndEditForTextField - 1
            if flagStartEndEditForTextField == 0 {
                changeVehiclePasswordView.frame.size.height = view.frame.size.height
            }
        case txtOldPassword_ChangeAppPassword, txtNewPassword_ChangeAppPassword, txtConfirmNewPassword_ChangeAppPassword:
            flagStartEndEditForTextField = flagStartEndEditForTextField - 1
            if flagStartEndEditForTextField == 0 {
                changeAppPasswordView.frame.size.height = view.frame.size.height
            }
        case txtNewUserPhoneNumber_ChangeUserPhoneNumber, txtVehiclePassword_ChangeUserPhoneNumberView:
            flagStartEndEditForTextField = flagStartEndEditForTextField - 1
            if flagStartEndEditForTextField == 0 {
                changeUserPhoneNumberView.frame.size.height = view.frame.size.height
            }
        case txtVehiclePassword:
            flagStartEndEditForTextField = flagStartEndEditForTextField - 1
            if flagStartEndEditForTextField == 0 {
                askPasswordView.frame.size.height = view.frame.size.height
            }
        default:
            break
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text
        let newLength = (text?.count)! + string.count - range.length
        let flag: Bool!
        flag = !(newLength > 0)
        
        if textField.text!.count <= 6 {
            if textField == txtOldPassword_ChangeVehiclePassword || textField == txtNewPassword_ChangeVehiclePassword || textField == txtConfirmNewPassword_ChangeVehiclePassword || textField == txtOldPassword_Registry || textField == txtNewPassword_Registry || textField == txtConfirmNewPassword_Registry || textField == txtVehiclePassword_ChangeUserPhoneNumberView {
                let set = NSCharacterSet(charactersIn: "0123456789").inverted
                return string.rangeOfCharacter(from: set) == nil
            }
            
            if txtOldPassword_ChangeVehiclePassword.text!.count < 1 {
                btnSecurityOldPassword_ChangeVehiclePassword.isHidden = true
                btnChangeVehiclePassClear1.isHidden = true
            } else if txtNewPassword_ChangeVehiclePassword.text!.count < 1 {
                btnSecurityNewPassword_ChangeVehiclePassword.isHidden = true
                btnChangeVehiclePassClear2.isHidden = true
            } else if txtConfirmNewPassword_ChangeVehiclePassword.text!.count < 1 {
                btnSecurityConfirmNewPassword_ChangeVehiclePassword.isHidden = true
                btnChangeVehiclePassClear3.isHidden = true
            }
        } else if textField.text!.count < 10 {
            if textField == txtNewUserPhoneNumber_ChangeUserPhoneNumber {
                let set = NSCharacterSet(charactersIn: "0123456789").inverted
                return string.rangeOfCharacter(from: set) == nil
            }
        }
        
        if textField.text!.count < 6, textField == txtVehiclePassword {
            let set = NSCharacterSet(charactersIn: "0123456789").inverted
            return string.rangeOfCharacter(from: set) == nil
        }
        if textField.text!.count < 5, textField == txtFence {
            let set = NSCharacterSet(charactersIn: "0123456789").inverted
            return string.rangeOfCharacter(from: set) == nil
        }
        if textField.text!.count < 3, textField == txtSpeed {
            let set = NSCharacterSet(charactersIn: "0123456789").inverted
            return string.rangeOfCharacter(from: set) == nil
        }
        
        // Check kilometer - Setting
        func limitNumber(txtString: String!) -> Int {
            var startString = ""
            if txtString != nil {
                startString += textField.text!
            }
            startString += string
            let limitNum = Int(startString)
            return limitNum!
        }
        
        switch textField {
        case txtOldPassword_Registry:
            return newLength <= 6
        case txtNewPassword_Registry:
//            btnSecurityNewPasswordVehicleRegistry.isHidden = flag
//            btnClearVehicleRegistry2.isHidden = flag
            return newLength <= 6
        case txtConfirmNewPassword_Registry:
//            btnSecurityConfirmNewPasswordVehicleRegistry.isHidden = flag
//            btnClearVehicleRegistry3.isHidden = flag
            return newLength <= 6
        case txtOldPassword_ChangeVehiclePassword:
            btnSecurityOldPassword_ChangeVehiclePassword.isHidden = flag
            btnChangeVehiclePassClear1.isHidden = flag
            return newLength <= 6
        case txtNewPassword_ChangeVehiclePassword:
            btnSecurityNewPassword_ChangeVehiclePassword.isHidden = flag
            btnChangeVehiclePassClear2.isHidden = flag
            return newLength <= 6
        case txtConfirmNewPassword_ChangeVehiclePassword:
            btnSecurityConfirmNewPassword_ChangeVehiclePassword.isHidden = flag
            btnChangeVehiclePassClear3.isHidden = flag
            return newLength <= 6
        case txtOldPassword_ChangeAppPassword:
            btnSecurityOldPassword_ChangeAppPassword.isHidden = flag
            return newLength <= 20
        case txtNewPassword_ChangeAppPassword:
            btnSecurityNewPassword_ChangeAppPassword.isHidden = flag
            return newLength <= 20
        case txtConfirmNewPassword_ChangeAppPassword:
            btnSecurityConfirmNewPassword_ChangeAppPassword.isHidden = flag
            return newLength <= 20
        case txtVehiclePassword_ChangeUserPhoneNumberView:
            btnSecurityVehiclePassword_ChangeUser.isHidden = flag
            btnClearVehiclePass_ChangeUserPhone.isHidden = flag
            return newLength <= 6
        case txtNewUserPhoneNumber_ChangeUserPhoneNumber:
            return newLength <= 10
        case txtVehiclePassword:
//            btnSecureVehiclePass.isHidden = flag
//            btnClearVehiclePass.isHidden = flag
            if txtVehiclePassword.text!.count > 0 {
                btnSecureVehiclePass.isHidden = false
                btnClearVehiclePass.isHidden = false
            }
            return newLength <= 6
        case txtFence: // 100 - 10000
            return !(limitNumber(txtString: textField.text) > 99999)
        case txtSpeed: // 20 - 150
            return !(limitNumber(txtString: textField.text) > 999)
        default:
            break
        }
        
        return true
    }
    
    // MARK: - Keyboard
    
    func getKeyBoardHeight() {
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func keyBoardHide() {
//        self.view.endEditing(true)
//        UIView.animate(withDuration: 0.5) {
//            self.view.layoutIfNeeded()
//        }
    }
}
