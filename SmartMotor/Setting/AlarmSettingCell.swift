//
//  AlarmSettingCell.swift
//  Smart Motor
//
//  Created by vietnv2 on 27/10/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

class AlarmSettingCell: UITableViewCell {
    @IBOutlet var lblAlarm: UILabel!
    @IBOutlet var imgAlarm: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
