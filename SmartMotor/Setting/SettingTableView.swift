//
//  SettingTableView.swift
//  SmartMotor
//
//  Created by vietnv2 on 12/5/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import MessageUI
import UIKit

extension SettingVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.isScrollEnabled = false
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRow: Int!
        if tableView == settingTableView {
            numberOfRow = 6 // disappear Language Item
            // numberOfRow = 8
            // numberOfRow = 9
        } else if tableView == alarmTableView {
            numberOfRow = 4 //txtAlarmSettingArray.count
        }
        return numberOfRow
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell2Return: UITableViewCell!
        if tableView == settingTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell", for: indexPath) as! SettingCustumCell
            cell.imgIcon.image = settingImageArray[indexPath.row]
            switch indexPath.row {
            case 0:
                cell.lblSetting.text = txtSettingArray_DeviceActive[languageChooseSave]
            case 1:
                cell.lblSetting.text = txtSettingArray_ChangeDevicePassword[languageChooseSave]
            case 2:
                cell.lblSetting.text = txtSettingArray_ChangeUserPhone[languageChooseSave]
            case 3:
                cell.lblSetting.text = txtSettingArray_Reboot[languageChooseSave]
            case 4:
                cell.lblSetting.text = txtSettingArray_ChangeConfig[languageChooseSave]
            case 5:
                cell.lblSetting.text = txtSettingArray_FactoryReset[languageChooseSave]
//            case 6:
//                cell.lblSetting.text = txtSettingArray_Feedback[languageChooseSave]
//            case 7:
//                cell.lblSetting.text = txtSettingArray_Language[languageChooseSave]
            default:
                break
            }
            
            cell2Return = cell
        } else if tableView == alarmTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "alarmCell", for: indexPath) as! AlarmSettingCell
            cell.imgAlarm.image = alarmImageArray[indexPath.row]
            switch indexPath.row {
            case 0:
                cell.lblAlarm.text = txtAlarmSettingArray_AccAlert[languageChooseSave]
            case 1:
                cell.lblAlarm.text = txtAlarmSettingArray_VibrateAlert[languageChooseSave]
            case 2:
                cell.lblAlarm.text = txtAlarmSettingArray_FenceAlert[languageChooseSave]
            case 3:
                cell.lblAlarm.text = txtAlarmSettingArray_OverSpeedAlert[languageChooseSave]
            default:
                break
            }
            cell.selectionStyle = .none
            cell2Return = cell
        }
        return cell2Return
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == settingTableView {
            switch indexPath.row {
            case 0: displayVehicleRegistryView()
            case 1: displayChangeVehiclePasswordView()
            case 2: displayChangeUserPhoneNumberView()
            case 3: displayDeviceRebootView()
            case 4: displayListAlarmSettingView()
            case 5: displayDeviceFormatView()
//            case 6:sendEmail()
//            case 7:displayChooseLanguageView()
            default: break
            }
        } else if tableView == alarmTableView {
//            removeListAlarmSettingView()
            switch indexPath.row {
            case 0: displayAccAlarmSettingView()
            case 1: displayVibrateAlarmSettingView()
            case 2: displayFenceAlarmSettingView()
            case 3: displaySpeedAlarmSettingView()
            default: break
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat!
        if tableView == settingTableView {
            height = 60
        } else if tableView == alarmTableView {
            height = 60
        }
        return height
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = UIColor.lightGray
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = .clear
    }
}

extension SettingVC: MFMailComposeViewControllerDelegate {
    func sendEmail() {
        if !MFMailComposeViewController.canSendMail() {
            // print("Mail services are not available")
            return
        }
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        // Configure the fields of the interface.
        composeVC.setToRecipients(["cskh@viettel.com.vn"])
        composeVC.setSubject(txtEmailSubject[languageChooseSave])
        // composeVC.setMessageBody("Hello this is my message body!", isHTML: false)
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            // print("Mail cancelled")
            break
        case MFMailComposeResult.saved.rawValue:
            // print("Mail saved")
            break
        case MFMailComposeResult.sent.rawValue:
            // print("Mail sent")
            break
        case MFMailComposeResult.failed.rawValue:
            // print("Mail sent failure: %@", [error?.localizedDescription])
            break
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
}
