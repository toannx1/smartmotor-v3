//
//  SettingVC.swift
//  Smart Motor
//
//  Created by vietnv2 on 26/10/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import MessageUI
import UIKit

class SettingVC: UIViewController {
    @IBOutlet var btnClearVehiclePass: UIButton!
    @IBOutlet var btnSecureVehiclePass: UIButton!
    @IBOutlet var bottomScrollVehiclePassConstraint: NSLayoutConstraint!
    @IBOutlet var btnClearPassConfirmAlarm: UIButton!
    
    @IBOutlet var switchAlarmSpeed_Siren: UISwitch!
    @IBOutlet var switchAlarmSpeed_Call: UISwitch!
    @IBOutlet var switchAlarmSpeed_SMS: UISwitch!
    @IBOutlet var switchAlarmSpeed_Speed: UISwitch!
    @IBOutlet var switchAlarmFence_Siren: UISwitch!
    @IBOutlet var switchAlarmFence_Call: UISwitch!
    @IBOutlet var switchAlarmFence_SMS: UISwitch!
    @IBOutlet var switchAlarmFence_Fence: UISwitch!
    @IBOutlet var switchAlarmVib_Siren: UISwitch!
    @IBOutlet var switchAlarmVib_Call: UISwitch!
    @IBOutlet var switchAlarmVib_SMS: UISwitch!
    @IBOutlet var switchAlarmVib_Vib: UISwitch!
    @IBOutlet var switchAlarmSiren: UISwitch!
    @IBOutlet var switchAlarmCall: UISwitch!
    @IBOutlet var switchAlarmSMS: UISwitch!
    @IBOutlet var switchAlarmAlarm: UISwitch!
    
    @IBOutlet var viewOldPass_ChangeVehiclePas: UIView!
    @IBOutlet var viewVehiclePass_ChangeUser: UIView!
    @IBOutlet var btnClearVehiclePass_ChangeUserPhone: UIButton!
    @IBOutlet var lblChangeUser_PhoneError: UILabel!
    @IBOutlet var lblChangeUser_Description: UILabel!
    @IBOutlet var lblChangeUser_PassError: UILabel!
    @IBOutlet var lblOldPasswordDescription: UILabel!
    @IBOutlet var btnChangeVehiclePassClear3: UIButton!
    @IBOutlet var btnChangeVehiclePassClear2: UIButton!
    @IBOutlet var btnChangeVehiclePassClear1: UIButton!
    @IBOutlet var lbChangeVehiclePassError3: UILabel!
    @IBOutlet var lbChangeVehiclePassError2: UILabel!
    @IBOutlet var lbChangeVehiclePassError1: UILabel!
    
    @IBOutlet var lbRegistryError3: UILabel!
    @IBOutlet var lbRegistryError2: UILabel!
    @IBOutlet var lbRegistryError1: UILabel!
    @IBOutlet var btnClearVehicleRegistry1: UIButton!
    
    @IBOutlet var btnClearVehicleRegistry2: UIButton!
    
    @IBOutlet var btnClearVehicleRegistry3: UIButton!
    @IBOutlet var settingTableView: UITableView!
    @IBOutlet var alarmTableView: UITableView!
    @IBOutlet var titleSettingView: UIView!
    @IBOutlet var btnCheckBoxAccAlarm: UIButton!
    @IBOutlet var btnCheckBoxAccSMS: UIButton!
    @IBOutlet var btnCheckBoxAccCall: UIButton!
    @IBOutlet var btnCheckBoxAccSiren: UIButton!
    @IBOutlet var btnCheckBoxVibrateAlarm: UIButton!
    @IBOutlet var btnCheckBoxVibrateSMS: UIButton!
    @IBOutlet var btnCheckBoxVibrateCall: UIButton!
    @IBOutlet var btnCheckBoxVibrateSiren: UIButton!
    @IBOutlet var btnCheckBoxFenceAlarm: UIButton!
    @IBOutlet var btnCheckBoxFenceSMS: UIButton!
    @IBOutlet var btnCheckBoxFenceCall: UIButton!
    @IBOutlet var btnCheckBoxFenceSiren: UIButton!
    @IBOutlet var txtFence: UITextField!
    @IBOutlet var btnCheckBoxSpeedAlarm: UIButton!
    @IBOutlet var btnCheckBoxSpeedSMS: UIButton!
    @IBOutlet var btnCheckBoxSpeedCall: UIButton!
    @IBOutlet var btnCheckBoxSpeedSiren: UIButton!
    @IBOutlet var txtSpeed: UITextField!
    @IBOutlet var txtOldPassword_ChangeVehiclePassword: UITextField!
    @IBOutlet var txtNewPassword_ChangeVehiclePassword: UITextField!
    @IBOutlet var txtConfirmNewPassword_ChangeVehiclePassword: UITextField!
    @IBOutlet var txtOldPassword_ChangeAppPassword: UITextField!
    @IBOutlet var txtNewPassword_ChangeAppPassword: UITextField!
    @IBOutlet var txtConfirmNewPassword_ChangeAppPassword: UITextField!
    @IBOutlet var txtOldPassword_Registry: UITextField!
    @IBOutlet var txtNewPassword_Registry: UITextField!
    @IBOutlet var txtConfirmNewPassword_Registry: UITextField!
    @IBOutlet var lblRegNoTitle: UILabel!
    @IBOutlet var lblVehicleRegistrySim: UILabel!
    @IBOutlet var btnSecurityVehiclePassword: UIButton!
    @IBOutlet var btnSecurityVehiclePassword_ChangeUser: UIButton!
    @IBOutlet var btnSecurityOldPassword_ChangeVehiclePassword: UIButton!
    @IBOutlet var btnSecurityNewPassword_ChangeVehiclePassword: UIButton!
    @IBOutlet var btnSecurityConfirmNewPassword_ChangeVehiclePassword: UIButton!
    @IBOutlet var btnSecurityOldPassword_ChangeAppPassword: UIButton!
    @IBOutlet var btnSecurityNewPassword_ChangeAppPassword: UIButton!
    @IBOutlet var btnSecurityConfirmNewPassword_ChangeAppPassword: UIButton!
    @IBOutlet var btnSecurityOldPasswordVehicleRegistry: UIButton!
    @IBOutlet var btnSecurityNewPasswordVehicleRegistry: UIButton!
    @IBOutlet var btnSecurityConfirmNewPasswordVehicleRegistry: UIButton!
    @IBOutlet var txtVehiclePassword: UITextField!
    
    @IBOutlet var lblTitleVehicleRegistry: UILabel!
//    @IBOutlet weak var lblContent01VehicleRegistry: UILabel!
    @IBOutlet var lblContent02VehicleRegistry: UILabel!
    @IBOutlet var lblContent03VehicleRegistry: UILabel!
    @IBOutlet var lblContent04VehicleRegistry: UILabel!
    @IBOutlet var btnConfirmVehicleRegistry: UIButton!
    @IBOutlet var btnCancelVehicleRegistry: UIButton!
    
    @IBOutlet var lblTitleChangeAppPassword: UILabel!
    @IBOutlet var lblContent01ChangeAppPassword: UILabel!
    @IBOutlet var lblContent02ChangeAppPassword: UILabel!
    @IBOutlet var lblContent03ChangeAppPassword: UILabel!
    @IBOutlet var btnConfirmChangeAppPassword: UIButton!
    @IBOutlet var btnCancelChangeAppPassword: UIButton!
    
    @IBOutlet var lblTitleChangeVehiclePassword: UILabel!
    @IBOutlet var lblContent01ChangeVehiclePassword: UILabel!
    @IBOutlet var lblContent02ChangeVehiclePassword: UILabel!
    @IBOutlet var lblContent03ChangeVehiclePassword: UILabel!
    @IBOutlet var btnConfirmChangeVehiclePassword: UIButton!
    @IBOutlet var btnCancelChangeVehiclePassword: UIButton!
    
    @IBOutlet var lblTitleChangeUserPhone: UILabel!
    @IBOutlet var lblContent01ChangeUserPhone: UILabel!
    @IBOutlet var lblContent02ChangeUserPhone: UILabel!
    @IBOutlet var btnConfirmChangeUserPhone: UIButton!
    @IBOutlet var btnCancelChangeUserPhone: UIButton!
    
    @IBOutlet var lblTitleDeviceReboot: UILabel!
    @IBOutlet var lblContentDeviceReboot: UILabel!
    @IBOutlet var btnConfirmDeviceReboot: UIButton!
    @IBOutlet var btnCancelDeviceReboot: UIButton!
    
    @IBOutlet var lblTitleListAlarmSetting: UILabel!
    @IBOutlet var btnCancelListAlarmSetting: UIButton!
    
    @IBOutlet var lblTitleAccAlarmSetting: UILabel!
    @IBOutlet var lblContent01AccAlarmSetting: UILabel!
    @IBOutlet var lblContent02AccAlarmSetting: UILabel!
    @IBOutlet var lblContent03AccAlarmSetting: UILabel!
    @IBOutlet var lblContent04AccAlarmSetting: UILabel!
    @IBOutlet var btnConfirmAccAlarmSetting: UIButton!
    @IBOutlet var btnCancelAccAlarmSetting: UIButton!
    
    @IBOutlet var lblTitleVibrateAlarmSetting: UILabel!
    @IBOutlet var lblContent01VibrateAlarmSetting: UILabel!
    @IBOutlet var lblContent02VibrateAlarmSetting: UILabel!
    @IBOutlet var lblContent03VibrateAlarmSetting: UILabel!
    @IBOutlet var lblContent04VibrateAlarmSetting: UILabel!
    @IBOutlet var btnConfirmVibrateAlarmSetting: UIButton!
    @IBOutlet var btnCancelVibrateAlarmSetting: UIButton!
    
    @IBOutlet var lblTitleFenceAlarmSetting: UILabel!
    @IBOutlet var lblContent01FenceAlarmSetting: UILabel!
    @IBOutlet var lblContent02FenceAlarmSetting: UILabel!
    @IBOutlet var lblContent03FenceAlarmSetting: UILabel!
    @IBOutlet var lblContent04FenceAlarmSetting: UILabel!
    @IBOutlet var lblContent05FenceAlarmSetting: UILabel!
    @IBOutlet var btnConfirmFenceAlarmSetting: UIButton!
    @IBOutlet var btnCancelFenceAlarmSetting: UIButton!
    
    @IBOutlet var lblTitleSpeedAlarmSetting: UILabel!
    @IBOutlet var lblContent01SpeedAlarmSetting: UILabel!
    @IBOutlet var lblContent02SpeedAlarmSetting: UILabel!
    @IBOutlet var lblContent03SpeedAlarmSetting: UILabel!
    @IBOutlet var lblContent04SpeedAlarmSetting: UILabel!
    @IBOutlet var lblContent05SpeedAlarmSetting: UILabel!
    @IBOutlet var btnConfirmSpeedAlarmSetting: UIButton!
    @IBOutlet var btnCancelSpeedAlarmSetting: UIButton!
    
    @IBOutlet var lblTitleDeviceFormat: UILabel!
    @IBOutlet var lblContentDeviceFormat: UILabel!
    @IBOutlet var btnConfirmDeviceFormat: UIButton!
    @IBOutlet var btnCancelDeviceFormat: UIButton!
    
    @IBOutlet var lblTitleChooseLanguage: UILabel!
    @IBOutlet var btnConfirmChooseLanguage: UIButton!
    @IBOutlet var btnCancelChooseLanguage: UIButton!
    
    @IBOutlet var lblTitleAskPassword: UILabel!
    @IBOutlet var lblContentAskPassword: UILabel!
    @IBOutlet var btnConfirmAskPassword: UIButton!
    @IBOutlet var btnCancelCAskPassword: UIButton!
    
    var checkBoxAccAlarmFlag: Bool! = false
    var checkBoxAccSMSFlag: Bool! = false
    var checkBoxAccCallFlag: Bool! = false
    var checkBoxAccSirenFlag: Bool = false
    var checkBoxVibrateAlarmFlag: Bool! = false
    var checkBoxVibrateSMSFlag: Bool! = false
    var checkBoxVibrateCallFlag: Bool! = false
    var checkBoxVibrateSirenFlag: Bool = false
    var checkBoxFenceAlarmFlag: Bool! = false
    var checkBoxFenceSMSFlag: Bool! = false
    var checkBoxFenceCallFlag: Bool! = false
    var checkBoxFenceSirenFlag: Bool = false
    var checkBoxSpeedAlarmFlag: Bool! = false
    var checkBoxSpeedSMSFlag: Bool! = false
    var checkBoxSpeedCallFlag: Bool! = false
    var checkBoxSpeedSirenFlag: Bool = false
    var flagStartEndEditForTextField: Int! = 0
    var sendBySmsFlag: Bool! = true
    var mutableData: NSMutableData = NSMutableData()
    var currentElementName: NSString = ""
    var resetSettingFlag: Bool = false
    
    let settingImageArray: [UIImage] = [#imageLiteral(resourceName: "ic_activate2"), #imageLiteral(resourceName: "ic_chang_pass_device2"), #imageLiteral(resourceName: "icon_setting03"), #imageLiteral(resourceName: "icon_setting04"), #imageLiteral(resourceName: "ic_protect2"), #imageLiteral(resourceName: "icon_setting06")]
    
    let alarmImageArray: [UIImage] = [#imageLiteral(resourceName: "ic_alert_unlock"),
                                      #imageLiteral(resourceName: "ic_shake"),
                                      #imageLiteral(resourceName: "icon_alarmsetting03"),
                                      #imageLiteral(resourceName: "icon_alarmsetting04")]
    
    var vehiclePassword: String! = ""
    let isYes: String! = "=1"
    let isNo: String! = "=0"
    let getString: String = "GET"
    let setString: String = "SET"
    let rebootString: String = "REBOOT"
    let formatString: String = "FORMAT"
    let posString: String = "POS"
    
    enum alarmString: String {
        case accAlarm = ",ACCALARM"
        case accSMS = ",ACCSMS"
        case accCall = ",ACCCALL"
        case accSiren = ",ACCSIREN"
        //
        case vib = ",VIB"
        case vibSMS = ",VIBSMS"
        case vibCall = ",VIBCALL"
        case vibSiren = ",VIBSIREN"
        //
        case radAlarm = ",RADALARM"
        case radSMS = ",RADSMS"
        case radCall = ",RADCALL"
        case radSiren = ",RADSIREN"
        case rad = ",RAD"
        //
        case speedAlarm = ",SPEEDALARM"
        case speedSMS = ",SPEEDSMS"
        case speedCall = ",SPEEDCALL"
        case speedSiren = ",SPEEDSIREN"
        case speed = ",SPEED"
    }
    
    var alarmSet: Int!
    var textFieldCurrent: UITextField!
    
    @IBOutlet var btnBackChangePass: UIButton!
    @IBOutlet var btnBackChangeUser: UIButton!
    
    // MARK: - Main Viewdidload
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetting()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        edgesForExtendedLayout = []
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - init setting
    
    func initSetting() {
        initTextField()
        txtOldPassword_Registry.addTarget(self, action: #selector(changedTextfield1), for: .editingChanged)
        txtNewPassword_Registry.addTarget(self, action: #selector(changedTextfield2), for: .editingChanged)
        txtConfirmNewPassword_Registry.addTarget(self, action: #selector(changedTextfield3), for: .editingChanged)
        txtOldPassword_ChangeVehiclePassword.addTarget(self, action: #selector(changedTextfield1), for: .editingChanged)
        txtNewPassword_ChangeVehiclePassword.addTarget(self, action: #selector(changedTextfield2), for: .editingChanged)
        txtConfirmNewPassword_ChangeVehiclePassword.addTarget(self, action: #selector(changedTextfield3), for: .editingChanged)
        txtVehiclePassword_ChangeUserPhoneNumberView.addTarget(self, action: #selector(changedTextfield1), for: .editingChanged)
        txtNewUserPhoneNumber_ChangeUserPhoneNumber.addTarget(self, action: #selector(changedTextfield2), for: .editingChanged)
        txtVehiclePassword.addTarget(self, action: #selector(changedTextfield1), for: .editingChanged)
        txtFence.addTarget(self, action: #selector(changedTextfield1), for: .editingChanged)
        txtSpeed.addTarget(self, action: #selector(changedTextfield1), for: .editingChanged)
        initSecurityButton()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    // MARK: - Textfield changed
    
    let timeCursor = 0.3
    @objc func changedTextfield1() {
        if txtOldPassword_Registry.text!.count < 1 {
            btnClearVehicleRegistry1.isHidden = true
            btnSecurityOldPasswordVehicleRegistry.isHidden = true
        } else if txtOldPassword_Registry.text!.count > 6 {
            btnClearVehicleRegistry1.isHidden = false
            btnSecurityOldPasswordVehicleRegistry.isHidden = false
            let string = txtOldPassword_Registry.text?.prefix(6)
            txtOldPassword_Registry.text = String(string!)
            DispatchQueue.main.asyncAfter(deadline: .now() + timeCursor) {
                self.txtOldPassword_Registry.setCursor(position: 6)
            }
        } else if txtOldPassword_Registry.text!.count > 0 {
            btnSecurityOldPasswordVehicleRegistry.isHidden = false
            btnClearVehicleRegistry1.isHidden = false
        }
        if txtOldPassword_Registry.text!.count > 5 {
            lbRegistryError1.isHidden = true
        }
        
        if txtOldPassword_ChangeVehiclePassword.text!.count < 1 {
            btnChangeVehiclePassClear1.isHidden = true
            btnSecurityOldPassword_ChangeVehiclePassword.isHidden = true
        } else if txtOldPassword_ChangeVehiclePassword.text!.count > 6 {
            btnChangeVehiclePassClear1.isHidden = false
            btnSecurityOldPassword_ChangeVehiclePassword.isHidden = false
            let string = txtOldPassword_ChangeVehiclePassword.text?.prefix(6)
            txtOldPassword_ChangeVehiclePassword.text = String(string!)
            DispatchQueue.main.asyncAfter(deadline: .now() + timeCursor) {
                self.txtOldPassword_ChangeVehiclePassword.setCursor(position: 6)
            }
        } else if txtOldPassword_ChangeVehiclePassword.text!.count > 0 {
            btnChangeVehiclePassClear1.isHidden = false
            btnSecurityOldPassword_ChangeVehiclePassword.isHidden = false
        }
        if txtOldPassword_ChangeVehiclePassword.text!.count > 5 {
            lbChangeVehiclePassError1.backgroundColor = .white
        }
        
        if txtVehiclePassword_ChangeUserPhoneNumberView.text!.count < 1 {
            btnClearVehiclePass_ChangeUserPhone.isHidden = true
            btnSecurityVehiclePassword_ChangeUser.isHidden = true
        } else if txtVehiclePassword_ChangeUserPhoneNumberView.text!.count > 6 {
            btnClearVehiclePass_ChangeUserPhone.isHidden = false
            btnSecurityVehiclePassword_ChangeUser.isHidden = false
            let string = txtVehiclePassword_ChangeUserPhoneNumberView.text?.prefix(6)
            txtVehiclePassword_ChangeUserPhoneNumberView.text = String(string!)
            DispatchQueue.main.asyncAfter(deadline: .now() + timeCursor) {
                self.txtVehiclePassword_ChangeUserPhoneNumberView.setCursor(position: 6)
            }
        } else if txtVehiclePassword_ChangeUserPhoneNumberView.text!.count > 0 {
            btnClearVehiclePass_ChangeUserPhone.isHidden = false
            btnSecurityVehiclePassword_ChangeUser.isHidden = false
        }
        if txtVehiclePassword_ChangeUserPhoneNumberView.text!.count > 5 {
            lblChangeUser_PassError.backgroundColor = .white
        }
        
        if txtVehiclePassword.text!.count < 1 {
            btnClearVehiclePass.isHidden = true
            btnSecureVehiclePass.isHidden = true
        } else if txtVehiclePassword.text!.count > 6 {
            btnClearVehiclePass.isHidden = false
            btnSecureVehiclePass.isHidden = false
            let string = txtVehiclePassword.text?.prefix(6)
            txtVehiclePassword.text = String(string!)
            DispatchQueue.main.asyncAfter(deadline: .now() + timeCursor) {
                self.txtVehiclePassword.setCursor(position: 6)
            }
        } else if txtVehiclePassword.text!.count > 1 {
            btnClearVehiclePass.isHidden = false
            btnSecureVehiclePass.isHidden = false
        }
        if txtVehiclePassword.text!.count > 0 {
            lbWarningError.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        
        if txtFence.text!.count > 5 {
            let string = txtFence.text?.prefix(5)
            txtFence.text = String(string!)
            DispatchQueue.main.asyncAfter(deadline: .now() + timeCursor) {
                self.txtFence.setCursor(position: 5)
            }
        }
        if txtSpeed.text!.count > 3 {
            let string = txtSpeed.text?.prefix(3)
            txtSpeed.text = String(string!)
            DispatchQueue.main.asyncAfter(deadline: .now() + timeCursor) {
                self.txtSpeed.setCursor(position: 3)
            }
        }
    }
    
    @objc func changedTextfield2() {
        if txtNewPassword_Registry.text!.count < 1 {
            btnClearVehicleRegistry2.isHidden = true
            btnSecurityNewPasswordVehicleRegistry.isHidden = true
        } else if txtNewPassword_Registry.text!.count > 6 {
            btnClearVehicleRegistry2.isHidden = false
            btnSecurityNewPasswordVehicleRegistry.isHidden = false
            let string = txtNewPassword_Registry.text?.prefix(6)
            txtNewPassword_Registry.text = String(string!)
            DispatchQueue.main.asyncAfter(deadline: .now() + timeCursor) {
                self.txtNewPassword_Registry.setCursor(position: 6)
            }
        } else if txtNewPassword_Registry.text!.count > 0 {
            btnClearVehicleRegistry2.isHidden = false
            btnSecurityNewPasswordVehicleRegistry.isHidden = false
        }
        if txtNewPassword_Registry.text!.count > 5 {
            lbRegistryError2.isHidden = true
        }
        
        if txtNewPassword_ChangeVehiclePassword.text!.count < 1 {
            btnChangeVehiclePassClear2.isHidden = true
            btnSecurityNewPassword_ChangeVehiclePassword.isHidden = true
        } else if txtNewPassword_ChangeVehiclePassword.text!.count > 6 {
            btnChangeVehiclePassClear2.isHidden = false
            btnSecurityNewPassword_ChangeVehiclePassword.isHidden = false
            let string = txtNewPassword_ChangeVehiclePassword.text?.prefix(6)
            DispatchQueue.main.asyncAfter(deadline: .now() + timeCursor) {
                self.txtNewPassword_ChangeVehiclePassword.setCursor(position: 6)
            }
            txtNewPassword_ChangeVehiclePassword.text = String(string!)
        } else if txtNewPassword_ChangeVehiclePassword.text!.count > 0 {
            btnChangeVehiclePassClear2.isHidden = false
            btnSecurityNewPassword_ChangeVehiclePassword.isHidden = false
        }
        if txtNewPassword_ChangeVehiclePassword.text!.count > 5 {
            lbChangeVehiclePassError2.backgroundColor = .white
        }
        
        if txtNewUserPhoneNumber_ChangeUserPhoneNumber.text!.count > 9 {
            lblChangeUser_PhoneError.backgroundColor = .white
        }
    }
    
    @objc func changedTextfield3() {
        if txtConfirmNewPassword_Registry.text!.count < 1 {
            btnClearVehicleRegistry3.isHidden = true
            btnSecurityConfirmNewPasswordVehicleRegistry.isHidden = true
        } else if txtConfirmNewPassword_Registry.text!.count > 6 {
            btnClearVehicleRegistry3.isHidden = false
            btnSecurityConfirmNewPasswordVehicleRegistry.isHidden = false
            let string = txtConfirmNewPassword_Registry.text?.prefix(6)
            txtConfirmNewPassword_Registry.text = String(string!)
            DispatchQueue.main.asyncAfter(deadline: .now() + timeCursor) {
                self.txtConfirmNewPassword_Registry.setCursor(position: 6)
            }
        } else if txtConfirmNewPassword_Registry.text!.count > 0 {
            btnClearVehicleRegistry3.isHidden = false
            btnSecurityConfirmNewPasswordVehicleRegistry.isHidden = false
        }
        if txtConfirmNewPassword_Registry.text!.count > 5 {
            lbRegistryError3.isHidden = true
        }
        
        if txtConfirmNewPassword_ChangeVehiclePassword.text!.count < 1 {
            btnChangeVehiclePassClear3.isHidden = true
            btnSecurityConfirmNewPassword_ChangeVehiclePassword.isHidden = true
        } else if txtConfirmNewPassword_ChangeVehiclePassword.text!.count > 6 {
            btnChangeVehiclePassClear3.isHidden = false
            btnSecurityConfirmNewPassword_ChangeVehiclePassword.isHidden = false
            let string = txtConfirmNewPassword_ChangeVehiclePassword.text?.prefix(6)
            txtConfirmNewPassword_ChangeVehiclePassword.text = String(string!)
            DispatchQueue.main.asyncAfter(deadline: .now() + timeCursor) {
                self.txtConfirmNewPassword_ChangeVehiclePassword.setCursor(position: 6)
            }
        } else if txtConfirmNewPassword_ChangeVehiclePassword.text!.count > 0 {
            btnChangeVehiclePassClear3.isHidden = false
            btnSecurityConfirmNewPassword_ChangeVehiclePassword.isHidden = false
        }
        if txtConfirmNewPassword_ChangeVehiclePassword.text!.count > 5 {
            lbChangeVehiclePassError3.backgroundColor = .white
        }
    }
    
    func initSecurityButton() {
        btnSecurityVehiclePassword.isHidden = true
        btnSecurityVehiclePassword_ChangeUser.isHidden = true
        btnSecurityOldPassword_ChangeVehiclePassword.isHidden = true
        btnSecurityNewPassword_ChangeVehiclePassword.isHidden = true
        btnSecurityConfirmNewPassword_ChangeVehiclePassword.isHidden = true
        btnSecurityOldPassword_ChangeAppPassword.isHidden = true
        btnSecurityNewPassword_ChangeAppPassword.isHidden = true
        btnSecurityConfirmNewPassword_ChangeAppPassword.isHidden = true
        btnSecurityOldPasswordVehicleRegistry.isHidden = true
        btnSecurityNewPasswordVehicleRegistry.isHidden = true
        btnSecurityConfirmNewPasswordVehicleRegistry.isHidden = true
    }
    
    // MARK: - navigate action
    
    @IBAction func btnBack2MapTapped(_ sender: AnyObject) {
//        gotoMapView()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCancelAlarmSettingTapped(_ sender: Any) {
        removeListAlarmSettingView()
    }
    
    func gotoLoginView() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "login") as! LoginVC
        nextViewController.modalPresentationStyle = .fullScreen
        present(nextViewController, animated: false, completion: nil)
    }
    
    func gotoMapView() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "map") as! MapVC
        nextViewController.modalPresentationStyle = .fullScreen
        present(nextViewController, animated: false, completion: nil)
    }
    
    // MARK: List Alarm Setting View
    
    @IBOutlet var listAlarmSettingView: UIView!
    func displayListAlarmSettingView() {
        listAlarmSettingView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(listAlarmSettingView)
    }
    
    func removeListAlarmSettingView() {
        listAlarmSettingView.removeFromSuperview()
    }
    
    @IBAction func closeListAlarmSettingView(_ sender: Any) {
        removeListAlarmSettingView()
//        gotoMapView()
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        switch sender {
        case switchAlarmAlarm:
            checkBoxAccAlarmFlag = sender.isOn
        case switchAlarmSMS:
            checkBoxAccSMSFlag = sender.isOn
        case switchAlarmCall:
            checkBoxAccCallFlag = sender.isOn
        case switchAlarmSiren:
            checkBoxAccSirenFlag = sender.isOn
        case switchAlarmVib_Vib:
            checkBoxVibrateAlarmFlag = sender.isOn
        case switchAlarmVib_SMS:
            checkBoxVibrateSMSFlag = sender.isOn
        case switchAlarmVib_Call:
            checkBoxVibrateCallFlag = sender.isOn
        case switchAlarmVib_Siren:
            checkBoxVibrateSirenFlag = sender.isOn
        case switchAlarmFence_Fence:
            checkBoxFenceAlarmFlag = sender.isOn
        case switchAlarmFence_SMS:
            checkBoxFenceSMSFlag = sender.isOn
        case switchAlarmFence_Call:
            checkBoxFenceCallFlag = sender.isOn
        case switchAlarmFence_Siren:
            checkBoxFenceSirenFlag = sender.isOn
        case switchAlarmSpeed_Speed:
            checkBoxSpeedAlarmFlag = sender.isOn
        case switchAlarmSpeed_SMS:
            checkBoxSpeedSMSFlag = sender.isOn
        case switchAlarmSpeed_Call:
            checkBoxSpeedCallFlag = sender.isOn
        case switchAlarmSpeed_Siren:
            checkBoxSpeedSirenFlag = sender.isOn
        default:
            break
        }
    }
    
    // MARK: - Warning setting Alarm
    
    @IBOutlet var accAlarmSettingView: UIView!
    @IBOutlet var lbWarningError: UILabel!
    @IBOutlet var btnCancelWarningSettingView: UIButton!
    func displayAccAlarmSettingView() {
        accAlarmSettingView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(accAlarmSettingView)
        lblTitleAccAlarmSetting.text = txtTitleAccAlarmSetting[languageChooseSave].uppercased()
        lblContent01AccAlarmSetting.text = txtContent01AccAlarmSetting[languageChooseSave]
        lblContent02AccAlarmSetting.text = txtContent02AccAlarmSetting[languageChooseSave]
        lblContent03AccAlarmSetting.text = txtContent03AccAlarmSetting[languageChooseSave]
        lblContent04AccAlarmSetting.text = txtContent04AccAlarmSetting[languageChooseSave]
        sendBySmsFlag = true
        btnCancelWarningSettingView.addTarget(self, action: #selector(removeAccAlarmSettingView), for: .touchUpInside)
        
        lbWarningError.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        btnConfirmAccAlarmSetting.drawShadow(offset: CGSize(width: 0, height: 3))
        btnCancelAccAlarmSetting.drawShadow(offset: CGSize(width: 0, height: 3))
        btnCancelWarningSettingView.drawShadow(offset: CGSize(width: 0, height: 3))
    }
    
    @objc func removeAccAlarmSettingView() {
        accAlarmSettingView.removeFromSuperview()
    }
    
    @IBAction func closeAccAlarmSettingView(_ sender: Any) {
        removeAccAlarmSettingView()
    }
    
    @IBAction func btnCheckBoxAccAlarmTapped(_ sender: AnyObject) {
        if !checkBoxAccAlarmFlag {
            checkBoxAccAlarmFlag = true
            btnCheckBoxAccAlarm.setImage(checkBoxImage, for: UIControl.State.normal)
        } else {
            checkBoxAccAlarmFlag = false
            btnCheckBoxAccAlarm.setImage(uncheckBoxImage, for: UIControl.State.normal)
        }
    }
    
    @IBAction func btnCheckBoxAccSMSTapped(_ sender: AnyObject) {
        if !checkBoxAccSMSFlag {
            checkBoxAccSMSFlag = true
            btnCheckBoxAccSMS.setImage(checkBoxImage, for: UIControl.State.normal)
        } else {
            checkBoxAccSMSFlag = false
            btnCheckBoxAccSMS.setImage(uncheckBoxImage, for: UIControl.State.normal)
        }
    }
    
    @IBAction func btnCheckBoxAccCallTapped(_ sender: AnyObject) {
        if !checkBoxAccCallFlag {
            checkBoxAccCallFlag = true
            btnCheckBoxAccCall.setImage(checkBoxImage, for: UIControl.State.normal)
        } else {
            checkBoxAccCallFlag = false
            btnCheckBoxAccCall.setImage(uncheckBoxImage, for: UIControl.State.normal)
        }
    }
    
    @IBAction func btnCheckBoxAccSirenTapped(_ sender: AnyObject) {
        if !checkBoxAccSirenFlag {
            checkBoxAccSirenFlag = true
            btnCheckBoxAccSiren.setImage(checkBoxImage, for: UIControl.State.normal)
        } else {
            checkBoxAccSirenFlag = false
            btnCheckBoxAccSiren.setImage(uncheckBoxImage, for: UIControl.State.normal)
        }
    }
    
    func accSetParameter() {
        var smsContent: String! = setString
        smsContent = smsContent + "," + vehiclePassword
        if !checkBoxAccAlarmFlag {
            smsContent = smsContent + alarmString.accAlarm.rawValue + isNo
        } else {
            smsContent = smsContent + alarmString.accAlarm.rawValue + isYes
        }
        if !checkBoxAccSMSFlag {
            smsContent = smsContent + alarmString.accSMS.rawValue + isNo
        } else {
            smsContent = smsContent + alarmString.accSMS.rawValue + isYes
        }
        if !checkBoxAccCallFlag {
            smsContent = smsContent + alarmString.accCall.rawValue + isNo
        } else {
            smsContent = smsContent + alarmString.accCall.rawValue + isYes
        }
        if !checkBoxAccSirenFlag {
            smsContent = smsContent + alarmString.accSiren.rawValue + isNo
        } else {
            smsContent = smsContent + alarmString.accSiren.rawValue + isYes
        }
        smsSettingContext = smsContent
        removeAccAlarmSettingView()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsSettingContext)
    }
    
    @IBAction func btnAccAlarmConfirm2SetParameterTapped(_ sender: AnyObject) {
        alarmSet = 0
        displayAskPasswordView()
    }
    
    @IBAction func btnAccAlarmConfirm2GetParameterTapped(_ sender: Any) {
        smsSettingContext = "GET,PSW,ACCALARM,ACCSMS,ACCCALL,ACCSIREN"
        removeAccAlarmSettingView()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsSettingContext)
    }
    
    func clearCheckBoxAccFlag() {
        checkBoxAccAlarmFlag = false
        checkBoxAccSMSFlag = false
        checkBoxAccCallFlag = false
        checkBoxAccSirenFlag = false
        
        switchAlarmAlarm.isOn = false
        switchAlarmSMS.isOn = false
        switchAlarmCall.isOn = false
        switchAlarmSiren.isOn = false
    }
    
    //
    
    // MARK: - Vibrate Alarm
    
    @IBOutlet var vibrateAlarmSettingView: UIView!
    @IBOutlet var btnCancelVibrateAlarmView: UIButton!
    func displayVibrateAlarmSettingView() {
        vibrateAlarmSettingView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(vibrateAlarmSettingView)
        lblTitleVibrateAlarmSetting.text = txtTitleVibrateAlarmSetting[languageChooseSave].uppercased()
        lblContent01VibrateAlarmSetting.text = txtContent01VibrateAlarmSetting[languageChooseSave]
        lblContent02VibrateAlarmSetting.text = txtContent02VibrateAlarmSetting[languageChooseSave]
        lblContent03VibrateAlarmSetting.text = txtContent03VibrateAlarmSetting[languageChooseSave]
        lblContent04VibrateAlarmSetting.text = txtContent04VibrateAlarmSetting[languageChooseSave]
        sendBySmsFlag = true
        btnCancelVibrateAlarmView.addTarget(self, action: #selector(removeVibrateAlarmSettingView), for: .touchUpInside)
        
        lbWarningError.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        btnConfirmVibrateAlarmSetting.drawShadow(offset: CGSize(width: 0, height: 3))
        btnCancelVibrateAlarmView.drawShadow(offset: CGSize(width: 0, height: 3))
        btnCancelVibrateAlarmSetting.drawShadow(offset: CGSize(width: 0, height: 3))
    }
    
    @objc func removeVibrateAlarmSettingView() {
        vibrateAlarmSettingView.removeFromSuperview()
    }
    
    @IBAction func closeVibrateAlarmSettingView(_ sender: Any) {
        removeVibrateAlarmSettingView()
    }
    
    @IBAction func btnCheckBoxVibrateAlarmTapped(_ sender: AnyObject) {
        if !checkBoxVibrateAlarmFlag {
            checkBoxVibrateAlarmFlag = true
            btnCheckBoxVibrateAlarm.setImage(checkBoxImage, for: UIControl.State.normal)
        } else {
            checkBoxVibrateAlarmFlag = false
            btnCheckBoxVibrateAlarm.setImage(uncheckBoxImage, for: UIControl.State.normal)
        }
    }
    
    @IBAction func btnCheckBoxVibrateSMSTapped(_ sender: AnyObject) {
        if !checkBoxVibrateSMSFlag {
            checkBoxVibrateSMSFlag = true
            btnCheckBoxVibrateSMS.setImage(checkBoxImage, for: UIControl.State.normal)
        } else {
            checkBoxVibrateSMSFlag = false
            btnCheckBoxVibrateSMS.setImage(uncheckBoxImage, for: UIControl.State.normal)
        }
    }
    
    @IBAction func btnCheckBoxVibrateCallTapped(_ sender: AnyObject) {
        if !checkBoxVibrateCallFlag {
            checkBoxVibrateCallFlag = true
            btnCheckBoxVibrateCall.setImage(checkBoxImage, for: UIControl.State.normal)
        } else {
            checkBoxVibrateCallFlag = false
            btnCheckBoxVibrateCall.setImage(uncheckBoxImage, for: UIControl.State.normal)
        }
    }
    
    @IBAction func btnCheckBoxVibrateSirenTapped(_ sender: AnyObject) {
        if !checkBoxVibrateSirenFlag {
            checkBoxVibrateSirenFlag = true
            btnCheckBoxVibrateSiren.setImage(checkBoxImage, for: UIControl.State.normal)
        } else {
            checkBoxVibrateSirenFlag = false
            btnCheckBoxVibrateSiren.setImage(uncheckBoxImage, for: UIControl.State.normal)
        }
    }
    
    func vibSetParameter() {
        var smsContent: String! = setString
        smsContent = smsContent + "," + vehiclePassword
        if !checkBoxVibrateAlarmFlag {
            smsContent = smsContent + alarmString.vib.rawValue + isNo
        } else {
            smsContent = smsContent + alarmString.vib.rawValue + isYes
        }
        if !checkBoxVibrateSMSFlag {
            smsContent = smsContent + alarmString.vibSMS.rawValue + isNo
        } else {
            smsContent = smsContent + alarmString.vibSMS.rawValue + isYes
        }
        if !checkBoxVibrateCallFlag {
            smsContent = smsContent + alarmString.vibCall.rawValue + isNo
        } else {
            smsContent = smsContent + alarmString.vibCall.rawValue + isYes
        }
        if !checkBoxVibrateSirenFlag {
            smsContent = smsContent + alarmString.vibSiren.rawValue + isNo
        } else {
            smsContent = smsContent + alarmString.vibSiren.rawValue + isYes
        }
        smsSettingContext = smsContent
        removeVibrateAlarmSettingView()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsSettingContext)
    }
    
    @IBAction func btnVibrateAlarmConfirm2SetParameterTapped(_ sender: AnyObject) {
        alarmSet = 1
//        removeVibrateAlarmSettingView()
        displayAskPasswordView()
    }
    
    @IBAction func btnVibrateAlarmConfirm2GetParameterTapped(_ sender: Any) {
        smsSettingContext = "GET,PSW,VIB,VIBSMS,VIBCALL,VIBSIREN"
        removeVibrateAlarmSettingView()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsSettingContext)
    }
    
    func clearCheckBoxVibrateFlag() {
        checkBoxVibrateAlarmFlag = false
        checkBoxVibrateSMSFlag = false
        checkBoxVibrateCallFlag = false
        checkBoxVibrateSirenFlag = false
        
        switchAlarmVib_Vib.isOn = false
        switchAlarmVib_SMS.isOn = false
        switchAlarmVib_Call.isOn = false
        switchAlarmVib_Siren.isOn = false
//        btnCheckBoxVibrateAlarm.setImage(uncheckBoxImage, for: UIControl.State.normal)
//        btnCheckBoxVibrateSMS.setImage(uncheckBoxImage, for: UIControl.State.normal)
//        btnCheckBoxVibrateCall.setImage(uncheckBoxImage, for: UIControl.State.normal)
//        btnCheckBoxVibrateSiren.setImage(uncheckBoxImage, for: UIControl.State.normal)
    }
    
    //
    
    // MARK: - Fence Alarm
    
    @IBOutlet var fenceAlarmSettingView: UIView!
    @IBOutlet var btnCancelFenceView: UIButton!
    func displayFenceAlarmSettingView() {
        fenceAlarmSettingView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(fenceAlarmSettingView)
        lblTitleFenceAlarmSetting.text = txtTitleFenceAlarmSetting[languageChooseSave].uppercased()
        lblContent01FenceAlarmSetting.text = txtContent01FenceAlarmSetting[languageChooseSave]
        lblContent02FenceAlarmSetting.text = txtContent02FenceAlarmSetting[languageChooseSave]
        lblContent03FenceAlarmSetting.text = txtContent03FenceAlarmSetting[languageChooseSave]
        lblContent04FenceAlarmSetting.text = txtContent04FenceAlarmSetting[languageChooseSave]
        lblContent05FenceAlarmSetting.text = txtContent05FenceAlarmSetting[languageChooseSave]
        
        txtFence.text = "200"
        sendBySmsFlag = true
        btnCancelFenceView.addTarget(self, action: #selector(removeFenceAlarmSettingView), for: .touchUpInside)
        
        btnConfirmFenceAlarmSetting.drawShadow(offset: CGSize(width: 0, height: 3))
        btnCancelFenceAlarmSetting.drawShadow(offset: CGSize(width: 0, height: 3))
        btnCancelFenceView.drawShadow(offset: CGSize(width: 0, height: 3))
    }
    
    @objc func removeFenceAlarmSettingView() {
        fenceAlarmSettingView.removeFromSuperview()
    }
    
    @IBAction func closeFenceAlarmSettingView(_ sender: Any) {
        removeFenceAlarmSettingView()
    }
    
    @IBAction func btnCheckBoxFenceAlarmTapped(_ sender: AnyObject) {
        if !checkBoxFenceAlarmFlag {
            checkBoxFenceAlarmFlag = true
            btnCheckBoxFenceAlarm.setImage(checkBoxImage, for: UIControl.State.normal)
        } else {
            checkBoxFenceAlarmFlag = false
            btnCheckBoxFenceAlarm.setImage(uncheckBoxImage, for: UIControl.State.normal)
        }
    }
    
    @IBAction func btnCheckBoxFenceSMSTapped(_ sender: AnyObject) {
        if !checkBoxFenceSMSFlag {
            checkBoxFenceSMSFlag = true
            btnCheckBoxFenceSMS.setImage(checkBoxImage, for: UIControl.State.normal)
        } else {
            checkBoxFenceSMSFlag = false
            btnCheckBoxFenceSMS.setImage(uncheckBoxImage, for: UIControl.State.normal)
        }
    }
    
    @IBAction func btnCheckBoxFenceCall(_ sender: AnyObject) {
        if !checkBoxFenceCallFlag {
            checkBoxFenceCallFlag = true
            btnCheckBoxFenceCall.setImage(checkBoxImage, for: UIControl.State.normal)
        } else {
            checkBoxFenceCallFlag = false
            btnCheckBoxFenceCall.setImage(uncheckBoxImage, for: UIControl.State.normal)
        }
    }
    
    @IBAction func btnCheckBoxFenceSiren(_ sender: AnyObject) {
        if !checkBoxFenceSirenFlag {
            checkBoxFenceSirenFlag = true
            btnCheckBoxFenceSiren.setImage(checkBoxImage, for: UIControl.State.normal)
        } else {
            checkBoxFenceSirenFlag = false
            btnCheckBoxFenceSiren.setImage(uncheckBoxImage, for: UIControl.State.normal)
        }
    }
    
    func fenceSetParameter() {
        var smsContent: String! = setString
        smsContent = smsContent + "," + vehiclePassword
        if !checkBoxFenceAlarmFlag {
            smsContent = smsContent + alarmString.radAlarm.rawValue + isNo
        } else {
            smsContent = smsContent + alarmString.radAlarm.rawValue + isYes
        }
        if !checkBoxFenceSMSFlag {
            smsContent = smsContent + alarmString.radSMS.rawValue + isNo
        } else {
            smsContent = smsContent + alarmString.radSMS.rawValue + isYes
        }
        if !checkBoxFenceCallFlag {
            smsContent = smsContent + alarmString.radCall.rawValue + isNo
        } else {
            smsContent = smsContent + alarmString.radCall.rawValue + isYes
        }
        if !checkBoxFenceSirenFlag {
            smsContent = smsContent + alarmString.radSiren.rawValue + isNo
        } else {
            smsContent = smsContent + alarmString.radSiren.rawValue + isYes
        }
        smsContent = smsContent + alarmString.rad.rawValue + "=" + txtFence.text!
        smsSettingContext = smsContent
        removeFenceAlarmSettingView()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsSettingContext)
    }
    
    @IBAction func btnFenceAlarmConfirm2SetParameterTapped(_ sender: AnyObject) {
        if txtFence.text?.count == 0 {
            showBannerNotifi(text: "Vui lòng nhập phạm vi cho phép trong khoảng 100 - 10000 m", background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            // alertCustom(message: "Vui lòng nhập phạm vi cho phép trong khoảng 100 - 10000", delay: 2.5)
            txtFence.becomeFirstResponder()
            return
        }
        let fenceString = Int(txtFence.text!)
        if fenceString! < 100 || fenceString! > 10000 {
            showBannerNotifi(text: txtAlertFenceAlarmSetting[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            // alertCustom(message: txtAlertFenceAlarmSetting[languageChooseSave], delay: 2.5)
            txtFence.becomeFirstResponder()
            return
        }
        alarmSet = 2
        displayAskPasswordView()
    }
    
    @IBAction func btnFenceAlarmConfirm2GetParameterTapped(_ sender: Any) {
        smsSettingContext = "GET,PSW,RADALARM,RADSMS,RADCALL,RADSIREN,RAD"
        removeFenceAlarmSettingView()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsSettingContext)
    }
    
    func clearCheckBoxFenceFlag() {
        checkBoxFenceAlarmFlag = false
        checkBoxFenceSMSFlag = false
        checkBoxFenceCallFlag = false
        checkBoxFenceSirenFlag = false
        
        switchAlarmFence_Fence.isOn = false
        switchAlarmFence_SMS.isOn = false
        switchAlarmFence_Call.isOn = false
        switchAlarmFence_Siren.isOn = false
//        btnCheckBoxFenceAlarm.setImage(uncheckBoxImage, for: UIControl.State.normal)
//        btnCheckBoxFenceSMS.setImage(uncheckBoxImage, for: UIControl.State.normal)
//        btnCheckBoxFenceCall.setImage(uncheckBoxImage, for: UIControl.State.normal)
//        btnCheckBoxFenceSiren.setImage(uncheckBoxImage, for: UIControl.State.normal)
    }
    
    //
    
    // MARK: - Speed Alarm
    
    @IBOutlet var speedAlarmSettingView: UIView!
    @IBOutlet var btnCancelSpeedView: UIButton!
    func displaySpeedAlarmSettingView() {
        speedAlarmSettingView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(speedAlarmSettingView)
        lblTitleSpeedAlarmSetting.text = txtTitleSpeedAlarmSetting[languageChooseSave].uppercased()
        lblContent01SpeedAlarmSetting.text = txtContent01SpeedAlarmSetting[languageChooseSave]
        lblContent02SpeedAlarmSetting.text = txtContent02SpeedAlarmSetting[languageChooseSave]
        lblContent03SpeedAlarmSetting.text = txtContent03SpeedAlarmSetting[languageChooseSave]
        lblContent04SpeedAlarmSetting.text = txtContent04SpeedAlarmSetting[languageChooseSave]
        lblContent05SpeedAlarmSetting.text = txtContent05SpeedAlarmSetting[languageChooseSave]
        
        txtSpeed.text = "60"
        sendBySmsFlag = true
        
        btnCancelSpeedView.addTarget(self, action: #selector(removeSpeedAlarmSettingView), for: .touchUpInside)
        lbWarningError.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        btnConfirmSpeedAlarmSetting.drawShadow(offset: CGSize(width: 0, height: 3))
        btnCancelSpeedAlarmSetting.drawShadow(offset: CGSize(width: 0, height: 3))
        btnCancelSpeedView.drawShadow(offset: CGSize(width: 0, height: 3))
    }
    
    @objc func removeSpeedAlarmSettingView() {
        speedAlarmSettingView.removeFromSuperview()
    }
    
    @IBAction func closeSpeedAlarmSettingView(_ sender: Any) {
        removeSpeedAlarmSettingView()
    }
    
    @IBAction func btnCheckBoxSpeedAlarmTapped(_ sender: AnyObject) {
        if !checkBoxSpeedAlarmFlag {
            checkBoxSpeedAlarmFlag = true
            btnCheckBoxSpeedAlarm.setImage(checkBoxImage, for: UIControl.State.normal)
        } else {
            checkBoxSpeedAlarmFlag = false
            btnCheckBoxSpeedAlarm.setImage(uncheckBoxImage, for: UIControl.State.normal)
        }
    }
    
    @IBAction func btnCheckBoxSpeedSMSTapped(_ sender: AnyObject) {
        if !checkBoxSpeedSMSFlag {
            checkBoxSpeedSMSFlag = true
            btnCheckBoxSpeedSMS.setImage(checkBoxImage, for: UIControl.State.normal)
        } else {
            checkBoxSpeedSMSFlag = false
            btnCheckBoxSpeedSMS.setImage(uncheckBoxImage, for: UIControl.State.normal)
        }
    }
    
    @IBAction func btnCheckBoxSpeedCallTapped(_ sender: AnyObject) {
        if !checkBoxSpeedCallFlag {
            checkBoxSpeedCallFlag = true
            btnCheckBoxSpeedCall.setImage(checkBoxImage, for: UIControl.State.normal)
        } else {
            checkBoxSpeedCallFlag = false
            btnCheckBoxSpeedCall.setImage(uncheckBoxImage, for: UIControl.State.normal)
        }
    }
    
    @IBAction func btnCheckBoxSpeedSirenTapped(_ sender: AnyObject) {
        if !checkBoxSpeedSirenFlag {
            checkBoxSpeedSirenFlag = true
            btnCheckBoxSpeedSiren.setImage(checkBoxImage, for: UIControl.State.normal)
        } else {
            checkBoxSpeedSirenFlag = false
            btnCheckBoxSpeedSiren.setImage(uncheckBoxImage, for: UIControl.State.normal)
        }
    }
    
    func speedSetParameter() {
        var smsContent: String! = setString
        smsContent = smsContent + "," + vehiclePassword
        if !checkBoxSpeedAlarmFlag {
            smsContent = smsContent + alarmString.speedAlarm.rawValue + isNo
        } else {
            smsContent = smsContent + alarmString.speedAlarm.rawValue + isYes
        }
        if !checkBoxSpeedSMSFlag {
            smsContent = smsContent + alarmString.speedSMS.rawValue + isNo
        } else {
            smsContent = smsContent + alarmString.speedSMS.rawValue + isYes
        }
        if !checkBoxSpeedCallFlag {
            smsContent = smsContent + alarmString.speedCall.rawValue + isNo
        } else {
            smsContent = smsContent + alarmString.speedCall.rawValue + isYes
        }
        if !checkBoxSpeedSirenFlag {
            smsContent = smsContent + alarmString.speedSiren.rawValue + isNo
        } else {
            smsContent = smsContent + alarmString.speedSiren.rawValue + isYes
        }
        smsContent = smsContent + alarmString.speed.rawValue + "=" + txtSpeed.text!
        smsSettingContext = smsContent
        removeSpeedAlarmSettingView()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsSettingContext)
    }
    
    @IBAction func btnSpeedAlarmConfirm2SetParameterTapped(_ sender: AnyObject) {
        if txtSpeed.text?.count == 0 {
            showBannerNotifi(text: "Vui lòng nhập tốc độ cho phép trong khoảng 20 - 150 km/h", background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            // alertCustom(message: "Vui lòng nhập tốc độ cho phép trong khoảng 20 - 150", delay: 2.5)
            txtSpeed.becomeFirstResponder()
            return
        }
        let speedString = Int(txtSpeed.text!)
        if speedString! < 20 || speedString! > 150 {
            showBannerNotifi(text: txtAlertSpeedAlarmSetting[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            // alertCustom(message: txtAlertSpeedAlarmSetting[languageChooseSave], delay: 2.5)
            txtSpeed.becomeFirstResponder()
            return
        }
        alarmSet = 3
        displayAskPasswordView()
    }
    
    @IBAction func btnSpeedAlarmConfirm2GetParameterTapped(_ sender: Any) {
        smsSettingContext = "GET,PSW,SPEEDALARM,SPEEDSMS,SPEEDCALL,SPEEDSIREN,SPEED"
        removeSpeedAlarmSettingView()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsSettingContext)
    }
    
    func clearCheckBoxSpeedFlag() {
        checkBoxSpeedAlarmFlag = false
        checkBoxSpeedSMSFlag = false
        checkBoxSpeedCallFlag = false
        checkBoxSpeedSirenFlag = false
        
        switchAlarmSpeed_Speed.isOn = false
        switchAlarmSpeed_SMS.isOn = false
        switchAlarmSpeed_Call.isOn = false
        switchAlarmSpeed_Siren.isOn = false
        
//        btnCheckBoxSpeedAlarm.setImage(uncheckBoxImage, for: UIControl.State.normal)
//        btnCheckBoxSpeedSMS.setImage(uncheckBoxImage, for: UIControl.State.normal)
//        btnCheckBoxSpeedCall.setImage(uncheckBoxImage, for: UIControl.State.normal)
//        btnCheckBoxSpeedSiren.setImage(uncheckBoxImage, for: UIControl.State.normal)
    }
    
    //
    
    // MARK: - Change Device's password
    
    @IBOutlet var changeVehiclePasswordView: UIView!
    func displayChangeVehiclePasswordView() {
        txtNewPassword_ChangeVehiclePassword.becomeFirstResponder()
        changeVehiclePasswordView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        btnBackChangePass.addTarget(self, action: #selector(backClicked), for: .touchUpInside)
        view.addSubview(changeVehiclePasswordView)
        txtOldPassword_ChangeVehiclePassword.text?.removeAll()
        txtNewPassword_ChangeVehiclePassword.text?.removeAll()
        txtConfirmNewPassword_ChangeVehiclePassword.text?.removeAll()
        btnSendByGPRS_ChangeVehiclePassword.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
        btnSendBySMS_ChangeVehiclePassword.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        hideOldPassword()
        sendBySmsFlag = false
        
        txtOldPassword_ChangeVehiclePassword.isSecureTextEntry = true
        txtNewPassword_ChangeVehiclePassword.isSecureTextEntry = true
        txtConfirmNewPassword_ChangeVehiclePassword.isSecureTextEntry = true
        btnSecurityOldPassword_ChangeVehiclePassword.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
        btnSecurityNewPassword_ChangeVehiclePassword.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
        btnSecurityConfirmNewPassword_ChangeVehiclePassword.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
        
        btnConfirmChangeVehiclePassword.add(Border: .right, withColor: .lightGray, andHeight: 1)
        txtNewPassword_ChangeVehiclePassword.becomeFirstResponder()
        lblTitleChangeVehiclePassword.text = txtTitleChangeVehiclePassword[languageChooseSave].uppercased()
        // lblContent01ChangeVehiclePassword.text = txtContent01ChangeVehiclePassword[languageChooseSave]
        lblContent02ChangeVehiclePassword.text = txtContent02ChangeVehiclePassword[languageChooseSave]
        lblContent03ChangeVehiclePassword.text = txtContent03ChangeVehiclePassword[languageChooseSave]
        
        lbChangeVehiclePassError1.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lbChangeVehiclePassError2.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lbChangeVehiclePassError3.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        lbChangeVehiclePassError1.text = ""
        lbChangeVehiclePassError2.text = ""
        lbChangeVehiclePassError3.text = ""
        btnChangeVehiclePassClear1.isHidden = true
        btnChangeVehiclePassClear2.isHidden = true
        btnChangeVehiclePassClear3.isHidden = true
        btnSecurityOldPassword_ChangeVehiclePassword.isHidden = true
        btnSecurityNewPassword_ChangeVehiclePassword.isHidden = true
        btnSecurityConfirmNewPassword_ChangeVehiclePassword.isHidden = true
        
        btnCancelChangeVehiclePassword.drawShadow(offset: CGSize(width: 0, height: 3))
        btnConfirmChangeVehiclePassword.drawShadow(offset: CGSize(width: 0, height: 3))
    }
    
    @objc func backClicked() {
        changeVehiclePasswordView.removeFromSuperview()
        btnSendBySMS_ChangeVehiclePassword.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        btnSendBySMS_ChangeVehiclePassword.setTitleColor(.black, for: .normal)
    }
    
    func hideOldPassword() {
        lbChangeVehiclePassError1.isHidden = true
        viewOldPass_ChangeVehiclePas.isHidden = true
        lblOldPasswordDescription.isHidden = true
        txtOldPassword_ChangeVehiclePassword.isHidden = true
        lblContent01ChangeVehiclePassword.isHidden = true
        btnSecurityOldPassword_ChangeVehiclePassword.isHidden = true
        btnChangeVehiclePassClear1.isHidden = true
        txtOldPassword_ChangeVehiclePassword.text = ""
    }
    
    func showOldPassword() {
        lbChangeVehiclePassError1.isHidden = false
        lblOldPasswordDescription.isHidden = false
        txtOldPassword_ChangeVehiclePassword.isHidden = false
        lblContent01ChangeVehiclePassword.isHidden = false
        viewOldPass_ChangeVehiclePas.isHidden = false
    }
    
    func removeChangeVehiclePasswordView() {
        changeVehiclePasswordView.removeFromSuperview()
        btnSendBySMS_ChangeVehiclePassword.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        btnSendBySMS_ChangeVehiclePassword.setTitleColor(.black, for: .normal)
    }
    
    @IBAction func closeChangeVehiclePasswordView(_ sender: Any) {
        changeVehiclePasswordView.removeFromSuperview()
    }
    
    @IBAction func btnChangeVehiclePasswordConfirm2SetParameterTapped(_ sender: AnyObject) {
        if sendBySmsFlag == true {
            if txtOldPassword_ChangeVehiclePassword.text?.count != 6 {
//                alertCustom(message: txtAlert01ChangeVehiclePassword[languageChooseSave], delay: 2)
                lbChangeVehiclePassError1.backgroundColor = #colorLiteral(red: 0.933637917, green: 0.5668299198, blue: 0.1574823558, alpha: 1)
                lbChangeVehiclePassError1.text = txtAlert01ChangeVehiclePassword[languageChooseSave]
                lbChangeVehiclePassError2.backgroundColor = .white
                lbChangeVehiclePassError2.text = ""
                lbChangeVehiclePassError3.backgroundColor = .white
                lbChangeVehiclePassError3.text = ""
                txtOldPassword_ChangeVehiclePassword.becomeFirstResponder()
                return
            } else {
                lbChangeVehiclePassError1.backgroundColor = .white
                lbChangeVehiclePassError1.text = ""
            }
        }
        if txtNewPassword_ChangeVehiclePassword.text?.count != 6 {
//            alertCustom(message: txtAlert02ChangeVehiclePassword[languageChooseSave], delay: 2)
            lbChangeVehiclePassError2.backgroundColor = #colorLiteral(red: 0.933637917, green: 0.5668299198, blue: 0.1574823558, alpha: 1)
            lbChangeVehiclePassError2.text = txtAlert02ChangeVehiclePassword[languageChooseSave]
            lbChangeVehiclePassError3.backgroundColor = .white
            lbChangeVehiclePassError3.text = ""
            txtNewPassword_ChangeVehiclePassword.becomeFirstResponder()
            return
        } else {
            lbChangeVehiclePassError2.backgroundColor = .white
            lbChangeVehiclePassError2.text = ""
        }
        if txtConfirmNewPassword_ChangeVehiclePassword.text?.count != 6 {
//            alertCustom(message: txtAlert03ChangeVehiclePassword[languageChooseSave], delay: 2)
            lbChangeVehiclePassError3.backgroundColor = #colorLiteral(red: 0.933637917, green: 0.5668299198, blue: 0.1574823558, alpha: 1)
            lbChangeVehiclePassError3.text = txtAlert03ChangeVehiclePassword[languageChooseSave]
            
            txtConfirmNewPassword_ChangeVehiclePassword.becomeFirstResponder()
            return
        } else {
            lbChangeVehiclePassError3.backgroundColor = .white
            lbChangeVehiclePassError3.text = ""
        }
        if txtNewPassword_ChangeVehiclePassword.text != txtConfirmNewPassword_ChangeVehiclePassword.text {
//            alertCustom(message: txtAlert04ChangeVehiclePassword[languageChooseSave], delay: 2)
            lbChangeVehiclePassError3.backgroundColor = #colorLiteral(red: 0.933637917, green: 0.5668299198, blue: 0.1574823558, alpha: 1)
            lbChangeVehiclePassError3.text = txtAlert04ChangeVehiclePassword[languageChooseSave]
            txtConfirmNewPassword_ChangeVehiclePassword.becomeFirstResponder()
            return
        } else {
            lbChangeVehiclePassError3.backgroundColor = .white
            lbChangeVehiclePassError3.text = ""
        }
        removeChangeVehiclePasswordView()
        if sendBySmsFlag == true {
            smsSettingContext = "CP," + txtOldPassword_ChangeVehiclePassword.text! + "," + txtConfirmNewPassword_ChangeVehiclePassword.text!
            removeChangeVehiclePasswordView()
            sendMessage(phoneNumber: vehicle2show.sim, content: smsSettingContext)
        } else {
            if checkVehicleState() == false {
                return
            }
            let content = "PSW=" + txtNewPassword_ChangeVehiclePassword.text!
            SoapHandShaking(commandIndex: "3", inputContent: content)
            // serverHandShaking(commandIndex:"3",inputContent: content)
        }
    }
    
    @IBAction func btnChangeVehiclePasswordCancelSetParameterTapped(_ sender: AnyObject) {
        removeChangeVehiclePasswordView()
    }
    
    @IBAction func btnSecurityOldPasswordInChangeVehiclePasswordTapped(_ sender: Any) {
        if txtOldPassword_ChangeVehiclePassword.isSecureTextEntry == false {
            btnSecurityOldPassword_ChangeVehiclePassword.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
        } else {
            btnSecurityOldPassword_ChangeVehiclePassword.setImage(UIImage(named: "invisible_eye_dark"), for: .normal)
        }
        txtOldPassword_ChangeVehiclePassword.isSecureTextEntry = !txtOldPassword_ChangeVehiclePassword.isSecureTextEntry
    }
    
    @IBAction func btnSecurityNewPasswordInChangeVehiclePasswordTapped(_ sender: Any) {
        if txtNewPassword_ChangeVehiclePassword.isSecureTextEntry == false {
            btnSecurityNewPassword_ChangeVehiclePassword.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
        } else {
            btnSecurityNewPassword_ChangeVehiclePassword.setImage(UIImage(named: "invisible_eye_dark"), for: .normal)
        }
        txtNewPassword_ChangeVehiclePassword.isSecureTextEntry = !txtNewPassword_ChangeVehiclePassword.isSecureTextEntry
    }
    
    @IBAction func btnSecurityConfirmNewPasswordInChangeVehiclePasswordTapped(_ sender: Any) {
        if txtConfirmNewPassword_ChangeVehiclePassword.isSecureTextEntry == false {
            btnSecurityConfirmNewPassword_ChangeVehiclePassword.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
        } else {
            btnSecurityConfirmNewPassword_ChangeVehiclePassword.setImage(UIImage(named: "invisible_eye_dark"), for: .normal)
        }
        txtConfirmNewPassword_ChangeVehiclePassword.isSecureTextEntry = !txtConfirmNewPassword_ChangeVehiclePassword.isSecureTextEntry
    }
    
    @IBOutlet var btnSendBySMS_ChangeVehiclePassword: UIButton!
    @IBAction func btnSendBySMS_ChangeVehiclePasswordTapped(_ sender: Any) {
        btnSendBySMS_ChangeVehiclePassword.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
        btnSendByGPRS_ChangeVehiclePassword.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        showOldPassword()
        sendBySmsFlag = true
        btnSendByGPRS_ChangeVehiclePassword.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        btnSendBySMS_ChangeVehiclePassword.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        btnSendByGPRS_ChangeVehiclePassword.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnSendBySMS_ChangeVehiclePassword.setTitleColor(#colorLiteral(red: 0.1490196078, green: 0.3490196078, blue: 0.6705882353, alpha: 1), for: .normal)
        txtOldPassword_ChangeVehiclePassword.becomeFirstResponder()
        view.layoutIfNeeded()
        lbChangeVehiclePassError1.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lbChangeVehiclePassError2.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lbChangeVehiclePassError3.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    @IBOutlet var btnSendByGPRS_ChangeVehiclePassword: UIButton!
    @IBAction func btnSendByGPRS_ChangeVehiclePasswordTapped(_ sender: Any) {
        btnSendByGPRS_ChangeVehiclePassword.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
        btnSendBySMS_ChangeVehiclePassword.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        hideOldPassword()
        sendBySmsFlag = false
        btnSendByGPRS_ChangeVehiclePassword.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        btnSendBySMS_ChangeVehiclePassword.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        btnSendByGPRS_ChangeVehiclePassword.setTitleColor(#colorLiteral(red: 0.1490196078, green: 0.3490196078, blue: 0.6705882353, alpha: 1), for: .normal)
        btnSendBySMS_ChangeVehiclePassword.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        txtNewPassword_ChangeVehiclePassword.becomeFirstResponder()
        view.layoutIfNeeded()
        lbChangeVehiclePassError1.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lbChangeVehiclePassError2.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lbChangeVehiclePassError3.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    //
    
    // MARK: - Change App Password
    
    @IBOutlet var changeAppPasswordView: UIView!
    func displayChangeAppPasswordView() {
        txtOldPassword_ChangeAppPassword.becomeFirstResponder()
        changeAppPasswordView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(changeAppPasswordView)
        txtOldPassword_ChangeAppPassword.text?.removeAll()
        txtNewPassword_ChangeAppPassword.text?.removeAll()
        txtConfirmNewPassword_ChangeAppPassword.text?.removeAll()
        lblTitleChangeAppPassword.text = txtTitleChangeAppPassword[languageChooseSave].uppercased()
        lblContent01ChangeAppPassword.text = txtContent01ChangeAppPassword[languageChooseSave]
        lblContent02ChangeAppPassword.text = txtContent02ChangeAppPassword[languageChooseSave]
        lblContent03ChangeAppPassword.text = txtContent03ChangeAppPassword[languageChooseSave]
//        btnConfirmChangeAppPassword.setTitle(txtButtonConfirmChangeAppPassword[languageChooseSave], for: .normal)
//        btnCancelChangeAppPassword.setTitle(txtButtonCancelChangeAppPassword[languageChooseSave], for: .normal)
        sendBySmsFlag = false
    }
    
    func removeChangeAppPasswordView() {
        changeAppPasswordView.removeFromSuperview()
    }
    
    @IBAction func closeChangeAppPasswordView(_ sender: Any) {
        changeAppPasswordView.removeFromSuperview()
    }
    
    @IBAction func btnChangeAppPasswordConfirm2SetParameterTapped(_ sender: Any) {
        if (txtOldPassword_ChangeAppPassword.text?.count)! < 8 {
            showBannerNotifi(text: txtAlert01ChangeAppPassword[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            // alertCustom(message: txtAlert01ChangeAppPassword[languageChooseSave], delay: 2)
            txtOldPassword_ChangeAppPassword.becomeFirstResponder()
            return
        }
        if (txtNewPassword_ChangeAppPassword.text?.count)! < 8 {
            showBannerNotifi(text: txtAlert02ChangeAppPassword[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            // alertCustom(message: txtAlert02ChangeAppPassword[languageChooseSave], delay: 2)
            txtNewPassword_ChangeAppPassword.becomeFirstResponder()
            return
        }
        if (txtConfirmNewPassword_ChangeAppPassword.text?.count)! < 8 {
            showBannerNotifi(text: txtAlert03ChangeAppPassword[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            txtConfirmNewPassword_ChangeAppPassword.becomeFirstResponder()
            // alertCustom(message: txtAlert03ChangeAppPassword[languageChooseSave], delay: 2)
            
            return
        }
        if (txtNewPassword_ChangeAppPassword.text != txtOldPassword_ChangeAppPassword.text) == false {
            showBannerNotifi(text: txtAlert05ChangeAppPassword[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            // alertCustom(message: txtAlert05ChangeAppPassword[languageChooseSave], delay: 2)
            txtNewPassword_ChangeAppPassword.becomeFirstResponder()
            return
        }
        if (txtNewPassword_ChangeAppPassword.text == txtConfirmNewPassword_ChangeAppPassword.text) == false {
            showBannerNotifi(text: txtAlert04ChangeAppPassword[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            // alertCustom(message: txtAlert04ChangeAppPassword[languageChooseSave], delay: 2)
            txtConfirmNewPassword_ChangeAppPassword.becomeFirstResponder()
            return
        }
        removeChangeAppPasswordView()
        changeAppPassword()
    }
    
    @IBAction func btnChangeAppPasswordCancelSetParameterTapped(_ sender: Any) {
        removeChangeAppPasswordView()
    }
    
    @IBAction func btnSecurityOldPasswordInChangeAppPasswordTapped(_ sender: Any) {
        txtOldPassword_ChangeAppPassword.isSecureTextEntry = !txtOldPassword_ChangeAppPassword.isSecureTextEntry
    }
    
    @IBAction func btnSecurityNewPasswordInChangeAppPasswordTapped(_ sender: Any) {
        txtNewPassword_ChangeAppPassword.isSecureTextEntry = !txtNewPassword_ChangeAppPassword.isSecureTextEntry
    }
    
    @IBAction func btnSecurityConfirmNewPasswordInChangeAppPasswordTapped(_ sender: Any) {
        txtConfirmNewPassword_ChangeAppPassword.isSecureTextEntry = !txtConfirmNewPassword_ChangeAppPassword.isSecureTextEntry
    }
    
    //
    
    // MARK: - Change User permission
    
    @IBOutlet var changeUserPhoneNumberView: UIView!
    @IBOutlet var txtVehiclePassword_ChangeUserPhoneNumberView: UITextField!
    @IBOutlet var txtNewUserPhoneNumber_ChangeUserPhoneNumber: UITextField!
    func displayChangeUserPhoneNumberView() {
        txtNewUserPhoneNumber_ChangeUserPhoneNumber.becomeFirstResponder()
        changeUserPhoneNumberView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(changeUserPhoneNumberView)
        txtOldPassword_ChangeVehiclePassword.text?.removeAll()
        txtNewPassword_ChangeVehiclePassword.text?.removeAll()
        txtConfirmNewPassword_ChangeVehiclePassword.text?.removeAll()
        hideVehiclePass()
        lblTitleChangeUserPhone.text = txtTitleChangeUserPhone[languageChooseSave].uppercased()
        lblContent01ChangeUserPhone.text = txtContent01ChangeUserPhone[languageChooseSave]
        lblContent02ChangeUserPhone.text = txtContent02ChangeUserPhone[languageChooseSave]
        sendBySmsFlag = false
        btnSendByGPSR_ChangeUserPhoneNumber.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
        btnSendBySMS_ChangeUserPhoneNumber.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        btnSendByGPSR_ChangeUserPhoneNumber.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        btnSendByGPSR_ChangeUserPhoneNumber.setTitleColor(#colorLiteral(red: 0.1490196078, green: 0.3490196078, blue: 0.6705882353, alpha: 1), for: .normal)
        btnConfirmChangeUserPhone.drawShadow(offset: CGSize(width: 0, height: 3))
        btnCancelChangeUserPhone.drawShadow(offset: CGSize(width: 0, height: 3))
        txtNewUserPhoneNumber_ChangeUserPhoneNumber.text = ""
        txtVehiclePassword_ChangeUserPhoneNumberView.text = ""
        lblChangeUser_PhoneError.backgroundColor = .white
        lblChangeUser_PassError.backgroundColor = .white
        
        txtVehiclePassword_ChangeUserPhoneNumberView.isSecureTextEntry = true
        btnSecurityVehiclePassword_ChangeUser.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
        btnBackChangeUser.addTarget(self, action: #selector(CancelChangeUserPhoneNumber), for: .touchUpInside)
    }
    
    func hideVehiclePass() {
        lblContent01ChangeUserPhone.isHidden = true
        lblChangeUser_PassError.isHidden = true
        btnClearVehiclePass_ChangeUserPhone.isHidden = true
        txtVehiclePassword_ChangeUserPhoneNumberView.isHidden = true
        lblChangeUser_Description.isHidden = true
        btnSecurityVehiclePassword_ChangeUser.isHidden = true
        txtVehiclePassword_ChangeUserPhoneNumberView.text = ""
        viewVehiclePass_ChangeUser.isHidden = true
    }
    
    func showVehiclePass() {
        lblContent01ChangeUserPhone.isHidden = false
        lblChangeUser_Description.isHidden = false
        txtVehiclePassword_ChangeUserPhoneNumberView.isHidden = false
        viewVehiclePass_ChangeUser.isHidden = false
        lblChangeUser_PassError.isHidden = false
    }
    
    func removeChangeUserPhoneNumberView() {
        changeUserPhoneNumberView.removeFromSuperview()
        btnSendBySMS_ChangeUserPhoneNumber.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnSendBySMS_ChangeUserPhoneNumber.titleLabel?.font = UIFont.systemFont(ofSize: 16)
    }
    
    @IBAction func closeChangeUserPhoneNumberView(_ sender: Any) {
        changeUserPhoneNumberView.removeFromSuperview()
    }
    
    @IBAction func btnChangeUserPhoneNumberConfirm2SetParameterTapped(_ sender: Any) {
        if sendBySmsFlag == true {
            if txtVehiclePassword_ChangeUserPhoneNumberView.text?.count != 6 {
//                alertCustom(message: txtAlert01ChangeUserPhone[languageChooseSave], delay: 2)
                lblChangeUser_PassError.backgroundColor = #colorLiteral(red: 0.933637917, green: 0.5668299198, blue: 0.1574823558, alpha: 1)
                lblChangeUser_PassError.text = txtAlert01ChangeUserPhone[languageChooseSave]
                lblChangeUser_PhoneError.backgroundColor = .white
                txtVehiclePassword_ChangeUserPhoneNumberView.becomeFirstResponder()
                return
            } else {
                lblChangeUser_PassError.backgroundColor = .white
            }
        }
        if (txtNewUserPhoneNumber_ChangeUserPhoneNumber.text?.count)! < 10 {
//            alertCustom(message: txtAlert02ChangeUserPhone[languageChooseSave], delay: 2)
            lblChangeUser_PhoneError.backgroundColor = #colorLiteral(red: 0.933637917, green: 0.5668299198, blue: 0.1574823558, alpha: 1)
            lblChangeUser_PhoneError.text = txtAlert02ChangeUserPhone[languageChooseSave]
            txtNewUserPhoneNumber_ChangeUserPhoneNumber.becomeFirstResponder()
            return
        } else {
            lblChangeUser_PhoneError.backgroundColor = .white
        }
        removeChangeUserPhoneNumberView()
        
        if sendBySmsFlag == true {
            smsSettingContext = "SET," + txtVehiclePassword_ChangeUserPhoneNumberView.text! + "," + "USER=" + txtNewUserPhoneNumber_ChangeUserPhoneNumber.text!
            removeChangeUserPhoneNumberView()
            sendMessage(phoneNumber: vehicle2show.sim, content: smsSettingContext)
        } else {
            if checkVehicleState() == false {
                return
            }
            let content = "USER=" + txtNewUserPhoneNumber_ChangeUserPhoneNumber.text!
            SoapHandShaking(commandIndex: "3", inputContent: content)
            // serverHandShaking(commandIndex:"3",inputContent: content)
        }
    }
    
    @IBAction func btnSecurityVehiclePassword_ChangeUserPhoneNumberTapped(_ sender: Any) {
        if txtVehiclePassword_ChangeUserPhoneNumberView.isSecureTextEntry == false {
            btnSecurityVehiclePassword_ChangeUser.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
        } else {
            btnSecurityVehiclePassword_ChangeUser.setImage(UIImage(named: "invisible_eye_dark"), for: .normal)
        }
        txtVehiclePassword_ChangeUserPhoneNumberView.isSecureTextEntry = !txtVehiclePassword_ChangeUserPhoneNumberView.isSecureTextEntry
    }
    
    @IBAction func btnChangeUserPhoneNumberCancelSetParameterTapped(_ sender: Any) {
        removeChangeUserPhoneNumberView()
    }
    
    @objc func CancelChangeUserPhoneNumber() {
        removeChangeUserPhoneNumberView()
    }
    
    @IBOutlet var btnSendBySMS_ChangeUserPhoneNumber: UIButton!
    @IBAction func btnSendBySMS_ChangeUserPhoneNumberTapped(_ sender: Any) {
        showVehiclePass()
        btnSendBySMS_ChangeUserPhoneNumber.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
        btnSendByGPSR_ChangeUserPhoneNumber.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        sendBySmsFlag = true
        txtVehiclePassword_ChangeUserPhoneNumberView.becomeFirstResponder()
        
        btnSendByGPSR_ChangeUserPhoneNumber.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        btnSendBySMS_ChangeUserPhoneNumber.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        btnSendByGPSR_ChangeUserPhoneNumber.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnSendBySMS_ChangeUserPhoneNumber.setTitleColor(#colorLiteral(red: 0.1490196078, green: 0.3490196078, blue: 0.6705882353, alpha: 1), for: .normal)
        lblChangeUser_PassError.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblChangeUser_PhoneError.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    @IBOutlet var btnSendByGPSR_ChangeUserPhoneNumber: UIButton!
    @IBAction func btnSendByGPSR_ChangeUserPhoneNumberTapped(_ sender: Any) {
        hideVehiclePass()
        btnSendByGPSR_ChangeUserPhoneNumber.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
        btnSendBySMS_ChangeUserPhoneNumber.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        sendBySmsFlag = false
        txtNewUserPhoneNumber_ChangeUserPhoneNumber.becomeFirstResponder()
        
        btnSendByGPSR_ChangeUserPhoneNumber.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        btnSendBySMS_ChangeUserPhoneNumber.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        btnSendByGPSR_ChangeUserPhoneNumber.setTitleColor(#colorLiteral(red: 0.1490196078, green: 0.3490196078, blue: 0.6705882353, alpha: 1), for: .normal)
        btnSendBySMS_ChangeUserPhoneNumber.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        lblChangeUser_PassError.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        lblChangeUser_PhoneError.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    //
    
    // MARK: - Vehicle registry
    
    @IBOutlet var vehicleRegistryView: UIView!
    @IBAction func btnCloseViewRegister(_ sender: Any) {
        removeVehicleRegistryView()
        lbRegistryError1.isHidden = true
        lbRegistryError2.isHidden = true
        lbRegistryError3.isHidden = true
    }
    
    func displayVehicleRegistryView() {
        txtOldPassword_Registry.becomeFirstResponder()
        vehicleRegistryView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(vehicleRegistryView)
        txtOldPassword_Registry.text?.removeAll()
        txtNewPassword_Registry.text?.removeAll()
        txtConfirmNewPassword_Registry.text?.removeAll()
        lblTitleVehicleRegistry.text = txtTitleVehicleRegistry[languageChooseSave].uppercased()
        
        let attrs1 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17), NSAttributedString.Key.foregroundColor: UIColor.black]
        let attrs2 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor.black]
        let attributedString1 = NSMutableAttributedString(string: "Số SIM: ", attributes: attrs1)
        let attributedString2 = NSMutableAttributedString(string: vehicle2show.sim, attributes: attrs2)
        attributedString1.append(attributedString2)
        lblVehicleRegistrySim.attributedText = attributedString1
        lblVehicleRegistrySim.text = "Số SIM: " + vehicle2show.sim
        
        // Shadow button
        btnConfirmVehicleRegistry.drawShadow(offset: CGSize(width: 0, height: 3))
        btnCancelVehicleRegistry.drawShadow(offset: CGSize(width: 0, height: 3))
        sendBySmsFlag = true
        
        txtOldPassword_Registry.isSecureTextEntry = true
        txtNewPassword_Registry.isSecureTextEntry = true
        txtConfirmNewPassword_Registry.isSecureTextEntry = true
        btnSecurityOldPasswordVehicleRegistry.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
        btnSecurityNewPasswordVehicleRegistry.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
        btnSecurityConfirmNewPasswordVehicleRegistry.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
        
        btnSecurityOldPasswordVehicleRegistry.isHidden = true
        btnSecurityNewPasswordVehicleRegistry.isHidden = true
        btnSecurityConfirmNewPasswordVehicleRegistry.isHidden = true
        btnClearVehicleRegistry1.isHidden = true
        btnClearVehicleRegistry2.isHidden = true
        btnClearVehicleRegistry3.isHidden = true
    }
    
    func removeVehicleRegistryView() {
        txtOldPassword_Registry.text = ""
        txtNewPassword_Registry.text = ""
        txtConfirmNewPassword_Registry.text = ""
        vehicleRegistryView.removeFromSuperview()
    }
    
    @IBAction func closeVehicleRegistryView(_ sender: Any) {
        removeVehicleRegistryView()
    }
    
    @IBAction func btnVehicleRegistryConfirm2SetParameterTapped(_ sender: AnyObject) {
        if txtOldPassword_Registry.text?.count != 6 {
//            alertCustom(message: txtAlert01VehicleRegistry[languageChooseSave], delay: 2)
            lbRegistryError1.isHidden = false
            lbRegistryError1.text = txtAlert01VehicleRegistry[languageChooseSave]
            txtOldPassword_Registry.becomeFirstResponder()
            return
        }
        lbRegistryError1.isHidden = true
        if txtNewPassword_Registry.text?.count != 6 {
            lbRegistryError2.isHidden = false
            lbRegistryError2.text = txtAlert02VehicleRegistry[languageChooseSave]
//            alertCustom(message: txtAlert02VehicleRegistry[languageChooseSave], delay: 2)
            txtNewPassword_Registry.becomeFirstResponder()
            return
        }
        if txtConfirmNewPassword_Registry.text?.count != 6 {
            lbRegistryError3.isHidden = false
            lbRegistryError3.text = txtAlert03VehicleRegistry[languageChooseSave]
//            alertCustom(message: txtAlert03VehicleRegistry[languageChooseSave], delay: 2)
            txtConfirmNewPassword_Registry.becomeFirstResponder()
            return
        }
        lbRegistryError2.isHidden = true
        if (txtNewPassword_Registry.text == txtConfirmNewPassword_Registry.text) == false {
            lbRegistryError3.isHidden = false
            lbRegistryError3.text = txtAlert04VehicleRegistry[languageChooseSave]
//            alertCustom(message: txtAlert04VehicleRegistry[languageChooseSave], delay: 2)
            txtConfirmNewPassword_Registry.becomeFirstResponder()
            return
        }
        lbRegistryError3.isHidden = true
        removeVehicleRegistryView()
        if sendBySmsFlag == true {
            let sdt: String = vehicle2show.sim
            smsSettingContext = "START," + txtOldPassword_Registry.text! + "," + txtNewPassword_Registry.text! + "," + sdt
            removeVehicleRegistryView()
            sendMessage(phoneNumber: sdt, content: smsSettingContext)
        }
    }
    
    @IBAction func btnVehicleRegistryCancelSetParameterTapped(_ sender: AnyObject) {
        removeVehicleRegistryView()
        lbRegistryError1.isHidden = true
        lbRegistryError2.isHidden = true
        lbRegistryError3.isHidden = true
    }
    
    @IBAction func btnSecurityOldPasswordVehicleRegistryTapped(_ sender: Any) {
        if txtOldPassword_Registry.isSecureTextEntry == false {
            btnSecurityOldPasswordVehicleRegistry.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
        } else {
            btnSecurityOldPasswordVehicleRegistry.setImage(UIImage(named: "invisible_eye_dark"), for: .normal)
        }
        txtOldPassword_Registry.isSecureTextEntry = !txtOldPassword_Registry.isSecureTextEntry
    }
    
    @IBAction func btnSecurityNewPasswordVehicleRegistryTapped(_ sender: Any) {
        if txtNewPassword_Registry.isSecureTextEntry == false {
            btnSecurityNewPasswordVehicleRegistry.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
        } else {
            btnSecurityNewPasswordVehicleRegistry.setImage(UIImage(named: "invisible_eye_dark"), for: .normal)
        }
        txtNewPassword_Registry.isSecureTextEntry = !txtNewPassword_Registry.isSecureTextEntry
    }
    
    @IBAction func btnSecurityConfirmNewPasswordVehicleRegistryTapped(_ sender: Any) {
        if txtConfirmNewPassword_Registry.isSecureTextEntry == false {
            btnSecurityConfirmNewPasswordVehicleRegistry.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
        } else {
            btnSecurityConfirmNewPasswordVehicleRegistry.setImage(UIImage(named: "invisible_eye_dark"), for: .normal)
        }
        txtConfirmNewPassword_Registry.isSecureTextEntry = !txtConfirmNewPassword_Registry.isSecureTextEntry
    }
    
    //
    
    // MARK: - Device reboot
    
    @IBOutlet var deviceRebootView: UIView!
    @IBOutlet var lbNotifyDeviceReboot: UILabel!
    
    func displayDeviceRebootView() {
        deviceRebootView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(deviceRebootView)
        lblTitleDeviceReboot.text = txtTitleDeviceReboot[languageChooseSave].uppercased()
        sendBySmsFlag = false
        btnSendByGPRS_DeviceReboot.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
        btnSendBySMS_DeviceReboot.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        btnSendByGPRS_DeviceReboot.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        btnSendBySMS_DeviceReboot.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        btnSendByGPRS_DeviceReboot.setTitleColor(#colorLiteral(red: 0.1490196078, green: 0.3490196078, blue: 0.6705882353, alpha: 1), for: .normal)
        btnSendBySMS_DeviceReboot.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        lbNotifyDeviceReboot.text = "(Sử dụng data, không mất phí)"
    }
    
    func removeDeviceRebootView() {
        deviceRebootView.removeFromSuperview()
    }
    
    @IBAction func closeDeviceRebootView(_ sender: Any) {
        removeDeviceRebootView()
    }
    
    @IBAction func btnDeviceRebootConfirm2SetParameterTapped(_ sender: AnyObject) {
        removeDeviceRebootView()
        if sendBySmsFlag == true {
            smsSettingContext = rebootString
            removeDeviceRebootView()
            sendMessage(phoneNumber: vehicle2show.sim, content: smsSettingContext)
        } else {
            if checkVehicleState() == false {
                return
            }
            let content = "REBOOT"
            SoapHandShaking(commandIndex: "8", inputContent: content)
        }
    }
    
    @IBAction func btnDeviceRebootCancelSetParameterTapped(_ sender: AnyObject) {
        removeDeviceRebootView()
    }
    
    @IBOutlet var btnSendBySMS_DeviceReboot: UIButton!
    @IBAction func btnSendBySMS_DeviceRebootTapped(_ sender: Any) {
        btnSendBySMS_DeviceReboot.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
        btnSendByGPRS_DeviceReboot.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        sendBySmsFlag = true
        btnSendByGPRS_DeviceReboot.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        btnSendBySMS_DeviceReboot.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        btnSendByGPRS_DeviceReboot.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        btnSendBySMS_DeviceReboot.setTitleColor(#colorLiteral(red: 0.1490196078, green: 0.3490196078, blue: 0.6705882353, alpha: 1), for: .normal)
        lbNotifyDeviceReboot.text = "(Gửi tin nhắn mất phí)"
    }
    
    @IBOutlet var btnSendByGPRS_DeviceReboot: UIButton!
    @IBAction func btnSendByGPRS_DeviceRebootTapped(_ sender: Any) {
        btnSendByGPRS_DeviceReboot.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
        btnSendBySMS_DeviceReboot.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        sendBySmsFlag = false
        btnSendByGPRS_DeviceReboot.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        btnSendBySMS_DeviceReboot.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        btnSendByGPRS_DeviceReboot.setTitleColor(#colorLiteral(red: 0.1490196078, green: 0.3490196078, blue: 0.6705882353, alpha: 1), for: .normal)
        btnSendBySMS_DeviceReboot.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        lbNotifyDeviceReboot.text = "(Sử dụng data, không mất phí)"
    }
    
    //
    
    // MARK: - Device format
    
    @IBOutlet var deviceFormatView: UIView!
    func displayDeviceFormatView() {
        deviceFormatView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(deviceFormatView)
        lblTitleDeviceFormat.text = txtTitleDeviceFormat[languageChooseSave].uppercased()
//        lblContentDeviceFormat.text = txtContentDeviceFormat[languageChooseSave]
//        btnConfirmDeviceFormat.setTitle(txtButtonConfirmDeviceFormat[languageChooseSave], for: .normal)
//        btnCancelDeviceFormat.setTitle(txtButtonCancelDeviceFormat[languageChooseSave], for: .normal)
        sendBySmsFlag = true
    }
    
    func removeDeviceFormatView() {
        deviceFormatView.removeFromSuperview()
    }
    
    @IBAction func closeDeviceFormatView(_ sender: Any) {
        removeDeviceFormatView()
    }
    
    @IBAction func btnDeviceFormatConfirm2SetParameterTapped(_ sender: AnyObject) {
        smsSettingContext = formatString
        removeDeviceFormatView()
        sendMessage(phoneNumber: vehicle2show.sim, content: smsSettingContext)
    }
    
    @IBAction func btnDeviceFormatCancelSetParameterTapped(_ sender: AnyObject) {
        removeDeviceFormatView()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    //
    
    // MARK: - Ask Vehicle' password
    
    @IBOutlet var askPasswordView: UIView!
    func displayAskPasswordView() {
        askPasswordView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(askPasswordView)
        txtVehiclePassword.text?.removeAll()
        lblTitleAskPassword.text = txtTitleAskPassword[languageChooseSave].uppercased()
        lblContentAskPassword.text = txtContentAskPassword[languageChooseSave]
        txtVehiclePassword.becomeFirstResponder()
        lbWarningError.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    func removeAskPasswordView() {
        askPasswordView.removeFromSuperview()
        switch alarmSet {
        case 0:
            displayAccAlarmSettingView()
        case 1:
            displayVibrateAlarmSettingView()
        case 2:
            displayFenceAlarmSettingView()
        case 3:
            displaySpeedAlarmSettingView()
        default:
            break
        }
    }
    
    @IBAction func closeAskPasswordView(_ sender: Any) {
        removeAskPasswordView()
    }
    
    @IBAction func saveVehiclePasswordTapped(_ sender: AnyObject) {
        if txtVehiclePassword.text?.count != 6 {
            // alertCustom(message: txtAlert01SettingVC[languageChooseSave], delay: 2)
            lbWarningError.backgroundColor = #colorLiteral(red: 0.933637917, green: 0.5668299198, blue: 0.1574823558, alpha: 1)
            txtVehiclePassword.becomeFirstResponder()
            return
        }
        vehiclePassword = txtVehiclePassword.text
        removeAskPasswordView()
        view.endEditing(true)
        switch alarmSet {
        case 0: accSetParameter()
        case 1: vibSetParameter()
        case 2: fenceSetParameter()
        case 3: speedSetParameter()
        default: break
        }
        btnSecurityVehiclePassword.isHidden = true
    }
    
    @IBAction func cacelSaveVehiclePasswordTapped(_ sender: AnyObject) {
        removeAskPasswordView()
        btnSecurityVehiclePassword.isHidden = true
        keyBoardHide()
    }
    
    @IBAction func btnSecurityVehiclePasswordTapped(_ sender: Any) {
        txtVehiclePassword.isSecureTextEntry = !txtVehiclePassword.isSecureTextEntry
    }
    
    // MARK: - Choose Language
    
    @IBOutlet var chooseLanguageView: UIView!
    func displayChooseLanguageView() {
        chooseLanguageView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(chooseLanguageView)
        if languageChooseSave == 0 {
            btnVietNamese_ChooseLangague.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
            btnEnglish_ChooseLangague.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        } else if languageChooseSave == 1 {
            btnEnglish_ChooseLangague.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
            btnVietNamese_ChooseLangague.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        }
        lblTitleChooseLanguage.text = txtTitleChooseLanguage[languageChooseSave].uppercased()
        btnVietNamese_ChooseLangague.setTitle(txtButtonVietnamese_ChooseLanguage[languageChooseSave], for: .normal)
        btnEnglish_ChooseLangague.setTitle(txtButtonEnglish_ChooseLanguage[languageChooseSave], for: .normal)
//        btnConfirmChooseLanguage.setTitle(txtButtonConfirmChooseLanguage[languageChooseSave], for: .normal)
//        btnCancelChooseLanguage.setTitle(txtButtonCancelChooseLanguage[languageChooseSave], for: .normal)
    }
    
    func removeChooseLanguageView() {
        chooseLanguageView.removeFromSuperview()
    }
    
    @IBAction func closeChooseLanguageView(_ sender: Any) {
        removeChooseLanguageView()
    }
    
    @IBAction func btnChooseLanguageConfirmTapped(_ sender: Any) {
        languageChooseSaveFlag = true
        if languageChooseSave == languageChooseIndex {
            alertCustom(message: "Quý khách không thay đổi ngôn ngữ!", delay: 2)
            removeChooseLanguageView()
            return
        }
        languageChooseSave = languageChooseIndex
        gotoLoginView()
    }
    
    @IBAction func btnChooseLanguageCancelTapped(_ sender: Any) {
        removeChooseLanguageView()
    }
    
    @IBOutlet var btnVietNamese_ChooseLangague: UIButton!
    @IBAction func btnVietNamese_ChooseLangagueTapped(_ sender: Any) {
        btnVietNamese_ChooseLangague.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
        btnEnglish_ChooseLangague.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        languageChooseIndex = 0
    }
    
    @IBOutlet var btnEnglish_ChooseLangague: UIButton!
    @IBAction func btnEnglish_ChooseLangagueTapped(_ sender: Any) {
        btnEnglish_ChooseLangague.setImage(#imageLiteral(resourceName: "icon_RadioCheckbox"), for: .normal)
        btnVietNamese_ChooseLangague.setImage(#imageLiteral(resourceName: "icon_RadioUnCheckbox"), for: .normal)
        languageChooseIndex = 1
    }
    
    //
    
    func checkVehicleState() -> Bool {
        switch vehicle2show.state {
        case 5: // thiết bị đang ngủ đông k thể thực hiện
            // alertCustom(message: txtAlert02SettingVC[languageChooseSave], delay: 2)
            showBannerNotifi(text: txtAlert02SettingVC[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            return false
        case 6:
            // alertCustom(message: txtAlert03SettingVC[languageChooseSave], delay: 2)
            showBannerNotifi(text: txtAlert03SettingVC[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
            return false
        case 0: // Thiết bị chưa kết nối đến hệ thống!
            // alertCustom(message: txtAlert04SettingVC[languageChooseSave], delay: 2)
            showBannerNotifi(text: txtAlert04SettingVC[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!)
            return false
        default:
            break
        }
        return true
    }
    
    // MARK: Alert custom
    
    func alertCustom(message: String, delay: Double) {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        AlertCustom().Alert(delegate: self, message: message, textAlignment: "", delay: delay)
    }
}

extension SettingVC: URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
}
