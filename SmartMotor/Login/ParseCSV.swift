//
//  ParseCSV.swift
//  SmartMotor
//
//  Created by Home on 5/4/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import CSV
import UIKit

extension LoginVC {
    // MARK: - Parse data from csv
    
    func parseCSV() {
        let path = Bundle.main.path(forResource: "Language", ofType: "csv")
        do {
            let stream = InputStream(fileAtPath: path!)!
            let csv = try! CSVReader(stream: stream, hasHeaderRow: true, trimFields: false, delimiter: ",", whitespaces: .whitespaces)
//            let csv = try! CSV(url: URL(fileURLWithPath: path!))
            
            while csv.next() != nil {
                let key = csv["Key"]!
                let english = csv["en.xml"]!
                let vietnamese = csv["vi.xml"]!
                
                var arrayLanguage: [String] = [vietnamese, english]
                for i in 0...arrayLanguage.count - 1 {
                    arrayLanguage[i] = arrayLanguage[i].replacingOccurrences(of: ";", with: ",")
                    arrayLanguage[i] = arrayLanguage[i].replacingOccurrences(of: "\\\'\\\'%s\\\'\\\'", with: "")
                    arrayLanguage[i] = arrayLanguage[i].replacingOccurrences(of: "\\n\\n", with: "\n")
                    arrayLanguage[i] = arrayLanguage[i].replacingOccurrences(of: "\\n", with: "\n")
                    arrayLanguage[i] = arrayLanguage[i].replacingOccurrences(of: "%s", with: "%@")
                    arrayLanguage[i] = arrayLanguage[i].replacingOccurrences(of: "\\\"", with: "\"")
                    arrayLanguage[i] = arrayLanguage[i].replacingOccurrences(of: "\\\'s", with: "'s")
                    arrayLanguage[i] = arrayLanguage[i].replacingOccurrences(of: "\\'s", with: "'s")
                }
                
                switch key {
                case "text_empty_captcha":
                    txtEnterCaptcha = arrayLanguage
                case "LOADING":
                    txtLoading = arrayLanguage
                //
                case "USERNAME":
                    txtUserTextField_PlaceHolder = arrayLanguage
                case "PASSWORD":
                    txtPasswordTextField_PlaceHolder = arrayLanguage
                    txtContentAskPassword = arrayLanguage
                case "CAPTCHA":
                    txtCaptchaTextField_PlaceHolder = arrayLanguage
                case "LOGIN":
                    txtButton_Title = arrayLanguage
                case "text_lost_password":
                    txtForgotPassword_Title = arrayLanguage
                case "text_register":
                    txtServiceInfo_Title = arrayLanguage
                case "text_service":
                    txtHotline_title = arrayLanguage
                case "text_empty_account":
                    txtAlertContent_UserEmpty = arrayLanguage
                case "text_empty_password":
                    txtAlertContent_PasswordEmpty = arrayLanguage
                case "text_login_fail":
                    txtAlertContent_LoginFail = arrayLanguage
                case "text_message_charging_fail_invalid_captcha":
                    txtAlertContent_CaptchaInvalid = arrayLanguage
                    txtResult06ChargeCardCode = arrayLanguage
                case "text_message_charging_fail_invalid_captcha_system":
                    txtAlertContent_CaptchaInvalid_SystemError = arrayLanguage
                    txtResult05ChargeCardCode = arrayLanguage
                case "str_title_support":
                    txtTitleHotlineConfirm = arrayLanguage
                    txtTitleConfirmCallHotline = arrayLanguage
                case "str_call_confirm":
                    txtContentHotlineConfirm = arrayLanguage
                    txtContentConfirmCallHotline = arrayLanguage
                case "yes":
                    txtOKHotlineConfirm = arrayLanguage
                    txtButtonOKConfirmLogout = arrayLanguage
                    txtButtonOKConfirmChargeCardCode = arrayLanguage
                    txtButtonConfirmCallHotline = arrayLanguage
                    txtButtonOKConfirmPayMoney = arrayLanguage
                    txtButtonConfirmSelectDateTime = arrayLanguage
                    txtButtonConfirmDeviceReboot = arrayLanguage
                    txtButtonConfirmDeviceFormat = arrayLanguage
                    txtButtonOKConfirmLockVehicle = arrayLanguage
                    txtButtonOKConfirmUnLockVehicle = arrayLanguage
                    txtButtonOKConfirmCall2Vehicle = arrayLanguage
                    txtButtonOkWarningAllowAccessLocation = arrayLanguage
                    txtButtonConfirmChooseLanguage = arrayLanguage
                    txtButtonConfirmAskPassword = arrayLanguage
                    txtButtonOkWarningWrongLocation = arrayLanguage
                    txtButtonOKLockByCall = arrayLanguage
                    txtButtonOKUnlockByCall = arrayLanguage
                case "no":
                    txtCancelHotlineConfirm = arrayLanguage
                    txtButtonCancelConfirmLogout = arrayLanguage
                    txtButtonCancelConfirmChargeCardCode = arrayLanguage
                    txtButtonCancelConfirmCallHotline = arrayLanguage
                    txtButtonCancelConfirmPayMoney = arrayLanguage
                    txtButtonCancelDeviceReboot = arrayLanguage
                    txtButtonCancelDeviceFormat = arrayLanguage
                    txtButtonCancelConfirmLockVehicle = arrayLanguage
                    txtButtonCancelConfirmUnLockVehicle = arrayLanguage
                    txtButtonCancelConfirmCall2Vehicle = arrayLanguage
                    txtButtonCancelWarningWrongLocation = arrayLanguage
                    txtButtonCancelLockByCall = arrayLanguage
                    txtButtonCancelUnlockByCall = arrayLanguage
                case "text_connect_server_error":
                    txtcannotConnect2Server = arrayLanguage
                    connectionFail = arrayLanguage
                    txtConnectServerFail = arrayLanguage
                case "LOGIN_UNSUCCESS":
                    txtloginUnsuccess = arrayLanguage
                // list vehicle
                case "text_list_of_vehicle":
                    listVehicleVC_Title = arrayLanguage
                case "text_hint_search":
                    listVehicleVC_Searching_PlaceHolder = arrayLanguage
                case "str_type_device":
                    listVehicleVC_RegNo_Choose = arrayLanguage
                case "TEXT_MOTOR_SELECTED":
                    listVehicleVC_RegNo_Select = arrayLanguage
                case "ERROR_LOAD_DATA":
                    cannotLoadListVehicle = arrayLanguage
                    txtloadDataUnSuccess = arrayLanguage
                case "NO_DATA":
                    noDataString = arrayLanguage
                case "LOGOUT":
                    listVehicleVC_BtnLogout_Title = arrayLanguage
                case "TEXT_HELP_SIM_INFO":
                    txtButtonBalanceInfo = arrayLanguage
                    txtTitleAccountInfo = arrayLanguage
                case "text_tittle_sos":
                    txtButtonHelp = [vietnamese, english]
                    txtTitleSOS = [vietnamese, english]
                    
                case "str_content_sos":
                    txtContentSOS = arrayLanguage
                    
                case "text_tittle_topup":
                    txtButtonListChargeMoney = arrayLanguage
                case "LOGOUT_UNSUCCESS":
                    txtLogoutUnSuccessAlert = arrayLanguage
                case "text_app_info_alert":
                    txtTitleConfirmLogout = arrayLanguage
                    txtTitleWarningAllowAccessLocation = arrayLanguage
                case "TEXT_CONFIRM_LOGOUT":
                    txtContentConfirmLogout = arrayLanguage
                //
                case "text_pick_payment_type_title":
                    titleListChargeMoney = arrayLanguage
                case "text_payment_via_card":
                    listChargeMoney_PaymentViaCard = arrayLanguage
                case "text_payment_via_phone_balance":
                    listChargeMoney_PaymentViaPhoneBalance = arrayLanguage
                    txtTitlePayMoney = arrayLanguage
                case "close_button":
                    txtButtonCloseListChargeMoney = arrayLanguage
                //
                case "text_payment_via_card_title":
                    txtTitleChargeCardCode = arrayLanguage
                case "TITLE_POPUP_SIM_CODE_TITLE":
                    //txtContent01ChargeCardCode = arrayLanguage
                    txtAlertCardCode_ChargeCardCode = arrayLanguage
                case "TITLE_POPUP_SIM_CODE_NOTE":
                    txtContent02ChargeCardCode = arrayLanguage
                case "text_device_number":
                    txtContent03ChargeCardCode = arrayLanguage
                case "str_title_card_charging":
                    txtContent04ChargeCardCode = arrayLanguage
                case "text_hint_captcha_input":
                    txtContent05ChargeCardCode = arrayLanguage
                    txtContent04PayMoney = arrayLanguage
                case "CONFIRM":
                    txtButtonConfirmChargeCardCode = arrayLanguage
                    txtButtonOKPayMoney = arrayLanguage
                case "cancel":
                    txtButtonCancelChargeCardCode = arrayLanguage
                    txtButtonCancelChooseLanguage = arrayLanguage
                    txtButtonCancelAskPassword = arrayLanguage
                    txtButtonCancelVehicleRegistry = arrayLanguage
                    txtButtonCancelChangeAppPassword = arrayLanguage
                    txtButtonCancelChangeVehiclePassword = arrayLanguage
                    txtButtonCancelChangeUserPhone = arrayLanguage
                case "str_nap_tien":
                    txtTitleConfirmChargeCardCode = arrayLanguage
                    txtTitleConfirmPayMoney = arrayLanguage
                    txtButtonServicePaymentAlertForBidden = arrayLanguage
                case "text_message_confirm_charging":
                    txtContentConfirmChargeCardCode = arrayLanguage
                    txtContentConfirmPayMoney = arrayLanguage
                case "text_message_charging_success":
                    txtResult01_ChargeCardCode = arrayLanguage
                case "text_message_charging_fail_invalid_card":
                    txtResult02ChargeCardCode = arrayLanguage
                case "text_message_charging_fail_invalid_phone":
                    txtResult03ChargeCardCode = arrayLanguage
                case "TEXT_RE_LOGIN_ERROR":
                    txtResult04ChargeCardCode = arrayLanguage
                case "text_message_charging_fail_not_monitor_role":
                    txtResult07ChargeCardCode = arrayLanguage
                case "text_message_charging_fail_error_connect":
                    txtResult08ChargeCardCode = arrayLanguage
                //
                case "quit":
                    txtButtonCancelSOS = arrayLanguage
                    txtButtonCancelAccountInfo = arrayLanguage
                    txtButtonCancelPayMoney = arrayLanguage
                    txtButtonCancelAlertForBidden = arrayLanguage
                    txtButtonCancelSelectDateTime = arrayLanguage
                    txtButtonCancelListAlarmSetting = arrayLanguage
                    txtButtonCancelWarningAllowAccessLocation = arrayLanguage
                //
                case "text_sim_info_free_sms":
                    txtContent01AccountInfo = arrayLanguage
                case "text_sim_info_basic_balance":
                    txtContent02AccountInfo = arrayLanguage
                case "text_sim_info_free_data":
                    txtContent03AccountInfo = arrayLanguage
                case "text_sim_info_free_money":
                    txtContent04AccountInfo = arrayLanguage
                case "text_sim_info_sms_ngoaimang":
                    txtContent05AccountInfo = arrayLanguage
                case "text_sim_info_time_promotion":
                    txtContent06AccountInfo = arrayLanguage
                case "text_cannot_get_acc_info":
                    txtAlertAccountInfo = arrayLanguage
                //
                case "text_payment_month":
                    txtMonthPay = arrayLanguage
                case "text_payment_via_phone_balance_des":
                    txtContent01_PayMoney = arrayLanguage
                case "text_payment_months_title":
                    txtContent02PayMoney = arrayLanguage
                case "text_payment_money_total_title":
                    txtContent03PayMoney = arrayLanguage
                case "text_message_charging_owner_success":
                    txtPaymentSuccess = arrayLanguage
                case "str_topup_success_ful":
                    txtWaitAmomentAfterPaymentSuccess = arrayLanguage
                case "str_charging_not_successful":
                    txtPaymentUnSuccess01 = arrayLanguage
                case "str_require_condition":
                    txtPaymentUnSuccess02 = arrayLanguage
                case "str_incorrect_captcha":
                    txtPaymentUnSuccess03 = arrayLanguage
                case "str_cannot_charging":
                    txtPaymentUnSuccess04 = arrayLanguage
                case "text_message_charging_turn_on_3g":
                    txtAlert01PayMoney = arrayLanguage
                case "text_no_network":
                    txtAlert02PayMoney = arrayLanguage
                //
                case "str_title_forbidden":
                    txtTitleAlertForBidden = arrayLanguage
                case "str_alert_show_forbidden":
                    txtContent_AlertForBidden = arrayLanguage
                // MAPVC
                case "text_tittle_monitor_short":
                    txtTitle01MapVC = arrayLanguage
                case "text_vehicle_journey":
                    txtTitle02MapVC = arrayLanguage
                case "str_title_pick_date":
                    txtTitleSelectDateTime = arrayLanguage
                case "FROM_DATE":
                    txtContent01SelectDateTime = arrayLanguage
                case "TO_DATE":
                    txtContent02SelectDateTime = arrayLanguage
                case "str_content_gps":
                    txtContentWarningAllowAccessLocation = arrayLanguage
                case "str_alert_current_position_title":
                    txtTitleWarningWrongLocation = arrayLanguage
                case "str_alert_current_position_content":
                    txtContentWarningWrongLocation = arrayLanguage
                case "textUserLocationDistance":
                    txtUserLocationDistance = arrayLanguage
                    
                case "text_message_confirm_send_lock":
                    txtContentConfirmLockVehicle = arrayLanguage
                    
                case "text_message_confirm_send_unlock":
                    txtContentConfirmUnLockVehicle = arrayLanguage
                    
                case "text_alert_unlock":
                    txtTitleConfirmLockVehicle = arrayLanguage
                    txtAlarmSettingArray_AccAlert = arrayLanguage
                case "text_set_acc_title":
                    txtTitleConfirmUnLockVehicle = arrayLanguage
                    txtTitleAccAlarmSetting = arrayLanguage
                case "text_action_bot_find":
                    txtTitleConfirmCall2Vehicle = arrayLanguage
                    txtButtonCall = arrayLanguage
                case "text_message_confirm_call":
                    txtContentConfirmCall2Vehicle = [vietnamese, english]
                    for i in 0...txtContentConfirmCall2Vehicle.count - 1 {
                        txtContentConfirmCall2Vehicle[i] = txtContentConfirmCall2Vehicle[i].replacingOccurrences(of: "\\\"", with: "\"")
                        txtContentConfirmCall2Vehicle[i] = txtContentConfirmCall2Vehicle[i].replacingOccurrences(of: "%s", with: "%@")
                    }
                    
                case "lock_by_call_title":
                    txtTitleLockByCall = arrayLanguage
                case "lock_by_call_content":
                    txtContentLockByCall = arrayLanguage
                    
                case "unlock_by_call_title":
                    txtTitleUnlockByCall = arrayLanguage
                case "unlock_by_call_content":
                    txtContentUnlockByCall = arrayLanguage
                    
                case "text_vehicle_current_position":
                    txtStatus01 = arrayLanguage
                case "text_time":
                    txtStatus02 = arrayLanguage
                case "text_vehicle_state":
                    txtStatus03 = arrayLanguage
                case "text_vehicle_speed":
                    txtStatus04 = arrayLanguage
                case "str_pos_unvalid":
                    txtUnknownVehicleLocation = arrayLanguage
                    
                case "DAY":
                    txtDay = arrayLanguage
                case "HOUR":
                    txtHour = arrayLanguage
                case "MINUTE":
                    txtMinute = arrayLanguage
                case "SECOND":
                    txtSecond = arrayLanguage
                    
                case "ALERT_INVALID_TIME_REGION":
                    txtAlertTimeMax2Date = arrayLanguage
                case "ALERT_INVALID_TIME_START_END":
                    txtDateTimeInvalid = arrayLanguage
                case "text_message_not_connect_device":
                    txtAlert03MapVC = arrayLanguage
                    txtAlert04SettingVC = arrayLanguage
                case "str_lock":
                    txtButtonLock = arrayLanguage
                case "text_action_bot_unlock":
                    txtButtonUnLock = arrayLanguage
                case "str_vehicle_no_info":
                    txtNoVehicleInfomationString = arrayLanguage
                case "str_no_history_review":
                    txtWrongTimeOrNoDataThisTime = arrayLanguage
                case "text_message_hibernate_device":
                    txtAlert01MapVC = arrayLanguage
                    txtAlert02SettingVC = arrayLanguage
                case "text_message_lost_gprs_device":
                    txtAlert02MapVC = arrayLanguage
                    txtAlert03SettingVC = arrayLanguage
                case "text_message_cannot_control_vihecle":
                    txtAlert04MapVC = arrayLanguage
                case "text_action_right_journey":
                    txtButtonJourney = arrayLanguage
                case "SETTING":
                    txtButtonSetting = arrayLanguage
                case "text_action_right_map":
                    txtButtonMaps = arrayLanguage
                case "text_action_right_gps":
                    txtButtonMyLocation = arrayLanguage
                case "text_journey_dtl":
                    txtShowDetailPoint = arrayLanguage
                case "text_title_journey_dtl":
                    txtTitleDetailPoint = arrayLanguage
                case "text_vehicle_at_time":
                    txtContent01DetailPoint = arrayLanguage
                case "STATE":
                    txtContent02DetailPoint = arrayLanguage
                case "text_total_s":
                    txtContent03DetailPoint = arrayLanguage
                case "RUNNING":
                    txtStatus_DetailPoint_Run = arrayLanguage
                case "str_stop":
                    txtStatus_DetailPoint_Stop = arrayLanguage
                case "PARK":
                    txtStatus_DetailPoint_Park = arrayLanguage
                case "LOST_GPS":
                    txtStatus_DetailPoint_LossGPS = arrayLanguage
                case "HIBERNATE":
                    txtStatus_DetailPoint_Hibernate = arrayLanguage
                case "LOST_GPRS":
                    txtStatus_DetailPoint_LossGPRS = arrayLanguage
                case "NO_INFO":
                    txtStatus_DetailPoint_NoInfo = arrayLanguage
                // Setting
                case "str_config_vehicle":
                    txtTitleSettingView = arrayLanguage
                case "text_change_phone_number_vehicle":
                    txtSettingArray_DeviceActive = arrayLanguage
                    txtTitleVehicleRegistry = arrayLanguage
                case "text_change_password_user":
                    txtSettingArray_ChangeAppPassword = arrayLanguage
                    txtTitleChangeAppPassword = arrayLanguage
                case "text_password_invalid_admin_account":
                    txtAlert01ChangeAppPassword = arrayLanguage
                    txtAlert02ChangeAppPassword = arrayLanguage
                    txtAlert03ChangeAppPassword = arrayLanguage
                    txtAlert08ChangeAppPassword = arrayLanguage
                case "system_fail":
                    txtAlert10ChangeAppPassword = arrayLanguage
                case "text_change_pass_device_success":
                    txtSettingArray_ChangeDevicePassword = arrayLanguage
                    txtTitleChangeVehiclePassword = arrayLanguage
                    commandControlStringChangeVehiclePassword = arrayLanguage
                case "text_success":
                    txtControlSuccess = arrayLanguage
                    
                    commandControlStringSuccess = arrayLanguage
                    
                case "text_fail":
                    txtControlUnSuccess = arrayLanguage
                    
                    commandControlStringUnSuccess = arrayLanguage
                    
                case "text_change_permission_success":
                    txtSettingArray_ChangeUserPhone = arrayLanguage
                    txtTitleChangeUserPhone = arrayLanguage
                    commandControlStringChangeUserPhone = arrayLanguage
                case "text_reboot":
                    txtSettingArray_Reboot = arrayLanguage
                    commandControlStringReboot = arrayLanguage
                    txtTitleDeviceReboot = arrayLanguage
                case "text_setup_protected_alarm":
                    txtSettingArray_ChangeConfig = arrayLanguage
                case "text_reset_config":
                    txtSettingArray_FactoryReset = arrayLanguage
                    txtTitleDeviceFormat = arrayLanguage
                case "EMAIL":
                    txtSettingArray_Feedback = arrayLanguage
                case "str_languages":
                    txtSettingArray_Language = arrayLanguage
                    txtTitleChooseLanguage = arrayLanguage
                case "text_vehicle_vibrate":
                    txtAlarmSettingArray_VibrateAlert = arrayLanguage
                case "text_alert_max_area":
                    txtAlarmSettingArray_FenceAlert = arrayLanguage
                case "OVER_SPEED":
                    txtAlarmSettingArray_OverSpeedAlert = arrayLanguage
                case "text_set_current_pass_not_six_length":
                    txtAlert01SettingVC = arrayLanguage
                case "text_sim_number":
                    txtContent01VehicleRegistry = arrayLanguage
                case "text_first_password":
                    txtContent02VehicleRegistry = arrayLanguage
                    txtContent01ChangeAppPassword = arrayLanguage
                    txtContent01ChangeVehiclePassword = arrayLanguage
                case "text_new_password":
                    txtContent03VehicleRegistry = arrayLanguage
                    txtContent02ChangeAppPassword = arrayLanguage
                    txtContent02ChangeVehiclePassword = arrayLanguage
                case "ENTER_CONFIRM_NEW_PASSWORD":
                    txtContent04VehicleRegistry = arrayLanguage
                    txtContent03ChangeAppPassword = arrayLanguage
                    txtContent03ChangeVehiclePassword = arrayLanguage
                case "send":
                    txtButtonConfirmVehicleRegistry = arrayLanguage
                    txtButtonConfirmChangeAppPassword = arrayLanguage
                    txtButtonConfirmChangeVehiclePassword = arrayLanguage
                    txtButtonConfirmChangeUserPhone = arrayLanguage
                case "text_six_character_length_password_oldpass":
                    txtAlert01VehicleRegistry = arrayLanguage
                    txtAlert01ChangeVehiclePassword = arrayLanguage
                case "text_six_character_length_password_newpass":
                    txtAlert02VehicleRegistry = arrayLanguage
                    txtAlert02ChangeVehiclePassword = arrayLanguage
                case "text_six_character_length_password":
                    txtAlert03VehicleRegistry = arrayLanguage
                    txtAlert03ChangeVehiclePassword = arrayLanguage
                case "text_password_and_confirm_pass_not_equal":
                    txtAlert04VehicleRegistry = arrayLanguage
                    txtAlert04ChangeVehiclePassword = arrayLanguage
                    txtAlert04ChangeAppPassword = arrayLanguage
                case "text_change_password_success":
                    txtAlert06ChangeAppPassword = arrayLanguage
                    txtAlert11ChangeAppPassword = arrayLanguage
                case "text_pass_invalid_11":
                    txtAlert09ChangeAppPassword = arrayLanguage
                    txtAlert05ChangeAppPassword = arrayLanguage
                case "text_get_param_result_password":
                    txtContent01ChangeUserPhone = arrayLanguage
                case "str_vehicle_new_phonenumber":
                    txtContent02ChangeUserPhone = arrayLanguage
                case "text_six_character_length_password_device_oldpass":
                    txtAlert01ChangeUserPhone = arrayLanguage
                case "text_set_owner_number_not_correct":
                    txtAlert02ChangeUserPhone = arrayLanguage
                case "text_reboot_dtl":
                    txtContentDeviceReboot = arrayLanguage
                case "str_setting_alert":
                    txtTitleListAlarmSetting = arrayLanguage
                case "text_button_acc_unclock_alert":
                    txtContent01AccAlarmSetting = arrayLanguage
                case "str_send_to_sms":
                    txtContent02AccAlarmSetting = arrayLanguage
                    txtContent02VibrateAlarmSetting = arrayLanguage
                    txtContent02FenceAlarmSetting = arrayLanguage
                    txtContent02SpeedAlarmSetting = arrayLanguage
                case "str_send_to_call":
                    txtContent03AccAlarmSetting = arrayLanguage
                    txtContent03VibrateAlarmSetting = arrayLanguage
                    txtContent03FenceAlarmSetting = arrayLanguage
                    txtContent03SpeedAlarmSetting = arrayLanguage
                case "text_set_siren_title":
                    txtContent04AccAlarmSetting = arrayLanguage
                    txtContent04VibrateAlarmSetting = arrayLanguage
                    txtContent04FenceAlarmSetting = arrayLanguage
                    txtContent04SpeedAlarmSetting = arrayLanguage
                case "str_set_param":
                    txtButtonConfirmAccAlarmSetting = arrayLanguage
                    txtButtonConfirmVibrateAlarmSetting = arrayLanguage
                    txtButtonConfirmFenceAlarmSetting = arrayLanguage
                    txtButtonConfirmSpeedAlarmSetting = arrayLanguage
                case "str_get_param":
                    txtButtonCancelAccAlarmSetting = arrayLanguage
                    txtButtonCancelVibrateAlarmSetting = arrayLanguage
                    txtButtonCancelFenceAlarmSetting = arrayLanguage
                    txtButtonCancelSpeedAlarmSetting = arrayLanguage
                case "text_button_vibrate_alert":
                    txtTitleVibrateAlarmSetting = arrayLanguage
                case "text_get_param_vibrate_alert":
                    txtContent01VibrateAlarmSetting = arrayLanguage
                case "str_alert_radius":
                    txtTitleFenceAlarmSetting = arrayLanguage
                    txtContent01FenceAlarmSetting = arrayLanguage
                case "str_allow_radius":
                    txtContent05FenceAlarmSetting = arrayLanguage
                case "str_max_radius_content":
                    txtAlertFenceAlarmSetting = arrayLanguage
                case "meters":
                    txtContent06FenceAlarmSetting = arrayLanguage
                case "text_set_overspeed_title":
                    txtTitleSpeedAlarmSetting = arrayLanguage
                    txtContent01SpeedAlarmSetting = arrayLanguage
                case "str_allow_speed":
                    txtContent05SpeedAlarmSetting = arrayLanguage
                case "str_max_speed_content":
                    txtAlertSpeedAlarmSetting = arrayLanguage
                case "str_confirm_backup":
                    txtContentDeviceFormat = arrayLanguage
                case "str_vi":
                    txtButtonVietnamese_ChooseLanguage = arrayLanguage
                case "str_en":
                    txtButtonEnglish_ChooseLanguage = arrayLanguage
                case "device_password":
                    txtTitleAskPassword = arrayLanguage
                case "feed_back":
                    txtEmailSubject = arrayLanguage
                default: break
                }
            }
        }
    }
}
