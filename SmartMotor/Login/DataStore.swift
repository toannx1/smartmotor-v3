//
//  DataStore.swift
//  SmartMotor
//
//  Created by Vietnv2 on 3/10/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import CoreData
import CryptoSwift
import UIKit

extension LoginVC {
    // MARK: - Encrypt decrypt
    
    func encrypt(string: String) -> String {
        do {
            let iv = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] as [UInt8]
            let aes = try AES(key: encrypKeyForLogin.bytes, blockMode: .CBC(iv: iv), padding: .pkcs7)
//            let aes = try AES(key: encrypKeyForLogin, iv: "", padding: .pkcs7)
            let ciphertext = try aes.encrypt(string.utf8.map { $0 })
            let encryptValue = Data(bytes: ciphertext).base64EncodedString()
            return encryptValue
        } catch {}
        return ""
    }
    
    func decrypt(string: String) -> String {
        do {
            let iv = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] as [UInt8]
            let aes = try AES(key: encrypKeyForLogin.bytes, blockMode: .CBC(iv: iv), padding: .pkcs7)
            let cipherdata = Data(base64Encoded: string)
            let ciphertext = try aes.decrypt(cipherdata!.bytes)
            let decryptValue = String(bytes: ciphertext, encoding: String.Encoding.utf8)
            return decryptValue!
        } catch {}
        return ""
    }
    
    // MARK: - managedObjectContext
    
    func savePersonInfo2Sqlite(token: String, userid: String, groupRoot: Int) {
        let moc = DataController().managedObjectContext
        let entity = NSEntityDescription.insertNewObject(forEntityName: sqliteItemString.person.rawValue, into: moc) as! Person
        let _token = encrypt(string: token)
        entity.setValue(_token, forKey: sqliteItemString.token.rawValue)
        entity.setValue(userid, forKey: sqliteItemString.userId.rawValue)
        entity.setValue(String(groupRoot), forKey: sqliteItemString.groupRoot.rawValue)
        tokenSave = token
        userIdSave = userid
        groupRootSave = groupRoot
        do {
            try moc.save()
        } catch {
            fatalError("Failure\(error)")
        }
    }
    
    func saveLanguage2Sqlite(languageChoose: Int) {
        let moc = DataController().managedObjectContext
        let entity = NSEntityDescription.insertNewObject(forEntityName: sqliteItemString.language.rawValue, into: moc) as! Language
        entity.setValue(String(languageChoose), forKey: sqliteItemString.languageChoose.rawValue)
        languageChooseSave = languageChoose
        do {
            try moc.save()
        } catch {
            fatalError("Failure\(error)")
        }
    }
    
    func getPersonInfoFromSqlite() {
        let moc = DataController().managedObjectContext
        let personFetch = NSFetchRequest<NSFetchRequestResult>(entityName: sqliteItemString.person.rawValue)
        do {
            let result = try moc.fetch(personFetch) as! [Person]
            if result.count > 0 {
                let _token = decrypt(string: result.last!.token!)
                tokenSave = _token
                userIdSave = result.last!.userId!
                groupRootSave = Int(result.last!.groupRoot!)
            }
        } catch {
            fatalError("Failed\(error)")
        }
        checkData2Use()
    }
    
    func getLanguageFromSqlite() {
        let moc = DataController().managedObjectContext
        let personFetch = NSFetchRequest<NSFetchRequestResult>(entityName: sqliteItemString.language.rawValue)
        do {
            let result = try moc.fetch(personFetch) as! [Language]
            if result.count > 0 {
                languageChooseSave = Int(result.last!.languageChoose!)
            }
        } catch {
            fatalError("Failed\(error)")
        }
        checkLanguage2Use()
    }
    
    func clearPersonDataInSqlite() {
        tokenSave = nil
        userIdSave = nil
        groupRootSave = nil
        let moc = DataController().managedObjectContext
        let personFetch = NSFetchRequest<NSFetchRequestResult>(entityName: sqliteItemString.person.rawValue)
        do {
            let result = try moc.fetch(personFetch) as! [Person]
            if result.count > 0 {
                for item in result {
                    moc.delete(item)
                }
            }
            try moc.save()
        } catch {
            fatalError("Failed\(error)")
        }
    }
    
    func checkData2Use() {
        if tokenSave != nil, userIdSave != nil, groupRootSave != nil {
            if CheckNetwork().checkConnection() == false {
                showBannerNotifi(text: "Lỗi kết nối mạng. Vui lòng thử lại sau!", background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!)
                // alertCustom(message: txtAlert02PayMoney[languageChooseSave], delay: 3)
                return
            }
            loadListVehicle()
        } else {
            clearPersonDataInSqlite()
        }
    }
    
    func checkLanguage2Use() {
        if languageChooseSave == nil {
            // get current system language
            let identifier = Locale.current.identifier
            // print(identifier)
            if identifier == "vi_US" {
                languageChooseSave = 0
                saveLanguage2Sqlite(languageChoose: languageChooseSave)
            } else {
                languageChooseSave = 1
                saveLanguage2Sqlite(languageChoose: languageChooseSave)
            }
        }
    }
    
    //
    func saveMapType2Sqlite(mapType: String) {
        let moc = DataController().managedObjectContext
        let entity = NSEntityDescription.insertNewObject(forEntityName: "Map", into: moc) as! Map
        entity.setValue(mapType, forKey: "mapType")
        mapTypeSave = mapType
        do {
            try moc.save()
        } catch {
            fatalError("Failure\(error)")
        }
    }
    
    func getMapTypeFromSqlite() {
        let moc = DataController().managedObjectContext
        let personFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Map")
        do {
            let result = try moc.fetch(personFetch) as! [Map]
            if result.count > 0 {
                mapTypeSave = result.last!.mapType!
            }
        } catch {
            fatalError("Failed\(error)")
        }
        checkMapType2Use()
    }
    
    func checkMapType2Use() {
        if mapTypeSave == nil {
            mapTypeSave = "VTMap"
            saveMapType2Sqlite(mapType: "VTMap")
        }
    }
}
