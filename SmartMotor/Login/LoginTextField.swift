//
//  LoginTextField.swift
//  SmartMotor
//
//  Created by vietnv2 on 12/5/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

extension LoginVC: UITextFieldDelegate {
    // MARK: - init TextField
    
    func initTextField() {
        let imageForUser = UIImageView(frame: CGRect(x: 15, y: 12, width: 18, height: 18))
        imageForUser.image = #imageLiteral(resourceName: "icon_user")
        //txtUser.addSubview(imageForUser)
        
        let paddingLeftForUser = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 30))
        txtUser.leftView = paddingLeftForUser
        txtUser.leftViewMode = UITextField.ViewMode.always
        txtUser.returnKeyType = UIReturnKeyType.next
        
        let paddingRightForUser = UIView(frame: CGRect(x: 0, y: 12, width: 25, height: 18))
        txtUser.rightView = paddingRightForUser
        txtUser.rightViewMode = UITextField.ViewMode.always
        
        txtUser.delegate = self
        _ = textFieldShouldReturn(txtUser)
        //
        let imageForPassword = UIImageView(frame: CGRect(x: 15, y: 12, width: 16, height: 18))
        imageForPassword.image = #imageLiteral(resourceName: "icon_password")
        //txtPassword.addSubview(imageForPassword)
        
        let paddingLeftForPassword = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: 30))
        txtPassword.leftView = paddingLeftForPassword
        txtPassword.leftViewMode = UITextField.ViewMode.always
        txtPassword.returnKeyType = UIReturnKeyType.done
        
        txtPassword.delegate = self
        _ = textFieldShouldReturn(txtPassword)
        //
        txtCaptcha.leftViewMode = UITextField.ViewMode.always
        txtCaptcha.returnKeyType = UIReturnKeyType.done
        txtCaptcha.delegate = self
        _ = textFieldShouldReturn(txtCaptcha)
        btnClearTxtUser.isHidden = true
        btnSecurePassword.isHidden = true
        if #available(iOS 10.0, *) {
            txtUser.textContentType = UITextContentType(rawValue: "")
            txtPassword.textContentType = UITextContentType(rawValue: "")
        } else {
            // Fallback on earlier versions
        }
        
        tfPhoneNumber.delegate = self
        tfVerify.delegate = self
        tfPhoneNumber.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
    }
    
    // MARK: - Clear and Secured TextField
    
    @IBAction func btnClearTxtUserTapped(_ sender: Any) {
        txtUser.text = ""
        btnClearTxtUser.isHidden = true
        lbAccountError.isHidden = false
    }
    
    @IBAction func btnSecurePasswordTapped(_ sender: Any) {
        if txtPassword.isSecureTextEntry == false {
            btnSecurePassword.setImage(UIImage(named: "visible_eye"), for: .normal)
        } else {
            btnSecurePassword.setImage(UIImage(named: "invisible_eye"), for: .normal)
        }
        txtPassword.isSecureTextEntry = !txtPassword.isSecureTextEntry
    }
    
    @objc func btnClearPasswordClicked() {
        txtPassword.text = ""
        btnClearPassword.isHidden = true
        btnSecurePassword.isHidden = true
        lbPassError.isHidden = false
    }
    
    // MARK: - TextField Delegate
    
    @objc func textFieldDidChange(sender: UITextField) {
        sender.text = sender.text?.filter("01234567890+".contains)
        if sender.text == "" {
            lbPhoneError.isHidden = false
            lbPhoneError.text = "Vui lòng nhập số điện thoại!"
        } else {
            if (sender.text?.isVietNewPhone())! {
                lbPhoneError.isHidden = true
            } else {
                lbPhoneError.isHidden = false
                lbPhoneError.text = "Số điện thoại không hợp lệ!"
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool { // called when 'return' key pressed. return false to ignore.
        if textFieldDidBeginEditingFlag == false {
            return true
        }
        if textField == txtUser {
            txtPassword.becomeFirstResponder()
        } else if textField == txtPassword {
            if loginWithCaptchaFlag == false {
                textField.resignFirstResponder()
//                loginViewContraint.constant = 0
                login()
            } else {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
                    self.txtCaptcha.becomeFirstResponder()
                }
            }
        } else {
            if loginWithCaptchaFlag == true {
                textField.resignFirstResponder()
//                loginViewContraint.constant = 0
                login()
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text != "" {
            switch textField {
            case txtUser:
                btnClearTxtUser.isHidden = false
            case txtPassword:
                btnClearPassword.isHidden = false
            default: break
            }
        }
        textFieldDidBeginEditingFlag = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case txtUser:
            if textField.text == "" {
                lbAccountError.isHidden = false
            }
            btnClearTxtUser.isHidden = true
        case txtPassword:
            if textField.text == "" {
                lbPassError.isHidden = false
            }
            btnClearPassword.isHidden = true
        case tfPhoneNumber:
            if textField.text == "" {
                lbPhoneError.isHidden = false
                lbPhoneError.text = "Vui lòng nhập số điện thoại!"
            }
        case tfVerify:
            if textField.text == "" {
                lbVerifyError.isHidden = false
            }
        default: break
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text
        let newLength = (text?.count)! + string.count - range.length
        let flag: Bool!
        flag = !(newLength > 0)
        
        if textField == tfPhoneNumber {
            if tfPhoneNumber.text!.count < 11 {
                lbPhoneError.isHidden = false
            } else {
                lbPhoneError.isHidden = true
            }
        }
        
        switch textField {
        case txtUser:
            btnClearTxtUser.isHidden = flag
            lbAccountError.isHidden = !flag
            return newLength <= 100
        case txtPassword:
            btnSecurePassword.isHidden = flag
            btnClearPassword.isHidden = flag
            lbPassError.isHidden = !flag
            return newLength <= 100
        case tfVerify:
            lbVerifyError.isHidden = !flag
        case tfPhoneNumber:
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            textField.text = formattedNumber(number: newString)
            return false
            
        default: break
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        switch textField {
        case tfPhoneNumber:
            lbPhoneError.isHidden = false
            lbPhoneError.text = "Vui lòng nhập số điện thoại!"
        case tfVerify:
            lbVerifyError.isHidden = false
        default: break
        }
        return true
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return super.canPerformAction(action, withSender: sender)
    }
    
    private func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "XXXX XXX XXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
}
