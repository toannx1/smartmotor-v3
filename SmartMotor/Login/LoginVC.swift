//
//  LoginVC.swift
//  Smart Motor
//
//  Created by vietnv2 on 17/10/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//  Smart Motor Bilingual

import BRYXBanner
import CryptoSwift
import GoogleMaps
import IQKeyboardManagerSwift
import SwiftEntryKit
import UIKit

class LoginVC: UIViewController, URLSessionDownloadDelegate {
    @IBOutlet var lblHotline: UILabel!
    @IBOutlet var btnRefreshCaptcha: UIButton!
    @IBOutlet var viewContainerCaptcha: UIView!
    @IBOutlet var lbCaptcha: UILabel!
    @IBOutlet var lbCaptchaError: UILabel!
    @IBOutlet var lbPassError: UILabel!
    @IBOutlet var lbAccountError: UILabel!
    @IBOutlet var imgViewLogo: UIImageView!
    @IBOutlet var txtUser: UITextField!
    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var txtCaptcha: UITextField!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnSecurePassword: UIButton!
    @IBOutlet var btnDontRememberPassword: UIButton!
    
    @IBOutlet var btnCallToCustomCareCenter: UIButton!
    @IBOutlet var btnClearTxtUser: UIButton!
    @IBOutlet var btnClearPassword: UIButton!
    @IBOutlet var imgCaptcha: UIImageView!
    
    // MARK: - Popup hotline
    
    @IBOutlet var hotlineConfirmView: UIView!
    @IBOutlet var lblTitleHotlineConfirm: UILabel!
    @IBOutlet var lblContentHotlineConfirm: UILabel!
    @IBOutlet var btnOKHotlineConfirm: UIButton!
    @IBOutlet var btnCancelHotlineConfirm: UIButton!
    
    // MARK: - Popup forgot Password
    
    @IBOutlet var btnCancelFP: UIButton!
    @IBOutlet var btnGetPassword: UIButton!
    @IBOutlet var btnRefreshCaptchaFP: UIButton!
    @IBOutlet var imgCaptchaFogotPass: UIImageView!
    @IBOutlet var tfVerify: UITextField!
    @IBOutlet var tfPhoneNumber: UITextField!
    @IBOutlet var forgotPasswordView: UIView!
    
    @IBOutlet var lbPhoneError: UILabel!
    
    @IBOutlet var lbVerifyError: UILabel!
    
    var checkBoxSavePasswordFlag: Bool! = false
    var checkBoxAutoLoginFlag: Bool! = false
    var loginSuccessedFlag: Bool!
    var countLoginFail: Int! = 0
    var stringToCountTxtLoginFail: String!
    var loginWithCaptchaFlag: Bool! = false
    var textFieldDidBeginEditingFlag: Bool! = false
    var cookieString: String! = ""
    var session = URLSession()
    
    enum loginInformationString: String {
        case groupRoot
        case groupType
        case lat
        case lng
        case loginAttemp
        case message
        case resultCode
        case roleId
        case token
        case userId
    }
    
    enum sqliteItemString: String {
        case person = "Person"
        case language = "Language"
        case token
        case userId
        case groupRoot
        case languageChoose
    }
    
    // MARK: - Setup View
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        view.layoutIfNeeded()
        parseCSV()
        smartmotorUrl = smartmotorUrl1
        initLoginVC()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initLanguageLoginVC()
        getPersonInfoFromSqlite()
        getMapTypeFromSqlite()
        
//        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "OK"
//        IQKeyboardManager.shared.enable = true
    }
    
    func initLoginVC() {
        checkLogout2ClearInfo()
        initView()
        getKeyBoardHeight()
        initTextField()
        initURLSession()
    }
    
    func initView() {
        imgViewLogo.contentMode = UIView.ContentMode.scaleAspectFit
        imgCaptcha.isUserInteractionEnabled = true // image has been tapped
        heightScreen = view.frame.size.height
        widthScreen = view.frame.size.width
        hideCaptchaView()
        btnClearTxtUser.isHidden = true
        btnClearPassword.isHidden = true
        btnLogin.backgroundColor = .white
        btnLogin.addTarget(self, action: #selector(login), for: .touchUpInside)
        txtUser.layer.borderColor = UIColor.white.cgColor
        txtPassword.layer.borderColor = UIColor.white.cgColor
        txtCaptcha.layer.borderColor = UIColor.white.cgColor
        txtUser.layer.borderWidth = 1
        txtPassword.layer.borderWidth = 1
        txtCaptcha.layer.borderWidth = 1
        btnSecurePassword.imageView?.contentMode = .scaleAspectFit
        lbAccountError.isHidden = true
        lbPassError.isHidden = true
        lbCaptchaError.isHidden = true
        txtUser.keyboardDistanceFromTextField = 200
        txtPassword.keyboardDistanceFromTextField = 100
        txtCaptcha.keyboardDistanceFromTextField = 80
        btnRefreshCaptcha.imageView?.contentMode = .scaleAspectFit
        lblContentHotlineConfirm.add(Border: .bottom, withColor: .lightGray, andHeight: 1.0)
        btnOKHotlineConfirm.add(Border: .right, withColor: .lightGray, andHeight: 1.0)
        btnCallToCustomCareCenter.setImage(UIImage(named: "ic_call2"), for: .highlighted)
        btnRefreshCaptcha.addTarget(self, action: #selector(urlSessionDelegate), for: .touchUpInside)
        btnClearPassword.addTarget(self, action: #selector(btnClearPasswordClicked), for: .touchUpInside)
        
        txtPassword.paddingRight(padding: 50)
        
        // MARK: Forgot Password View
        
        btnRefreshCaptchaFP.addTarget(self, action: #selector(urlSessionDelegate), for: .touchUpInside)
        tfPhoneNumber.keyboardDistanceFromTextField = 224
        tfVerify.keyboardDistanceFromTextField = 140
        btnRefreshCaptchaFP.imageView?.contentMode = .scaleAspectFit
        btnGetPassword.add(Border: .top, withColor: .lightGray, andHeight: 1.0)
        btnGetPassword.add(Border: .right, withColor: .lightGray, andHeight: 1.0)
        btnCancelFP.add(Border: .top, withColor: .lightGray, andHeight: 1.0)
        btnCancelFP.addTarget(self, action: #selector(removeForgotPassView), for: .touchUpInside)
        btnGetPassword.addTarget(self, action: #selector(forgotPasswordClick), for: .touchUpInside)
        
        btnLogin.drawShadow(offset: CGSize(width: 0, height: 3))
        btnLogin.imageView?.contentMode = .scaleAspectFit
        btnLogin.setBackgroundColor(color: #colorLiteral(red: 0.8666666667, green: 0.8666666667, blue: 0.8666666667, alpha: 1), forState: .highlighted)
        
        let attrs1 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.black]
        let attrs3 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.black]
        let attrs2 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor.black]
        
        let attributedString1 = NSMutableAttributedString(string: "  Hotline: ", attributes: attrs1)
        
        let attributedString2 = NSMutableAttributedString(string: "18008098", attributes: attrs2)
        let attributedString3 = NSMutableAttributedString(string: " (Miễn phí)", attributes: attrs3)
        
        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        
        lblHotline.attributedText = attributedString1
    }
    
    func initLanguageLoginVC() {
        if languageChooseSaveFlag == true {
            languageChooseSaveFlag = false
            saveLanguage2Sqlite(languageChoose: languageChooseSave)
        }
        getLanguageFromSqlite()
        //
        languageChooseSave = 0 // Vietnamese
        //
        txtUser.placeholder = txtUserTextField_PlaceHolder[languageChooseSave]
        txtPassword.placeholder = txtPasswordTextField_PlaceHolder[languageChooseSave]
        txtCaptcha.placeholder = txtCaptchaTextField_PlaceHolder[languageChooseSave]
        btnLogin.setTitle(txtButton_Title[languageChooseSave], for: .normal)
        
        let textRange = NSMakeRange(0, txtForgotPassword_Title[languageChooseSave].count)
        let attributedText = NSMutableAttributedString(string: txtForgotPassword_Title[languageChooseSave])
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: textRange)
        
        btnDontRememberPassword.setAttributedTitle(attributedText, for: .normal)
//        btnAccountInfo.setTitle(txtServiceInfo_Title[languageChooseSave], for: .normal)
        btnCallToCustomCareCenter.setTitle(txtHotline_title[languageChooseSave], for: .normal)
    }
    
    // MARK: - Display popup
    
    func displayHotlineConfirmView() {
        hotlineConfirmView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(hotlineConfirmView)
        lblTitleHotlineConfirm.text = txtTitleHotlineConfirm[languageChooseSave].uppercased()
        lblContentHotlineConfirm.text = txtContentHotlineConfirm[languageChooseSave]
//        btnOKHotlineConfirm.setTitle(txtOKHotlineConfirm[languageChooseSave], for: .normal)
//        btnCancelHotlineConfirm.setTitle(txtCancelHotlineConfirm[languageChooseSave], for: .normal)
    }
    
    func displayForgotPassView() {
        forgotPasswordView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(forgotPasswordView)
        lbPhoneError.isHidden = true
        lbVerifyError.isHidden = true
        tfPhoneNumber.text?.removeAll()
        tfVerify.text?.removeAll()
        tfPhoneNumber.becomeFirstResponder()
        urlSessionDelegate()
    }
    
    @objc func removeForgotPassView() {
        forgotPasswordView.removeFromSuperview()
    }
    
    func removeHotlineConfirmView() {
        hotlineConfirmView.removeFromSuperview()
    }
    
    //
    
    //
    func hideCaptchaView() {
        lbCaptcha.isHidden = true
        viewContainerCaptcha.isHidden = true
        lbCaptchaError.isHidden = true
//        containerCaptchaView.isHidden = true
//        containerButtonLoginViewContraint.constant = 15
    }
    
    func showCaptchaView() {
        lbCaptcha.isHidden = false
        viewContainerCaptcha.isHidden = false
        loginWithCaptchaFlag = true
        txtPassword.keyboardDistanceFromTextField = 160
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
            self.txtCaptcha.becomeFirstResponder()
        }
    }
    
    func checkLogout2ClearInfo() {
        if logoutFlag == true {
            logoutFlag = false
            clearPersonDataInSqlite()
        }
    }
    
    func checkLoginTextField() -> Bool {
        if txtUser.text == "" {
            lbAccountError.isHidden = false
            lbPassError.isHidden = true
            txtUser.becomeFirstResponder()
            return false
        } else {
            lbAccountError.isHidden = true
        }
        if txtPassword.text == "" {
            lbPassError.isHidden = false
            txtPassword.becomeFirstResponder()
            return false
        } else {
            lbPassError.isHidden = true
        }
        if loginWithCaptchaFlag == true, txtCaptcha.text == "" {
            lbCaptchaError.isHidden = false
            lbCaptchaError.text = "Vui lòng nhập mã xác nhận!"
            txtCaptcha.becomeFirstResponder()
            return false
        } else {
            lbCaptchaError.isHidden = true
        }
        return true
    }
    
    // MARK: - ForgotPass click
    
    @objc func forgotPasswordClick() {
        checkForgotPasswordTextfield()
    }
    
    func checkForgotPasswordTextfield() {
        if tfPhoneNumber.text == "" {
            lbPhoneError.isHidden = false
            lbPhoneError.text = "Vui lòng nhập số điện thoại!"
            tfPhoneNumber.becomeFirstResponder()
            return
        } else {
            if !(tfPhoneNumber.text?.isVietNewPhone())! {
                lbPhoneError.text = "Số điện thoại không hợp lệ!"
                lbPhoneError.isHidden = false
                tfPhoneNumber.becomeFirstResponder()
                return
            } else {
                lbPhoneError.isHidden = true
            }
        }
        if tfVerify.text == "" {
            lbVerifyError.isHidden = false
            return
        } else {
            lbVerifyError.isHidden = true
        }
        forgotPassAPI()
    }
    
    // Mark:
    @IBAction func btnDontRememberPasswordTapped(_ sender: Any) {
        // UIApplication.shared.openURL(URL(string: "http://smartmotor.viettel.vn/fogetpassword.html")!)
//        UIApplication.shared.openURL(URL(string: smartmotorUrl + "/fogetpassword.html")!)
        displayForgotPassView()
    }
    
    @IBAction func btnAccountInfoTapped(_ sender: Any) {
        UIApplication.shared.openURL(URL(string: "http://vietteltelecom.vn/ung-dung-so/giam-sat-va-chong-trom-xe-may-smart-motor_dwpc4")!)
    }
    
    @IBAction func btnCallToCustomCareCenterTapped(_ sender: Any) {
        displayHotlineConfirmView()
    }
    
    @IBAction func btnHotlineAlertOKTapped(_ sender: Any) {
        removeHotlineConfirmView()
        call2APhone()
    }
    
    @IBAction func btnHotlineAlertCancelTapped(_ sender: Any) {
        removeHotlineConfirmView()
    }
    
    @IBAction func onRegister(_ sender: Any) {
        let vc = RegisterVc.loadFromNib()
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    // MARK: Call to vehicle
    
    func call2APhone() {
        let phoneUrl = "tel://\(18008098)"
        let url: URL = URL(string: phoneUrl)!
        UIApplication.shared.openURL(url)
    }
    
    @objc func login() {
        view.endEditing(true)
        loginSuccessedFlag = false
        fncLogin()
    }
    
    //
    func checkLoginFailToDisplayCaptcha() {
        if txtUser.text != stringToCountTxtLoginFail {
            stringToCountTxtLoginFail = txtUser.text
            countLoginFail = 1
            return
        }
        countLoginFail = countLoginFail + 1
        if countLoginFail >= 3 {
            showCaptchaView()
            urlSessionDelegate()
        }
    }
    
    func checkResultCode(resultCode: Int) {
        var string: String!
        switch resultCode {
        case -10:
            string = txtAlertContent_CaptchaInvalid[languageChooseSave]
            lbCaptchaError.isHidden = false
            lbCaptchaError.text = string
            return
        case -11:
            string = txtAlertContent_CaptchaInvalid_SystemError[languageChooseSave]
        case -1: string = txtAlertContent_LoginFail[languageChooseSave]
        default: break
        }
//        alertCustom(message: string, delay: 2)
        showBannerNotifi(text: string, background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
    }
    
    // Mark: touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // init image has been tapped
//        let touch:UITouch = touches.first!
//        if touch.view == imgCaptcha {
//            urlSessionDelegate()
//            return
//        }
//        UIView.animate(withDuration: 0.3) {
//            self.view.layoutIfNeeded()
//        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    // Mark:
    func gotoListVehicleView() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "listVehicle") as! ListVehicleVC
//        vehicleInformations.removeAll()
//        vehicle2show = nil
        loginFlag = true
        DispatchQueue.main.async {
            nextViewController.modalPresentationStyle = .fullScreen
            self.present(nextViewController, animated: false, completion: nil)
        }
    }
    
    func gotoMapView() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "map") as! MapVC
        registerNo2Query = vehicle2show.regNo
        loginFlag = true
        nextViewController.modalPresentationStyle = .fullScreen
        present(nextViewController, animated: false, completion: nil)
    }
    
    // Mark: Keyboard
    func getKeyBoardHeight() {
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            keyboardHeight = keyboardSize.height
//            var move:CGFloat!
//            if loginWithCaptchaFlag == false {
//                move = -keyboardHeight + loginView.frame.size.height/2 - txtPassword.frame.size.height/2 - btnLogin.frame.size.height - 15
//            }else {
//                move = -keyboardHeight + loginView.bounds.size.height/2 - txtPassword.frame.size.height/2 - btnLogin.frame.size.height - txtCaptcha.frame.size.height - 30
//            }
//            var top = -loginView.bounds.size.height/2 + txtPassword.frame.size.height/2 + imgViewLogo.frame.size.height + btnLogin.frame.size.height
//            top = top + 50
//            if move < top {
//                move = top
//            }
//            if move > 0 {
//                return
//            }
//            loginViewContraint.constant = move
//            UIView.animate(withDuration: 0.5) {
//                self.view.layoutIfNeeded()
//            }
//        }
    }
    
    // Mark: Alert custom
    func alertCustom(message: String, delay: Double) {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        AlertCustom().Alert(delegate: self, message: message, textAlignment: "", delay: delay)
    }
}

class EntryAttributeWrapper {
    var attributes: EKAttributes
    init(with attributes: EKAttributes) {
        self.attributes = attributes
    }
}

extension UIViewController {
    func showBanner(message: String, color: UIColor) {
        let banner = Banner(title: message, subtitle: "", image: UIImage(), backgroundColor: color)
        banner.dismissesOnTap = true
        banner.show(duration: 3.0)
    }
}
