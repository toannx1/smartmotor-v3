//
//  LoginServer.swift
//  SmartMotor
//
//  Created by Vietnv2 on 3/7/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import Alamofire
import CryptoSwift
import SwiftEntryKit
import SwiftyJSON
import UIKit

extension LoginVC {
    // MARK: - Login server
    
    func urlEncode(string: String) -> String {
        var rString: String! = ""
        for char in string {
            if char == " " {
                rString.append("+")
            } else if char == "." || char == "-" || char == "_" || char == "~" ||
                (char >= "a" && char <= "z") ||
                (char >= "A" && char <= "Z") ||
                (char >= "0" && char <= "9") {
                rString.append(String(char))
            } else {
                rString.append(String(format: "%%2X", char.hashValue))
            }
        }
        return rString
    }
    
    func rotateImageView(image: UIImageView) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear, animations: { () -> Void in
            image.transform = image.transform.rotated(by: .pi / 2)
        }, completion: { (_ finished: Bool) -> Void in
            if finished {
                self.rotateImageView(image: image)
            }
        })
    }
    
    // MARK: - Request data
    
    func fncLogin() {
        if checkLoginTextField() == false {
            return
        }
        if CheckNetwork().isInternet() == false {
//            alertCustom(message: txtAlert02PayMoney[languageChooseSave], delay: 3)
            
            self.showBannerNotifi(text: "Lỗi kết nối mạng. Vui lòng thử lại sau!", background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
            return
        }
        btnLogin.setImage(UIImage(named: "spinner"), for: .normal)
        btnLogin.setTitle("Đang đăng nhập", for: .normal)
        self.rotateImageView(image: btnLogin.imageView!)
        btnLogin.isEnabled = false
        btnLogin.alpha = 0.7
        
        txtUser.text = txtUser.text?.replacingOccurrences(of: "\\s+", with: "", options: .regularExpression, range: nil)
        var request = NSMutableURLRequest()
        var capcha = ""
        
        let iv = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] as [UInt8]
        let encryptedBase64Password = try! txtPassword.text!.encrypt(cipher: AES(key: encrypKeyForLogin.bytes, blockMode: .CBC(iv: iv)))
        
        let url = URL(string: smartmotorUrl + newloginUrl)
        request = NSMutableURLRequest(url: url!)
        request.httpMethod = postMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        if loginWithCaptchaFlag == true {
            capcha = self.urlEncode(string: txtCaptcha.text!)
            request.addValue(cookieString, forHTTPHeaderField: "Cookie")
        }
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let dataString: [String: String] = ["username": txtUser.text!, "password": encryptedBase64Password, "captcha": capcha] as Dictionary
        request.httpBody = try! JSONSerialization.data(withJSONObject: dataString, options: [])
        //
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        
        let task = defaultSession.dataTask(with: request as URLRequest) { data, response, error in
            if error != nil {
                // self.txtUser.becomeFirstResponder()
//                self.alertCustom(message: txtcannotConnect2Server[languageChooseSave], delay: 2)
                self.showBannerNotifi(text: txtcannotConnect2Server[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                self.doneLoadingLogin()
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
//                    self.alertCustom(message: txtloginUnsuccess[languageChooseSave], delay: 2)
                    self.showBannerNotifi(text: txtloginUnsuccess[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                    self.doneLoadingLogin()
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let resultCode = jsonResult["resultCode"] as! Int
                    if resultCode != 1 {
                        self.checkResultCode(resultCode: resultCode)
                        self.checkLoginFailToDisplayCaptcha()
                        self.doneLoadingLogin()
                        return
                    }
                    self.loginSuccessedFlag = true
                    loginInfomations = self.parseResult2GetPersonInfo(jsonResult)
                    numberOfVehcle = 0
                    self.savePersonInfo2Sqlite(token: tokenSave, userid: userIdSave, groupRoot: groupRootSave)
//                    self.gotoListVehicleView()
                    self.loadListVehicle()
                } catch {
//                    self.alertCustom(message: txtcannotConnect2Server[languageChooseSave], delay: 2)
                    self.showBannerNotifi(text: txtcannotConnect2Server[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                    self.doneLoadingLogin()
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    // MARK: - Forgot Pass
    
    func forgotPassAPI() {
        let url = smartmotorUrl2 + forgotPassUrl
        var param = Parameters()
        param["phone"] = tfPhoneNumber.text!
        param["captcha"] = tfVerify.text!
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .success:
                let data = try! JSON(data: response.data!)
                if let resultCode = data["resultCode"].string {
                    if resultCode == "1" {
                        self.showBannerNotifi(text: "Mật khẩu đã được gửi về số điện thoại của bạn, vui lòng kiểm tra trong tin nhắn!", background: colorSuccess, textColor: .white, imageTitle: UIImage(named: "checked")!, des: "")
                        self.removeForgotPassView()
                    } else {
                        if let mess = data["message"].string {
                            self.showBannerNotifi(text: mess, background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                            self.urlSessionDelegate()
                        }
                    }
                }
            case .failure:
                self.showBannerNotifi(text: "Lỗi kết nối mạng. Vui lòng thử lại sau!", background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
            }
        }
    }
    
    // MARK: - Load list vehicles
    
    func loadListVehicle() {
        pageIndex = 1
        registerNo2Query = ""
        let url1 = smartmotorUrl + listTransportUrl + userIdSave + listTransportRegNoUrl
        let url2 = registerNo2Query + listTransportPageIndexUrl + String(pageIndex)
        let urlString = url1 + url2
        let url = URL(string: urlString)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        request.addValue(tokenSave, forHTTPHeaderField: "user-token")
        //
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        //
        let task = defaultSession.dataTask(with: request as URLRequest) { data, response, error in
            if error != nil {
                self.doneLoadingLogin()
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    self.doneLoadingLogin()
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)) as! NSDictionary
                    let resultCode = jsonResult["resultCode"] as! Int
                    if resultCode != 1 {
                        self.txtUser.becomeFirstResponder()
//                        self.alertCustom(message: txtloginUnsuccess[languageChooseSave], delay: 2)
                        self.showBannerNotifi(text: jsonResult["message"] as! String, background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                        self.doneLoadingLogin()
                        return
                    }
                    let totalOfVehicle = jsonResult["total"] as! Int
                    if totalOfVehicle == 1 {
                        vehicleInformations.removeAll()
                        let jsonData: NSArray! = jsonResult["data"] as? NSArray
                        for element in jsonData {
                            let result = element as AnyObject
                            vehicleInformations.append(self.parseJson2GetListVehicle(result: result))
                            numberOfVehcle = vehicleInformations.count
                        }
                        vehicle2show = vehicleInformations[0]
                        textRegNoForbbiden = vehicle2show.regNo
                        if vehicle2show.isForbidden == false {
                            vehicle2showIndex = 0
//                            self.gotoMapView()
//                            return
                        }
                    }
                    self.doneLoadingLogin()
                    self.gotoListVehicleView()
                } catch {
//                    self.alertCustom(message: txtcannotConnect2Server[languageChooseSave], delay: 2)
                    self.showBannerNotifi(text: txtcannotConnect2Server[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                    self.doneLoadingLogin()
                    fatalError("Failure\(error)")
                }
            } else {}
        }
        task.resume()
    }
    
    func doneLoadingLogin() {
        self.btnLogin.setImage(UIImage(), for: .normal)
        self.btnLogin.setTitle("ĐĂNG NHẬP", for: .normal)
        self.btnLogin.isEnabled = true
        self.btnLogin.alpha = 1.0
    }
    
    // MARK: - Parse data
    
    func parseResult2GetPersonInfo(_ result: NSDictionary) -> loginInformation {
        var resultReturn: loginInformation!
        let groupRoot = result.value(forKey: loginInformationString.groupRoot.rawValue) as! Int
        groupRootSave = groupRoot
        let groupType = result.value(forKey: loginInformationString.groupType.rawValue) as! Int
        let lat = result.value(forKey: loginInformationString.lat.rawValue) as! Double
        let lng = result.value(forKey: loginInformationString.lng.rawValue) as! Double
        let loginAttemp = result.value(forKey: loginInformationString.loginAttemp.rawValue) as! Int
        let message = result.value(forKey: loginInformationString.message.rawValue) as! String
        let resultCode = result.value(forKey: loginInformationString.resultCode.rawValue) as! Int
        let roleId = result.value(forKey: loginInformationString.roleId.rawValue) as! Int
        let token = result.value(forKey: loginInformationString.token.rawValue) as! String
        tokenSave = token
        let userId = result.value(forKey: loginInformationString.userId.rawValue) as! Int
        userIdSave = String(userId)
        userIdSave_ext = userId
        resultReturn = loginInformation(groupRoot: groupRoot, groupType: groupType, lat: lat, lng: lng, loginAttemp: loginAttemp, message: message, resultCode: resultCode, roleId: roleId, token: token, userId: userId)
        return resultReturn
    }
    
    func parseJson2GetListVehicle(result: AnyObject) -> vehicleInformation {
        var resultReturn: vehicleInformation!
        var gpsDate: String! = ""
        var isForbidden: Bool! = false
        let accIllegalState = result.value(forKeyPath: vehicleInfoString.accIllegalState.rawValue) as! Int
        let devicePin = result.value(forKeyPath: vehicleInfoString.devicePin.rawValue) as! String
        let deviceType = result.value(forKeyPath: vehicleInfoString.deviceType.rawValue) as! Int
        let deviceTypeIdOrginal = result.value(forKeyPath: vehicleInfoString.deviceTypeIdOrginal.rawValue) as! Int
        let factory = result.value(forKeyPath: vehicleInfoString.factory.rawValue) as! String
        if let _gpsDate = (result.value(forKeyPath: vehicleInfoString.gpsDate.rawValue) as? String) {
            gpsDate = _gpsDate
        }
        let gpsSpeed = result.value(forKeyPath: vehicleInfoString.gpsSpeed.rawValue) as! Int
        let gpsState = result.value(forKeyPath: vehicleInfoString.gpsState.rawValue) as! Int
        let groupsCode = result.value(forKeyPath: vehicleInfoString.groupsCode.rawValue) as! String
        let id = result.value(forKeyPath: vehicleInfoString.id.rawValue) as! Int
        let illegalMoveState = result.value(forKeyPath: vehicleInfoString.illegalMoveState.rawValue) as! Int
        let lat = result.value(forKeyPath: vehicleInfoString.lat.rawValue) as! Double
        let lng = result.value(forKeyPath: vehicleInfoString.lng.rawValue) as! Double
        let lowBatteryState = result.value(forKeyPath: vehicleInfoString.lowBatteryState.rawValue) as! Int
        let motoSpeed = result.value(forKeyPath: vehicleInfoString.motoSpeed.rawValue) as! Int
        let offPowerState = result.value(forKeyPath: vehicleInfoString.offPowerState.rawValue) as! Int
        let regNo = result.value(forKeyPath: vehicleInfoString.regNo.rawValue) as! String
        let sim = result.value(forKeyPath: vehicleInfoString.sim.rawValue) as! String
        let sosState = result.value(forKeyPath: vehicleInfoString.sosState.rawValue) as! Int
        let state = result.value(forKeyPath: vehicleInfoString.state.rawValue) as! Int
        let timeState = result.value(forKeyPath: vehicleInfoString.timeState.rawValue) as! Int
        let type = result.value(forKeyPath: vehicleInfoString.type.rawValue) as! String
        let vibrationState = result.value(forKeyPath: vehicleInfoString.vibrationState.rawValue) as! Int
        if let _isForbidden = result.value(forKeyPath: vehicleInfoString.isForbidden.rawValue) as? Bool {
            isForbidden = _isForbidden
        }
        resultReturn = vehicleInformation(accIllegalState: accIllegalState, devicePin: devicePin, deviceType: deviceType, deviceTypeIdOrginal: deviceTypeIdOrginal, factory: factory, gpsDate: gpsDate, gpsSpeed: gpsSpeed, gpsState: gpsState, groupsCode: groupsCode, id: id, illegalMoveState: illegalMoveState, lat: lat, lng: lng, lowBatteryState: lowBatteryState, motoSpeed: motoSpeed, offPowerState: offPowerState, regNo: regNo, sim: sim, sosState: sosState, state: state, timeState: timeState, type: type, vibrationState: vibrationState, isForbidden: isForbidden)
        return resultReturn
    }
    
    // MARK: - Session Captcha
    
    func initURLSession() {
        let sessionConfig = URLSessionConfiguration.default
        session = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
    }
    
    func deleteCookies() {
        let cookieStore = HTTPCookieStorage.shared
        for cookie in cookieStore.cookies ?? [] {
            cookieStore.deleteCookie(cookie)
        }
    }
    
    @objc func urlSessionDelegate() {
        self.deleteCookies()
        let imageUrl: String = smartmotorUrl + captchaUrl
        let task: URLSessionDownloadTask = session.downloadTask(with: NSURL(string: imageUrl as String)! as URL)
        task.resume()
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let downloadedImage = UIImage(data: NSData(contentsOf: location)! as Data)
        if loginWithCaptchaFlag == true {
            DispatchQueue.main.async {
                self.imgCaptcha.image = downloadedImage
            }
        } else {
            DispatchQueue.main.async {
                self.imgCaptchaFogotPass.image = downloadedImage
            }
        }
        
        cookieString = ""
        for cookie in (session.configuration.httpCookieStorage?.cookies)! {
            cookieString = cookieString + cookie.name + "=" + cookie.value + ";"
        }
        Loading().hideLoading(delay: 0)
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {}
    
    //
}

extension LoginVC: URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
}
