//
//  LoginInfomation.swift
//  Smart Motor
//
//  Created by vietnv2 on 18/10/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import Foundation
import UIKit

class loginInformation: NSObject {
    var groupRoot: Int!
    var groupType: Int!
    var lat: Double!
    var lng: Double!
    var loginAttemp: Int!
    var message: String!
    var resultCode: Int!
    var roleId: Int!
    var token: String!
    var userId: Int!

    init(groupRoot: Int, groupType: Int, lat: Double, lng: Double, loginAttemp: Int, message: String, resultCode: Int, roleId: Int, token: String, userId: Int) {
        super.init()
        self.groupRoot = groupRoot
        self.groupType = groupType
        self.lat = lat
        self.lng = lng
        self.loginAttemp = loginAttemp
        self.message = message
        self.resultCode = resultCode
        self.roleId = roleId
        self.token = token
        self.userId = userId
    }
}

var loginInfomations: loginInformation!
