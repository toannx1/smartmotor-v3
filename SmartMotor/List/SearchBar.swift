//
//  SearchBar.swift
//  SmartMotor
//
//  Created by Vietnv2 on 3/19/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import LBTATools
import UIKit

extension ListVehicleVC: UISearchBarDelegate {
    // MARK: - Searchbar delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if checkInternet() == false {
            return
        }
        textSearchVehicle = searchText
        if searchText == "" {
            hideKeyBoad()
            searchVehicleFlag = false
            self.refreshSearch()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if searchVehicleFlag == true {
            registerNo2Query = textSearchVehicle
            txtSearchVehicleTextField = textSearchVehicle
            pageIndex = 1
            endOfListFlag = false
            numberOfVehcle = 0
            vehicleInformations.removeAll()
            self.listVehicleTableView.isHidden = false
            loadListVehicle()
        }
    }
    
    func refreshSearch() {
        registerNo2Query = ""
        txtSearchVehicleTextField = ""
        vehicleInformations.removeAll()
        numberOfVehcle = 0
        pageIndex = 1
        endOfListFlag = false
        self.listVehicleTableView.isHidden = false
        listVehicleTableView.reloadData()
        loadListVehicle()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        if searchVehicleFlag == true {
            registerNo2Query = textSearchVehicle
            txtSearchVehicleTextField = textSearchVehicle
            pageIndex = 1
            endOfListFlag = false
            numberOfVehcle = 0
            vehicleInformations.removeAll()
            loadListVehicle()
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        searchVehicleFlag = false
        
        if textSearchVehicle == "" {
            listVehicleTableView.isHidden = false
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchVehicleFlag = true
        self.listVehicleTableView.isHidden = true
    }
}

// MARK: - TableView Nodata Background

class NoVehicleFoundView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let img = UIImageView(image: UIImage(named: "ic_error_gray"), contentMode: .scaleAspectFit)
        let lblNotFound = UILabel(text: "Không tìm thấy xe.", font: UIFont.systemFont(ofSize: 16), textColor: .black, textAlignment: .center, numberOfLines: 0)
        let lbTryAgain = UILabel(text: "Vui lòng thử lại sau!", font: UIFont.systemFont(ofSize: 13), textColor: .black, textAlignment: .center, numberOfLines: 0)
        
        addSubview(img.withSize(.init(width: 70, height: 70)))
        addSubview(lblNotFound.withHeight(30))
        addSubview(lbTryAgain.withHeight(20))
        img.translatesAutoresizingMaskIntoConstraints = false
        lblNotFound.translatesAutoresizingMaskIntoConstraints = false
        lbTryAgain.translatesAutoresizingMaskIntoConstraints = false
        
        img.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        img.topAnchor.constraint(equalTo: topAnchor, constant: 30).isActive = true
        
        lblNotFound.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        lblNotFound.topAnchor.constraint(equalTo: img.bottomAnchor, constant: 8).isActive = true
        lblNotFound.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        
        lbTryAgain.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        lbTryAgain.topAnchor.constraint(equalTo: lblNotFound.bottomAnchor).isActive = true
        lbTryAgain.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
