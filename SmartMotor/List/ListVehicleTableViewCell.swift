//
//  ListVehicleTableViewCell.swift
//  SmartMotor
//
//  Created by nguyen hoan on 8/28/19.
//  Copyright © 2019 EPR. All rights reserved.
//

import UIKit

class ListVehicleTableViewCell: UITableViewCell {
    @IBOutlet var lblRegNo: UILabel!
    @IBOutlet var imgIcon: UIImageView!

    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPhoneNumber: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
