//
//  GuideTableViewController.swift
//  SmartMotor
//
//  Created by nguyen hoan on 8/28/19.
//  Copyright © 2019 EPR. All rights reserved.
//

import PDFReader
import UIKit

class GuideTableViewController: UITableViewController {
    let tableviewItem = [UIImage(named: "icon_manual"), UIImage(named: "icon_manual"), UIImage(named: "icon_manual"), UIImage(named: "icon_manual")]
    let tableViewItemTitle = ["Cách lắp đặt thiết bị W1", "Chính sách bán hàng", "Hướng dẫn sử dụng dịch vụ - Mobile", "Hướng dẫn sử dụng dịch vụ - Web"]
    
    //MARK: - init View
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let btnBack = UIButton()
        btnBack.setImage(UIImage(named: "btn_Back"), for: .normal)
        btnBack.addTarget(self, action: #selector(btnBackClicked), for: .touchUpInside)
        let barBack = UIBarButtonItem(customView: btnBack)
        navigationItem.leftBarButtonItem = barBack
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "backgroundTitle"), for: .default)
        navigationController?.navigationBar.isTranslucent = false
        title = "Hướng dẫn sử dụng".uppercased()
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.heavy), NSAttributedString.Key.foregroundColor: UIColor.white,
        ]
    }
    
    @objc func btnBackClicked() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tableviewItem.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        // cell.imageView?.image = image(tableviewItem[indexPath.row]!, withSize: CGSize(width: 24, height: 24))
        cell.textLabel?.text = tableViewItemTitle[indexPath.row]
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let documentFileURL = Bundle.main.url(forResource: "apple", withExtension: "pdf")!
        let document = PDFDocument(url: documentFileURL)!
        let readerController = PDFViewController.createNew(with: document)
        readerController.scrollDirection = .vertical
        
        let btnBack = UIButton(image: UIImage(named: "icBackX")!)
        btnBack.contentEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
        btnBack.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        // btnBack.imageView?.contentMode = .scaleAspectFit
        btnBack.addTarget(self, action: #selector(backClick), for: .touchUpInside)
        readerController.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btnBack)
        readerController.title = tableViewItemTitle[indexPath.row].uppercased()
        
        let tlabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
        tlabel.text = tableViewItemTitle[indexPath.row].uppercased()
        tlabel.textColor = .white
        tlabel.font = UIFont(name: "Helvetica-Bold", size: 18)
        tlabel.adjustsFontSizeToFitWidth = true
        readerController.navigationItem.titleView = tlabel
        
        navigationController?.pushViewController(readerController, animated: true)
        
    }
    
    @objc func backClick() {
        navigationController?.popViewController(animated: true)
    }
    
    func image(_ image: UIImage, withSize newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!.withRenderingMode(.automatic)
    }
}
