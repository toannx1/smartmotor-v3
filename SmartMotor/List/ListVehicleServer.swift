//
//  ListVehicleServer.swift
//  SmartMotor
//
//  Created by Vietnv2 on 3/7/17.
//  Copyright © 2017 vietnv2. All rights reserved.
//

import CryptoSwift
import UIKit

extension ListVehicleVC {
    // MARK: - Load list vehicle
    
    func loadListVehicle() {
        let flagSearch = searchVehicleFlag
        
        if tokenSave == nil || userIdSave == nil {
            gotoLoginView()
        }
        Loading().showLoading()
        if registerNo2Query == nil {
            registerNo2Query = ""
        }
        let url1 = smartmotorUrl + listTransportUrl + userIdSave + listTransportRegNoUrl
        let url2 = registerNo2Query + listTransportPageIndexUrl + String(pageIndex)
        var urlString = url1 + url2
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        let url = URL(string: urlString)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        request.addValue(tokenSave, forHTTPHeaderField: "user-token")
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        let task = defaultSession.dataTask(with: request as URLRequest) { data, response, error in
            Loading().hideLoading(delay: 0)
            self.searchVehicleFlag = flagSearch
            if error != nil {
//                self.retryLoadListVehicle()
                self.showBannerNotifi(text: connectionFail[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
//                    self.alertCustom(message: cannotLoadListVehicle[languageChooseSave], delay: 2)
                    self.showBannerNotifi(text: cannotLoadListVehicle[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                    return
                }
                
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)) as! NSDictionary
                    let resultCode = jsonResult["resultCode"] as! Int
                    if resultCode != 1 {
                        self.gotoLoginView()
                        return
                    }
                    self.totalOfVehicle = jsonResult["total"] as? Int
                    if self.totalOfVehicle == 0, self.searchVehicleFlag == true {
                        numberOfVehcle = 0
                        self.listVehicleTableView.reloadData()
                        self.listVehicleTableView.isHidden = false
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            self.viewContainer.removeFromSuperview()
                            return
                        }
                    }
                    
                    let jsonData: NSArray! = jsonResult["data"] as? NSArray
                    if jsonData.count == 0 {
                        return
                    }
                    if numberOfVehcle >= self.totalOfVehicle {
                        vehicleInformations.removeAll()
                    }
                    if numberOfVehcle < pageIndex * 10 {
                        for element in jsonData {
                            let result = element as AnyObject
                            vehicleInformations.append(self.parseJson2GetListVehicle(result: result))
                            numberOfVehcle = vehicleInformations.count
                        }
                    }
                    if self.loadingDataInListVCFlag == true {
                        self.loadingDataInListVCFlag = false
                    }
                    if numberOfVehcle % 10 > 0 {
                        self.endOfListFlag = true
                    }
                    self.listVehicleTableView.reloadData()
                    self.listVehicleTableView.isHidden = false
                    if numberOfVehcle > 0 {
                        if vehicle2showIndex == nil {
                            vehicle2show = vehicleInformations[0]
                            if vehicle2show.isForbidden == true {
                                self.viewContainer.removeFromSuperview()
                                self.displayIsForbiddenAlertView()
                            } else {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                    self.viewContainer.removeFromSuperview()
                                    return
                                }
                            }
                            
//                            self.lblDisplayRegNo.text = String(format: listVehicleVC_RegNo_Select[languageChooseSave], vehicle2show.regNo).uppercased()
//                            self.buttonView.isHidden = false
                        } else {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.viewContainer.removeFromSuperview()
                                return
                            }
                        }
                        
                    } else {
//                        self.lblDisplayRegNo.text = listVehicleVC_RegNo_Choose[languageChooseSave].uppercased()
//                        self.buttonView.isHidden = true
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            self.viewContainer.removeFromSuperview()
                            return
                        }
                        return
                    }
//                    if loginFlag == true {
//                        loginFlag = false
//                        if numberOfVehcle == 1 && self.searchVehicleFlag == false {
//                            vehicle2show = vehicleInformations[0]
//                            if vehicle2show.isForbidden == true {
//                                return
//                            }
//                            vehicle2showIndex = 0
                    ////                            self.gotoMapView()
//                        }
//                    }
                    
                } catch {
                    fatalError("Failure\(error)")
                }
            } else {
//                self.alertCustom(message: cannotLoadListVehicle[languageChooseSave], delay: 2)
                self.showBannerNotifi(text: cannotLoadListVehicle[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
            }
        }
        task.resume()
    }
    
    // MARK: - Logout
    
    func logout() {
        Loading().showLoading()
        if tokenSave == nil || userIdSave == nil {
            gotoLoginView()
        }
        let url = URL(string: smartmotorUrl + logoutUrl)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = postMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        request.addValue(tokenSave, forHTTPHeaderField: "user-token")
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let dataString: [String: String] = ["userId": userIdSave!] as Dictionary
        request.httpBody = try! JSONSerialization.data(withJSONObject: dataString, options: [])
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        let task = defaultSession.dataTask(with: request as URLRequest) { data, response, error in
            Loading().hideLoading(delay: 0)
            if error != nil {
                // self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
                self.showBannerNotifi(text: connectionFail[languageChooseSave], background: colorError, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    // self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
                    self.showBannerNotifi(text: connectionFail[languageChooseSave], background: colorError, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let result = jsonResult["resultCode"] as! Int
                    print(jsonResult)
                    if result != 1 {
                        // self.alertCustom(message: txtLogoutUnSuccessAlert[languageChooseSave], delay: 2)
                        self.showBannerNotifi(text: txtLogoutUnSuccessAlert[languageChooseSave], background: colorError, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!)
                        return
                    }
                    self.gotoLoginView()
                } catch {
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
//    func accountInfo() {
//        Loading().showLoading()
//        if tokenSave == nil || userIdSave == nil {
//            gotoLoginView()
//        }
//        let url = URL(string: smartmotorUrl + getAccountInfoUrl + userIdSave + "/" + vehicle2show.sim)
//        let request = NSMutableURLRequest(url: url!)
//        request.httpMethod = getMethod
//        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
//        request.addValue(tokenSave, forHTTPHeaderField: "user-token")
//        let defaultConfigObject = URLSessionConfiguration.default
//        defaultConfigObject.timeoutIntervalForRequest = 30
//        defaultConfigObject.timeoutIntervalForResource = 30
//        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
//        let task = defaultSession.dataTask(with: request as URLRequest) { (data, response, error) in
//            Loading().hideLoading(delay:0)
//            if error != nil {
//                self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
//                return
//            }
//            if let httpResponse = response as? HTTPURLResponse {
//                if httpResponse.statusCode != 200 {
//                    self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
//                    return
//                }
//                do {
//                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
//                    let resultCode = jsonResult["resultCode"] as! Int
//                    if resultCode != 1 {
//                        self.alertCustom(message: txtAlertAccountInfo[languageChooseSave], delay: 2)
//                        return
//                    }
//                    self.lblSmsPromotion.text = jsonResult["smsPromotion"] as? String
//                    self.lblBasicBalance.text = jsonResult["basicBalance"] as? String
//                    self.lblGprsFreePromotion.text = jsonResult["gprsFreePromotion"] as? String
//                    self.lblMoneyPromotion.text = jsonResult["moneyPromotion"] as? String
//                    self.lblSmsNgoaiMang.text = jsonResult["smsNgoaiMang"] as? String
//                    self.lblTimeCallNgoaiMang.text = jsonResult["timePromotion"] as? String
//                    self.displayAccountInfoView()
//                }catch {
//                    fatalError("Failure\(error)")
//                }
//            }
//        }
//        task.resume()
//    }
    
    // Mark: Parse data
    func parseJson2GetListVehicle(result: AnyObject) -> vehicleInformation {
        var resultReturn: vehicleInformation!
        var gpsDate: String! = ""
        var isForbidden: Bool! = false
        let accIllegalState = result.value(forKeyPath: vehicleInfoString.accIllegalState.rawValue) as! Int
        let devicePin = result.value(forKeyPath: vehicleInfoString.devicePin.rawValue) as! String
        let deviceType = result.value(forKeyPath: vehicleInfoString.deviceType.rawValue) as! Int
        let deviceTypeIdOrginal = result.value(forKeyPath: vehicleInfoString.deviceTypeIdOrginal.rawValue) as! Int
        let factory = result.value(forKeyPath: vehicleInfoString.factory.rawValue) as! String
        if let _gpsDate = (result.value(forKeyPath: vehicleInfoString.gpsDate.rawValue) as? String) {
            gpsDate = _gpsDate
        }
        let gpsSpeed = result.value(forKeyPath: vehicleInfoString.gpsSpeed.rawValue) as! Int
        let gpsState = result.value(forKeyPath: vehicleInfoString.gpsState.rawValue) as! Int
        let groupsCode = result.value(forKeyPath: vehicleInfoString.groupsCode.rawValue) as! String
        let id = result.value(forKeyPath: vehicleInfoString.id.rawValue) as! Int
        let illegalMoveState = result.value(forKeyPath: vehicleInfoString.illegalMoveState.rawValue) as! Int
        let lat = result.value(forKeyPath: vehicleInfoString.lat.rawValue) as! Double
        let lng = result.value(forKeyPath: vehicleInfoString.lng.rawValue) as! Double
        let lowBatteryState = result.value(forKeyPath: vehicleInfoString.lowBatteryState.rawValue) as! Int
        let motoSpeed = result.value(forKeyPath: vehicleInfoString.motoSpeed.rawValue) as! Int
        let offPowerState = result.value(forKeyPath: vehicleInfoString.offPowerState.rawValue) as! Int
        let regNo = result.value(forKeyPath: vehicleInfoString.regNo.rawValue) as! String
        let sim = result.value(forKeyPath: vehicleInfoString.sim.rawValue) as! String
        let sosState = result.value(forKeyPath: vehicleInfoString.sosState.rawValue) as! Int
        let state = result.value(forKeyPath: vehicleInfoString.state.rawValue) as! Int
        let timeState = result.value(forKeyPath: vehicleInfoString.timeState.rawValue) as! Int
        let type = result.value(forKeyPath: vehicleInfoString.type.rawValue) as! String
        let vibrationState = result.value(forKeyPath: vehicleInfoString.vibrationState.rawValue) as! Int
        if let _isForbidden = result.value(forKeyPath: vehicleInfoString.isForbidden.rawValue) as? Bool {
            isForbidden = _isForbidden
        }
        resultReturn = vehicleInformation(accIllegalState: accIllegalState, devicePin: devicePin, deviceType: deviceType, deviceTypeIdOrginal: deviceTypeIdOrginal, factory: factory, gpsDate: gpsDate, gpsSpeed: gpsSpeed, gpsState: gpsState, groupsCode: groupsCode, id: id, illegalMoveState: illegalMoveState, lat: lat, lng: lng, lowBatteryState: lowBatteryState, motoSpeed: motoSpeed, offPowerState: offPowerState, regNo: regNo, sim: sim, sosState: sosState, state: state, timeState: timeState, type: type, vibrationState: vibrationState, isForbidden: isForbidden)
        return resultReturn
    }
    
    // MARK: - Change App Password
    
    func changeAppPassword() {
        Loading().showLoading()
        var request = NSMutableURLRequest()
        let iv = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] as [UInt8]
        let encryptedBase64_OldPassword = try! changePassApplicationView?.textField1.text!.encrypt(cipher: AES(key: encrypKeyForLogin.bytes, blockMode: .CBC(iv: iv)))
        let encryptedBase64_NewPassword = try! changePassApplicationView?.textField2.text!.encrypt(cipher: AES(key: encrypKeyForLogin.bytes, blockMode: .CBC(iv: iv)))
        let url = URL(string: smartmotorUrl + "/mtapi/rest/auth/changePassword/")
        request = NSMutableURLRequest(url: url!)
        request.httpMethod = postMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        request.addValue(tokenSave, forHTTPHeaderField: "user-token")
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let dataString = ["userId": userIdSave, "oldpassword": encryptedBase64_OldPassword!, "newpassword": encryptedBase64_NewPassword!] as Dictionary
        request.httpBody = try! JSONSerialization.data(withJSONObject: dataString, options: [])
        //
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        
        let task = defaultSession.dataTask(with: request as URLRequest) { data, response, error in
            Loading().hideLoading(delay: 0)
            if error != nil {
                Loading().hideLoading(delay: 0)
                self.showBannerNotifi(text: txtcannotConnect2Server[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    Loading().hideLoading(delay: 0)
                    self.printUnSuccess()
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let resultCode = jsonResult.value(forKey: "resultCode") as! Int
                    print(jsonResult)
                    switch resultCode {
                    case -5:
                        Loading().hideLoading(delay: 0)
                        self.showBannerNotifi(text: "Sai mật khẩu hiện tại", background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                        return
                    case -6, -7, -8, -9:
                        Loading().hideLoading(delay: 0)
                        self.showBannerNotifi(text: "Mật khẩu không hợp lệ.\nMật khẩu phải bao gồm: Số, chữ thường, chữ hoa và ký tự đặc biệt.", background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                        print(txtAlert08ChangeAppPassword[languageChooseSave])
                        
                        return
                    case -11:
                        Loading().hideLoading(delay: 0)
                        self.showBannerNotifi(text: txtAlert09ChangeAppPassword[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                        return
                    default: // 1
                        Loading().hideLoading(delay: 0)
                        self.showBannerNotifi(text: txtAlert11ChangeAppPassword[languageChooseSave], background: colorSuccess, textColor: .white, imageTitle: UIImage(named: "checked")!, des: "")
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
                            logoutFlag = true
                            self.gotoLoginView()
                        }
                    }
                    
                } catch {
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    // MARK: - Generate message call back
    
    func printSuccess() {
        var string: String = commandControlString.components(separatedBy: "=")[0]
        switch string {
        case "REBOOT": string = commandControlStringReboot[languageChooseSave] + commandControlStringSuccess[languageChooseSave]
        case "USER=", "USER": string = commandControlStringChangeUserPhone[languageChooseSave] + commandControlStringSuccess[languageChooseSave]
        case "PSW=", "PSW": string = commandControlStringChangeVehiclePassword[languageChooseSave] + commandControlStringSuccess[languageChooseSave]
        default:
            return
        }
//        alertCustom(message: string, delay: 5)
        self.showBannerNotifi(text: string, background: colorSuccess, textColor: .white, imageTitle: UIImage(named: "checked")!, des: "")
    }
    
    func printUnSuccess() {
        var string: String = commandControlString.components(separatedBy: "=")[0]
        switch string {
        case "REBOOT": string = commandControlStringReboot[languageChooseSave] + commandControlStringUnSuccess[languageChooseSave]
        case "USER=", "USER": string = commandControlStringChangeUserPhone[languageChooseSave] + commandControlStringUnSuccess[languageChooseSave]
        case "PSW=", "PSW": string = commandControlStringChangeVehiclePassword[languageChooseSave] + commandControlStringUnSuccess[languageChooseSave]
        default:
            return
        }
//        alertCustom(message: string, delay: 5)
        self.showBannerNotifi(text: string, background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
    }
}
