//
//  ListVehicleTableView.swift
//  SmartMotor
//
//  Created by vietnv2 on 12/5/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import SwiftEntryKit
import SystemConfiguration
import UIKit

extension ListVehicleVC: UITableViewDelegate, UITableViewDataSource {
    enum dataConnection: String {
        case noConnection
        case wifiConnection
        case otherConnection = "2g3gConnection"
    }
    
    // MARK: - Init TableView
    
    func initTableView() {
        listChargeMoneyTypeTableView.tableFooterView = UIView(frame: CGRect.zero)
        listChargeMoneyTypeTableView.isScrollEnabled = false
        listMonth2PayTableView.tableFooterView = UIView(frame: CGRect.zero)
        listMonth2PayTableView.isScrollEnabled = false
        let footer = UIView(frame: CGRect.zero)
        footer.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1)
        listVehicleTableView.tableFooterView = footer
        tableViewAccountSupport.tableFooterView = footer
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == listVehicleTableView {
            var numOfSection: NSInteger = 0
            let noDataLabel: NoVehicleFoundView = NoVehicleFoundView(frame: CGRect(x: 0, y: 0, width: self.listVehicleTableView.bounds.size.width, height: self.listVehicleTableView.bounds.size.height))
            if numberOfVehcle > 0 {
                self.listVehicleTableView.backgroundView = nil
                numOfSection = 1
            } else {
                self.listVehicleTableView.backgroundView = noDataLabel
            }
            return numOfSection
        }
        return 1
    }
    
    func checkInternet() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) { zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case listVehicleTableView:
            return numberOfVehcle
        case listChargeMoneyTypeTableView:
            return 2 // listChargeMoneyString.count
        case listMonth2PayTableView:
            return numberOfMonth.count
        case tableViewAccountSupport:
            return 5
        case shimmerView.tableView:
            return 5
        default: break
        }
        return 0
    }
    
    // MARK: - DataSource
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell2Return: UITableViewCell!
        switch tableView {
        case listVehicleTableView:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListVehicleTableViewCell", for: indexPath) as! ListVehicleTableViewCell
            if numberOfVehcle != 0 {
                cell.lblRegNo.text = vehicleInformations[indexPath.row].regNo.uppercased()
                cell.lblName.text = vehicleInformations[indexPath.row].type.uppercased()
                cell.lblPhoneNumber.text = vehicleInformations[indexPath.row].sim
                cell.imgIcon.image = self.motorStateImage[vehicleInformations[indexPath.row].state]
            }
            if vehicle2showIndex != nil, indexPath.row == vehicle2showIndex {
                cell.backgroundColor = UIColor(red: 0xBB / 0xFF, green: 0xBB / 0xFF, blue: 0xBB / 0xFF, alpha: 0.3)
            } else {
                cell.backgroundColor = UIColor(red: 0x00 / 0xFF, green: 0x00 / 0xFF, blue: 0x00 / 0xFF, alpha: 0)
            }
            cell.lblPhoneNumber.text = vehicleInformations[indexPath.row].sim
            return cell
        case listChargeMoneyTypeTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellForChargeMoney", for: indexPath) as! ChargeMoneyCell
            if indexPath.row == 0 {
                cell.lblChargeMoneyStype.text = listChargeMoney_PaymentViaCard[languageChooseSave]
            } else {
                cell.lblChargeMoneyStype.text = listChargeMoney_PaymentViaPhoneBalance[languageChooseSave]
            }
            cell2Return = cell
        case listMonth2PayTableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellForChooseMonth", for: indexPath) as! ListMonthToPayCell
            cell.lblNumberOfMonth.text = String(numberOfMonth[indexPath.row]) + " " + txtMonthPay[languageChooseSave]
            if numberOfMonth[indexPath.row] > 1, languageChooseSave > 0 {
                cell.lblNumberOfMonth.text = cell.lblNumberOfMonth.text! + "s"
            }
            
            cell2Return = cell
        case tableViewAccountSupport:
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            cell.imageView?.image = self.image(accountImg[indexPath.row]!, withSize: CGSize(width: 24, height: 24))
            cell.textLabel?.text = accountItem[indexPath.row]
            cell.textLabel?.numberOfLines = 2
            if indexPath.row != 4 {
                cell.accessoryType = .disclosureIndicator
            }
            cell.selectionStyle = .none
            return cell
        case shimmerView.tableView:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShimmerTableViewCell") as! ShimmerTableViewCell
            return cell
        default:
            break
        }
        return cell2Return
    }
    
    func image(_ image: UIImage, withSize newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!.withRenderingMode(.automatic)
    }
    
    // MARK: - Did selected row
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView {
        case listVehicleTableView:
            self.listVehicleTableViewDidSelected(indexPath: indexPath)
        case tableViewAccountSupport:
            if indexPath.row == 0 {
                self.removeFeedback()
                let guide = GuideTableViewController()
                let nav = UINavigationController(rootViewController: guide)
                nav.modalTransitionStyle = .coverVertical
                self.present(nav, animated: true, completion: nil)
            } else if indexPath.row == 2 {
                self.showFeedbackView()
            } else if indexPath.row == 3 {
                self.removeFeedback()
                self.showChangePassApplication()
            } else if indexPath.row == 4 {
                self.removeFeedback()
                displayWarningLogoutView()
            }
        case listChargeMoneyTypeTableView:
            self.chargeMoneyTableDidSelected(indexPath: indexPath)
        case listMonth2PayTableView:
            removeChooseNumberOfMonthToPayView()
            monthPay = String(numberOfMonth[indexPath.row])
            let money = monthlyFee * numberOfMonth[indexPath.row]
            moneyPay = String(money)
            btnChooseNumberOfMonth2Pay.setTitle("\(numberOfMonth[indexPath.row]) \(txtMonthPay[languageChooseSave])", for: .normal)
            if languageChooseSave == 0 {
                txtMoneyPay.text = money.stringWithSepator + " VNĐ"
            } else {
                txtMoneyPay.text = money.stringWithSepator + " VND"
            }
        default:
            break
        }
    }
    
    // MARK: - Refactor didSelected tableView
    
    func listVehicleTableViewDidSelected(indexPath: IndexPath) {
        if self.checkInternet() == false {
            self.showBannerNotifi(text: "Lỗi kết nối mạng. Vui lòng thử lại sau!", background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
            return
        }
        vehicle2showIndex = indexPath.row
        vehicle2show = vehicleInformations[indexPath.row]
        listVehicleTableView.reloadData()
        var count: Int! = 0
        for info in vehiclePayInformation {
            if info.regNo == vehicle2show.regNo {
                if info.paySuccess == true, vehicle2show.isForbidden == true {
                    self.showBannerNotifi(text: txtAlert01PayMoney[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                    return
                }
                if info.paySuccess == true, vehicle2show.isForbidden == false {
                    vehiclePayInformation.remove(at: count)
                    gotoMapView()
                    return
                }
            }
            count = count + 1
        }
        if vehicle2show.isForbidden == true {
            lblContentAlertForBidden.text = String(format: txtContent_AlertForBidden[languageChooseSave], vehicle2show.regNo)
            removeListChargeMoneyView()
            displayIsForbiddenAlertView()
            return
        }
        gotoMapView()
    }
    
    func chargeMoneyTableDidSelected(indexPath: IndexPath) {
        if indexPath.row == 0 {
            removeListChargeMoneyView()
            displayChargeCardCodeContainerView()
            txtChargeCode.text = ""
            txtCaptchaCardCode.text = ""
            lblContent03ChargeCardCode.text = txtContent03ChargeCardCode[languageChooseSave] + vehicle2show.sim
            urlSessionDelegate()
        } else {
            if CheckNetwork().checkNetwork() == dataConnection.wifiConnection.rawValue {
                // alertCustom(message: txtAlert01PayMoney[languageChooseSave], delay: 3)
                self.showBannerNotifi(text: txtAlert01PayMoney[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                return
            } else if CheckNetwork().checkNetwork() == dataConnection.noConnection.rawValue {
                self.showBannerNotifi(text: "Lỗi kết nối mạng. Vui lòng thử lại sau!", background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!)
                // alertCustom(message: txtAlert02PayMoney[languageChooseSave], delay: 3)
                return
            }
            lblContent01PayMoney.text = ""
            txtCaptchaPayMoney.text = ""
            getIPAddress()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = HeaderListTableView()
        let attrs1 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.black]
        let attrs3 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor: UIColor.black]
        let attrs2 = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .semibold), NSAttributedString.Key.foregroundColor: UIColor.black]
        
        let attributedString1 = NSMutableAttributedString(string: "  Tổng số ", attributes: attrs1)
        
        let attributedString2 = NSMutableAttributedString(string: numberOfVehcle.description, attributes: attrs2)
        let attributedString3 = NSMutableAttributedString(string: " xe", attributes: attrs3)
        
        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        
        header.title.attributedText = attributedString1
        
        header.backgroundColor = .white
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == listVehicleTableView {
            if searchVehicleFlag {
                return 40
            } else {
                return 40
            }
        }
        return 0
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if endOfListFlag != false {
            return
        }
        if scrollView != listVehicleTableView {
            return
        }
        if scrollView.contentOffset.y + scrollView.frame.size.height < scrollView.contentSize.height {
            return
        }
        if loadingDataInListVCFlag != false {
            return
        }
        loadingDataInListVCFlag = true
        pageIndex = pageIndex + 1
        loadListVehicle()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == listMonth2PayTableView {
            return 40
        } else if tableView == listVehicleTableView || tableView == shimmerView.tableView {
            return 110
        } else if tableView == tableViewAccountSupport {
            return 60
        }
        
        return 50
    }
    
    // MARK: - Tabbar Action
    
    @IBAction func manageVehicleTapped(_ sender: Any) {
        btnManageVehicle.setImage(UIImage(named: "icon_motor1"), for: .normal)
        btnAccountAndSupport.setImage(UIImage(named: "thongtin"), for: .normal)
        self.scrollToPage(page: 0, animated: true)
        lblTitleListVehicle.text = listVehicleVC_Title[languageChooseSave].uppercased()
    }
    
    @IBAction func AccountSupportTapped(_ sender: Any) {
        btnManageVehicle.setImage(UIImage(named: "icon_motor2"), for: .normal)
        btnAccountAndSupport.setImage(UIImage(named: "thongtin_press"), for: .normal)
        self.scrollToPage(page: 1, animated: true)
        lblTitleListVehicle.text = "Tài khoản và hỗ trợ".uppercased()
    }
    
    func scrollToPage(page: Int, animated: Bool) {
        var frame: CGRect = self.scrollViewTabbar.frame
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0
        self.scrollViewTabbar.scrollRectToVisible(frame, animated: animated)
        
        if textSearchVehicle == "" {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.listVehicleTableView.isHidden = false
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == scrollViewTabbar {
            if textSearchVehicle == "" {
                listVehicleTableView.isHidden = false
            }
            let page = (scrollView.contentOffset.x / scrollView.frame.size.width)
            if page == 0 {
                btnManageVehicle.setImage(UIImage(named: "icon_motor1"), for: .normal)
                btnAccountAndSupport.setImage(UIImage(named: "thongtin"), for: .normal)
                lblTitleListVehicle.text = listVehicleVC_Title[languageChooseSave].uppercased()
                self.removeFeedback()
            } else {
                btnManageVehicle.setImage(UIImage(named: "icon_motor2"), for: .normal)
                btnAccountAndSupport.setImage(UIImage(named: "thongtin_press"), for: .normal)
                lblTitleListVehicle.text = "Tài khoản và hỗ trợ".uppercased()
            }
        }
    }
    
    // MARK: - Account Manage
    
    func showFeedbackView() {
        viewFeedback.btnCancel.addTarget(self, action: #selector(self.removeFeedback), for: .touchUpInside)
        viewFeedback.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(viewFeedback)
        viewFeedback.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        viewFeedback.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 2).isActive = true
        feedBackConstraint = viewFeedback.bottomAnchor.constraint(equalTo: bottomLayoutGuide.bottomAnchor, constant: 400)
        feedBackConstraint!.isActive = true
        shadowOnviewWithcornerRadius(customView: viewFeedback, cornerRadius: 30)
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, animations: {
                self.feedBackConstraint?.constant = 50
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @objc func removeFeedback() {
        UIView.animate(withDuration: 0.5) {
            self.feedBackConstraint?.constant = 400
            self.view.layoutIfNeeded()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.viewFeedback.removeFromSuperview()
        }
    }
    
    func showChangePassApplication() {
        changePassApplicationView = ChangePassApplicationView(frame: view.frame)
        view.addSubview(changePassApplicationView!)
        configChangePassView()
    }
    
    @objc func hideChangePasswordView() {
        changePassApplicationView?.removeFromSuperview()
        checkTf1 = false
        checkTf2 = false
        checkTf3 = false
    }
}

import LBTATools
class HeaderListTableView: UIView {
    let title = UILabel(text: "", font: UIFont.systemFont(ofSize: 16), textColor: .black, textAlignment: .left, numberOfLines: 0)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        stack(self.title, UIView(backgroundColor: .lightGray).withHeight(1)).withMargins(.init(top: 0, left: 10, bottom: 0, right: 0))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
