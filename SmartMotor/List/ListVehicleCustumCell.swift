//
//  ListVehicleCustumCell.swift
//  Smart Motor
//
//  Created by vietnv2 on 26/10/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

class ListVehicleCustumCell: UITableViewCell {
    
    var imgIcon = UIImageView(image: UIImage(named: "ic_sm_park"))
    var lblRegNo = UILabel()
    var lblName = UILabel()
    var lbPhoneNumber = UILabel()
    var icPhone = UIImageView(image: UIImage(named: "ic_phone_number"))
    var icName = UIImageView(image: UIImage(named: "ic_type_motor"))
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgIcon.translatesAutoresizingMaskIntoConstraints = false
        lblRegNo.translatesAutoresizingMaskIntoConstraints = false
        icPhone.translatesAutoresizingMaskIntoConstraints = false
        lbPhoneNumber.translatesAutoresizingMaskIntoConstraints = false
        icName.translatesAutoresizingMaskIntoConstraints = false
        lblName.translatesAutoresizingMaskIntoConstraints = false
        
        backgroundColor = .red
        lblRegNo.text = "ress"
        lblName.text = "sdfsdf"
        
        lblRegNo.textColor = .black
        lblName.textColor = .black
        
        addSubview(imgIcon)
        addSubview(lblRegNo)
        addSubview(icPhone)
        addSubview(lbPhoneNumber)
        addSubview(icName)
        addSubview(lblName)
    }
    
    func setupConstraint() {
        imgIcon.heightAnchor.constraint(equalToConstant: 36).isActive = true
        imgIcon.widthAnchor.constraint(equalToConstant: 25).isActive = true
        imgIcon.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        imgIcon.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        
        lblRegNo.leftAnchor.constraint(equalTo: imgIcon.rightAnchor, constant: 8).isActive = true
        lblRegNo.centerYAnchor.constraint(equalTo: imgIcon.centerYAnchor).isActive = true
        lblRegNo.heightAnchor.constraint(equalToConstant: 36).isActive = true
        lblRegNo.rightAnchor.constraint(equalTo: rightAnchor, constant: -60).isActive = true
        
        icPhone.topAnchor.constraint(equalTo: lblRegNo.bottomAnchor).isActive = true
        icPhone.leftAnchor.constraint(equalTo: lblRegNo.leftAnchor).isActive = true
        icPhone.widthAnchor.constraint(equalToConstant: 24).isActive = true
        icPhone.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        lbPhoneNumber.leftAnchor.constraint(equalTo: icPhone.rightAnchor, constant: 8).isActive = true
        lbPhoneNumber.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lbPhoneNumber.centerYAnchor.constraint(equalTo: icPhone.centerYAnchor).isActive = true
        lbPhoneNumber.rightAnchor.constraint(equalTo: rightAnchor, constant: -60).isActive = true
        
        icName.topAnchor.constraint(equalTo: icPhone.bottomAnchor, constant: 8).isActive = true
        icName.leftAnchor.constraint(equalTo: lblRegNo.leftAnchor).isActive = true
        icName.widthAnchor.constraint(equalToConstant: 24).isActive = true
        icName.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        lblName.leftAnchor.constraint(equalTo: icName.rightAnchor, constant: 8).isActive = true
        lblName.heightAnchor.constraint(equalToConstant: 24).isActive = true
        lblName.centerYAnchor.constraint(equalTo: icName.centerYAnchor).isActive = true
        lblName.rightAnchor.constraint(equalTo: rightAnchor, constant: -60).isActive = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
