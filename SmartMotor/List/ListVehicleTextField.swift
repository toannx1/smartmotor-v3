//
//  ListVehicleTextField.swift
//  SmartMotor
//
//  Created by vietnv2 on 12/5/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

extension ListVehicleVC: UITextFieldDelegate {
    // MARK: - init TextField
    
    func initTextField() {
        txtChargeCode.keyboardType = UIKeyboardType.numberPad
        txtCaptchaCardCode.keyboardType = UIKeyboardType.default
        txtChargeCode.delegate = self
        txtCaptchaCardCode.delegate = self
        txtCaptchaPayMoney.delegate = self
        changePassApplicationView = ChangePassApplicationView()
        configChangePassView()
    }
    
    // MARK: - TextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtChargeCode, txtChargeCode.text == "" {
            // alertCustom(message: txtAlertCardCode_ChargeCardCode[languageChooseSave], delay: 2)
            self.showBannerNotifi(text: txtAlertCardCode_ChargeCardCode[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
        } else if textField == txtCaptchaCardCode, txtCaptchaCardCode.text == "" {
            self.showBannerNotifi(text: txtEnterCaptcha[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
            // alertCustom(message: txtEnterCaptcha[languageChooseSave], delay: 2)
        }
        if textField == txtCaptchaPayMoney, txtCaptchaPayMoney.text == "" {
            self.showBannerNotifi(text: txtEnterCaptcha[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
            // alertCustom(message: txtEnterCaptcha[languageChooseSave], delay: 2)
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        // hideKeyBoad()
        if textField == changePassApplicationView?.textField1 {
            changePassApplicationView?.textField2.becomeFirstResponder()
        } else if textField == changePassApplicationView?.textField2 {
            changePassApplicationView?.textField3.becomeFirstResponder()
        } else if textField == changePassApplicationView?.textField3 {
            changePassApplicationView?.textField1.becomeFirstResponder()
        }
        return true
    }
    
    // MARK: Show/hide clear/secure buttons
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.returnKeyType = .next
        
        switch textField {
        case txtChargeCode, txtCaptchaCardCode:
            chargeCardCodeContainerView.frame.size.height = view.frame.size.height - keyboardHeight
            if chargeCardCodeView.frame.size.height > chargeCardCodeContainerView.frame.size.height {
                chargeCardCodeViewHeightContraint.constant = view.frame.size.height - keyboardHeight
            }
        case txtCaptchaPayMoney:
            payMoneyContainerView.frame.size.height = view.frame.size.height - keyboardHeight
            if payMoneyView.frame.size.height > payMoneyContainerView.frame.size.height {
                payMoneyViewHeightContraint.constant = view.frame.size.height - keyboardHeight
            }
        case changePassApplicationView?.textField1:
            if (changePassApplicationView?.textField1.text!.count)! > 0 {
                changePassApplicationView!.btnClear1.isHidden = false
                changePassApplicationView!.btnSecure1.isHidden = false
            }
            changePassApplicationView!.btnClear2.isHidden = true
            changePassApplicationView!.btnSecure2.isHidden = true
            changePassApplicationView!.btnClear3.isHidden = true
            changePassApplicationView!.btnSecure3.isHidden = true
        case changePassApplicationView?.textField2:
            if (changePassApplicationView?.textField2.text!.count)! > 0 {
                changePassApplicationView!.btnClear2.isHidden = false
                changePassApplicationView!.btnSecure2.isHidden = false
            }
            changePassApplicationView!.btnClear1.isHidden = true
            changePassApplicationView!.btnSecure1.isHidden = true
            changePassApplicationView!.btnClear3.isHidden = true
            changePassApplicationView!.btnSecure3.isHidden = true
        case changePassApplicationView?.textField3:
            if (changePassApplicationView?.textField3.text!.count)! > 0 {
                changePassApplicationView!.btnClear3.isHidden = false
                changePassApplicationView!.btnSecure3.isHidden = false
            }
            changePassApplicationView!.btnClear1.isHidden = true
            changePassApplicationView!.btnSecure1.isHidden = true
            changePassApplicationView!.btnClear2.isHidden = true
            changePassApplicationView!.btnSecure2.isHidden = true
        default:
            break
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case txtChargeCode:
            chargeCardCodeViewHeightContraint.constant = 416
            chargeCardCodeContainerView.frame.size.height = view.frame.size.height
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        case txtCaptchaCardCode:
            chargeCardCodeViewHeightContraint.constant = 416
            chargeCardCodeContainerView.frame.size.height = view.frame.size.height
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        case txtCaptchaPayMoney:
            payMoneyContainerView.frame.size.height = view.frame.size.height
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            
        // Change Pass App
        case changePassApplicationView?.textField1:
            if changePassApplicationView?.textField1.text == "" {
                changePassApplicationView?.lbCurrentPassError.isHidden = false
                changePassApplicationView?.lbCurrentPassError.text = "Vui lòng nhập mật khẩu hiện tại!"
            }
        case changePassApplicationView?.textField2:
            if changePassApplicationView?.textField2.text == "" {
                changePassApplicationView?.lbNewPassError.isHidden = false
                changePassApplicationView?.lbNewPassError.text = "Vui lòng nhập mật khẩu mới!"
            }
        case changePassApplicationView?.textField3:
            if changePassApplicationView?.textField3.text == "" {
                changePassApplicationView?.lbReNewPassError.isHidden = false
                changePassApplicationView?.lbReNewPassError.text = "Vui lòng xác nhận lại mật khẩu mới!"
            }
        default:
            break
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var maxLength = 20
        
        if textField == changePassApplicationView!.textField1 {
            changePassApplicationView!.btnClear1.isHidden = false
            changePassApplicationView!.btnSecure1.isHidden = false
            changePassApplicationView?.lbCurrentPassError.isHidden = true
            
            if range.length > 0, range.location == 0 {
                changePassApplicationView!.btnClear1.isHidden = true
                changePassApplicationView!.btnSecure1.isHidden = true
            }
        } else if textField == changePassApplicationView!.textField2 {
            changePassApplicationView!.btnClear2.isHidden = false
            changePassApplicationView!.btnSecure2.isHidden = false
            changePassApplicationView?.lbNewPassError.isHidden = true
            
            if range.length > 0, range.location == 0 {
                changePassApplicationView!.btnClear2.isHidden = true
                changePassApplicationView!.btnSecure2.isHidden = true
            }
        } else if textField == changePassApplicationView!.textField3 {
            changePassApplicationView!.btnClear3.isHidden = false
            changePassApplicationView!.btnSecure3.isHidden = false
            changePassApplicationView?.lbReNewPassError.isHidden = true
            
            if range.length > 0, range.location == 0 {
                changePassApplicationView!.btnClear3.isHidden = true
                changePassApplicationView!.btnSecure3.isHidden = true
            }
        }
        
        if textField.text!.count < 20 {
            if textField == changePassApplicationView?.textField1 || textField == changePassApplicationView?.textField2 || textField == changePassApplicationView?.textField3 {
                // dont allow special character
                maxLength = 20
                let set = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMONPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~`!@#$%^&*()_-+=[]{}|/?.>,<;:'0123456789").inverted
                
                //textField.text = textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
                return string.rangeOfCharacter(from: set) == nil
            }
        }
        
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
}
