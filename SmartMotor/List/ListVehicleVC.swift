//
//  ListVehicleVC.swift
//  Smart Motor
//
//  Created by vietnv2 on 18/10/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import ActiveLabel
import Reachability
import Shimmer
import UIKit

class ListVehicleVC: UIViewController {
    var feedBackConstraint: NSLayoutConstraint?
    var viewFeedback = FeedbackView()
    let shimmer = FBShimmeringView()
    var shimmerView = ShimmerView()
    
    @IBOutlet var listVehicleContraint: NSLayoutConstraint!
    @IBOutlet var hotlineView: UIView!
    @IBOutlet var imgCaptchaChargeCode: UIImageView!
    @IBOutlet var imgCaptchaPayMoney: UIImageView!
    @IBOutlet var imgRefressCaptchaChargeCode: UIImageView!
    @IBOutlet var btnCloseListChargeMoneyType: UIButton!
    @IBOutlet var btnChooseNumberOfMonth2Pay: UIButton!
    @IBOutlet var btnChargeMoney: UIButton!
    @IBOutlet var btnInfomation: UIButton!
    @IBOutlet var btnDangXuat: UIButton!
    @IBOutlet var btnSOS: UIButton!
    @IBOutlet var txtCaptchaCardCode: UITextField!
    @IBOutlet var txtCaptchaPayMoney: UITextField!
    @IBOutlet var txtChargeCode: UITextField!
    @IBOutlet var txtMoneyPay: UITextField!
    @IBOutlet var txtTestHyperLink: UITextField!
    @IBOutlet var lblDisplayRegNo: UILabel!
    @IBOutlet var lblSmsPromotion: UILabel!
    @IBOutlet var lblBasicBalance: UILabel!
    @IBOutlet var lblGprsFreePromotion: UILabel!
    @IBOutlet var lblMoneyPromotion: UILabel!
    @IBOutlet var lblSmsNgoaiMang: UILabel!
    @IBOutlet var lblTimeCallNgoaiMang: UILabel!
    @IBOutlet var payMoneyViewHeightContraint: NSLayoutConstraint!
    @IBOutlet var listChargeMoneyTypeTableView: UITableView!
    @IBOutlet var listMonth2PayTableView: UITableView!
    @IBOutlet var listVehicleTableView: UITableView!
    @IBOutlet var titleListView: UIView!
    @IBOutlet var lblContentConfirmChargeCardCode: UILabel!
    @IBOutlet var buttonView: UIView!
    @IBOutlet var listVehicleView: UIView!
    @IBOutlet var lblTitleListVehicle: UILabel!
    @IBOutlet var lblTitleListChargeMoney: UILabel!
    @IBOutlet var lblButtonListChargeMoney: UILabel!
    @IBOutlet var lblButtonBalanceInfo: UILabel!
    @IBOutlet var lblButtonHelp: UILabel!
    @IBOutlet var lblTitleChargeCardCode: UILabel!
    //    @IBOutlet weak var lblContent01ChargeCardCode: UILabel!
    @IBOutlet var lblContent02ChargeCardCode: UILabel!
    @IBOutlet var lblContent03ChargeCardCode: UILabel!
    @IBOutlet var lblContent04ChargeCardCode: UILabel!
    @IBOutlet var lblContent05ChargeCardCode: UILabel!
    @IBOutlet var btnConfirmChargeCardCode: UIButton!
    @IBOutlet var btnCancelChargeCardcode: UIButton!
    @IBOutlet var lblTitleConfirmChargeCardCode: UILabel!
    @IBOutlet var btnOKConfirmChargeCardCode: UIButton!
    @IBOutlet var btnCancelConfirmChargeCardCode: UIButton!
    
    @IBOutlet var lblTitleSOS: UILabel!
    @IBOutlet var btnCancelSOS: UIButton!
    
    @IBOutlet var lblTitleConfirmCallHotline: UILabel!
    @IBOutlet var lblContentConfirmCallHotline: UILabel!
    @IBOutlet var btnConfirmCallHotline: UIButton!
    @IBOutlet var btnCancelConfirmCallHotline: UIButton!
    
    @IBOutlet var lblTitleAccountInfo: UILabel!
    @IBOutlet var lblContent01AccountInfo: UILabel!
    @IBOutlet var lblContent02AccountInfo: UILabel!
    @IBOutlet var lblContent03AccountInfo: UILabel!
    @IBOutlet var lblContent04AccountInfo: UILabel!
    @IBOutlet var lblContent05AccountInfo: UILabel!
    @IBOutlet var lblContent06AccountInfo: UILabel!
    @IBOutlet var btnCancelAccountInfo: UIButton!
    
    @IBOutlet var lblTitlePayMoney: UILabel!
    @IBOutlet var lblContent01PayMoney: UILabel!
    @IBOutlet var lblContent02PayMoney: UILabel!
    @IBOutlet var lblContent03PayMoney: UILabel!
    @IBOutlet var btnConfirmPayMoney: UIButton!
    @IBOutlet var btnCancelPayMoney: UIButton!
    
    @IBOutlet var lblTitleConfirmPayMoney: UILabel!
    @IBOutlet var lblContentConfirmPayMoney: UILabel!
    @IBOutlet var btnOKConfirmPayMoney: UIButton!
    @IBOutlet var btnCancelConfirmPayMoney: UIButton!
    
    @IBOutlet var lblTitleAlertForBidden: UILabel!
    @IBOutlet var lblContentAlertForBidden: UILabel!
    @IBOutlet var btnServicePaymentAlertForBidden: UIButton!
    @IBOutlet var btnCancelAlertForBidden: UIButton!
    
    @IBOutlet var lblTitleConfirmLogout: UILabel!
    @IBOutlet var lblContentConfirmLogout: UILabel!
    @IBOutlet var btnOKConfirmLogout: UIButton!
    @IBOutlet var btnCancelConfirmLogout: UIButton!
    
    @IBOutlet var tableViewAccountSupport: UITableView!
    @IBOutlet var headerImage: UIImageView!
    @IBOutlet var btnAccountAndSupport: UIButton!
    @IBOutlet var btnManageVehicle: UIButton!
    @IBOutlet var scrollViewTabbar: UIScrollView!
    
    let lblHotline = ActiveLabel()
    
    @IBOutlet var searchBarListVehicle: UISearchBar!
    
    var textSearchVehicle: String = ""
    
    let motorStateImage: [UIImage] = [#imageLiteral(resourceName: "icon_sm_no_info"), // 0
                                      #imageLiteral(resourceName: "icon_sm_run"), // 1
                                      #imageLiteral(resourceName: "icon_sm_stop"), // 2
                                      #imageLiteral(resourceName: "icon_sm_park"), // 3
                                      #imageLiteral(resourceName: "icon_sm_lost_gps.png"), // 4
                                      #imageLiteral(resourceName: "icon_sm_hibernate"), // 5
                                      #imageLiteral(resourceName: "icon_sm_lost_gprs"), // 6
                                      #imageLiteral(resourceName: "icon_sm_no_info")] // 7
    
    let numberOfMonth: [Int] = [1, 2, 3, 6, 12]
    var monthPay: String! = "1"
    var moneyPay: String! = "30000"
    var loadingDataInListVCFlag: Bool! = false
    var searchVehicleFlag: Bool = false
    var endOfListFlag: Bool = false
    var requestedPhone: String!
    var deviceCode: String!
    var deviceSim: String!
    var monthlyFee: Int! = 0
    var registerNo: String!
    var cookieString: String! = ""
    var session = URLSession()
    var totalOfVehicle: Int! = 0
    var chargeCardCodeViewHeight: CGFloat!
    var payMoneyViewHeight: CGFloat!
    var location = CGPoint(x: 0, y: 0)
    var timerReloadListVehicle: Timer!
    
    var changePassApplicationView: ChangePassApplicationView?
    let reachability = Reachability()!
    var accountItem = ["Hướng dẫn sử dụng", "Quy định và chính sách dịch vụ", "Góp ý", "Đổi mật khẩu ứng dụng", "Đăng xuất"]
    var accountImg = [UIImage(named: "support"), UIImage(named: "ic_references"), UIImage(named: "comment"), UIImage(named: "ic_chang_app_pass"), UIImage(named: "ic_activate")]
    
    var checkTf1 = false
    var checkTf2 = false
    var checkTf3 = false
    
    @IBOutlet var lbTotalVeh: UILabel!
    let viewContainer = UIView(frame: .init(x: 0, y: 64, width: widthScreen, height: heightScreen - 136))
    
    // MARK: - Init View
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listVehicleTableView.register(UINib(nibName: "ListVehicleTableViewCell", bundle: nil), forCellReuseIdentifier: "ListVehicleTableViewCell")
        shimmer.frame = .init(x: 0, y: 0, width: widthScreen, height: heightScreen - 136)
        shimmerView.tableView.delegate = self
        shimmerView.tableView.dataSource = self
        shimmerView.tableView.register(UINib(nibName: "ShimmerTableViewCell", bundle: nil), forCellReuseIdentifier: "ShimmerTableViewCell")
        
        viewContainer.backgroundColor = .white
        view.addSubview(viewContainer)
        
        viewContainer.addSubview(shimmer)
        shimmerView.frame = .init(x: 0, y: 0, width: widthScreen, height: heightScreen - 136)
        shimmer.contentView = shimmerView
        shimmer.isShimmering = true
        
        edgesForExtendedLayout = []
        initListVehicleVC()
        loadListVehicle()
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do {
            try reachability.startNotifier()
        } catch {
            showBannerNotifi(text: "Lỗi kết nối mạng. Vui lòng thử lại sau!", background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .none:
            showBannerNotifi(text: "Lỗi kết nối mạng. Vui lòng thử lại sau!", background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
        default:
            loadListVehicle()
        }
    }
    
    func initListVehicleVC() {
        initLanguageListVehicleVC()
        initVar()
        initTableView()
        initTextField()
        initURLSession()
        btnManageVehicle.imageView?.contentMode = .scaleAspectFit
        btnAccountAndSupport.imageView?.contentMode = .scaleAspectFit
        searchBarListVehicle.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
    }
    
    func configChangePassView() {
        changePassApplicationView!.textField2.delegate = self
        changePassApplicationView!.textField1.delegate = self
        changePassApplicationView!.textField3.delegate = self
        changePassApplicationView!.btnCancel.addTarget(self, action: #selector(hideChangePasswordView), for: .touchUpInside)
        changePassApplicationView!.btnConfirm.addTarget(self, action: #selector(changePassClicked), for: .touchUpInside)
        changePassApplicationView?.textField1.addTarget(self, action: #selector(textfieldChanged1), for: .editingChanged)
        changePassApplicationView?.textField2.addTarget(self, action: #selector(textfieldChanged2), for: .editingChanged)
        changePassApplicationView?.textField3.addTarget(self, action: #selector(textfieldChanged3), for: .editingChanged)
    }
    
    func initVar() {
        vehicleInformations.removeAll()
        
        if numberOfVehcle == 1 {
            lblContentAlertForBidden.text = String(format: txtContent_AlertForBidden[languageChooseSave], vehicle2show.regNo)
        }
        numberOfVehcle = 0
        pageIndex = 1
        searchVehicleFlag = false
        
        if vehicle2show == nil {
            return
        }
        
        let searchToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
        searchToolbar.barStyle = .default
        let cancelBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        cancelBtn.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .semibold)], for: .normal)
        searchToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), cancelBtn,
        ]
        searchToolbar.sizeToFit()
        searchBarListVehicle.inputAccessoryView = searchToolbar
    }
    
    func initLanguageListVehicleVC() {
        lblTitleListVehicle.text = listVehicleVC_Title[languageChooseSave].uppercased()
    }
    
    // MARK: - ObjectiveC function
    
    @objc func doneClick() {
        searchBarListVehicle.resignFirstResponder()
        if checkInternet() != false {
            if textSearchVehicle == "" {
                listVehicleTableView.isHidden = false
            }
        }
    }
    
    @objc func didTapImage(gesture: UIGestureRecognizer) {
        urlSessionDelegate()
    }
    
    // MARK: - Log out
    
    @IBAction func btnLogoutTapped(_ sender: AnyObject) {
        displayWarningLogoutView()
    }
    
    @IBAction func btnConfirmLogoutTapped(_ sender: Any) {
        removeWarningLogoutView()
        logout()
    }
    
    @IBAction func btnCancelLogoutTapped(_ sender: Any) {
        removeWarningLogoutView()
    }
    
    // MARK: - Change Password Check TextField
    
    @objc func textfieldChanged1() {
        if (changePassApplicationView?.textField1.text!.count)! < 1 {
            changePassApplicationView?.btnClear1.isHidden = true
            changePassApplicationView?.btnSecure1.isHidden = true
        }
    }
    
    @objc func textfieldChanged2() {
        if (changePassApplicationView?.textField2.text!.count)! < 1 {
            changePassApplicationView?.btnClear2.isHidden = true
            changePassApplicationView?.btnSecure2.isHidden = true
        }
    }
    
    @objc func textfieldChanged3() {
        if (changePassApplicationView?.textField3.text!.count)! < 1 {
            changePassApplicationView?.btnClear3.isHidden = true
            changePassApplicationView?.btnSecure3.isHidden = true
        }
    }
    
    @objc func changePassClicked() {
        view.endEditing(true)
        checkTextFieldChangePass()
    }
    
    func checkTextFieldChangePass() {
        if changePassApplicationView!.textField1.text == "" {
            changePassApplicationView!.lbCurrentPassError.isHidden = false
            changePassApplicationView!.lbNewPassError.isHidden = true
            changePassApplicationView!.lbReNewPassError.isHidden = true
            changePassApplicationView!.textField1.becomeFirstResponder()
            return
        } else {
            changePassApplicationView!.lbCurrentPassError.isHidden = true
        }
        if changePassApplicationView!.textField2.text == "" {
            changePassApplicationView?.lbNewPassError.text = "Vui lòng nhập mật khẩu mới"
            changePassApplicationView!.lbNewPassError.isHidden = false
            changePassApplicationView!.lbReNewPassError.isHidden = true
            changePassApplicationView!.textField2.becomeFirstResponder()
            return
        } else {
            changePassApplicationView!.lbNewPassError.isHidden = true
        }
        if changePassApplicationView!.textField3.text == "" {
            changePassApplicationView?.lbReNewPassError.text = "Vui lòng nhập xác nhận mật khẩu mới"
            changePassApplicationView!.lbReNewPassError.isHidden = false
            changePassApplicationView!.textField3.becomeFirstResponder()
            return
        } else {
            changePassApplicationView!.lbReNewPassError.isHidden = true
        }
        
        if (changePassApplicationView?.textField2.text!.count)! < 8 {
            changePassApplicationView?.lbNewPassError.text = "Mật khẩu mới phải chứa ít nhất 8 ký tự"
            changePassApplicationView!.lbNewPassError.isHidden = false
            changePassApplicationView!.lbReNewPassError.isHidden = true
            changePassApplicationView!.textField2.becomeFirstResponder()
            return
        } else {
            changePassApplicationView!.lbNewPassError.isHidden = true
        }
        
        if changePassApplicationView?.textField2.text != changePassApplicationView?.textField3.text {
            changePassApplicationView?.lbReNewPassError.isHidden = false
            changePassApplicationView?.lbReNewPassError.text = "Xác nhận mật khẩu mới không đúng"
            return
        } else {
            changePassApplicationView!.lbReNewPassError.isHidden = true
        }
        
//        let whiteSpace = " "
//        if let hasWhiteSpace = changePassApplicationView?.textField1.text?.range(of: whiteSpace) {
//            print ("has whitespace \(hasWhiteSpace)")
//            changePassApplicationView?.lbNewPassError.text = "Mật khẩu chứa khoảng trắng"
//            changePassApplicationView!.lbNewPassError.isHidden = false
//            changePassApplicationView!.lbReNewPassError.isHidden = true
//            changePassApplicationView!.textField2.becomeFirstResponder()
//            return
//        } else {
//            print("no whitespace")
//            changePassApplicationView!.lbNewPassError.isHidden = true
//        }
        
        changeAppPassword()
    }
    
    // MARK: - Alert to retry load list vehicle
    
    func retryLoadListVehicle() {
        displayWarningLoadListVehicleView()
    }
    
    @IBAction func btnRetryLoadListVehicleTapped(_ sender: Any) {
        removeWarningLoadListVehicleView()
        loadListVehicle()
    }
    
    @IBAction func btnCancelRetryLoadListVehicleTapped(_ sender: Any) {
        removeWarningLoadListVehicleView()
        gotoLoginView()
    }
    
    //
    
    // MARK: - Go to other VC
    
    func gotoLoginView() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "login") as! LoginVC
        logoutFlag = true
        nextViewController.modalPresentationStyle = .fullScreen
        present(nextViewController, animated: true, completion: nil)
    }
    
    func gotoMapView() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "map") as! MapVC
        // nextViewController.modalTransitionStyle = .crossDissolve
        registerNo2Query = vehicle2show.regNo
        vtMapDisplayFlag = true
        nextViewController.BKS = vehicle2show.regNo
        nextViewController.modalTransitionStyle = .coverVertical
        nextViewController.modalPresentationStyle = .fullScreen
        present(nextViewController, animated: true, completion: nil)
    }
    
    //
    
    // MARK: - Keyboard
    
    func hideKeyBoad() {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
    }
    
    // MARK: - SOS VIEW
    
    @IBOutlet var sosView: UIView!
    func displaySOSView() {
        sosView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(sosView)
        initSOSView()
    }
    
    func removeSOSView() {
        sosView.removeFromSuperview()
    }
    
    @IBAction func closeSOSView(_ sender: Any) {
        sosView.removeFromSuperview()
    }
    
    func initSOSView() {
        lblTitleSOS.text = txtTitleSOS[languageChooseSave].uppercased()
        //
        let customType = ActiveType.custom(pattern: "\\s18008098\\b")
        lblHotline.enabledTypes.append(customType)
        lblHotline.urlMaximumLength = 31
        lblHotline.customize { _ in
            lblHotline.text = txtContentSOS[languageChooseSave]
            
            lblHotline.textColor = UIColor(red: 20.0 / 255, green: 71.0 / 255, blue: 110.0 / 255, alpha: 1)
            lblHotline.numberOfLines = 0
            lblHotline.lineSpacing = 4
            lblHotline.lineBreakMode = NSLineBreakMode.byWordWrapping
            lblHotline.customColor[customType] = UIColor.red
            lblHotline.customSelectedColor[customType] = UIColor.blue
            lblHotline.handleCustomTap(for: customType) { _ in self.confirmCallToHotline() }
        }
        lblHotline.frame = CGRect(x: 0, y: 0, width: hotlineView.frame.size.width, height: 270)
        lblHotline.textAlignment = NSTextAlignment.justified
        
        lblHotline.font = lblHotline.font.withSize(17.5)
        hotlineView.addSubview(lblHotline)
        btnCancelSOS.setTitle(txtButtonCancelSOS[languageChooseSave], for: .normal)
    }
    
    func call2ToHotline() {
        let phoneUrl = "tel://\(18008098)"
        let url: URL = URL(string: phoneUrl)!
        UIApplication.shared.openURL(url)
    }
    
    @IBAction func btnConfirmCallHotlineTapped(_ sender: Any) {
        removeSOSView()
        removeWarningHotlineView()
        call2ToHotline()
    }
    
    @IBAction func btnCancelCallHotlineTapped(_ sender: Any) {
        removeSOSView()
        removeWarningHotlineView()
    }
    
    func confirmCallToHotline() {
        removeSOSView()
        displayWarningHotlineView()
    }
    
    @IBAction func btnSOSTapped(_ sender: Any) {
        if idleFlag == false {
            return
        }
        idleFlag = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            idleFlag = true
        }
        displaySOSView()
    }
    
    @IBAction func btnCancelSOSTapped(_ sender: Any) {
        removeSOSView()
    }
    
    //
    
    // MARK: - Warning Hotline view
    
    @IBOutlet var warningHotlineView: UIView!
    func displayWarningHotlineView() {
        warningHotlineView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(warningHotlineView)
        lblTitleConfirmCallHotline.text = txtTitleConfirmCallHotline[languageChooseSave].uppercased()
        lblContentConfirmCallHotline.text = txtContentConfirmCallHotline[languageChooseSave]
        btnConfirmCallHotline.setTitle(txtButtonConfirmCallHotline[languageChooseSave], for: .normal)
        btnCancelConfirmCallHotline.setTitle(txtButtonCancelConfirmCallHotline[languageChooseSave], for: .normal)
    }
    
    func removeWarningHotlineView() {
        warningHotlineView.removeFromSuperview()
    }
    
    @IBAction func closeWarningHotlineView(_ sender: Any) {
        warningHotlineView.removeFromSuperview()
    }
    
    //
    
    // MARK: - Warning log out
    
    @IBOutlet var warningLogoutView: UIView!
    func displayWarningLogoutView() {
        warningLogoutView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(warningLogoutView)
//        lblTitleConfirmLogout.text = txtTitleConfirmLogout[languageChooseSave].uppercased()
//        lblContentConfirmLogout.text = txtContentConfirmLogout[languageChooseSave]
//        btnOKConfirmLogout.setTitle(txtButtonOKConfirmLogout[languageChooseSave], for: .normal)
//        btnCancelConfirmLogout.setTitle(txtButtonCancelConfirmLogout[languageChooseSave], for: .normal)
        
        lblTitleConfirmLogout.text = "ĐĂNG XUẤT"
        btnCancelConfirmLogout.setTitle("Hủy bỏ", for: .normal)
        btnOKConfirmLogout.setTitle("Đăng xuất", for: .normal)
    }
    
    func removeWarningLogoutView() {
        warningLogoutView.removeFromSuperview()
    }
    
    @IBAction func closeWarningLogoutView(_ sender: Any) {
        warningLogoutView.removeFromSuperview()
    }
    
    //
    
    // MARK: - Warning loading list vehicle
    
    @IBOutlet var warningLoadListVehicleView: UIView!
    func displayWarningLoadListVehicleView() {
        warningLoadListVehicleView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(warningLoadListVehicleView)
    }
    
    func removeWarningLoadListVehicleView() {
        warningLoadListVehicleView.removeFromSuperview()
    }
    
    @IBAction func closeWarningLoadListVehicleView(_ sender: Any) {
        warningLoadListVehicleView.removeFromSuperview()
    }
    
    //
    
    // MARK: - ForBidden View
    
    @IBOutlet var isForbiddenAlertView: UIView!
    func displayIsForbiddenAlertView() {
        isForbiddenAlertView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(isForbiddenAlertView)
        lblTitleAlertForBidden.text = txtTitleAlertForBidden[languageChooseSave].uppercased()
        btnServicePaymentAlertForBidden.setTitle(txtButtonServicePaymentAlertForBidden[languageChooseSave], for: .normal)
        btnCancelAlertForBidden.setTitle(txtButtonCancelAlertForBidden[languageChooseSave], for: .normal)
    }
    
    func removeIsForbiddenAlertView() {
        isForbiddenAlertView.removeFromSuperview()
    }
    
    @IBAction func closeIsForbiddenAlertView(_ sender: Any) {
        isForbiddenAlertView.removeFromSuperview()
    }
    
    @IBAction func btnIsForbiddenChargeMoneyTapped(_ sender: Any) {
        removeIsForbiddenAlertView()
        displayListChargeMoneyView()
    }
    
    @IBAction func btnIsForbiddenAlertCancelTapped(_ sender: Any) {
        removeIsForbiddenAlertView()
    }
    
    // MARK: - Alert custom
    
    func alertCustom(message: String, delay: Double) {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        AlertCustom().Alert(delegate: self, message: message, textAlignment: "", delay: delay)
    }
    
    @IBOutlet var listChargeMoneyView: UIView!
    func displayListChargeMoneyView() {
        listChargeMoneyView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(listChargeMoneyView)
        lblTitleListChargeMoney.text = titleListChargeMoney[languageChooseSave].uppercased()
        btnCloseListChargeMoneyType.setTitle(txtButtonCloseListChargeMoney[languageChooseSave], for: .normal)
    }
    
    func removeListChargeMoneyView() {
        listChargeMoneyView.removeFromSuperview()
    }
    
    @IBAction func closeListChargeMoneyView(_ sender: Any) {
        listChargeMoneyView.removeFromSuperview()
    }
    
    @IBAction func btnCloseListChargeMoneyTypeTapped(_ sender: Any) {
        removeListChargeMoneyView()
    }
    
    @IBAction func chargeMoneyTapped(_ sender: Any) {
        if idleFlag == false {
            return
        }
        idleFlag = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            idleFlag = true
        }
        displayListChargeMoneyView()
    }
    
    // MARK: - Charge Card Code View
    
    @IBOutlet var chargeCardCodeViewHeightContraint: NSLayoutConstraint!
    @IBOutlet var chargeCardCodeView: UIScrollView!
    @IBOutlet var chargeCardCodeContainerView: UIView!
    func displayChargeCardCodeContainerView() {
        txtChargeCode.becomeFirstResponder()
        chargeCardCodeContainerView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(chargeCardCodeContainerView)
        lblTitleChargeCardCode.text = txtTitleChargeCardCode[languageChooseSave].uppercased()
        lblContent02ChargeCardCode.text = txtContent02ChargeCardCode[languageChooseSave]
        lblContent03ChargeCardCode.text = txtContent03ChargeCardCode[languageChooseSave]
        lblContent04ChargeCardCode.text = txtContent04ChargeCardCode[languageChooseSave]
        lblContent05ChargeCardCode.text = txtContent05ChargeCardCode[languageChooseSave]
        btnConfirmChargeCardCode.setTitle(txtButtonConfirmChargeCardCode[languageChooseSave], for: .normal)
        btnCancelChargeCardcode.setTitle(txtButtonCancelChargeCardCode[languageChooseSave], for: .normal)
    }
    
    func removeChargeCardCodeContainerView() {
        chargeCardCodeContainerView.removeFromSuperview()
    }
    
    @IBAction func closeChargeCardCodeContainerView(_ sender: Any) {
        chargeCardCodeContainerView.removeFromSuperview()
    }
    
    // MARK: - Confirm Charge Card Code
    
    @IBOutlet var confirmChargeCardCodeView: UIView!
    func displayConfirmChargeCardCodeView() {
        confirmChargeCardCodeView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(confirmChargeCardCodeView)
        lblTitleConfirmChargeCardCode.text = txtTitleConfirmChargeCardCode[languageChooseSave].uppercased()
        lblContentConfirmChargeCardCode.text = String(format: txtContentConfirmChargeCardCode[languageChooseSave], vehicle2show.sim)
        btnOKConfirmChargeCardCode.setTitle(txtButtonOKConfirmChargeCardCode[languageChooseSave], for: .normal)
        btnCancelConfirmChargeCardCode.setTitle(txtButtonCancelConfirmChargeCardCode[languageChooseSave], for: .normal)
    }
    
    func removeConfirmChargeCardCodeView() {
        confirmChargeCardCodeView.removeFromSuperview()
    }
    
    @IBAction func closeConfirmChargeCardCodeView(_ sender: Any) {
        confirmChargeCardCodeView.removeFromSuperview()
    }
    
    //
    
    // MARK: - Pay money View
    
    @IBOutlet var payMoneyView: UIScrollView!
    @IBOutlet var payMoneyContainerView: UIView!
    func displayPayMoneyContainerView() {
        txtCaptchaPayMoney.becomeFirstResponder()
        payMoneyContainerView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(payMoneyContainerView)
        lblTitlePayMoney.text = txtTitlePayMoney[languageChooseSave].uppercased()
        lblContent02PayMoney.text = txtContent02PayMoney[languageChooseSave]
        lblContent03PayMoney.text = txtContent03PayMoney[languageChooseSave]
        btnConfirmPayMoney.setTitle(txtButtonOKPayMoney[languageChooseSave], for: .normal)
        btnCancelPayMoney.setTitle(txtButtonCancelPayMoney[languageChooseSave], for: .normal)
        btnChooseNumberOfMonth2Pay.setTitle("1 \(txtMonthPay[languageChooseSave])", for: .normal)
        txtCaptchaPayMoney.placeholder = txtContent04PayMoney[languageChooseSave]
    }
    
    func removePayMoneyContainerView() {
        payMoneyContainerView.removeFromSuperview()
    }
    
    @IBAction func closePayMoneyContainerView(_ sender: Any) {
        payMoneyContainerView.removeFromSuperview()
    }
    
    @IBAction func btnChooseNumberOfMonth2PayTapped(_ sender: Any) {
        displayChooseNumberOfMonthToPayView()
    }
    
    // MARK: - Choose number of month to pay
    
    @IBOutlet var chooseNumberOfMonthToPayView: UIView!
    func displayChooseNumberOfMonthToPayView() {
        chooseNumberOfMonthToPayView.frame = CGRect(x: btnChooseNumberOfMonth2Pay.frame.origin.x, y: btnChooseNumberOfMonth2Pay.frame.origin.y, width: 125, height: 200)
        payMoneyView.addSubview(chooseNumberOfMonthToPayView)
    }
    
    func removeChooseNumberOfMonthToPayView() {
        chooseNumberOfMonthToPayView.removeFromSuperview()
    }
    
    // MARK: - Confirm Pay Money
    
    @IBOutlet var confirmPayMoneyView: UIView!
    func displayConfirmPayMoneyView() {
        confirmPayMoneyView.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        view.addSubview(confirmPayMoneyView)
        lblTitleConfirmPayMoney.text = txtTitleConfirmPayMoney[languageChooseSave].uppercased()
        lblContentConfirmPayMoney.text = String(format: txtContentConfirmPayMoney[languageChooseSave], vehicle2show.sim)
        btnOKConfirmPayMoney.setTitle(txtButtonOKConfirmPayMoney[languageChooseSave], for: .normal)
        btnCancelConfirmPayMoney.setTitle(txtButtonCancelConfirmPayMoney[languageChooseSave], for: .normal)
    }
    
    func removeConfirmPayMoneyView() {
        confirmPayMoneyView.removeFromSuperview()
    }
    
    @IBAction func closeConfirmPayMoneyView(_ sender: Any) {
        confirmPayMoneyView.removeFromSuperview()
    }
}

extension ListVehicleVC: URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
    }
}
