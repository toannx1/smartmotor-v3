
//
//  ChargeMoney.swift
//  SmartMotor
//
//  Created by vietnv2 on 11/25/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

struct Number {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = "." // or possibly "." / ","
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension Int {
    var stringWithSepator: String {
        let espriceDecimal = NumberFormatter()
        espriceDecimal.numberStyle = .decimal
        espriceDecimal.groupingSeparator = "."
        let formatNumber = espriceDecimal.string(from: NSNumber(value: self))
        
        return formatNumber!
    }
}

extension ListVehicleVC: URLSessionDownloadDelegate {
    enum info2PayMoneyString: String {
        case data
        case deviceCode
        case deviceSim
        case monthlyFee
        case registerNo
        case requestedPhone
    }
    
    // MARK: - Charge Card Code
    
    func resultViewChargeCode(code: Int, money: String) {
        var string: String!
        var delay: Double! = 2
        switch code {
        case 1:
            string = String(format: txtResult01_ChargeCardCode[languageChooseSave], money, vehicle2show.sim)
            delay = 4
            self.showBannerNotifi(text: string, background: colorSuccess, textColor: .white, imageTitle: UIImage(named: "checked")!, des: "")
        case -8: string = txtResult02ChargeCardCode[languageChooseSave]
        case -9: string = txtResult03ChargeCardCode[languageChooseSave]
        case -1: string = txtResult04ChargeCardCode[languageChooseSave]
        case -2: string = txtResult05ChargeCardCode[languageChooseSave]
        case -3: string = txtResult06ChargeCardCode[languageChooseSave]
        case -6: string = txtResult07ChargeCardCode[languageChooseSave]
        default: string = txtResult08ChargeCardCode[languageChooseSave]
        }
        self.showBannerNotifi(text: string, background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
    }
    
    @IBAction func btnconfirmChargeCardCodeTapped(_ sender: Any) {
        hideKeyBoad()
        if txtChargeCode.text == "" {
            // alertCustom(message: txtAlertCardCode_ChargeCardCode[languageChooseSave], delay: 2)
            self.showBannerNotifi(text: txtAlertCardCode_ChargeCardCode[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
            self.txtChargeCode.becomeFirstResponder()
            return
        } else if txtCaptchaCardCode.text == "" {
            self.showBannerNotifi(text: txtEnterCaptcha[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
            // alertCustom(message: txtEnterCaptcha[languageChooseSave], delay: 2)
            self.txtCaptchaCardCode.becomeFirstResponder()
            return
        }
        removeChargeCardCodeContainerView()
        displayConfirmChargeCardCodeView()
    }
    
    @IBAction func btncancelChargeCardCodeTapped(_ sender: Any) {
        removeChargeCardCodeContainerView()
    }
    
    // MARK: Confirm Charge Card Code
    
    @IBAction func btnOKChargeCardCodeTapped(_ sender: Any) {
        removeConfirmChargeCardCodeView()
        self.chargeMoney()
    }
    
    @IBAction func btnNOChargeCardCodeTapped(_ sender: Any) {
        removeConfirmChargeCardCodeView()
    }
    
    // MARK: - Pay Money
    
    @IBAction func btnconfirmPayMoneyTapped(_ sender: Any) {
        hideKeyBoad()
        if txtCaptchaPayMoney.text == "" {
            self.showBannerNotifi(text: txtEnterCaptcha[languageChooseSave], background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
            // alertCustom(message: txtEnterCaptcha[languageChooseSave], delay: 2)
            self.txtCaptchaPayMoney.becomeFirstResponder()
            return
        }
        removePayMoneyContainerView()
        displayConfirmPayMoneyView()
    }
    
    @IBAction func btncancelPayMoneyTapped(_ sender: Any) {
        removePayMoneyContainerView()
    }
    
    // MARK: Confirm Pay Money
    
    @IBAction func btnOKPayMoneyTapped(_ sender: Any) {
        removeConfirmPayMoneyView()
        self.payMoney()
    }
    
    @IBAction func btnNOPayMoneyTapped(_ sender: Any) {
        removeConfirmPayMoneyView()
    }
    
    // MARK: - url encode
    
    func urlEncode(string: String) -> String {
        var rString: String! = ""
        for char in string {
            if char == " " {
                rString.append("+")
            } else if char == "." || char == "-" || char == "_" || char == "~" ||
                (char >= "a" && char <= "z") ||
                (char >= "A" && char <= "Z") ||
                (char >= "0" && char <= "9") {
                rString.append(String(char))
            } else {
                // rString.append(String(format: "%%2X", char.hashValue))
                rString.append(String(format: "0", char.hashValue))
            }
        }
        return rString
    }
    
    // MARK: - Charge Money API
    
    func chargeMoney() {
        Loading().showLoading()
        if tokenSave == nil || userIdSave == nil {
            gotoLoginView()
        }
        let cardCode = self.urlEncode(string: txtChargeCode.text!)
        let captcha = self.urlEncode(string: txtCaptchaCardCode.text!)
        var urlString = smartmotorUrl + chargingUrl
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        let test = String(urlString)
        let url = URL(string: test)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = postMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        request.addValue(cookieString, forHTTPHeaderField: "Cookie")
        request.addValue(tokenSave, forHTTPHeaderField: "user-token")
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let dataString: [String: Any] = ["userId": userIdSave, "transportId": vehicle2show.id, "phoneNumber": vehicle2show.sim, "cardCode": cardCode, "captcha": captcha, "os": "ios"] as Dictionary
        request.httpBody = try! JSONSerialization.data(withJSONObject: dataString, options: [])
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        let task = defaultSession.dataTask(with: request as URLRequest) { data, response, error in
            Loading().hideLoading(delay: 0)
            if error != nil {
                self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    self.showBannerNotifi(text: connectionFail[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                    // self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let resultCode = jsonResult["resultCode"] as! Int
                    var money: String! = ""
                    if let value = jsonResult["value"] as? String {
                        money = value
                    }
                    self.resultViewChargeCode(code: resultCode, money: money)
                    
                } catch {
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    // MARK: - Get IP Adress API
    
    func getIPAddress() {
        Loading().showLoading()
        if deviceIPAddress.count >= 7 {
            Loading().hideLoading(delay: 0)
            self.getInfo2PayMoney()
            return
        }
        let url = URL(string: ipAdressURL)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        let task = defaultSession.dataTask(with: request as URLRequest) { data, response, error in
            Loading().hideLoading(delay: 0)
            if error != nil {
                self.showBannerNotifi(text: connectionFail[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                // self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    self.showBannerNotifi(text: connectionFail[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                    // self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    deviceIPAddress = jsonResult["ip"] as! String
                    self.getInfo2PayMoney()
                } catch {
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    // MARK: - Get Info PayMoney API
    
    func getInfo2PayMoney() {
        Loading().showLoading()
        if tokenSave == nil || userIdSave == nil {
            gotoLoginView()
        }
        let urlString = smartmotorUrl + getInfoUrl + userIdSave + "/" + String(vehicle2show.id) + "/" + deviceIPAddress
        let url = URL(string: urlString)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = getMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        request.addValue(tokenSave, forHTTPHeaderField: "user-token")
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        let task = defaultSession.dataTask(with: request as URLRequest) { data, response, error in
            Loading().hideLoading(delay: 0)
            if error != nil {
                self.showBannerNotifi(text: connectionFail[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                // self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
                self.removePayMoneyContainerView()
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    self.showBannerNotifi(text: connectionFail[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                    // self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
                    self.removePayMoneyContainerView()
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let resultCode = jsonResult["resultCode"] as! Int
                    if resultCode != 1 {
                        self.showBannerNotifi(text: connectionFail[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                        // self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
                        return
                    }
                    if let jsonData = jsonResult[info2PayMoneyString.data.rawValue] as? NSDictionary {
                        self.deviceCode = jsonData[info2PayMoneyString.deviceCode.rawValue] as? String
                        self.deviceSim = jsonData[info2PayMoneyString.deviceSim.rawValue] as? String
                        self.monthlyFee = jsonData[info2PayMoneyString.monthlyFee.rawValue] as? Int
                        self.registerNo = jsonData[info2PayMoneyString.registerNo.rawValue] as? String
                        self.requestedPhone = jsonData[info2PayMoneyString.requestedPhone.rawValue] as? String
                        
                        self.lblContent01PayMoney.text = String(format: txtContent01_PayMoney[languageChooseSave], self.requestedPhone, self.deviceSim, self.deviceCode, self.registerNo)
                        //
                        self.monthPay = "1"
                        
                        self.moneyPay = String(self.monthlyFee)
                        if languageChooseSave == 0 {
                            self.txtMoneyPay.text = self.monthlyFee.stringWithSepator + " VNĐ"
                        } else {
                            self.txtMoneyPay.text = self.monthlyFee.stringWithSepator + " VND"
                        }
                        //
                        self.displayPayMoneyContainerView()
                        self.removeListChargeMoneyView()
                        self.urlSessionDelegate()
                    }
                } catch {
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    // MARK: - PayMoney API
    
    func payMoney() {
        Loading().showLoading()
        if tokenSave == nil || userIdSave == nil {
            gotoLoginView()
        }
        let captcha = self.urlEncode(string: txtCaptchaPayMoney.text!)
        var urlString = smartmotorUrl + payMoneyUrl
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        let url = URL(string: urlString)
        let request = NSMutableURLRequest(url: url!)
        request.httpMethod = postMethod
        request.addValue(applicationJsonString, forHTTPHeaderField: contentTypeString)
        request.addValue(cookieString, forHTTPHeaderField: "Cookie")
        request.addValue(tokenSave, forHTTPHeaderField: "user-token")
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let dataString: [String: Any] = ["userId": userIdSave, "transportId": vehicle2show.id, "chargingMoney": moneyPay, "month": monthPay, "captcha": captcha, "ip": deviceIPAddress] as Dictionary
        request.httpBody = try! JSONSerialization.data(withJSONObject: dataString, options: [])
        let defaultConfigObject = URLSessionConfiguration.default
        defaultConfigObject.timeoutIntervalForRequest = 30
        defaultConfigObject.timeoutIntervalForResource = 30
        let defaultSession = URLSession(configuration: defaultConfigObject, delegate: self, delegateQueue: OperationQueue.main)
        let task = defaultSession.dataTask(with: request as URLRequest) { data, response, error in
            Loading().hideLoading(delay: 0)
            if error != nil {
                self.showBannerNotifi(text: connectionFail[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                // self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    self.showBannerNotifi(text: connectionFail[languageChooseSave], background: colorError, textColor: .white, imageTitle: UIImage(named: "icon_not_find")!, des: "")
                    // self.alertCustom(message: connectionFail[languageChooseSave], delay: 2)
                    return
                }
                do {
                    let jsonResult = (try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let resultCode = jsonResult["resultCode"] as! Int
                    self.payMoneyResultDisplay(result: resultCode)
                } catch {
                    fatalError("Failure\(error)")
                }
            }
        }
        task.resume()
    }
    
    func payMoneyResultDisplay(result: Int) {
        var display: String!
        switch result {
        case 1:
            if vehicle2show.isForbidden == false {
                display = txtPaymentSuccess[languageChooseSave]
                break
            }
            display = txtWaitAmomentAfterPaymentSuccess[languageChooseSave]
            let info = forbiddenPaySuccess(regNo: vehicle2show.regNo, paySuccess: true)
            vehiclePayInformation.append(info)
            self.initTimerReloadListVehicle()
        case -1, -2, -4, -5, -6, -7, -9, -10, -11, -12, -14:
            display = String(format: txtPaymentUnSuccess01[languageChooseSave], String(result))
        case -3:
            display = String(format: txtPaymentUnSuccess02[languageChooseSave], String(result))
        case -8:
            display = String(format: txtPaymentUnSuccess03[languageChooseSave], String(result))
        case -13:
            display = String(format: txtPaymentUnSuccess04[languageChooseSave], String(result))
            
        default: break
        }
        // alertCustom(message: display, delay: 4)
        self.showBannerNotifi(text: display, background: colorWarning, textColor: .black, imageTitle: UIImage(named: "icon_not_find")!, des: "")
    }
    
    // MARK: - Reload TableView
    
    func initTimerReloadListVehicle() {
        if timerReloadListVehicle != nil {
            timerReloadListVehicle.invalidate()
            timerReloadListVehicle = nil
        }
        timerReloadListVehicle = Timer.scheduledTimer(timeInterval: 300, target: self, selector: #selector(self.stopTimerReloadListVehicle), userInfo: nil, repeats: true)
    }
    
    @objc func stopTimerReloadListVehicle() {
        if timerReloadListVehicle != nil {
            loadListVehicle()
            timerReloadListVehicle.invalidate()
            timerReloadListVehicle = nil
        }
    }
    
    // MARK: - URL session captcha
    
    func initURLSession() {
        let sessionConfig = URLSessionConfiguration.default
        session = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
    }
    
    func deleteCookies() {
        let cookieStore = HTTPCookieStorage.shared
        for cookie in cookieStore.cookies ?? [] {
            cookieStore.deleteCookie(cookie)
        }
    }
    
    func urlSessionDelegate() {
        var imageUrl: String!
        self.deleteCookies()
        imageUrl = smartmotorUrl + captchaUrl
        let task: URLSessionDownloadTask = session.downloadTask(with: NSURL(string: imageUrl as String)! as URL)
        task.resume()
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        let downloadedImage = UIImage(data: NSData(contentsOf: location)! as Data)
        DispatchQueue.main.async {
            if self.payMoneyContainerView != nil {
                self.imgCaptchaPayMoney.image = downloadedImage
            }
            if self.chargeCardCodeContainerView != nil {
                self.imgCaptchaChargeCode.image = downloadedImage
            }
        }
        cookieString = ""
        for cookie in (session.configuration.httpCookieStorage?.cookies)! {
            cookieString = cookieString + cookie.name + "=" + cookie.value + ";"
        }
        Loading().hideLoading(delay: 0)
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {}
}
