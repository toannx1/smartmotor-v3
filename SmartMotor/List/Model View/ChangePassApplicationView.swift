//
//  ChangePassApplicationView.swift
//  SmartMotor
//
//  Created by nguyen hoan on 8/28/19.
//  Copyright © 2019 EPR. All rights reserved.
//

import LBTATools
import UIKit

class ChangePassApplicationView: UIView {
    @IBOutlet var btnConfirm: UIButton!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnClear3: UIButton!
    @IBOutlet var btnClear2: UIButton!
    @IBOutlet var btnClear1: UIButton!
    @IBOutlet var btnSecure3: UIButton!
    @IBOutlet var btnSecure2: UIButton!
    @IBOutlet var btnSecure1: UIButton!
    @IBOutlet var lbCurrentPassError: UILabel!
    @IBOutlet var lbNewPassError: UILabel!
    @IBOutlet var lbReNewPassError: UILabel!
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet var textField1: UITextField!
    @IBOutlet var textField2: UITextField!
    @IBOutlet var textField3: UITextField!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    func commonInit() {
        Bundle.main.loadNibNamed("ChangePassApplicationView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        textField1.keyboardDistanceFromTextField = 160
        textField2.keyboardDistanceFromTextField = 160
        textField3.keyboardDistanceFromTextField = 80
        
        textField1.paddingRight(padding: 45)
        textField2.paddingRight(padding: 45)
        textField3.paddingRight(padding: 45)
        
        btnConfirm.add(Border: .top, withColor: .lightGray, andHeight: 1)
        btnConfirm.add(Border: .right, withColor: .lightGray, andHeight: 1)
        btnCancel.add(Border: .top, withColor: .lightGray, andHeight: 1)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    // MARK: - Secure and clear textfield
    
    @IBAction func secureTextField(_ sender: UIButton) {
        if sender == btnSecure1 {
            if textField1.isSecureTextEntry == false {
                btnSecure1.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
            } else {
                btnSecure1.setImage(UIImage(named: "invisible_eye_dark"), for: .normal)
            }
            textField1.isSecureTextEntry = !textField1.isSecureTextEntry
        } else if sender == btnSecure2 {
            if textField2.isSecureTextEntry == false {
                btnSecure2.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
            } else {
                btnSecure2.setImage(UIImage(named: "invisible_eye_dark"), for: .normal)
            }
            textField2.isSecureTextEntry = !textField2.isSecureTextEntry
        } else {
            if textField3.isSecureTextEntry == false {
                btnSecure3.setImage(UIImage(named: "visible_eye_dark"), for: .normal)
            } else {
                btnSecure3.setImage(UIImage(named: "invisible_eye_dark"), for: .normal)
            }
            textField3.isSecureTextEntry = !textField3.isSecureTextEntry
        }
    }
    
    @IBAction func clearTextField(_ sender: UIButton) {
        if sender == btnClear1 {
            textField1.text = ""
            btnSecure1.isHidden = true
            btnClear1.isHidden = true
            lbCurrentPassError.isHidden = false
        } else if sender == btnClear2 {
            textField2.text = ""
            btnSecure2.isHidden = true
            btnClear2.isHidden = true
            lbNewPassError.isHidden = false
        } else {
            textField3.text = ""
            btnSecure3.isHidden = true
            btnClear3.isHidden = true
            lbReNewPassError.isHidden = false
        }
    }
}
