//
//  CustomViewList.swift
//  SmartMotor
//
//  Created by nguyen hoan on 9/12/19.
//  Copyright © 2019 EPR. All rights reserved.
//

import LBTATools
import UIKit

class FeedbackView: UIView {
    let btnMess = UIButton()
    let btnEmail = UIButton()
    let btnGmail = UIButton()
    let btnCancel = UIButton(title: "Huỷ bỏ", titleColor: .darkGray, font: .systemFont(ofSize: 16, weight: .semibold), backgroundColor: .white)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        let lbTitle = UILabel(text: "Ý kiến phản hồi về ứng dụng \nSmart Motor Ver 1.1.1.1", font: UIFont.systemFont(ofSize: 15, weight: .medium), textColor: #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1), textAlignment: .center, numberOfLines: 0)
        let lbBtn1 = UILabel(text: "Tin nhắn", font: UIFont.systemFont(ofSize: 13), textColor: .darkGray, textAlignment: .center, numberOfLines: 0)
        let lbBtn2 = UILabel(text: "Email", font: UIFont.systemFont(ofSize: 13), textColor: .darkGray, textAlignment: .center, numberOfLines: 0)
        let lbBtn3 = UILabel(text: "Gmail", font: UIFont.systemFont(ofSize: 13), textColor: .darkGray, textAlignment: .center, numberOfLines: 0)
        
        btnCancel.add(Border: .top, withColor: .lightGray, andHeight: 1.0)
        
        btnMess.setImage(UIImage(named: "btn_mess"), for: .normal)
        btnEmail.setImage(UIImage(named: "btn_email"), for: .normal)
        btnGmail.setImage(UIImage(named: "btn_gg"), for: .normal)
        
        btnMess.imageView?.contentMode = .scaleAspectFit
        btnEmail.imageView?.contentMode = .scaleAspectFit
        btnGmail.imageView?.contentMode = .scaleAspectFit
        
        stack(lbTitle.withHeight(60),
              hstack(
                  stack(btnMess.withSize(.init(width: 40, height: 40)), lbBtn1.withHeight(20), spacing: 0, alignment: .center),
                  stack(btnEmail.withSize(.init(width: 40, height: 40)), lbBtn2.withHeight(20), spacing: 0, alignment: .center),
                  stack(btnGmail.withSize(.init(width: 40, height: 40)), lbBtn3.withHeight(20), spacing: 0, alignment: .center),
                  spacing: 0, distribution: .fillEqually),
              UIView().withHeight(16),
              btnCancel.withHeight(50), UIView().withHeight(50), spacing: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
