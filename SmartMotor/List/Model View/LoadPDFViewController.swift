//
//  LoadPDFViewController.swift
//  SmartMotor
//
//  Created by Hoang Linh on 9/19/19.
//  Copyright © 2019 EPR. All rights reserved.
//

import PDFReader
import UIKit
import WebKit

class LoadPDFViewController: UIViewController {
    let imgHeader = UIImageView()
    let lbTitle = UILabel()
    let btBack = UIButton()
    let webView = WKWebView()
    var titleString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        loadPdf()
    }
    
    func loadPdf() {
//        let path = Bundle.main.path(forResource: "apple", ofType: "pdf")
//        let url = URL(fileURLWithPath: path!)
//        let request = URLRequest(url: url)
//        webView.load(request)
    }
    
    func setupViews() {
        title = titleString
        view.backgroundColor = .white
        
        let img = UIImage(named: "backgroundTitle")
        imgHeader.image = img
        imgHeader.contentMode = .scaleToFill
        view.addSubview(imgHeader)
        imgHeader.translatesAutoresizingMaskIntoConstraints = false
        imgHeader.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        imgHeader.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        imgHeader.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        imgHeader.heightAnchor.constraint(equalToConstant: 64).isActive = true
        
        view.addSubview(lbTitle)
        lbTitle.translatesAutoresizingMaskIntoConstraints = false
        lbTitle.text = titleString.uppercased()
        lbTitle.font = UIFont.systemFont(ofSize: 17, weight: .heavy)
        lbTitle.adjustsFontSizeToFitWidth = true
        lbTitle.minimumScaleFactor = 0.5
        lbTitle.textColor = .white
        lbTitle.centerXAnchor.constraint(equalTo: imgHeader.centerXAnchor, constant: 0).isActive = true
        lbTitle.bottomAnchor.constraint(equalTo: imgHeader.bottomAnchor, constant: -8).isActive = true
        lbTitle.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 55).isActive = true
        lbTitle.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -55).isActive = true
        lbTitle.textAlignment = .center
        
        view.addSubview(btBack)
        btBack.translatesAutoresizingMaskIntoConstraints = false
        btBack.addTarget(self, action: #selector(backClick), for: .touchUpInside)
        btBack.setImage(UIImage(named: "icBackX"), for: .normal)
        btBack.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        // btBack.setTitle("X", for: .normal)
        btBack.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        btBack.centerYAnchor.constraint(equalTo: lbTitle.centerYAnchor, constant: 0).isActive = true
        btBack.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8)
        btBack.widthAnchor.constraint(equalToConstant: 44).isActive = true
        btBack.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        webView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        webView.topAnchor.constraint(equalTo: imgHeader.bottomAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    @objc func backClick() {
        dismiss(animated: true, completion: nil)
    }
}
