//
//  VehicleInformation.swift
//  Smart Motor
//
//  Created by vietnv2 on 18/10/16.
//  Copyright © 2016 vietnv2. All rights reserved.
//

import UIKit

class vehicleInformation {
    var accIllegalState: Int!
    var devicePin: String!
    var deviceType: Int!
    var deviceTypeIdOrginal: Int!
    var factory: String!
    var gpsDate: String!
    var gpsSpeed: Int!
    var gpsState: Int!
    var groupsCode: String!
    var id: Int!
    var illegalMoveState: Int!
    var lat: Double!
    var lng: Double!
    var lowBatteryState: Int!
    var motoSpeed: Int!
    var offPowerState: Int!
    var regNo: String!
    var sim: String!
    var sosState: Int!
    var state: Int!
    var timeState: Int!
    var type: String!
    var vibrationState: Int!
    var isForbidden: Bool!
    init(accIllegalState: Int, devicePin: String, deviceType: Int, deviceTypeIdOrginal: Int, factory: String, gpsDate: String, gpsSpeed: Int, gpsState: Int, groupsCode: String, id: Int, illegalMoveState: Int, lat: Double, lng: Double, lowBatteryState: Int, motoSpeed: Int, offPowerState: Int, regNo: String, sim: String, sosState: Int, state: Int, timeState: Int, type: String, vibrationState: Int, isForbidden: Bool) {
        self.accIllegalState = accIllegalState
        self.devicePin = devicePin
        self.deviceType = deviceType
        self.deviceTypeIdOrginal = deviceTypeIdOrginal
        self.factory = factory
        self.gpsDate = gpsDate
        self.gpsSpeed = gpsSpeed
        self.gpsState = gpsState
        self.groupsCode = groupsCode
        self.id = id
        self.illegalMoveState = illegalMoveState
        self.lat = lat
        self.lng = lng
        self.lowBatteryState = lowBatteryState
        self.motoSpeed = motoSpeed
        self.offPowerState = offPowerState
        self.regNo = regNo
        self.sim = sim
        self.sosState = sosState
        self.state = state
        self.timeState = timeState
        self.type = type
        self.vibrationState = vibrationState
        self.isForbidden = isForbidden
    }
}

class forbiddenPaySuccess {
    var regNo: String!
    var paySuccess: Bool!

    init(regNo: String, paySuccess: Bool) {
        self.regNo = regNo
        self.paySuccess = paySuccess
    }
}

var vehicleInformations: [vehicleInformation]! = []
var vehicle2show: vehicleInformation!
var vehiclePayInformation: [forbiddenPaySuccess]! = []
var numberOfVehcle: Int! = 0
var vehicle2showIndex: Int!
var registerNo2Query: String! = ""
var pageIndex: Int!
var txtSearchVehicleTextField: String! = ""
var textRegNoForbbiden: String! = ""
