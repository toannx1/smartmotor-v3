//
//  RegisterModel.swift
//  SmartMotor
//
//  Created by Toan on 5/31/21.
//  Copyright © 2021 vietnv2. All rights reserved.
//

import UIKit
import SwiftyJSON
class OtpModel: NSObject {
    var groupRoot : String
    var groupType : String
    var lat : String
    var lng : String
    var loginAttemp : String
    var roleId : String
    var userId : String
    var mess : String
    init(json : JSON) {
        self.groupRoot = json["groupRoot"].stringValue
        self.groupType = json["groupType"].stringValue
        self.lat = json["lat"].stringValue
        self.lng = json["lng"].stringValue
        self.loginAttemp = json["loginAttemp"].stringValue
        self.roleId = json["roleId"].stringValue
        self.userId = json["userId"].stringValue
        self.mess = json["message"].stringValue
    }
}
