//
//  PopUpBaseViewController.swift
//  SmartMotor
//
//  Created by Toan on 6/2/21.
//  Copyright © 2021 vietnv2. All rights reserved.
//

import UIKit
protocol PopUpDelegate {
    func getSucess( _ completion : (() -> Void))
}
class PopUpBaseViewController: UIViewController {
     @IBOutlet weak var titleLb: UILabel!
       @IBOutlet weak var desLb: UILabel!
       @IBOutlet weak var btnSucess: UIButton!
       let delegate : PopUpDelegate! = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapDissmiss = UITapGestureRecognizer(target: self, action: #selector(self.dismissView))
       self.view.addGestureRecognizer(tapDissmiss)
    }
    
    @objc func dismissView(){
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func onSucess(_ sender: Any) {
        delegate.getSucess {
            self.dismissView()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
