//
//  RegisterViewModel.swift
//  SmartMotor
//
//  Created by Toan on 6/2/21.
//  Copyright © 2021 vietnv2. All rights reserved.
//

import UIKit

class RegisterViewModel: NSObject {
    var nameUser : String
    var passUser : String
    var rePass : String
    var email : String
    var phone : String
    var gender : String
    init(name : String , password : String , email : String ,rePass : String ,phone : String , gender : String) {
        self.nameUser = name
        self.passUser = password
        self.email = email
        self.phone = phone
        self.gender = gender
        self.rePass = rePass
    }
}
