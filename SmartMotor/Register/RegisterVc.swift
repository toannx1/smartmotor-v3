//
//  RegisterVc.swift
//  SmartMotor
//
//  Created by Toan on 5/31/21.
//  Copyright © 2021 vietnv2. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Toast
class RegisterVc: UIViewController {
    @IBOutlet weak var contanner: UIView!
    @IBOutlet weak var tfNameUser: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfRePassWord: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var lbGender: UILabel!
    @IBOutlet weak var tfOtp: UITextField!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var privacyLb: UILabel!
    let genderArr = ["male","female","another"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.contanner.backgroundColor = UIColor.init(hexString: "2ba7f0")
     
        self.setUpView()
    }
    func setUpView(){
        self.titleLb.text = "Register"
        self.tfNameUser.placeholder = "nhập tên đăng nhập"
        self.tfPassword.placeholder = "nhập mật khẩu"
        self.tfRePassWord.placeholder = "nhập lại mật khẩu"
        self.tfPhoneNumber.placeholder = "nhập số điện thoại"
        self.tfEmail.placeholder = "nhập email"
        self.tfOtp.placeholder = "nhập mã xác nhận"
        self.lbGender.text = "Hãy chọn giớ tính"
        self.privacyLb.text = "Tôi xác nhận đồng ý với các điền khoản của smartMotor"
        self.privacyLb.numberOfLines = 0
    }
    @IBAction func onDismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func onAccept(_ sender: Any) {
    }
    @IBAction func onSelectSex(_ sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Choose gender", rows: genderArr, initialSelection: 1, doneBlock: { (picker, index, value) in
            self.lbGender.text = self.genderArr[index]
        }, cancel: { (picker) in
            
        }, origin: sender)
    }
    @IBAction func onCancel(_ sender: Any) {
    }
    @IBAction func onRegister(_ sender: Any) {
        guard let nameUser = tfNameUser.text else {
            return
        }
        guard let password = tfPassword.text else {
            return
        }
        guard let rePass = tfRePassWord.text else {
            return
        }
        guard let email = tfEmail.text else {
            return
        }
        guard let gender = lbGender.text else {
            return
        }
        guard let phone = tfPhoneNumber.text else {
            return
        }
       
        let model = RegisterViewModel(name: nameUser, password: password, email: email, rePass: rePass, phone: phone , gender: gender)
        let vc = OtpView(model: model)
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false, completion: nil)
        
        
    }
}
