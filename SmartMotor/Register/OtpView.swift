//
//  OtpView.swift
//  SmartMotor
//
//  Created by Toan on 6/2/21.
//  Copyright © 2021 vietnv2. All rights reserved.
//

import UIKit
import Toast
class OtpView: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var lbNumberFirst: UILabel!
    @IBOutlet weak var lbNumberSecond: UILabel!
    @IBOutlet weak var lbNumberthird: UILabel!
    @IBOutlet weak var lbNumberFour: UILabel!
    @IBOutlet weak var lbNumberFive: UILabel!
    @IBOutlet weak var tfNoBorder: UITextField!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var lbNumberSix: UILabel!
    var maxIndexHasString  : Int!
    var currentEditLb : UILabel?
    var arrLb : [UILabel]!
    var  model : RegisterViewModel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    init(model  : RegisterViewModel) {
        self.model = model
        super.init(nibName: "OtpView" , bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        arrLb = [lbNumberFirst,lbNumberSecond,lbNumberthird,lbNumberFour,lbNumberFive,lbNumberSix]
        self.tfNoBorder.delegate = self
        tfNoBorder.keyboardType = .phonePad
        self.tfNoBorder.tintColor = .clear
        self.tfNoBorder.textColor = .clear
        self.maxIndexHasString = -1;
        self.setUpLabel()
        //        let tapDissmiss = UITapGestureRecognizer(target: self, action: #selector(tapGesture(gesture:)))
        //        self.view.addGestureRecognizer(tapDissmiss)
        self.cancelBtn.setTitle("Cancel", for: .normal)
        self.doneBtn.setTitle("Register", for: .normal)
        self.doneBtn.isEnabled = false
        ApiProvider.share.SendOtp(phoneNumber: "0989380668") { (_ resurt : Result<OtpModel>) in
            
            switch resurt {
            case .sucess(let model):
                self.view.makeToast(model.mess, duration: TimeInterval(2.0), position: self.view.center)
                break
            case .faiure(let err as NSError) :
                break
            }
        }
    }
    @objc func tapGesture(gesture : UITapGestureRecognizer ){
        self.dismiss(animated: false, completion: nil)
    }
    func setUpLabel(){
        for label in self.arrLb
        {
            label.text = ""
            label.layer.cornerRadius = 6
            label.layer.borderColor = UIColor.init(hexString: "D3D3D3").cgColor
            label.layer.borderWidth = 1
            label.textColor = .darkGray
            self.resetStatusLabel(label, status: 0)
        }
    }
    func resetStatusLabel (_ label : UILabel , status : Int ) {
        if status == 0 {
            label.layer.borderColor = UIColor.init(hexString: "D3D3D3").cgColor
        } else if status == 1 {
            label.layer.borderColor = UIColor.init(hexString: "1E9AC0").cgColor
        }else if status == 2 {
            label.layer.borderColor = UIColor.init(hexString: "1E9AC0").cgColor
        }
    }
    @IBAction func tfDidBeginEdit(_ sender: Any) {
        if self.maxIndexHasString + 1 < self.arrLb.count {
            let index = self.maxIndexHasString + 1
            let nextlabel = self.arrLb[index]
            self.currentEditLb = nextlabel
            self.resetStatusLabel(nextlabel, status: 1)
        }
    }
    @IBAction func endEditAct(_ sender: Any) {
        self.currentEditLb?.text = ""
        if  self.tfNoBorder.text != nil  && self.tfNoBorder.text!.count > 0 {
            self.currentEditLb = nil
            self.maxIndexHasString = -1
        }
        for i in 0...self.arrLb.count - 1 {
            let label = self.arrLb[i]
            if label.text != nil || label.text!.count > 0 {
                self.resetStatusLabel(label, status: 2)
            }else{
                resetStatusLabel(label, status: 0)
            }
        }
    }
    
    
    
    @IBAction func onCancel(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onDone(_ sender: Any) {
        guard let otpCode = self.tfNoBorder.text else { return  }
        ApiProvider.share.RegisterAcount(name: model.nameUser, password: model.passUser, repeatPass: model.rePass, phoneNumber: model.phone, otp: otpCode, username: model.nameUser, email: model.email) { (_ resurlt) in
            switch resurlt {
            case .sucess( let json) :
                self.view.makeToast(json["message"].stringValue, duration: TimeInterval(2.0), position: self.view.center)
                break
            case .faiure(let err as NSError) :
                break
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        let index =  string.count == 0 ? newText.count : newText.count - 1
        if index >= 0 && index < self.arrLb.count {
            let label = self.arrLb[index]
            label.text = string
        }
        self.maxIndexHasString = -1
        for i in  0..<self.arrLb.count {
            let label = self.arrLb[i]
            if  label.text != nil && label.text!.count > 0 {
                self.resetStatusLabel(label, status: 2)
            }else{
                self.resetStatusLabel(label, status: 0)
            }
        }
        if self.maxIndexHasString + 1 < self.arrLb.count {
//            if  (newText.count <= textField.text!.count){
//                self.currentEditLb?.text = ""
//            }
            let index =  self.maxIndexHasString + 1
            let nextLabel =  self.arrLb[index]
            self.currentEditLb = nextLabel
            self.resetStatusLabel(nextLabel, status: 1)
        }else{
            self.currentEditLb = nil
        }
        let doneInput = newText.count > 6  ? false : true
        if (newText.count > 5) {
            self.doneBtn.isEnabled = true
        }else{
            
        }
        return doneInput
    }
}
